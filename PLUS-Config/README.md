#Plus-Toolkit

The Plus_Config files are used for starting a PLUS Server.

The .rom files are used to define the position and distance between Markers on a Tool. They are needed by some Plus_Config files.


More Information on the Plus-Toolkit can be found on the offical side https://plustoolkit.github.io

To build your own Plus_Config files see the Configuration part on the user manual http://perk-software.cs.queensu.ca/plus/doc/nightly/user/Configuration.html

To build your own .rom files you can use a the 6D architect from NDI

License
----

MIT