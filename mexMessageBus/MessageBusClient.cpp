/*! \file   
 *  \brief      Message bus client class.
 *  \author     Thomas Moeller - 2011: first implementation
 *  \details
 *
 *  Copyright (C) Siemens AG 2011-2014 All Rights Reserved
 */


#pragma warning ( disable : 4127 ) // FD_SET(): conditional expression is constant

#define _USE_MESSAGEBUS_CLIENT_STATIC

#ifdef _USE_MESSAGEBUS_CLIENT_STATIC
#include "MessageBusClient.h"
#else
#include <Communication/MessageBus/Client.h>
#endif

#include <ws2tcpip.h>
#include <process.h>
#include <stdio.h>
#include <wchar.h>

#define DEFAULT_HOST "localhost"


#ifdef _USE_MESSAGEBUS_CLIENT_STATIC
namespace Messages {
#else
namespace AX
{
	namespace Communication
	{
		namespace MessageBus
		{
#endif


Client::Client(const char *host, const char *port)
{
	mSocket = INVALID_SOCKET;
	mServerAddrInfo = NULL;
	mMessageQueueRead = 0;
	mMessageQueueWrite = 0;
	mLargeBufferSize = LARGE_BUFFER_INC;
	mLargeBuffer = (wchar_t*) malloc(mLargeBufferSize * sizeof(wchar_t));
	mLargeBufferLength = 0;
	mThreadAbortSignal = 0;
	
	mLargeBuffer[0] = 0;
	mReceivedEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	InitializeCriticalSection(&mMutex);
	
	int rc;

	// Initialize Winsock
	WSADATA wsaData;
	rc = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (rc != 0) 
	{
		return;
	}


	struct addrinfo hints;
	ZeroMemory(&hints, sizeof (hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the local address and port to be used by the server
	rc = getaddrinfo((host && (strlen(host) > 0)) ? host : DEFAULT_HOST,
			(port && (strlen(port) > 0)) ? port : AX_MSGBUS_DEFAULT_PORT, &hints, &mServerAddrInfo);
	if (rc != 0) 
	{
		WSACleanup();
		return;
	}

	// Attempt to connect to the first address returned by
	// the call to getaddrinfo
	struct addrinfo *ptr = mServerAddrInfo;

	// Create a SOCKET for connecting to server
	mSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
	if (mSocket != INVALID_SOCKET) 
	{
		// Connect to server.
		rc = connect(mSocket, ptr->ai_addr, (int) ptr->ai_addrlen);
		if (rc == SOCKET_ERROR) 
		{
			closesocket(mSocket);
			mSocket = INVALID_SOCKET;
		}
	}


	// Start receiver thread
	_beginthread(receiverThread, 0, this);
}


Client::~Client()
{
	mThreadAbortSignal++;
	while (mThreadAbortSignal < 2)
		::Sleep(100);

	if (mServerAddrInfo)
		freeaddrinfo(mServerAddrInfo);

	if (mSocket != INVALID_SOCKET)
	{
		closesocket(mSocket);
		WSACleanup();
	}

	if (mReceivedEvent)
		CloseHandle(mReceivedEvent);

	DeleteCriticalSection(&mMutex);

	if (mLargeBuffer)
		free(mLargeBuffer);
}


void Client::receiverThread(void *param)
{
	Client* thisPtr = (Client*) param;
	if (thisPtr)
		thisPtr->receiver();
}


void Client::receiver()
{
	int rc;
	int received = 0;

	while (!mThreadAbortSignal)
	{

		// If connection was lost then try to reconnect
		if (mSocket == INVALID_SOCKET)
		{
			// Create a SOCKET for connecting to server
			struct addrinfo *ptr = mServerAddrInfo;
			mSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
			if (mSocket == INVALID_SOCKET) 
			{
				// Retry
				::Sleep(1000);
				continue;
			}

			// Connect to server.
			rc = connect(mSocket, ptr->ai_addr, (int) ptr->ai_addrlen);
			if (rc == SOCKET_ERROR) 
			{
				closesocket(mSocket);
				mSocket = INVALID_SOCKET;

				// Retry connecting
				::Sleep(1000);
				continue;
			}
		}

		// Check for incomming data or abort signal
		FD_SET fdSet;
		timeval timeout;
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		for (int selectStatus = 0; (selectStatus == 0) && !mThreadAbortSignal; )
		{
			FD_ZERO(&fdSet);
			FD_SET(mSocket, &fdSet);
			selectStatus = select(0, &fdSet, NULL, NULL, &timeout);
		}

		// Exit thread after destructor has been called
		if (mThreadAbortSignal)
			break;


		// Read all incomming data
		received = recv(mSocket, mBuffer, BUFFER_SIZE, 0);
		wprintf(L"received %s\n",mBuffer);

		if (received <= 0)
		{
			closesocket(mSocket);
			mSocket = INVALID_SOCKET;
			continue;
		}


		// Copy data to message queue
		const wchar_t *s = (const wchar_t *) mBuffer;
		received /= sizeof(wchar_t);

		const wchar_t *si = s;
		for (int i = 0; i < received; i++)
		{
			if (s[i] == '\0')
			{
				if (mLargeBufferLength > 0)
					addToLargeBuffer(si, i);

				putMessage(mLargeBufferLength > 0 ? mLargeBuffer : si);

				mLargeBufferLength = 0;
				mLargeBuffer[0] = 0;

				si = s + i + 1;
			}
		}

		// If buffer not yet terminated then copy to large buffer
		if (s[received - 1])
			addToLargeBuffer(si, int(received - (si - s)));
	}

	closesocket(mSocket);
	mSocket = INVALID_SOCKET;

	mThreadAbortSignal++;
}


void Client::putMessage(const wchar_t* msg)
{
	::EnterCriticalSection(&mMutex);

	if (((mMessageQueueWrite + 1) % QUEUE_SIZE) != mMessageQueueRead)
	{
		mMessageQueue[mMessageQueueWrite] = _wcsdup(msg);
		mMessageQueueWrite = (mMessageQueueWrite + 1) % QUEUE_SIZE;
	}

	::LeaveCriticalSection(&mMutex);

	::SetEvent(mReceivedEvent);
	::Sleep(0);
}


void Client::addToLargeBuffer(const wchar_t *str, int len)
{
	if (len < 0)
		len = (int) wcslen(str);

	// Enlarge buffer?
	if ((mLargeBufferLength + len + 1) >= mLargeBufferSize)
	{
		mLargeBufferSize += LARGE_BUFFER_INC;
		mLargeBuffer = (wchar_t*) realloc(mLargeBuffer, mLargeBufferSize * sizeof(wchar_t));
		if (!mLargeBuffer)
		{
			mLargeBufferSize = 0;
			mLargeBufferLength = 0;
		}
	}

	// Concatinate buffers
//	wcsncpy_s(mLargeBuffer + mLargeBufferLength, mLargeBufferSize - mLargeBufferLength, str, len);
	mLargeBufferLength += len;
}


wchar_t* Client::getMessage(DWORD timeout)
{
	wchar_t *r = 0;

	switch (WaitForSingleObject(mReceivedEvent, timeout))
	{
	case WAIT_TIMEOUT:
		break;

	case WAIT_OBJECT_0:
		{
			EnterCriticalSection(&mMutex);

			if (mMessageQueueRead != mMessageQueueWrite)
			{
				r = mMessageQueue[mMessageQueueRead];
				mMessageQueueRead = (mMessageQueueRead + 1) % QUEUE_SIZE;
			}

			if (mMessageQueueRead == mMessageQueueWrite)
				ResetEvent(mReceivedEvent);

			LeaveCriticalSection(&mMutex);
		} break;

	default:
		; // error
	}

	return r;
}


int Client::sendMessage(const wchar_t* msg)
{
	int rc = 0;

	if (msg && (mSocket != INVALID_SOCKET))
	{
		rc = send(mSocket, (const char*) msg, int((wcslen(msg) + 1) * sizeof(wchar_t)), 0);
	}

	return rc;
}


size_t Client::encodeBinary(size_t bufferLength, const unsigned char *buffer, size_t strLen, wchar_t *str)
{
	wchar_t *strWriter = str;
	size_t i = 0;
	static const char encChars[] = 
	{
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
	};

	// output buffer too small
	if (strLen < (bufferLength + (bufferLength / 3) + 2))
		return 0;

	for (i = 0; i < bufferLength; ++i)
	{
		switch (i % 3)
		{
		case 0:	*strWriter++ = encChars[63 & (buffer[i] >> 2)]; break;
		case 1:	*strWriter++ = encChars[63 & ((buffer[i - 1] << 4) | (buffer[i] >> 4))]; break;
		case 2:	*strWriter++ = encChars[63 & ((buffer[i - 1] << 2) | (buffer[i] >> 6))];
				*strWriter++ = encChars[63 & buffer[i]]; break;
		}		
	}

	// deal with last bits left
	if (bufferLength % 3)
	{
		switch (i % 3)
		{
		case 1:	*strWriter++ = encChars[(buffer[i - 1] & 3) << 4]; break;
		case 2:	*strWriter++ = encChars[(buffer[i - 1] & 15) << 2]; break;
		}
	}

	// terminate string
	*strWriter++ = 0;

	return (strWriter - str);
}


size_t Client::decodeBinary(const wchar_t *str, size_t bufferSize, unsigned char *buffer)
{
	// returns numerical value of character
	#define CHAR_VAL(ch) (((ch >= 'A') && (ch <= 'Z')) ? ch - 'A' : ((ch >= 'a') && (ch <= 'z')) ? 26 + ch - 'a' : ((ch >= '0') && (ch <= '9')) ? 52 + ch - '0' : ch == '+' ? 62 : 63)

	unsigned char *bufWriter = buffer;
	size_t strLen = wcslen(str);

	for (size_t i = 0; (i < strLen) && (size_t(bufWriter - buffer) < bufferSize) && (str[i] != '='); ++i)
	{
		switch (i % 4)
		{
		case 1:	*bufWriter++ = 0xff & (CHAR_VAL(str[i - 1]) << 2 | (CHAR_VAL(str[i]) >> 4)); break;
		case 2:	*bufWriter++ = 0xff & (CHAR_VAL(str[i - 1]) << 4 | (CHAR_VAL(str[i]) >> 2)); break;
		case 3:	*bufWriter++ = 0xff & (CHAR_VAL(str[i - 1]) << 6 | (CHAR_VAL(str[i]) >> 0)); break;
		}
	}

	return (bufWriter - buffer);
}


// namespace
#ifndef _USE_MESSAGEBUS_CLIENT_STATIC
		}
	}
}


////////////////////////////////////////////////////////////////////////
// simple C interfaces for testing (singleton client, not thread safe)

static AX::Communication::MessageBus::Client* sSingletonClient = 0;
static wchar_t *sLastMessage = 0;


void MessageBus_Start(const char *host, const char *port)
{
	MessageBus_Stop();
	sSingletonClient = new AX::Communication::MessageBus::Client(host, port);
}


void MessageBus_Stop()
{
	if (sLastMessage)
	{
		free(sLastMessage);
		sLastMessage = 0;
	}

	if (sSingletonClient)
	{
		delete sSingletonClient;
		sSingletonClient = 0;
	}
}


void MessageBus_Put(const wchar_t *msg)
{
	if (!sSingletonClient)
		MessageBus_Start();

	sSingletonClient->putMessage(msg);
}


void MessageBus_Send(const wchar_t *msg)
{
	if (!sSingletonClient)
		MessageBus_Start();

	sSingletonClient->sendMessage(msg);
}


const wchar_t* MessageBus_Get(DWORD timeout)
{
	if (!sSingletonClient)
		MessageBus_Start();

	if (sLastMessage)
	{
		free(sLastMessage);
		sLastMessage = 0;
	}

	sLastMessage = sSingletonClient->getMessage(timeout);

	return sLastMessage;
}


#else
}
#endif
