/*! \file   
 *  \brief      Message bus client class.
 *  \author     Thomas Moeller - 2011: first implementation
 *  \details	Copyright (C) Siemens AG 2011-2014 All Rights Reserved
 */

#pragma once

#define _USE_MESSAGEBUS_CLIENT_STATIC


#ifdef _USE_MESSAGEBUS_CLIENT_STATIC
#ifndef AX_MSGBUS_DEFAULT_PORT
#define AX_MSGBUS_DEFAULT_PORT "12999"
#endif
#define EXP_IMP_AX_Communication_MessageBus 
#else
#include <Communication/MessageBus/MessageBus.h>
#endif

#include <winsock2.h>

typedef struct addrinfo* addrinfoPtr_t;


#ifdef _USE_MESSAGEBUS_CLIENT_STATIC
namespace Messages {
#else

namespace AX
{
	namespace Communication
	{
		namespace MessageBus
		{
#endif


/**
 * Message bus client.
 * 
 * Applications participating in the message bus communication
 * need to instantiate a message bus client.
 * - In this simplified implementation the clients subscribe to all messages.
 * - An instance of this class will connect to a message bus server and
 *   put all received data from other connected clients into a a message
 *   queue. The application can read messages from that queue using the
 *   getMessage() method.
 * - Any message sent via the sendMessage() method is forwarded to all
 *   other connected clients. This call is thread-safe.
 * - An application can put new messages into it's own queue with the
 *   putMessage() method. This call is also thread-safe.
 * - Should the connection to the server break down the client will
 *   automatically retry reconnecting until a connection is established.
 *
 * \sa AX::Communication::MessageBus::Server
 * 
 * \note This class is written for highest portability and has therefore
 *       no dependencies to AX::Base or other libraries.
 *  
 *
 * Example for main message loop:
 * \code
 * #include <Communication/MessageBus/Client.h>
 * 
 * int main(int argc, const char *argv[])
 * {
 *   AX::Communication::MessageBus::Client msgbus;
 *   wchar_t *msgStr = 0;
 * 
 *   while ((msgStr = msgbus.getMessage()) != 0)
 *   {
 *     // do somethig with the received message before deleting it
 * 
 *     free(msgStr);
 *   }
 * 
 *   return 1;
 * }
 * \endcode
 *
 * \headerfile Client.h <Communication/MessageBus/Client.h>
 */
class EXP_IMP_AX_Communication_MessageBus Client
{
public:
	//! Constructor, which will try establishing a connection to the server.
	Client(const char *host = NULL, const char *port = NULL);

	//! Destructor.
	virtual ~Client();

	//! Gets the oldest message from the queue, which must be freed afterwards. A timeout in milliseconds is optional.
	virtual wchar_t* getMessage(DWORD timeout = INFINITE);

	//! Broadcasts the given message to all clients connected to the message bus server.
	virtual int sendMessage(const wchar_t* msg);

	//! Puts a message into the clients message queue.
	virtual void putMessage(const wchar_t* msg);

	//! Encodes a binary memory buffer into ASCII using radix-64 representation. Returns number of characters written.
	static size_t encodeBinary(size_t bufferLength, const unsigned char *buffer, size_t strLen, wchar_t *str);

	//! Decodes binary data in radix-64 format from an ASCII string. Returns number of bytes written into buffer.
	static size_t decodeBinary(const wchar_t *str, size_t bufferSize, unsigned char *buffer);

protected:
	//! Main receiver function putting received data into the message queue.
	virtual void receiver();

	//! Utility function adding data to a large buffer if data exceeds normal buffer size.
	virtual void addToLargeBuffer(const wchar_t *str, int len = -1);

protected:
	//! Normal receiver buffer size.
	static const size_t BUFFER_SIZE = 16 * 1024;
	//! Number of enties in receiver message queue.
	static const size_t QUEUE_SIZE = 1024;
	//! Initial and incremental size of large receiver buffer.
	static const size_t LARGE_BUFFER_INC = 1024 * 1024;

	//! Address information needed to connect to server.
	addrinfoPtr_t mServerAddrInfo;
	//! Socket handle for connection to server.
	SOCKET mSocket;
	//! Event that is signaled when data is available in message queue.
	HANDLE mReceivedEvent;
	//! Normal receiver buffer.
	char mBuffer[BUFFER_SIZE];
	//! Message queue array.
	wchar_t *mMessageQueue[QUEUE_SIZE];
	//! Read pointer in queue.
	size_t mMessageQueueRead;
	//! Write pointer in queue.
	size_t mMessageQueueWrite;
	//! Mutex protecting access to message queue.
	CRITICAL_SECTION mMutex;
	//! pointer to large receiver buffer.
	wchar_t *mLargeBuffer;
	//! Current allocated size of large receiver buffer in characters.
	size_t mLargeBufferSize;
	//! Data length currently stored in large buffer.
	size_t mLargeBufferLength;
	//! Receiver thread abort flag.
	int mThreadAbortSignal;

private:
	//! Static thread entry function. Calls receiver() method.
	static void receiverThread(void *param);
};


// namespace
#ifndef _USE_MESSAGEBUS_CLIENT_STATIC
		}
	}
}


////////////////////////////////////////////////////////////////////////
// simple C interfaces for testing (singleton client, not thread safe)

extern "C" 
{
	/**
	 * Starts a message bus client singleton.
	 */
	EXP_IMP_AX_Communication_MessageBus void MessageBus_Start(const char *host = 0, const char *port = 0);

	/**
	 * Stops a message bus client singleton previously started with MessageBusStartClient().
	 */
	EXP_IMP_AX_Communication_MessageBus void MessageBus_Stop();

	/**
	 * Calls AX::Communication::MessageBus::Client::putMessage() on singleton instance.
	 */
	EXP_IMP_AX_Communication_MessageBus void MessageBus_Put(const wchar_t *msg);

	/**
	 * Calls AX::Communication::MessageBus::Client::sendMessage() on singleton instance.
	 */
	EXP_IMP_AX_Communication_MessageBus void MessageBus_Send(const wchar_t *msg);

	/**
	 * Calls AX::Communication::MessageBus::Client::getMessage() on singleton instance.
	 *
	 * The C interfaces can be used to connect test scripts to the message bus
	 * communication.
	 *
	 * Python example:
	 * \code
	 * import ctypes
	 * mbinterface = ctypes.cdll["AX.Services.AppFw3D.MessageBus.dll"]
	 * mbinterface.MessageBus_Put("cmd=Hello")
	 * print(ctypes.c_wchar_p(mbinterface.MessageBus_Get(1000)).value)
	 * \endcode
	 * 
	 * \return Returns next message from queue or NULL otherwise. The returned string
	 *         is valid until the next call to this function and does not need to be
	 *         freed by caller.
	 */
	EXP_IMP_AX_Communication_MessageBus const wchar_t* MessageBus_Get(DWORD timeout = INFINITE);
}

#else
}
#endif
