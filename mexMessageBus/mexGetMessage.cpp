/*=========================================================================

  Program:   Matlab Open IGT Link Interface -- igtlwaitcon
  Module:    $RCSfile: $
  Language:  C++
  Date:      $Date: $
  Version:   $Revision: $

  Copyright (c) Insight Software Consortium. All rights reserved.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

//
//  cfd = igtlwaitcon(SD, DATA);
//
//    SD      : (integer)   Server Socket descriptor (-1 if failed to connect)
//    TIMEOUT : (integer)   Time out in millisecond
//    cfd     : (integer)   Client file descriptor
//


#include "mex.h"
#include <math.h>
#include <string.h>
#include <wchar.h>
#include <stdio.h>

#include "MessageBusClient.h"

using namespace std;

const static int MSG_QUEUE_SIZE = 512;

Messages::Client m_MessageBusClient;
wchar_t *m_Messages[MSG_QUEUE_SIZE];
char decodedMessage[MSG_QUEUE_SIZE];
unsigned char *decMsg;
const char *str;

// -----------------------------------------------------------------
// Function definitions.
void mexFunction (int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
	wchar_t *msg = m_MessageBusClient.getMessage(0);
	wprintf(L"return %s\n",msg);
	
	int ret = wcstombs ( decodedMessage, msg, sizeof(decodedMessage) );
	printf("converted string %s",decodedMessage);
	
	//wprintf(L"length %d",wcslen(msg));

//	decMsg=new unsigned char[MSG_QUEUE_SIZE];
	//Messages::Client::decodeBinary(msg,wcslen(msg),decMsg);
	//Messages::Client::decodeBinary(msg,10,decodedMessage);
	plhs[0] = mxCreateString(decodedMessage);
}

