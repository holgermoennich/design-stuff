# mesMessageBus

This function is a matlab/octave mex interface to the AXCS MessageBus for Prototyping.

The MessageBus Functions are copied from the IAE SDK. The get and (set) function is the implemented Matlab/Octave Mex Interface File.

The .mex files are compiled/binaries for Octave 4.2.1

The function should be compiled in Octave using the following commands

    mkoctfile -mex MessageBusClient.cpp -c
    mkoctfile -mex mexGetMessage.cpp MessageBusClient.o

For Matkab use the function "mex" instead of "mkoctfile"


License
----

MIT
