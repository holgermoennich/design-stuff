# OpenIGT Matlab Interface

This function is a matlab/octave mex interface to the OpenIGT Link for Prototyping.

The source code is from the official website

http://openigtlink.org/tutorials/matlabigtl.html

http://openigtlink.org/developers/cpplib


With minor changes and bugfixes (Vector was not implenented in the mex interface...) important to get the Fiducials from Slicer.

Most of the files are from the OpenIGT Library. A compiled version of the Library for MinGW Compiler is libOpenIGTLink.a - This reduced the overhead to setup the MinGW Compiler.

If you want to compile the Library on your own... use the following MinGW Compiler - https://sourceforge.net/projects/mingw-w64/ - keep in mind to use the same version as the corresponding Octave version!


To compile ... use the following commands

    mkoctfile --mex igtlclose.cpp -ID:/build/openigt -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source/igtlutil -ID:/build/MatlabOpenIGT -LD:/build/openigt/bin -LD:/build/MatlabOpenIGT -lOpenIGTLink -lws2_32 -lwsock32  igtlMexSocket.o  igtlMexServerSocket.o igtlMexClientSocket.o

    mkoctfile --mex igtlsend.cpp -ID:/build/openigt -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source/igtlutil -ID:/build/MatlabOpenIGT -LD:/build/openigt/bin -LD:/build/MatlabOpenIGT -lOpenIGTLink -lws2_32 -lwsock32  igtlMexSocket.o  igtlMexServerSocket.o igtlMexClientSocket.o

    mkoctfile --mex igtlreceive.cpp -ID:/build/openigt -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source/igtlutil -ID:/build/MatlabOpenIGT -LD:/build/openigt/bin -LD:/build/MatlabOpenIGT -lOpenIGTLink -lws2_32 -lwsock32  igtlMexSocket.o  igtlMexServerSocket.o igtlMexClientSocket.o

    mkoctfile --mex igtlopen.cpp -ID:/build/openigt  -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source/igtlutil -ID:/build/MatlabOpenIGT -LD:/build/openigt/bin -LD:/build/MatlabOpenIGT -lOpenIGTLink -lws2_32 -lwsock32  igtlMexSocket.o  igtlMexServerSocket.o igtlMexClientSocket.o

    mkoctfile --mex igtlwaitcon.cpp -ID:/build/openigt -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source -ID:/tracker/openigtlink-OpenIGTLink-61cb0a8/openigtlink-OpenIGTLink-61cb0a8/Source/igtlutil -ID:/build/MatlabOpenIGT -LD:/build/openigt/bin -LD:/build/MatlabOpenIGT -lOpenIGTLink -lws2_32 -lwsock32  igtlMexSocket.o  igtlMexServerSocket.o igtlMexClientSocket.o


License
----

MIT
