%
% Example to use igtlreceive()
%

%%% read image data
sd = igtlopen('localhost', 18944);
%cfd = igtlwaitcon(sd, 0);

if sd == -1
  error('Could not connect to the server.');
end

DATA = igtlreceive(sd);
DATA

igtlclose(sd);
