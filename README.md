# Lemmium

This howto is hopefully helping in understanding the content and structure of this repository...

> This is a prototype and hacking repository
> By nature... it is a little bit chaotic
> This is not a product code repository!

### Architecture

Please check the [Overview] PowerPoint to understand how this Repro works.

### Used Open Source Software

Some fundamental or useful OpenSource Software packages for this project

* [OpenRave] - Open Robotics Automation Virtual Environment (OpenRAVE)
* [Slicer] - Package for image analysis
* [SlicerIGT] - Image-guided therapy
* [OpenIGTLink] - Real-time image streaming
* [IGSTK] - Image-Guided Surgery Toolkit.
* [ITK] - Insight Segmentation and Registration Toolkit
* [VTK] - The Visualization Toolkit
* [OpenCV] - Open Source Computer Vision Library
* [OpenCVContrib] - OpenCVs extra modules, including another mex interface
* [mexopencv] -  MATLAB MEX files for OpenCV
* [Octave] - OpenSource Matlab (you dont need a licence! ;-))
* [ITK2D3D] - Example for 2D3D Registration
* [VISP] - Visual Servoing Platform
* [plastimatch] - perfect 3D3D registration - cone beam reconstruction - drr renderer
* [python] - Python Scripting Language
* [MatlabRotToolbox] - Vector and Rotation Tools
* [RobotHandEye] - Calibration and registration techniques for robots



### Folders

Description of the content for the Top-Folders

| Folder | Content |
| ------ | ------ |
| 2D3D | Peters 2D3D Registration Code |
| CT_marker_tracking | Solution for CT Marker Tracking from Mazen & Bastian |
| Hand-eye-calibration | Petras Work for Hand Eye Calibration |
| IGSTK_Source_Code | IGSTK Source Code with Atracsys Integration |
| matlab | Matlab Source Files |
| MatlabOpenIGT | MatlabOpenIGT Source Code with Patch for Fiducials and Version for Octave |
| mexMessageBus | Mex Function to Interface with AXCS |
| models | CAD Models extracted from ArtisSim |
| OpenIGTLink32 | Compiled Version for VS2010 - static lib |
| openrave_octave | Compiled Interface for Octave to OpenRave |
| pheno calibration | Marker Pose Detecttion from Artis - e.g. PDS2 |
| Robot | Robot Projects for LBR & Agilus & Pheno |
| Va09_Robot_Pheno_Solution | VA09 Solution |

### Other relevant internal code
* [PhenoCalibration] - From PhD thesis of Sabine Thuerauf
* [INCA] - SW library from NT MCA
* [Monaco] - AT IN prototype framework and clinical prototypes

### Todos

 - Write some Howtos

License
----

MIT


**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [OpenRave]: <http://openrave.org/docs/0.8.2/examples/>
   [Slicer]: <https://www.slicer.org/>
   [SlicerIGT]: <http://www.slicerigt.org/wp/>
   [OpenIGTLink]: <http://openigtlink.org/>
   [IGSTK]: <http://www.igstk.org/>
   [ITK]: <https://itk.org/>
   [VTK]: <https://www.vtk.org/>
   [OpenCV]: <https://opencv.org/>
   [OpenCVContrib]: <https://github.com/opencv/opencv_contrib>
   [mexopencv]: <https://github.com/kyamagu/mexopencv>
   [Octave]: <https://www.gnu.org/software/octave/>
   [ITK2D3D]: <http://insight-journal.org/browse/publication/800>
   [VISP]: <https://visp.inria.fr/>
   [plastimatch]: <http://plastimatch.org/>
   [python]: <https://www.python.org/>
   [MatlabRotToolbox]: <https://github.com/tuckermcclure/vector-and-rotation-toolbox>
   [RobotHandEye]: <http://math.loyola.edu/~mili/Calibration/index.html>

   [Overview]:https://code.siemens.com/holger.moennich/Lemmium-Code/blob/master/Prototyp_Demo_Roadmap_04_04_2017.pptx
   [PhenoCalibration]: <file:\\ww005.siemens.net\meddfsroot\1_AX\03_Projects\35_PM\20_Lemmium\70_Engineering\30_Robot\SabineZeegoKalibrierung>
   [INCA]: <https://code.siemens.com/INCA>
   [MONACO]: <https://code.siemens.com/prototypes_at_in>
