/* joystick_encoder_readout - DT-Labs_Handle-Interface
  
  Six-channel encoder readout and joystick interface
  
  Description:
  - Runs on Arduino Due, connects as native USB-device
  - Interrupt driven readout of 6 encoders (using 12 interrupts)
  - Using Joystick library to be recognized as HID-Device under Windows   
  - Work as 6-axis game-controller under Windows  

  Dependencies:
  - Arduino Joystick Library (https://github.com/MHeironimus/ArduinoJoystickLibrary)
    Follow manual installation instruction on GitHub page!
  
  Revision History
  2022-02-28 MGAR:  - add three axis Rx, Ry, RZ, Add Zero Buttons + RGB LED
  2022-01-28 KLT:   - add two additional axis to support X, Y, Z
  2021-11-25 MGAR:  - tweak encoder readout logic. Test single axis control.
  2021-11-24 KLT:   - initial version
 */

#include <Joystick.h>

constexpr char DEBUG = 0; // switch debug output via serial on (1) or off (0)
constexpr char DEBUG_I = 1; // switch debug output via serial on (1) or off (0)

//************************************************************
// HANDLE 1 JOYSTICK AXIS DEFINITIONS
//************************************************************
constexpr int X_LIM_UP = 511;
constexpr int X_LIM_LO = -511;
constexpr int Y_LIM_UP = 511;
constexpr int Y_LIM_LO = -511;
constexpr int Z_LIM_UP = 511;
constexpr int Z_LIM_LO = -511;

//***********************************************************
// HANDLE 2 JOYSTICK AXIS DEFINITIONS
//***********************************************************
constexpr int RX_LIM_UP = 511;
constexpr int RX_LIM_LO = -511;
constexpr int RY_LIM_UP = 511;
constexpr int RY_LIM_LO = -511;
constexpr int RZ_LIM_UP = 511;
constexpr int RZ_LIM_LO = -511;

//**************************************************BEGIN
// PIN DEFINITIONS 
//Handle 1
constexpr byte PIN_XA = 2;
constexpr byte PIN_XB = 3;
constexpr byte PIN_XI = 4;
constexpr byte PIN_ZA = 5;
constexpr byte PIN_ZB = 6;
constexpr byte PIN_ZI = 7;
constexpr byte PIN_YA = 8;
constexpr byte PIN_YB = 9;
constexpr byte PIN_YI = 10;

//Handle 2
constexpr byte PIN_RZA = 16;
constexpr byte PIN_RZB = 15;
constexpr byte PIN_RZI = 14;
constexpr byte PIN_RXA = 19;
constexpr byte PIN_RXB = 18;
constexpr byte PIN_RXI = 17;
constexpr byte PIN_RYA = 22;
constexpr byte PIN_RYB = 21;
constexpr byte PIN_RYI = 20;

//Zero Buttons
constexpr byte PIN_ZERO1 = 52;
constexpr byte PIN_ZERO2 = 53;

//RGB LED
constexpr byte PIN_LED_RED = 24;
constexpr byte PIN_LED_GREEN = 26;
constexpr byte PIN_LED_BLUE = 28;

// PIN DEFINITIONS
//**************************************************END

//*****************************************************
// GLOBAL VARIABLES + INTERRUPT FLAGS
// ****************************************************
//use these to detect any movement as to not spam the serial monitor
// Handle 1
volatile int countX;
volatile int prevCountX;
volatile char directionX;  // bool: 1/0 , left/right
volatile int countY;
volatile int prevCountY;
volatile char directionY;  // bool: 1/0 , left/right
volatile int countZ;
volatile int prevCountZ;
volatile char directionZ;  // bool: 1/0 , left/right
volatile int X_index_offset = 0;
volatile int Y_index_offset = 0;
volatile int Z_index_offset = 0;

// Handle 2
volatile int countRx;
volatile int prevCountRx;
volatile char directionRx;  // bool: 1/0 , left/right
volatile int countRy;
volatile int prevCountRy;
volatile char directionRy;  // bool: 1/0 , left/right
volatile int countRz;
volatile int prevCountRz;
volatile char directionRz;  // bool: 1/0 , left/right
volatile int Rx_index_offset = 0;
volatile int Ry_index_offset = 0;
volatile int Rz_index_offset = 0;
// User Interface
volatile boolean g_zero1_is_pushed = false;
volatile boolean g_zero2_is_pushed = false;

//*****************************************************
// JOYSTICK CREATION
// ****************************************************
Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID,JOYSTICK_TYPE_GAMEPAD,
  0, 0,                   // Button Count, Hat Switch Count
  true, true, true,       // X, Y and Z Axis
  true, true, true,       // No Rx, Ry, or Rz
  false, false,           // No rudder or throttle
  false, false, false);   // No accelerator, brake, or steering

//*****************************************************
// SETUP
// ****************************************************
void setup() {
  Serial.begin(115200);
  //Interface Box User Interaction
  pinMode(PIN_ZERO1, INPUT_PULLUP);
  pinMode(PIN_ZERO2, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_LED_RED, OUTPUT);
  pinMode(PIN_LED_GREEN, OUTPUT);
  pinMode(PIN_LED_BLUE, OUTPUT);
  // Handle 1 Encoder Inputs
  pinMode(PIN_XA, INPUT_PULLUP);
  pinMode(PIN_XB, INPUT_PULLUP);
  pinMode(PIN_XI, INPUT_PULLUP);
  pinMode(PIN_YA, INPUT_PULLUP);
  pinMode(PIN_YB, INPUT_PULLUP);
  pinMode(PIN_YI, INPUT_PULLUP);
  pinMode(PIN_ZA, INPUT_PULLUP);
  pinMode(PIN_ZB, INPUT_PULLUP);
  pinMode(PIN_ZI, INPUT_PULLUP);
  // Handle 2 Encoder Inputs
  pinMode(PIN_RXA, INPUT_PULLUP);
  pinMode(PIN_RXB, INPUT_PULLUP);
  pinMode(PIN_RXI, INPUT_PULLUP);
  pinMode(PIN_RYA, INPUT_PULLUP);
  pinMode(PIN_RYB, INPUT_PULLUP);
  pinMode(PIN_RYI, INPUT_PULLUP);
  pinMode(PIN_RZA, INPUT_PULLUP);
  pinMode(PIN_RZB, INPUT_PULLUP);
  pinMode(PIN_RZI, INPUT_PULLUP);
  // User interface ISR setup
  
  attachInterrupt(digitalPinToInterrupt(PIN_ZERO1), isr_zero1_pushed, FALLING);
  attachInterrupt(digitalPinToInterrupt(PIN_ZERO2), isr_zero2_pushed, FALLING);
  // Handle 1 ISR setup
  attachInterrupt(digitalPinToInterrupt(PIN_XA), isrXA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_XB), isrXB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_XI), isrXI, FALLING);
  attachInterrupt(digitalPinToInterrupt(PIN_YA), isrYA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_YB), isrYB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_YI), isrYI, FALLING);
  attachInterrupt(digitalPinToInterrupt(PIN_ZA), isrZA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_ZB), isrZB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_ZI), isrZI, FALLING);
  // Handle 2 ISR setup
  attachInterrupt(digitalPinToInterrupt(PIN_RXA), isrRxA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_RXB), isrRxB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_RXI), isrRxI, FALLING);
  attachInterrupt(digitalPinToInterrupt(PIN_RYA), isrRyA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_RYB), isrRyB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_RYI), isrRyI, FALLING);
  attachInterrupt(digitalPinToInterrupt(PIN_RZA), isrRzA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_RZB), isrRzB, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_RZI), isrRzI, FALLING);
  // Handle 1 Initialization
  countX = 0;
  prevCountX = countX;
  directionX = 0;
  countY = 0;
  prevCountY = countY;
  directionY = 0;
  countZ = 0;
  prevCountZ = countZ;
  directionZ = 0;
  // Handle 2 Initialization
  countRx = 0;
  prevCountRx = countRx;
  directionRx = 0;
  countRy = 0;
  prevCountRy = countRy;
  directionRy = 0;
  countRz = 0;
  prevCountRz = countRz;
  directionRz = 0;
  // Joystick Axis Initialization
  Joystick.setXAxisRange(X_LIM_LO, X_LIM_UP);
  Joystick.setYAxisRange(Y_LIM_LO, Y_LIM_UP);
  Joystick.setZAxisRange(Z_LIM_LO, Z_LIM_UP);
  Joystick.setRxAxisRange(RX_LIM_LO, RX_LIM_UP);
  Joystick.setRyAxisRange(RY_LIM_LO, RY_LIM_UP);
  Joystick.setRzAxisRange(RZ_LIM_LO, RZ_LIM_UP);
  Joystick.setXAxis(0);
  Joystick.setYAxis(0);
  Joystick.setZAxis(0);
  Joystick.setRxAxis(0);
  Joystick.setRyAxis(0);
  Joystick.setRzAxis(0);
  Joystick.begin();
  set_all_led_off();
  set_led_green();
}

//*****************************************************
// LOOP
// ****************************************************
void loop() {
  //only do stuff if stuff is happening
  //X-Axis change
  if (countX != prevCountX){
    Joystick.setXAxis(countX);    
    prevCountX = countX;
    if(directionX){
      if(DEBUG){Serial.print("left  ");}
    }
    else{
      if(DEBUG){Serial.print("right  ");}
    }
    // implementing range limits in the interface, Currently not needed
    /*if(countX > X_LIM_UP){
      countX = X_LIM_UP;
    }
    else if(countX < X_LIM_LO){
      countX = X_LIM_LO;
    }*/
    if(DEBUG){Serial.println(countX);}
    //flash built-in LED when activity is detected. Delay function should work fine with Interrupts
    //digitalWrite(LED_BUILTIN, HIGH);
    //delay(50);
    //digitalWrite(LED_BUILTIN, LOW);    
  }
  //Y-Axis change
  if (countY != prevCountY){
    Joystick.setYAxis(countY);    
    prevCountY = countY;
  }
  //Z Axis change
  if (countZ != prevCountZ){
    Joystick.setZAxis(countZ);    
    prevCountZ = countZ;
  }
  //Rx Axis change
  if (countRx != prevCountRx){
    Joystick.setRxAxis(countRx);    
    prevCountRx = countRx;
  }
  //Ry Axis change
  if (countRy != prevCountRy){
    Joystick.setRyAxis(countRy);    
    prevCountRy = countRy;
  }
  //Rz Axis change
  if (countRz != prevCountRz){
    Joystick.setRzAxis(countRz);    
    prevCountRz = countRz;
  }
  //**********************************************
  // ZERO BUTTON HANDLING
  if(g_zero1_is_pushed){
    // de-bounce
    delay(20);
    if(is_zero_button_pushed(PIN_ZERO1)){
        set_zero_position_1();}
    g_zero1_is_pushed = false;
  }

  if(g_zero2_is_pushed){
    // de-bounce
    delay(20);
    if(is_zero_button_pushed(PIN_ZERO2)){
        set_zero_position_2();}
    g_zero2_is_pushed = false;
  }
}

//#################################################BEGIN
// ISR HANDLE 1
//*******************************************************
// X-AXIS INTERRUPT HANDLING 
//*******************************************************
void isrXA(){
  if (digitalRead(PIN_XA) != digitalRead(PIN_XB)){
    if (DEBUG){directionX = 0;} // directionX right
    countX++;}
  else {
    if (DEBUG){directionX = 1;} // directionX left
    countX--;}
}

void isrXB(){
  if (digitalRead(PIN_XA) == digitalRead(PIN_XB)){
    if (DEBUG){directionX = 0;} // directionX right
    countX++;}
  else {
    if (DEBUG){directionX = 1;} // directionX left
    countX--;}
}

void isrXI(){
  if (DEBUG_I){Serial.println("Index");}
  countX = X_index_offset;
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);
}

//*******************************************************
// Y-AXIS INTERRUPT HANDLING 
//*******************************************************
void isrYA(){
  if (digitalRead(PIN_YA) != digitalRead(PIN_YB)){
    if (DEBUG){directionY = 0;} // directionY right
    countY++;}
  else {
    if (DEBUG){directionY = 1;} // directionY left
    countY--;}
}

void isrYB(){
  if (digitalRead(PIN_YA) == digitalRead(PIN_YB)){
    if (DEBUG){directionY = 0;} // directionY right
    countY++;}
  else {
    if (DEBUG){directionY = 1;} // directionY left
    countY--;}
}

void isrYI(){
  if (DEBUG_I){Serial.println("Index");}
  countY = Y_index_offset;
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);
}

//*******************************************************
// Z-AXIS INTERRUPT HANDLING 
//*******************************************************
void isrZA(){
  if (digitalRead(PIN_ZA) != digitalRead(PIN_ZB)){
    if (DEBUG){directionZ = 0;} // directionZ right
    countZ++;}
  else {
    if (DEBUG){directionZ = 1;} // directionZ left
    countZ--;}
}

void isrZB(){
  if (digitalRead(PIN_ZA) == digitalRead(PIN_ZB)){
    if (DEBUG){directionZ = 0;} // directionZ right
    countZ++;}
  else {
    if (DEBUG){directionZ = 1;} // directionZ left
    countZ--;}
}

void isrZI(){
  if (DEBUG_I){Serial.println("Index");}
  countZ = Z_index_offset;
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);
}
// ISR HANDLE 1
//#################################################END


//#################################################BEGIN
// ISR HANDLE 2
//*******************************************************
// Rx-AXIS INTERRUPT HANDLING 
//*******************************************************
void isrRxA(){
  if (digitalRead(PIN_RXA) != digitalRead(PIN_RXB)){
    if (DEBUG){directionRx = 0;} // directionRx right
    countRx++;}
  else {
    if (DEBUG){directionRx = 1;} // directionRx left
    countRx--;}
}

void isrRxB(){
  if (digitalRead(PIN_RXA) == digitalRead(PIN_RXB)){
    if (DEBUG){directionRx = 0;} // directionRx right
    countRx++;}
  else {
    if (DEBUG){directionRx = 1;} // directionRx left
    countRx--;}
}

void isrRxI(){
  if (DEBUG_I){Serial.println("Index");}
  countRx = Rx_index_offset;
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);
}

//*******************************************************
// Ry-AXIS INTERRUPT HANDLING 
//*******************************************************
void isrRyA(){
  if (digitalRead(PIN_RYA) != digitalRead(PIN_RYB)){
    if (DEBUG){directionRy = 0;} // directionRy right
    countRy++;}
  else {
    if (DEBUG){directionRy = 1;} // directionRy left
    countRy--;}
}

void isrRyB(){
  if (digitalRead(PIN_RYA) == digitalRead(PIN_RYB)){
    if (DEBUG){directionRy = 0;} // directionRy right
    countRy++;}
  else {
    if (DEBUG){directionRy = 1;} // directionRy left
    countRy--;}
}

void isrRyI(){
  if (DEBUG_I){Serial.println("Index");}
  countRy = Ry_index_offset;
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);
}

//*******************************************************
// Rz-AXIS INTERRUPT HANDLING 
//*******************************************************
void isrRzA(){
  if (digitalRead(PIN_RZA) != digitalRead(PIN_RZB)){
    if (DEBUG){directionRz = 0;} // directionRz right
    countRz++;}
  else {
    if (DEBUG){directionRz = 1;} // directionRz left
    countRz--;}
}

void isrRzB(){
  if (digitalRead(PIN_RZA) == digitalRead(PIN_RZB)){
    if (DEBUG){directionRz = 0;} // directionRz right
    countRz++;}
  else {
    if (DEBUG){directionRz = 1;} // directionRz left
    countRz--;}
}

void isrRzI(){
  if (DEBUG_I){Serial.println("Index");}
  countRz = Rz_index_offset;
  digitalWrite(LED_BUILTIN, HIGH);
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);
}
// ISR HANDLE 2
//####################################################END

// set flag if zeroing 1 push button is pressed
void isr_zero1_pushed(){
  g_zero1_is_pushed = true;
}
// set flag if zeroing 1 push button is pressed
void isr_zero2_pushed(){
  g_zero2_is_pushed = true;
}

/*******************************************************************
 * CHECK IF ZERO BUTTON IS PRESSED
 * return 'true' if button is pressed, 'false' otherwise
 ******************************************************************/
int is_zero_button_pushed(int zero_button_pin){
  char button_state;
  // pushed button is LOW
  button_state = digitalRead(zero_button_pin);
  button_state = !button_state; 
  return button_state;
}

/*******************************************************************
 * SET CURRENT HANDLE POSITION AS NEW ZERO POSITION
 ******************************************************************/
void set_zero_position_1(void){
  // indicate zeroing on LEDs
  set_all_led_off();
  // set new offset towards index signals
  X_index_offset = countX;
  Y_index_offset = countY;
  Z_index_offset = countZ;
  countX = 0;
  countY = 0;
  countZ = 0; 
  // flashing LED for 1 second gives time to release button
  flash_led_blue();
  set_led_green();
}

void set_zero_position_2(void){
  // indicate zeroing on LEDs
  set_all_led_off();
  // set new offset towards index signals
  Rx_index_offset = countRx;
  Ry_index_offset = countRy;
  Rz_index_offset = countRz;
  countRx = 0;
  countRy = 0;
  countRz = 0; 
  // flashing LED for 1 second gives time to release button
  flash_led_blue();
  set_led_green();
}

/*******************************************************************
 * Flash LED twice
 ******************************************************************/
void flash_led_blue(void){
  digitalWrite(PIN_LED_BLUE, HIGH);
  delay(300);
  digitalWrite(PIN_LED_BLUE, LOW);
  delay(100);
  digitalWrite(PIN_LED_BLUE, HIGH);
  delay(1000);
  digitalWrite(PIN_LED_BLUE, LOW);
}
/*******************************************************************
 * Switch off all LEDs to get defined initial condition
 ******************************************************************/
 void set_all_led_off(void){
  digitalWrite(PIN_LED_RED, LOW);
  digitalWrite(PIN_LED_GREEN, LOW);
  digitalWrite(PIN_LED_BLUE, LOW);
}
/*******************************************************************
 * Set defined LED colours
 ******************************************************************/
void set_led_blue(){
  digitalWrite(PIN_LED_BLUE, HIGH);
}

void set_led_green(){
  digitalWrite(PIN_LED_GREEN, HIGH);
}

void set_led_red(){
  digitalWrite(PIN_LED_RED, HIGH);
}
void set_led_yellow(){
  digitalWrite(PIN_LED_RED, HIGH);
  digitalWrite(PIN_LED_GREEN, HIGH);
}
