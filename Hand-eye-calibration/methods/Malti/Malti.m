function [q_lsq, q_lsq_new] = Malti(a_theta_meas, a_d, a_l_vec, a_m_vec, b_theta_meas, b_d, b_l_vec, b_m_vec)
% 
% The function Malti calculates the transformation between  
% a world coordinate system to a camera coordinate system with screw 
% parameters, dual quaternions and the hand eye calibration after Abdel Malti and Joao Baretto.  
%
% INPUT parameters
% a_:           screw motion of hand (here TCP)
% a_theta: rotation angle around screw axis (size 1x amount of motions)
% a_d:          shift along screw axis (size 1x amount of motions)
% a_l_vec:      screw axis (size 3x amount of motions)
% a_m_vec:      orthogonal momentum vector of screw (size 3x amount of motions)
% b_:           screw motion of eye (here marker of tracking system)
% b_theta: rotation angle around screw axis
% b_d:          shift along screw axis% 
% b_l_vec:      screw axis
% b_m_vec:      orthogonal momentum vector of screw 
%
% OUTPUT parameters
% q:            dual quaternion which describes transformation from world 
%               to camera coordinate system (size 8x1)
%
%% history
% 2012-10-22 0.1    ChN first version

%% Code

    % initialize variables
    a_vec = zeros(3,size(b_d,2));
    a_prime_vec = zeros(3,size(b_d,2));
    b_vec = zeros(3,size(b_d,2));
    b_prime_vec = zeros(3,size(b_d,2));
    
for motion = 1:size(b_d,2)

    % a and b from Artificial input are checked, they represent the motions
    % of the TCP or the Marker in their own coordinate system
    % screw axis of TCP coordinate system    
    a_theta = a_theta_meas(1,motion);
    
    a_0(1,motion) = cos(a_theta/2);    
    a_vec(1,motion) = sin(a_theta/2)*a_l_vec(1,motion);
    a_vec(2,motion) = sin(a_theta/2)*a_l_vec(2,motion);
    a_vec(3,motion) = sin(a_theta/2)*a_l_vec(3,motion);

    a_prime_0(1,motion) = -(a_d(1,motion)/2)*sin(a_theta/2);
    a_prime_vec(1,motion) = sin(a_theta/2)*a_m_vec(1,motion) + (a_d(1,motion)/2)*cos(a_theta/2)*a_l_vec(1,motion);
    a_prime_vec(2,motion) = sin(a_theta/2)*a_m_vec(2,motion) + (a_d(1,motion)/2)*cos(a_theta/2)*a_l_vec(2,motion);
    a_prime_vec(3,motion) = sin(a_theta/2)*a_m_vec(3,motion) + (a_d(1,motion)/2)*cos(a_theta/2)*a_l_vec(3,motion);
    
   % screw axis of camera coordinate system
    b_theta = b_theta_meas(1,motion);
   
    b_0(1,motion) = cos(b_theta/2);
    b_vec(1,motion) = sin(b_theta/2)*b_l_vec(1,motion);
    b_vec(2,motion) = sin(b_theta/2)*b_l_vec(2,motion);
    b_vec(3,motion) = sin(b_theta/2)*b_l_vec(3,motion);

    b_prime_0(1,motion) = -(b_d(1,motion)/2)*sin(b_theta/2);
    b_prime_vec(1,motion) = sin(b_theta/2)*b_m_vec(1,motion) + (b_d(1,motion)/2)*cos(b_theta/2)*b_l_vec(1,motion);
    b_prime_vec(2,motion) = sin(b_theta/2)*b_m_vec(2,motion) + (b_d(1,motion)/2)*cos(b_theta/2)*b_l_vec(2,motion);
    b_prime_vec(3,motion) = sin(b_theta/2)*b_m_vec(3,motion) + (b_d(1,motion)/2)*cos(b_theta/2)*b_l_vec(3,motion);
    
    
    % create the L of size 4x4 (Malti eq 23)
    L(4*(motion-1)+1,1) = a_0(:,motion) - b_0(:,motion);
    L(4*(motion-1)+[2;3;4],1) = a_vec(:,motion) - b_vec(:,motion);
    L(4*(motion-1)+1,[2:4]) = -(a_vec(:,motion) - b_vec(:,motion))';
        
    
     % Skew (2:4,2:4)
    % T(2,2)
    L((4*(motion-1)+2),2) = a_0(:,motion) - b_0(:,motion);
    % T(2,3)
    L((4*(motion-1)+2),3) = -(a_vec(3,motion)+b_vec(3,motion));
    % T(2,4)
    L((4*(motion-1)+2),4) = (a_vec(2,motion)+b_vec(2,motion));
        
    % T(3,2)
    L((4*(motion-1)+3),2) = (a_vec(3,motion)+b_vec(3,motion));
    % T(3,3)
    L((4*(motion-1)+3),3) = a_0(:,motion) - b_0(:,motion);
    % T(3,4)
    L((4*(motion-1)+3),4) = -(a_vec(1,motion)+b_vec(1,motion));
    
    % T(4,2)
    L((4*(motion-1)+4),2) = -(a_vec(2,motion)+b_vec(2,motion));
    % T(4,3)
    L((4*(motion-1)+4),3) = (a_vec(1,motion)+b_vec(1,motion));
    % T(4,4)
    L((4*(motion-1)+4),4) = a_0(:,motion) - b_0(:,motion);
    
    
    % create the L_prime of size 4x4 (Malti eq 20)
    % eq 21: K(a,b)q = 0
    L_prime(4*(motion-1)+1,1) = a_prime_0(:,motion) - b_prime_0(:,motion);
    L_prime(4*(motion-1)+[2;3;4],1) = a_prime_vec(:,motion) - b_prime_vec(:,motion);
    L_prime(4*(motion-1)+1,[2:4]) = -(a_prime_vec(:,motion) - b_prime_vec(:,motion))';
        
    
     % Skew (2:4,2:4)
    % T(2,2)
    L_prime((4*(motion-1)+2),2) = a_prime_0(:,motion) - b_prime_0(:,motion);
    % T(2,3)
    L_prime((4*(motion-1)+2),3) = -(a_prime_vec(3,motion)+b_prime_vec(3,motion));
    % T(2,4)
    L_prime((4*(motion-1)+2),4) = (a_prime_vec(2,motion)+b_prime_vec(2,motion));
        
    % T(3,2)
    L_prime((4*(motion-1)+3),2) = (a_prime_vec(3,motion)+b_prime_vec(3,motion));
    % T(3,3)
    L_prime((4*(motion-1)+3),3) = a_prime_0(:,motion) - b_prime_0(:,motion);
    % T(3,4)
    L_prime((4*(motion-1)+3),4) = -(a_prime_vec(1,motion)+b_prime_vec(1,motion));
    
    % T(4,2)
    L_prime((4*(motion-1)+4),2) = -(a_prime_vec(2,motion)+b_prime_vec(2,motion));
    % T(4,3)
    L_prime((4*(motion-1)+4),3) = (a_prime_vec(1,motion)+b_prime_vec(1,motion));
    % T(4,4)
    L_prime((4*(motion-1)+4),4) = a_prime_0(:,motion) - b_prime_0(:,motion);
      
end

% Compute singualr value decomposition
% Compute singular value decomposition
if size(L,1) > 1000
    [U,S,V]=svd(L,0);
    %% 2015-01-27: Test Low-rank matrix approximation to reduce noise influence
    LV = 0;
    for i=1:size(S,1)
       LV = LV + S(i,i)^2; 
    end
    
    for k = 1:size(S,1)
        LVk(:,k) = 0;
        for i = 1:k
            LVk(:,k) = LVk(:,k) + S(i,i)^2;
        end
        LVk_power(:,k) = LVk(:,k) / LV;
    end
    
    S_approx = [S(:,1:3),zeros(size(S,1),1)];
    L_approx = U*S_approx*V';
    [U_new,S_new,V_new] = svd(L_approx,0);
    diff = V_new - V;
    %%
else
    [U,S,V]=svd(L);
    %% 2015-01-27: Test Low-rank matrix approximation to reduce noise influence
    LV = 0;
    for i=1:size(S,2)
       LV = LV + S(i,i)^2; 
    end
    
    for k = 1:size(S,2)
        LVk(:,k) = 0;
        for i = 1:k
            LVk(:,k) = LVk(:,k) + S(i,i)^2;
        end
        LVk_power(:,k) = LVk(:,k) / LV;
    end
    
    S_approx = [S(:,1:3),zeros(size(S,1),1)];
    L_approx = U*S_approx*V';
    [U_new,S_new,V_new] = svd(L_approx,0);
    diff = V_new - V;
    %%
end
% [U,S,V]=svd(L);

q_Malti = V(1:4,4); % represents the rotation quaternion
q_conjugate_Malti = [q_Malti(1,:);-1*q_Malti(2:4,:)];

b_LS = -L_prime*q_Malti;
A = L;
[xhat, p, ~] = lsq(A, b_LS);
q_lsq = [q_Malti; xhat];

%% 2015-01-27: Test Low-rank matrix approximation to reduce noise influence
% check singular values of L_prime
U = 0;
S = 0;
V = 0;
[U,S,V] = svd(L_prime,0);
S_approx = 0;
S_approx = [S(:,1:(size(S,2)-1)),zeros(size(S,1),1)];
L_prime_approx = U*S_approx*V';
[U_prime,S_prime,V_prime] = svd(L_prime_approx,0);
diff_prime_L = L_prime_approx - L_prime;
diff_prime = V_prime - V;

q_Malti_new = V_new(1:4,4); % represents the rotation quaternion
q_conjugate_Malti = [q_Malti_new(1,:);-1*q_Malti_new(2:4,:)];

b_LS_new = -L_prime_approx*q_Malti_new;
A_new = L_approx;
[xhat_new, p, ~] = lsq(A_new, b_LS_new);
q_lsq_new = [q_Malti_new; xhat_new];
%%

% xhat_lscov = lscov(A,b_LS);
% q_lscov =  [q_Malti; xhat_lscov];



% min ||A*x - b||_2 -> min||L*q_prime - b_LS||_2
% B*x = d -> dot(q,q_prime) = 0

% Tried to calculate LS solution with after Golub MAtrix chap 12.1.4 computations 
% this code crases if noise is present
% d = 0;
% [Q,R] = qr(q_Malti);
% p = rank(R);
% n = rank(L);
% [AQ] = L*Q;
% A1 = AQ(:,1:p);
% A2 = AQ(:,p+1:end);
% y = R(1:p,1:p)'\d;
% z = lsqnonneg(AQ(:,p+1:n),b_LS-AQ(:,1:p)*y);
% q_prime_Malti = Q(:,1:p)*y + Q(:,p+1:n)*z;
% [q_dual_Malti] = [q_Malti; q_prime_Malti];
% t_dual = 2*(dual_quaternion_multiplication([q_prime_Malti;0;0;0;0],[q_conjugate_Malti;0;0;0;0]));
% t = t_dual(2:4,:);
% [P_WCS P_CCSinWCS_Golub] =TransformAPointWithDualQuaternion(P_WCS,[q_dual_Malti])
end