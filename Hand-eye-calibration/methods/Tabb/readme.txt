How to build Tabb with cmake:
- Build OpenCV
- Build glog
- Build Eigen
- Build gflags
- Build Ceres
- Build dirent
- add newmat libraries
- Build Tabb

required folder structure:
- images -> for one camera there must be a subfolder with "camera0"
- internal_images -> for one camera there must be a subfolder with "camera0"
- calibration_object.txt

-> can be copied unchanged

- robot_cali.txt (including the number of poses and the transformation between robot base and flange) 
- num_meas.txt (including the number of poses)
- As.txt (including the number of transformations between camera and marker)

-> octave files "tabb.m"/"RunMethodComparison.m"/"RunMethodComparison_Agilus.m" convert measurements to requried format 
	-> adaption of paths required

input data
- path to the input directory
- path to the write directory
- Flag: 1 for hand-eye calibration only

exemplary data can be found under:
https://data.nal.usda.gov/dataset/data-solving-robot-world-hand-eyes-calibration-problem-iterative-methods_3501

Further remarks:
- The residuals are computed in: CostFunctions.hpp