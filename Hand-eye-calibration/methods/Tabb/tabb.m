function [X,Z] = tabb(A,B)
  %% input: m x n transformation matrices A and B
  %% output: 4x4 hand-eye transformation matrices
  %% the function writes A and B to the required format for the C++ code anc computes X and Z
  
  [r,c] = size(A);
  n_HEC = c/4;

  %% avoids interruptions due to messages on the display when the code is running
  
  oldpager = PAGER('/dev/null');
  oldpso = page_screen_output(1);
  oldpoi = page_output_immediately(1); 
  
  %% delete the old result directory and create a new one 
  
  %rmdir('D:\Agilus\AGILUS\results\Tabb\results', "s");  
  confirm_recursive_rmdir(rmdir('D:\Agilus\AGILUS\results\Tabb\results')) = false;
  mkdir('D:\Agilus\AGILUS\results\Tabb\results');

  %% calculating results of iterative methods (Tabb)
  %% Converts the TrafoMatrices for the iterative method in C++ 
  f = 1;
  for b = 1:n_HEC
    for c = 1:4
      for d = b*4-3:b*4
      conv_Robot_to_Flange(f) = B(c,d);
      f++;
      end
    end
  end

  %% save Bs as robot_cali.txt (required as input for C++)
  dlmwrite('D:\Agilus\AGILUS\results\Tabb\Input_Cpp\robot_cali.txt', n_HEC);
  dlmwrite('D:\Agilus\AGILUS\results\Tabb\Input_Cpp\robot_cali.txt', conv_Robot_to_Flange, " ", "-append");

  dlmwrite('D:\Agilus\AGILUS\results\Tabb\Input_Cpp\num_meas.txt', n_HEC);
  dlmwrite('D:\Agilus\AGILUS\results\Tabb\Input_Cpp\As.txt', A);

  %%run cmd with C++ Code
  dos('D:\XRay\Mono\Tabb_AhmadYousef_RWHEC_Jan2017_Project.exe D:\Agilus\AGILUS\results\Tabb\Input_Cpp D:\Agilus\AGILUS\results\Tabb\results 1');

  PAGER(oldpager);
  page_screen_output(oldpso);
  page_output_immediately(oldpoi);

  %% saving results in the same convention as the other methods
  Transformations = dlmread('D:\Agilus\AGILUS\results\Tabb\results\rwhe_Q_c1_separable\transformations.txt');

  X = Transformations(2:5, 1:4);
  Z = Transformations(7:10, 1:4);