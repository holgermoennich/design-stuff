function [delta_ang, delta_trans] = delta_matrix(ground_truth, error_matrix)
  %% calculates the difference angle and translation between ground truth and error prone matrix
  
  %% rotation
  delta_m = inv(ground_truth)*error_matrix; %ground_truth*inv(error_matrix);
  R_delta = delta_m(1:3,1:3);
  
  arg = (trace(R_delta)-1)/2;

  if abs(arg) > 1.001
    error("Not a rotation matrix");
  end
  
  delta_ang = abs(acosd(arg));  
    
  if delta_ang < 100*eps
    delta_ang = 0;
  end

%  [w, delta_ang] = to_axis(R_delta);
  
  %% translation
%  delta_trans = norm(ground_truth(1:3,4)-error_matrix(1:3,4))/norm(ground_truth(1:3,4));
%  delta_trans = norm(ground_truth(1:3,4)-error_matrix(1:3,4));
  delta_trans = norm(delta_m(1:3,4));
end