function [ err_c ] = e_c(As, Bs, X, Z)
  %% mean combined rotation and translation error without units
  
  sum_dN = 0;

  [r,c] = size(As); 
  num_meas = c/4;
  
  for i = 1:num_meas
    Ai = As(:,4*i-3:4*i);
    Bi = Bs(:,4*i-3:4*i);
    dH = Ai*X - Z*Bi; % 4x4 difference matrix
    dN = norm(dH, "fro"); % frobenius norm of the difference matrix
    sum_dN += dN^2;
  end
  err_c = sum_dN/num_meas;
end