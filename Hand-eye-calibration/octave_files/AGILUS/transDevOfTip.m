function [ tip_dev ] = transDevOfTip(Marker_home, As)
  %% calculates the RMS error of the marker deviation after a pivot movement
  %% input: first marker position = "ground truth", marker positions of the evaluation (pivot movement)
  %% output rms error in x,y,z direction
  
  [r,c] = size(As); 
  num_meas = c/4;
  
  sum_trans = zeros(3,1);
  
  for i = 1:num_meas
    t = As(1:3,i*4);
    sqdiff = (Marker_home(1:3,4) - t).^2;
    sum_trans += sqdiff;
  end

  tip_dev = sqrt(sum_trans/num_meas);
  
end