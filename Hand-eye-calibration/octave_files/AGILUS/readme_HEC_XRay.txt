Octave files for hand-eye calibration with X-Ray

SetupMethodComparison_XRay.m
------------------------------
Requires setup with optical tracking and IBR marker, make 2-D acquisitions after each movement

monoTrack.m
------------
Requires paths, model geometry, Python script for dicom -> png, C++ script for Monotracking 
Converts dicom into png, computes 6D marker pose from 2D file, saves transformation matrices into one single matrix needed for HEC

HEC_XRay.m
-----------
Performs hand-eye calibration of optical/robot X-Ray/robot and optical/X-Ray loop, makes cross validation and evaluates data