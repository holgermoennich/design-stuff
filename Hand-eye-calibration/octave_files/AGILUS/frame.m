%
% Creates a KUKA Java frame object
% Input can be either:
% 6 variables as: X, Y, Z, alpha (radians), beta (radians), gamma (radians)
% or as a 6x1 matrix with (X,Y,Z,alpha(rad),beta(rad),gamma(rad))
%
function myFrame = frame(varargin)
 
 myFrame = 0;
 
 switch (length(varargin))
  case 1
    if(length(varargin{1})==6)
      myFrame = varargin{1};
    else
    error("Requires 6 parameters or an array of parameter");
    end
  case 6
    myFrame = [varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6}];
  otherwise
    error("Requires either 6 parameters or an array of parameter");
endswitch

myFrame = javaObject("com.kuka.roboticsAPI.geometricModel.Frame", ....
myFrame(1),myFrame(2),myFrame(3),myFrame(4),myFrame(5),myFrame(6));

end