function [M_noise] = add_noise_Euler(M, sigma_trans, sigma_rot)
  %% adds Gaussian noise on the translation and Euler angles of the transformation matrices of M
  %% input: sigma as single number or 1x3 vector
  
  [r,c] = size(M);
  n = c/4;
  
  for i = 1:n
    
    H = M(:,i*4-3:i*4);
    trans = H(1:3,4);
    rot = H(1:3,1:3);
    
    %% adding noise on rotation
    angles_noise = sigma_rot.*randn(1,3);
    R_noise = EtoR(angles_noise, 'Rob');
    
    %% adding noise on translation
    trans_noise = trans + sigma_trans'.*randn(3,1);

    M_noise(1:3,i*4) = trans_noise;
    M_noise(1:3, i*4-3:i*4-1) = R_noise*rot;
    M_noise(4,i*4-3:i*4) = [0,0,0,1];
  end
end