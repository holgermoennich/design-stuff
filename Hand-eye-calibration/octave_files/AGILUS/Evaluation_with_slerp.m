%% Takes the X (transformation between marker and robot flange) 
%% and rotates around the calculated tip/marker origin
%% -> marker origin should not move in an ideal case

clc;
clear all;

addpath D:\Agilus\AGILUS;

port = 59152; % Defined by XML inside on the KUKA smartpad
addr = '172.31.1.140'; % Defined by XML inside the KUKA smartpad

%% Confirming Robot Connection
robot = connectAgilus(port,addr);

%% X and Z from the hand-eye method 
X = dlmread('D:\Agilus\AGILUS\results\Tabb\X.txt');
Z = dlmread('D:\Agilus\AGILUS\results\Tabb\Z.txt');

%% First motion is PTP motion to Home Position
homeJoint = jointPosition(122.1, -67.9, 100.5, -1.5, 1, -68.6);
%homeJoint = jointPosition(20.28,-70.65,87.88,-156.27,-28.32,82.27);
robot.move(homeJoint);

%% Homogeneous coordinates from home position
Flange_home = robot.getCurrentCartesianPosition();
home = [Flange_home.getX() Flange_home.getY() Flange_home.getZ() Flange_home.getAlphaRad() Flange_home.getBetaRad() Flange_home.getGammaRad()];
HFlange_home = CtoH(home);

dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Flange_home.txt', HFlange_home);

%% Home position of the marker
Base_to_Marker_home = HFlange_home*inv(X);

HMarker_home = getCameraData2(); 

dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Marker_home.txt', HMarker_home);

%% Number of measurements
num_meas = 50; 

markerPosition = [];
flangePosition = [];

i = 1;

robot.setSpeed(10); 

%%% slerp initialize
center_frame = Base_to_Marker_home;
Rot_angle = eye(3);

alpha = 20*pi/180;

R1 = [cos(alpha) 0 sin(alpha); 0 1 0; -sin(alpha) 0 cos(alpha)];
%R2 = [1   0   0; 0 cos(alpha) -sin(alpha); 0   sin(alpha) cos(alpha)];
R2 = [cos(alpha) -sin(alpha) 0; sin(alpha) cos(alpha) 0; 0 0 1];
R3 = [cos(alpha) 0 -sin(alpha); 0 1 0; sin(alpha) 0 cos(alpha)];
%R4 = [1   0   0; 0 cos(alpha) sin(alpha); 0   -sin(alpha) cos(alpha)];
R4 = [cos(alpha) sin(alpha) 0; -sin(alpha) cos(alpha) 0; 0 0 1];


q1 = rotm2q(R1);
q2 = rotm2q(R2);
q3 = rotm2q(R3);
q4 = rotm2q(R4);

phi = 2*pi/num_meas;

d = num_meas/4;

k = 1;

for t = 0:1/d:1-1/d    

  q_R = slerp(q1, q2, t);

  [a, beta] = q2rot(q_R);
  R = rotv(a', beta);
  NewPos_of_Tip(1:3,1:3) = Base_to_Marker_home(1:3,1:3)*R';
  
  NewPos_of_Tip(1:3,4) = Base_to_Marker_home(1:3,4);
  NewPos_of_Tip(4,:) = [0 0 0 1];
 
  NewPos_of_Flange = NewPos_of_Tip*X;
  B_t_P = NewPos_of_Flange(1:3,4);
  R = NewPos_of_Flange(1:3,1:3);
  
    % Rotation
  rot_flange = RtoE(NewPos_of_Flange(1:3,1:3), 'Rob'); %alpha, beta, gamma of flange
  
  % Translation
  t = NewPos_of_Flange(1:3,4);

  robot.move(frame(t(1), t(2), t(3), rot_flange(1), rot_flange(2), rot_flange(3)));
  
  % Pause to allow the Robot to settle down
  pause(5);
  
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
 
 % Camera data
  markerPosition = getCameraData2();
  
  if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    
    k += 1;
   end
  
  i += 1;
  phi += 2*pi/num_meas;
end

for t = 0:1/d:1-1/d    
%  c = rotm2q(center_frame(1:3,1:3));
  q_R = slerp(q2, q3, t);
  
%  npot = times(q_R,c);
%  a = q2rot(npot);
  [a, beta] = q2rot(q_R);
  R = rotv(a', beta);
  NewPos_of_Tip(1:3,1:3) = Base_to_Marker_home(1:3,1:3)*R';
  
  NewPos_of_Tip(1:3,4) = Base_to_Marker_home(1:3,4);
  NewPos_of_Tip(4,:) = [0 0 0 1];
 
  NewPos_of_Flange = NewPos_of_Tip*X;
  B_t_P = NewPos_of_Flange(1:3,4);
  R = NewPos_of_Flange(1:3,1:3);
  
      % Rotation
  rot_flange = RtoE(NewPos_of_Flange(1:3,1:3), 'Rob'); %alpha, beta, gamma of flange
  
  % Translation
  t = NewPos_of_Flange(1:3,4);

  robot.move(frame(t(1), t(2), t(3), rot_flange(1), rot_flange(2), rot_flange(3)));
  
  % Pause to allow the Robot to settle down
  pause(5);
  
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
 
 % Camera data
  markerPosition = getCameraData2();
  
  if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    
    k += 1;
   end
  
  i += 1;
  phi += 2*pi/num_meas;
end

for t = 0:1/d:1-1/d    
%  c = rotm2q(center_frame(1:3,1:3));
  q_R = slerp(q3, q4, t);
  
%  npot = times(q_R,c);
%  a = q2rot(npot);
  [a, beta] = q2rot(q_R);
  R = rotv(a', beta);
  NewPos_of_Tip(1:3,1:3) = Base_to_Marker_home(1:3,1:3)*R';
  
  NewPos_of_Tip(1:3,4) = Base_to_Marker_home(1:3,4);
  NewPos_of_Tip(4,:) = [0 0 0 1];
 
  NewPos_of_Flange = NewPos_of_Tip*X;
%  NewPos_of_Flange(1:3,1:3)*(NewPos_of_Flange(1:3,1:3))'
  B_t_P = NewPos_of_Flange(1:3,4);
  R = NewPos_of_Flange(1:3,1:3);
  
      % Rotation
  rot_flange = RtoE(NewPos_of_Flange(1:3,1:3), 'Rob'); %alpha, beta, gamma of flange
  
  % Translation
  t = NewPos_of_Flange(1:3,4);

  robot.move(frame(t(1), t(2), t(3), rot_flange(1), rot_flange(2), rot_flange(3)));
  
  % Pause to allow the Robot to settle down
  pause(5);
  
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
 
 % Camera data
  markerPosition = getCameraData2();
  
  if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    
    k += 1;
   end
  
  i += 1;
  phi += 2*pi/num_meas;
end
 
for t = 0:1/d:1-1/d    
%  c = rotm2q(center_frame(1:3,1:3));
  q_R = slerp(q4, q1, t);
  
%  npot = times(q_R,c);
%  a = q2rot(npot);
  [a, beta] = q2rot(q_R);
  R = rotv(a', beta);
  NewPos_of_Tip(1:3,1:3) = Base_to_Marker_home(1:3,1:3)*R';
  
  NewPos_of_Tip(1:3,4) = Base_to_Marker_home(1:3,4);
  NewPos_of_Tip(4,:) = [0 0 0 1];
 
  NewPos_of_Flange = NewPos_of_Tip*X;
  NewPos_of_Flange(1:3,1:3)*(NewPos_of_Flange(1:3,1:3))';
  B_t_P = NewPos_of_Flange(1:3,4);
  R = NewPos_of_Flange(1:3,1:3);
  
      % Rotation
  rot_flange = RtoE(NewPos_of_Flange(1:3,1:3), 'Rob'); %alpha, beta, gamma of flange
  
  % Translation
  t = NewPos_of_Flange(1:3,4);

  robot.move(frame(t(1), t(2), t(3), rot_flange(1), rot_flange(2), rot_flange(3)));
  
  % Pause to allow the Robot to settle down
  pause(5);
  
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
 
 % Camera data
  markerPosition = getCameraData2();
  
  if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    
    k += 1;
   end
  
  i += 1;
  phi += 2*pi/num_meas;
end


  
[ l, m, n ] = size( mpHomoData );
numCamPoints = n;

[ p, q, r ] = size( fpCartData );
numRobotPoints = r;


if numCamPoints ~= numRobotPoints
    
    error('Number of position data from the Camera and Robot is NOT equal')

else
  
  for i=1:numCamPoints
    
    fpHomoData(:,:,i) = CtoH(fpCartData(:,:,i));
    Base_to_Marker(:,:,i) = fpHomoData(:,:,i)*inv(X);
  end

end  


Marker_Positions = reshape(mpHomoData, [4, 4*numCamPoints]);
Flange_Positions = reshape(fpHomoData, [4, 4*numCamPoints]);
Base_to_Marker_Positions = reshape(Base_to_Marker, [4,4*numCamPoints]);

dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Marker_Positions.txt', Marker_Positions);
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Flange_Positions.txt', Flange_Positions); 
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Base_to_Marker.txt', Base_to_Marker_Positions);

robot.closeConnection();