clc;
clear all;

%% Training data
Marker_Positions = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Camera_to_Marker.txt');

[r,c] = size(Marker_Positions);
num_tr_data = c/4; 

%% plot
for i = 1:num_tr_data  
  %% Trafo matrices
  Traf_Marker = Marker_Positions(1:4, 4*i-3:4*i);
  %% Rotation matrices
  Rot_Marker = Traf_Marker(1:3, 1:3);
     
  figure(1);
  title("Training data - Coordinate frames of Marker w.r.t. Camera");
  hold on;
  scale = 10;
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,1))', "r");
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,2))', "g");
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,3))', "b");
  xlabel ("x"); 
  ylabel ("y");
  zlabel ("z");
  view(3);
end