function R = EtoR( E, str )
%ETOR Summary of this function goes here
%   Detailed explanation goes here

a=E(1)*pi/180;
b=E(2)*pi/180;
c=E(3)*pi/180;

if strcmp(str, 'x')         %   x-Konvention              (Z,X',Z'')
    
    Ra=[cos(a)  -sin(a) 0       % a=psi
        sin(a)  cos(a)  0       % b=theta
        0   0   1];             % c=phi
    
    Rb=[1   0   0
        0   cos(b)  -sin(b)     
        0   sin(b)  cos(b)];
    
    Rc=[cos(c)  -sin(c) 0
        sin(c)  cos(c)  0
        0   0   1];
    
    R=Ra*Rb*Rc;
end

if strcmp(str, 'y')         %   y-Konvention              (Z,Y',Z'') 
    
    Ra=[cos(a)  -sin(a) 0       % a=psi
        sin(a)  cos(a)  0       % b=theta
        0   0   1];             % c=phi
    
    Rb=[cos(b)   0    sin(b)
        0        1     0
        -sin(b)  0    cos(b)];
    
    Rc=[cos(c)  -sin(c) 0
        sin(c)  cos(c)  0
        0   0   1];
    
    R=Ra*Rb*Rc;
end

if strcmp(str, 'DIN')       %    Luftfahrtnorm (DIN 9300) (Yaw-Pitch-Roll, Z,Y',X'')
    
    Ra=[1   0   0               % a=phi
        0   cos(a) -sin(a)      % b=theta
        0   sin(a) cos(a)];    % c=psi
    
    Rb=[cos(b)   0     sin(b)
        0        1     0
        -sin(b)  0    cos(b)];
    
    Rc=[cos(c)  -sin(c) 0
        sin(c)  cos(c)  0
        0   0   1];
    
    R=Ra*Rb*Rc;
end


if strcmp(str, 'Rob')
    
    Ra=[cos(a)  -sin(a) 0
        sin(a)  cos(a)  0
        0   0   1];
    
    Rb=[cos(b)   0     sin(b)
        0        1     0
        -sin(b)  0    cos(b)];

    Rc=[1   0   0          
        0   cos(c) -sin(c)  
        0   sin(c) cos(c)]; 
        
    R=Ra*Rb*Rc;
    
end