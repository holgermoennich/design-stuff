clc
clear all


homeJoint = jointPosition(25.7,-58, 76.7, 78.8, 23.3, 13);
%homeJoint = jointPosition(5,-80,70,173,-10,-100);
%homeJoint = jointPosition(105,-90,111,23,-1.6,-85.35); %where the camera can see the marker, positions from each axes
%% OCATVE_SLICER OpenIGTLink PATH

addpath D:\Agilus\AGILUS;

port = 59152; % Defined by XML inside on the KUKA smartpad
addr = '172.31.1.140'; % Defined by XML inside the KUKA smartpad

%% Confirming Robot Connection
robot = connectAgilus(port,addr);

%% First motion is PTP motion to Home Position
robot.move(homeJoint);


robot.moveRel(frame(20,    0,   0,    0,   0,   0)); %% moves in cartesian coordinates!!!! not axes
pause(5);
robot.moveRel(frame( 0,    20,  0,    0,   0,   0));
pause(5);
robot.moveRel(frame(0,    0,    20,    0,    0,    0));
pause(5);
robot.moveRel(frame(0,    0,    0,    0,    0,    10));
pause(5);

robot.closeConnection;