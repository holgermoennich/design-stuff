clc;
clear all;

B = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\large_volume\Robot_to_Flange.txt');
A = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\large_volume\Camera_to_Marker.txt');

A_piv = dlmread('D:\Agilus\AGILUS\results\Evaluation\large_volume\slerp_20deg\Marker_Positions.txt');
B_piv = dlmread('D:\Agilus\AGILUS\results\Evaluation\large_volume\slerp_20deg\Flange_Positions.txt');

X = dlmread('D:\Agilus\AGILUS\results\Shah\large_volume\X.txt');
Z = dlmread('D:\Agilus\AGILUS\results\Shah\large_volume\Z.txt');

disp('Standard deviation tip: '); disp(StdDevTip(A_piv));
disp('Rotational error for pivot movement'); disp(RMSE_rot(A_piv, B_piv, X, Z));

A_ev = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\large_volume\Camera_to_Marker_for_eval.txt');
B_ev = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\large_volume\Robot_to_Flange_for_eval.txt');

disp('Translational error for test data');disp(RMSE_trans(A_ev, B_ev, X, Z));
disp('Rotational error for test data');disp(RMSE_rot(A_ev, B_ev, X, Z));
disp('Combined error for test data');disp(e_c(A_ev, B_ev, X, Z));

[r,c] = size(B);
num_pos = c/4;
scale = 10;

for j = 1:num_pos  
  t_B = B(1:3,4*j);
  R_B = B(1:3,4*j-3:4*j-1);
  t_A = A(1:3,4*j);
  R_A = A(1:3,4*j-3:4*j-1);
  
  figure(1);
  title("Flange Positions - Large Volume");
  hold on;
  drawVector3d((t_B)', scale*(R_B(:,1))', "r");
  drawVector3d((t_B)', scale*(R_B(:,2))', "g");
  drawVector3d((t_B)', scale*(R_B(:,3))', "b");
  xlabel ("x in mm"); 
  ylabel ("y in mm");
  zlabel ("z in mm");
  view(3);
  
  figure(2);
  title("Marker Positions - Large Volume");
  hold on;
  drawVector3d((t_A)', scale*(R_A(:,1))', "r");
  drawVector3d((t_A)', scale*(R_A(:,2))', "g");
  drawVector3d((t_A)', scale*(R_A(:,3))', "b");
  xlabel ("x in mm"); 
  ylabel ("y in mm");
  zlabel ("z in mm");
  view(3);
end

B_sm = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\small_volume\Robot_to_Flange.txt');
A_sm = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\small_volume\Camera_to_Marker.txt');

A_piv_sm = dlmread('D:\Agilus\AGILUS\results\Evaluation\small_volume\slerp_20deg\Marker_Positions.txt');
B_piv_sm = dlmread('D:\Agilus\AGILUS\results\Evaluation\small_volume\slerp_20deg\Flange_Positions.txt');

X_sm = dlmread('D:\Agilus\AGILUS\results\Shah\small_volume\X.txt');
Z_sm = dlmread('D:\Agilus\AGILUS\results\Shah\small_volume\Z.txt');

disp('Standard deviation tip: '); disp(StdDevTip(A_piv_sm));
disp('Rotational error for pivot movement'); disp(RMSE_rot(A_piv_sm, B_piv_sm, X_sm, Z_sm));

A_ev_sm = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\small_volume\Camera_to_Marker_for_eval.txt');
B_ev_sm = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\small_volume\Robot_to_Flange_for_eval.txt');

disp('Translational error for test data');disp(RMSE_trans(A_ev_sm, B_ev_sm, X_sm, Z_sm));
disp('Rotational error for test data');disp(RMSE_rot(A_ev_sm, B_ev_sm, X_sm, Z_sm));
disp('Combined error for test data');disp(e_c(A_ev_sm, B_ev_sm, X_sm, Z_sm));


[r,c] = size(B_sm);
num_pos = c/4;
scale = 10;

for j = 1:num_pos  
  t_B = B_sm(1:3,4*j);
  R_B = B_sm(1:3,4*j-3:4*j-1);
  t_A = A_sm(1:3,4*j);
  R_A = A_sm(1:3,4*j-3:4*j-1);
  
  figure(3);
  title("Flange Positions - Small Volume");
  hold on;
  drawVector3d((t_B)', scale*(R_B(:,1))', "r");
  drawVector3d((t_B)', scale*(R_B(:,2))', "g");
  drawVector3d((t_B)', scale*(R_B(:,3))', "b");
  xlabel ("x in mm"); 
  ylabel ("y in mm");
  zlabel ("z in mm");
  view(3);
  
  figure(4);
  title("Marker Positions - Small Volume");
  hold on;
  drawVector3d((t_A)', scale*(R_A(:,1))', "r");
  drawVector3d((t_A)', scale*(R_A(:,2))', "g");
  drawVector3d((t_A)', scale*(R_A(:,3))', "b");
  xlabel ("x in mm"); 
  ylabel ("y in mm");
  zlabel ("z in mm");
  view(3);
end



