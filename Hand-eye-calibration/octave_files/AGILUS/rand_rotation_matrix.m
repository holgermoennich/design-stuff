function [M] = rand_rotation_matrix(deflection, randnums)
    %%
%    Creates a random rotation matrix.
%
%    deflection: the magnitude of the rotation. For 0, no rotation; for 1, completely random
%    rotation. Small deflection => small perturbation.
%    randnums: 3 random numbers in the range [0, 1]. If `None`, they will be auto-generated.
    %%%
    % from http://www.realtimerendering.com/resources/GraphicsGems/gemsiii/rand_rotation.c

    if randnums == 0
        randnums = unifrnd(0,1,1,3);
    end
    theta = randnums(1);
    phi = randnums(2);
    z = randnums(3);

    theta = theta * 2.0 * deflection * pi;  % Rotation about the pole (Z).
    phi = phi * 2.0 * pi;  % For direction of pole deflection.
    z = z * 2.0 * deflection;  % For magnitude of pole deflection.

    # Compute a vector V used for distributing points over the sphere
    # via the reflection I - V Transpose(V).  This formulation of V
    # will guarantee that if x[1] and x[2] are uniformly distributed,
    # the reflected points will be uniform on the sphere.  Note that V
    # has length sqrt(2) to eliminate the 2 in the Householder matrix.

    r = sqrt(z);
    V = [sin(phi) * r, cos(phi) * r, sqrt(2.0 - z)];

    st = sin(theta);
    ct = cos(theta);

    R = [ct, st, 0; -st, ct, 0; 0, 0, 1];

    # Construct the rotation matrix  ( V Transpose(V) - I ) R.

%    M = np.dot(np.diag([-1,-1,1]), (np.outer(V, V) - eye(3)).dot(R))
    M = diag([-1,-1,1])*((V'*V - eye(3))*R);
end