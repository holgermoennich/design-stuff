%% test of behavior of delta_matrix
clc;
clear all;

%R_GT = rand_rotation_matrix(0.001,0);
GT = [eye(3),[0.1,0.1,0.1]'; [0 0 0 1]];%eye(4); %[R_GT, [0.1 0.2 0]';[0 0 0 1]]; %eye(4); %[eye(3),[0.1,0.1,0.1]'; [0 0 0 1]];
R_EM = ...
  [0.9998812   0.0022781  -0.0152451;
  -0.0021364   0.9999545   0.0092992;
   0.0152656  -0.0092656   0.9998405]; %rand_rotation_matrix(0.001,0);
   
EM = [R_EM, [0.1 0.1 0]';[0 0 0 1]]; %[EtoR([1,0,1], 'Rob'),[0.1,0.1,0.001]'; [0 0 0 1]];

T = [eye(3),[500 700 20]'; [0 0 0 1]]; %[rand_rotation_matrix(0.001,0),[0 0 0]'; [0 0 0 1]];%[eye(3),[500 700 20]'; [0 0 0 1]];

GT1 = GT*T;
EM1 = EM*T;

delta_m = inv(GT1)*EM1; %GT1*inv(EM1);%GT1*inv(EM1);%inv(GT1)*EM1; 
R_delta = delta_m(1:3,1:3);
  
arg = (trace(R_delta)-1)/2;

if abs(arg) > 1.001
    error("Not a rotation matrix");
end
  
delta_ang = abs(acosd(arg));
    
if delta_ang < 100*eps
   delta_ang = 0;
end

  %% translation
%  delta_trans = norm(GT1(1:3,4)-EM1(1:3,4))/norm(GT1(1:3,4));
%  delta_trans = norm(GT1(1:3,4)-EM1(1:3,4));
  delta_trans = norm(delta_m(1:3,4));

disp('translational error');
disp(delta_trans);

disp('rotational error');
disp(delta_ang);