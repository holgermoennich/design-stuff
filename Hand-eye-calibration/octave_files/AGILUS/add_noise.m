function [M_noise] = add_noise(M, sigma_trans, sigma_rot)
  %% adds Gaussian noise on the translation and Euler angles of the transformation matrices of M
  %% input: sigma as single number or 1x3 vector
  
  [r,c] = size(M);
  n = c/4;
  
  for i = 1:n
    
    H = M(:,i*4-3:i*4);
    trans = H(1:3,4);
    rot = H(1:3,1:3);
    
    %% adding noise on rotation
    %% alternative 1: using unit quaternions
    %% sigma_rot has to be between 0 and 0.025 in order to get errors below 4�
%    q_R = rotm2q(rot);
%    q = sigma_rot*unifrnd(0,1,1,4);
%    q4 = quaternion(q(1), q(2), q(3), q(4));
%    q_sum = q_R+q4;
%    q_hat = q_sum/norm([q_sum.w, q_sum.x, q_sum.y, q_sum.z]);
%    [v, theta] = q2rot(q_hat);
%    inR = rotv(v', theta);
%    R_noise = inv(inR);

    %% alternative 2: using Householder matrix
    %% sigma_rot has to be between 0 and 0.001 in order to get errors below 3.4�
    R_n = rand_rotation_matrix(sigma_rot, 0);
    R_noise = R_n*rot;
    
    %% adding noise on translation
    trans_noise = trans + sigma_trans'.*randn(3,1);

    M_noise(1:3,i*4) = trans_noise;
    M_noise(1:3, i*4-3:i*4-1) = R_noise; %EtoR(angles_noise, 'Rob');  % rotation with noise
    M_noise(4,i*4-3:i*4) = [0,0,0,1];
  end
end