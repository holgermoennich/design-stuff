function [ err_R ] = RMSE_rot(As, Bs, X, Z)
  %% calculates mean rotational error in degrees
  
  [r,c] = size(As); 
  num_meas = c/4;
  
  sum_rot2 = 0;
  
  for i = 1:num_meas
    A_i = As(:,4*i-3:4*i); % measured Camera to Marker Transformation
    B_i = Bs(:,4*i-3:4*i);
    E_i = inv(Z*B_i)*A_i*X;
    R_i = E_i(1:3,1:3);
    
    arg = (trace(R_i)-1)/2;
    
    if abs(arg) > 1.001
      error("Not a rotation matrix");
    end

    ang = abs(acosd(arg));
    
    if ang < 100*eps
      ang = 0;
    end
    sum_rot2 += ang^2;
  end
    
%% alternative (only rotation matrices)
%  R_Z = Z(1:3,1:3);
%  R_X = X(1:3,1:3);
%  sum_rot2 = 0;
%
%  [r,c] = size(As); 
%  num_meas = c/4;
%  
%  for i = 1:num_meas
%    T_Bi = Bs(1:4, 4*i-3:4*i);
%    R_Bi = T_Bi(1:3,1:3);
%    
%    T_Ai = As(1:4, 4*i-3:4*i);
%    R_Ai = T_Ai(1:3,1:3);
%    
%    R_i = (R_Z*R_Bi)'*(R_Ai*R_X); 
%    
%    ang = acos((trace(R_i)-1)/2)*180/pi;
%    
%    if ang < 100*eps
%      ang = 0;
%    end
%    
%    sum_rot2 += (ang)^2;
%  end
  err_R = sqrt(sum_rot2/num_meas);
end