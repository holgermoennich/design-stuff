%% saves robot-world and hand-eye calibration for Tabb
%% make sure to run the C++ code previously!!!

clc;
clear all;

Transformations = dlmread('D:\Agilus\AGILUS\results\Tabb\results\rwhe_Q_c2_simul\transformations.txt');
[m, n] = size(Transformations);

X = Transformations(2:5, 1:4);
Z = Transformations(7:10, 1:4);

dlmwrite('D:\Agilus\AGILUS\results\Tabb\X.txt',X);
dlmwrite('D:\Agilus\AGILUS\results\Tabb\Z.txt',Z);