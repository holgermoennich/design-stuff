%% Takes the X (transformation between marker and robot flange) 
%% and rotates around the calculated tip/marker origin
%% -> marker origin should not move in an ideal case

clc;
clear all;

addpath D:\Agilus\AGILUS;

port = 59152; % Defined by XML inside on the KUKA smartpad
addr = '172.31.1.140'; % Defined by XML inside the KUKA smartpad

%% Confirming Robot Connection
robot = connectAgilus(port,addr);

%% X and Z from the hand-eye method 
X = dlmread('D:\Agilus\AGILUS\results\Daniilidis\X.txt');
Z = dlmread('D:\Agilus\AGILUS\results\Daniilidis\Z.txt');

%% First motion is PTP motion to Home Position
homeJoint = jointPosition(122.1, -75.4, 100, -15, 3.5, -14.3);
%homeJoint = jointPosition(20.28,-70.65,87.88,-156.27,-28.32,82.27);
robot.move(homeJoint);

%% Homogeneous coordinates from home position
Flange_home = robot.getCurrentCartesianPosition();
home = [Flange_home.getX() Flange_home.getY() Flange_home.getZ() Flange_home.getAlphaRad() Flange_home.getBetaRad() Flange_home.getGammaRad()];
HFlange_home = CtoH(home);

dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Flange_home.txt', HFlange_home);

%% Home position of the marker
Base_to_Marker_home = HFlange_home*inv(X);

HMarker_home = getCameraData2(); 

dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Marker_home.txt', HMarker_home);

%% Number of measurements
num_meas = 50; 

markerPosition = [];
flangePosition = [];

i = 1;
k = 1;
r = 10; % Distance in degrees from the home orientation
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\r.txt',r);

rand1 = randi([-r, r], num_meas, 3);

robot.setSpeed(10); 

%% Move robot around the marker and track the marker
for phi = (2*pi/num_meas):(2*pi/num_meas):2*pi
  
  %% random positions on a sphere
  
  Rot_angle = EtoR([rand1(i,1), 0, rand1(i,3)], 'Rob');
  Matrix_angle = [Rot_angle, [0,0,0]';0,0,0,1]; % change of rotation as transformation matrix
  NewPos_of_Tip = Base_to_Marker_home*Matrix_angle;
  Base_to_Marker(:,:,i) = NewPos_of_Tip; % expected orientation of the marker in robot frame
  NewPos_of_Flange = NewPos_of_Tip*X;
  
  
  %% Euler angle movement w.r.t. robot base
%  Rot_angle = EtoR([r*cos(phi), r*sin(phi), 0], 'Rob'); % change of rotation as rotation matrix
%  Matrix_angle = [Rot_angle, [0,0,0]';0,0,0,1]; % change of rotation as transformation matrix
%  NewPos_of_Tip = Base_to_Marker_home*Matrix_angle;
%  Base_to_Marker(:,:,i) = NewPos_of_Tip; % expected orientation of the marker in robot frame
%  NewPos_of_Flange = NewPos_of_Tip*X;

  % Rotation
  rot_flange = RtoE(NewPos_of_Flange(1:3,1:3), 'Rob'); %alpha, beta, gamma of flange
  
  % Translation
  t = NewPos_of_Flange(1:3,4);

  robot.move(frame(t(1), t(2), t(3), rot_flange(1), rot_flange(2), rot_flange(3)));
  
  % Pause to allow the Robot to settle down
  pause(5);
  
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
 
 % Camera data
  markerPosition = getCameraData2();
  
  if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    
    k += 1;
   end
  
  i += 1;
end
  
[ l, m, n ] = size( mpHomoData );
numCamPoints = n;

[ p, q, r ] = size( fpCartData );
numRobotPoints = r;


if numCamPoints ~= numRobotPoints
    
    error('Number of position data from the Camera and Robot is NOT equal')

else
  
  for i=1:numCamPoints
    
    fpHomoData(:,:,i) = CtoH(fpCartData(:,:,i));
    Base_to_Marker(:,:,i) = fpHomoData(:,:,i)*inv(X);
  end

end  

%disp('Marker Positions: '); disp(mpHomoData);
%disp('Flange Positions: '); disp(fpHomoData);

Marker_Positions = reshape(mpHomoData, [4, 4*numCamPoints]);
Flange_Positions = reshape(fpHomoData, [4, 4*numCamPoints]);
Base_to_Marker_Positions = reshape(Base_to_Marker, [4, 4*numCamPoints]);

dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Marker_Positions.txt', Marker_Positions);
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Flange_Positions.txt', Flange_Positions); 
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Base_to_Marker.txt', Base_to_Marker_Positions);

robot.closeConnection();