clc;
clear all;
s = 0.0000001:0.00001:0.001;
%sigma_rot =  linspace(0.00001,0.025,2000);
%rot = eye(3);
%s = linspace(0.0000001, 0.001, 100);

%err_opt_r = [1, 1, 1]; 
%a = 0.001:0.01:2;

for i = 1:length(s) %a
  %% Euler angles
%   sigma_rot = a(i)*err_opt_r;
   
  for m = 1:1000
%    angles_noise = sigma_rot.*randn(1,3);
%    R_n = EtoR(angles_noise, 'Rob');
    R_n = rand_rotation_matrix(s(i), 0);
   [ax, ang(m)] = to_axis(R_n);
  end
  rot_ang(i) = mean(ang);
  
  
  
  %% unit quaternions
%q = ny(i)*randn(1,4);
%q_n = q/norm(q);
%quat = quaternion(q_n(1), q_n(2), q_n(3), q_n(4));
%[v, theta] = q2rot(quat);
%th(i) = theta*180/pi;
%inR = rotv(v', theta);
%R = inv(inR);
%

%    q_R = rotm2q(rot);
%    q = sigma_rot(i)*unifrnd(0,1,1,4);
%    q4 = quaternion(q(1), q(2), q(3), q(4));
%    q_sum = q_R+q4;
%    q_hat = q_sum/norm([q_sum.w, q_sum.x, q_sum.y, q_sum.z]);
%    [v, theta] = q2rot(q_hat);
%    th(i) = theta*180/pi;
%    inR = rotv(v', theta);
%    R_noise = inv(inR);


%  for j = 1: 100
%    R_ax = rand_rotation_matrix(s(i),0);
%    [w,a(i)] = to_axis(R_ax);
%  end
%  ang(i) = mean(a(j));
end

figure(1);
%plot(a,rot_ang);
plot(s,rot_ang);
%xlabel('Standard deviation');
xlabel('Deflection', 'fontsize', 18);
ylabel('Rotation angle in degrees', 'fontsize', 18);
set(gca,'fontsize',16);
title({'Dependency between the deflection parameter d', 'and the rotation angle'}, 'fontsize', 18);

%figure(2);
%plot(sigma_rot,th);

