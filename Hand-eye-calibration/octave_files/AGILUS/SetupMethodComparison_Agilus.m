clc
clear all

numPositions = 25;

homeJoint = jointPosition(122.1, -81.6, 100.5, 15, 0.5, -57); %where the camera can see the marker, positions from each axes
%homeJoint = jointPosition(105,-90,111,23,-1.6,-85.35); 

%% OCATVE_SLICER OpenIGTLink PATH
addpath D:\Agilus\AGILUS;

port = 59152; % Defined by XML inside on the KUKA smartpad
addr = '172.31.1.140'; % Defined by XML inside the KUKA smartpad

%% Confirming Robot Connection
robot = connectAgilus(port,addr);

%% First motion is PTP motion to Home Position
robot.move(homeJoint);


markerPosition = [];
flangePosition = [];

%relPositions = ...
%[  -10   -1   -1   10   -8    2;
%   -1    0    2   -6   -3     1;
%    6   -5   -3    4    0     -4;
%   10    9    0   -4    6     1;
%   -5    6    3    3   -9     5;
%    0    1   -5   -6    2     2;
%   -2   -2    6    2   10     -4;
%    1   -2    0   -1   -3     2;
%    0   -5    7    8    2     1;
%   -7    6    9    3    1     1];

%relPositions = [  2    9   -2    5   -4   -2;
%                  -3   -6  -8    7   10   -1;
%                  8    7    4    4    6    0;
%                 -6   -5    2    5   -8   -3;
%                 -1   -3   -3    1    8    2;
%                  7   2    10   -5    2   -2;
%                 -8   -1   -2   -4    2   4;
%                 2    4     4    4    -2    3;
%                 -5   -2    5   -3    5    1;
%                 5    -1   -1   10   -8   -5  ];

%relPositions = ...
%[  -5   -1   -1   1   -3    1;
%   -1    6    2   -2   2    1;
%    6   -5   -9    4    0   2;
%   10    9    0   -2    3   -5;
%   -5    -6    3    3   -2   2;
%    0    1   -5   -6    2   6;
%   -2   -2    6    2    4   -7;
%    1   -2    0   -7   -3   2;
%    0   -5    9    5    2   1;
%   -7    3    9    3    1   8];


%% small volume of 20x20x20, angles in a range between -35 and 35 degrees

%relPositions = [  5   -4   -2     2    9    -2;
%                  7   10   -18   -10   -6   -8;
%                 -4   6     5    -8    7    -24;
%                  5   -8    13    12   20   12;
%                  7   -18   2    -30   -3   -13;
%                 -5    7    7    -1    8    10;
%                 -4   -13   4    18   -40   20;
%                 -14   10   9   -18   -10   -14;
%                 -3    5    -1   5    -15    5;
%                 -10   -8   -15  15    5    29;  
%                  -4   -4   -8   35    9    20;
%                  7    10   -2   15    22   -10;
%                  4    20   -11  -7    3    -20;
%                  15   7    -3   -20   -16   10;
%                  1    -8    12  -8    -10  -23];
                 
%% large volume of 50x50x50, angles in a range between -35 and 35 degrees                 
relPositions = [  15   -4   -2     2    9   -2;
                  27   10   -48   -10   -6  -8;
                  8    16    5    -8    7   -24;
                 -15   -8    13    12   20   12;
                 -12   -18   22   -30   -3  -13;
                 -5    47    7    -1     8   10;
                 -4    -13   4     18   -40  20;
                 -14   20    9    -18   -10 -14;
                 -3    -35   -1    5    -20   5;
                 -30   -8    -15   15    5   29;  
                 -17   -14   -8    35    9   20;
                  7    -30   50    15    22  -10;
                  14   -13   14    -7    3   -20;
                  15    7    -3    -20  -16   10;
                  1     25   -12   -8   -10  -23];               


relPositions(16:numPositions, :) = randi([-6 6], 10, 6);
%relPositions = randi([-6 6], numPositions, 6);
 
%relPositions = [  3    9   -2    5   -4   -2;
%                  5    6  -10    7   10   -1;
%                  8    9    4    4    6    0;
%                 -2   -6    0    5    8   -3;
%                 -8    1   -3    1    8    2;
%                  7   -4  -10   -5    2   -2;
%                 -8   -4   -2   -4    -2   -4;
%                 -3    3    4    4    8    3;
%                  9  -10    5   -3    -5    1;
%                 -4    2   -1   10   -8   -5  ];
%  
%positions = [0,-60,68,3,3.6,2;
%             2.1,-57,68,3,3.6,2;
%             2.1,-60,60,3,3.6,2;
%             2.1,-60,68,6,3.6,2;
%             2.1,-60,68,3,7.1,2;
%             2.1,-60,68,3,3.6,6;
%             2.1,-60,68,3,5,7.2;
%             -2,-60,70,3,3.6,2;
%             2.1,-60,68,4,1.1,2;
%             2,-57,65,3,3.6,2.2];
  
%% home position
Flange_home = robot.getCurrentCartesianPosition(); % as Frame, angles in degrees
home = [Flange_home.getX() Flange_home.getY() Flange_home.getZ() Flange_home.getAlphaRad() Flange_home.getBetaRad() Flange_home.getGammaRad()];
HFlange_home = CtoH(home); %*inv(rel_T);

HMarker_home = getCameraData2(); 
                 
dlmwrite('D:\Agilus\AGILUS\results\TrafoMatrices\HMarker_home.txt', HMarker_home);
dlmwrite('D:\Agilus\AGILUS\results\TrafoMatrices\HFlange_home.txt', HFlange_home);


%% test if random positions have sufficient rotation
cart_pos = []; 
tmp = zeros(1,6);
for i = 1:numPositions
  tmp += relPositions(i,:);
  cart_pos(:,:,i) = tmp;
end

min_alpha = min(cart_pos(:,4,:));
max_alpha = max(cart_pos(:,4,:));
disp('Range of alpha - min: '); disp(min_alpha); disp(' max:'); disp(max_alpha);
min_beta = min(cart_pos(:,5,:));
max_beta = max(cart_pos(:,5,:));
disp('Range of beta - min: '); disp(min_beta); disp(' max:'); disp(max_beta);
min_gamma = min(cart_pos(:,6,:));
max_gamma = max(cart_pos(:,6,:));
disp('Range of gamma - min: '); disp(min_gamma); disp(' max:'); disp(max_gamma);

min_x = min(cart_pos(:,1,:));
max_x = max(cart_pos(:,1,:));
disp('Range of x - min: '); disp(min_x); disp(' max:'); disp(max_x);
min_y = min(cart_pos(:,2,:));
max_y = max(cart_pos(:,2,:));
disp('Range of y - min: '); disp(min_y); disp(' max:'); disp(max_y);
min_z = min(cart_pos(:,3,:));
max_z = max(cart_pos(:,3,:));
disp('Range of z - min: '); disp(min_z); disp(' max:'); disp(max_z);


%% Slow down the robot speed
robot.setSpeed(10);   
k = 1;
     
%% Collect data to perform AX = ZB
% Different Flange and Marker positions relative to Robot and Camera respectively
for i=1:numPositions

  robot.moveRel( frame(relPositions(i,1), 
                       relPositions(i,2), 
                       relPositions(i,3), 
                       relPositions(i,4), 
                       relPositions(i,5), 
                       relPositions(i,6)) ); 

%  robot.move(jointPosition(positions(i,1),
%                           positions(i,2),
%                           positions(i,3),
%                           positions(i,4),
%                           positions(i,5),
%                           positions(i,6)));
               
  
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
  
  % Pause to allow the Robot to settle down
  pause(5)
  
   markerPosition = getCameraData2();
  
%  [ markerPosition, pointerPosition ] = getCameraData();

if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  %% save position only when the marker can be seen by the camera 
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
   end

end


disp('Data Collection Ended...')

%% Check if measurements are complete
[ l, m, n ] = size( mpHomoData );
numCamPoints = n;

[ p, q, r ] = size( fpCartData );
numRobotPoints = r

if numCamPoints ~= numRobotPoints
    
    error('Number of position data from the Camera and Robot is NOT equal')

else
  
  for i=1:numCamPoints
    
    fpHomoData(:,:,i) = CtoH(fpCartData(:,:,i));%*inv(rel_T);
  
  end

end 

%% Reshape for 'dornaika' and 'shah'
Camera_to_Marker = reshape(mpHomoData, [4, 4*numCamPoints]);
Robot_to_Flange = reshape(fpHomoData, [4, 4*numCamPoints]);

dlmwrite('D:\Agilus\AGILUS\results\TrafoMatrices\Camera_to_Marker_all_pos.txt', Camera_to_Marker);
dlmwrite('D:\Agilus\AGILUS\results\TrafoMatrices\Robot_to_Flange_all_pos.txt', Robot_to_Flange);


% Save absolute trafo matrices 
n = 15;
%% use first n entries for HEC
dlmwrite('D:\Agilus\AGILUS\results\TrafoMatrices\Camera_to_Marker.txt', Camera_to_Marker(:,1:4*n));
dlmwrite('D:\Agilus\AGILUS\results\TrafoMatrices\Robot_to_Flange.txt', Robot_to_Flange(:,1:4*n));

%% use remaining entries for the evaluation
if numCamPoints > n
  dlmwrite('D:\Agilus\AGILUS\results\TrafoMatrices\Camera_to_Marker_for_eval.txt', Camera_to_Marker(:,(4*n+1):4*numCamPoints));
  dlmwrite('D:\Agilus\AGILUS\results\TrafoMatrices\Robot_to_Flange_for_eval.txt', Robot_to_Flange(:,(4*n+1):4*numCamPoints));
end

robot.closeConnection;