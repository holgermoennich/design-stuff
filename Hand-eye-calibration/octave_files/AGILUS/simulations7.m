%% Simulation -- how inaccuracies influence the hand-eye calibration and errors
%% creation of ideal data (based on measurements of the robot)
%% addition of gaussian noise

clc;
clear all;

%%%%%%%%%%%%%%%%
% creation of ideal data
%%%%%%%%%%%%%%%%

n = 20; 
n_HEC = 15; % number of data sets for hand eye calibration

start_flange = [1000 100 500 0 0 0];
robot_0 = CtoH(start_flange);
start_rob = start_flange;% zeros(1,6);% start_flange;  % %start_flange

%base_to_fl_0 = CtoH([400 400 400 0 0 0]); 

% X
Opt_to_flange = [eye(3),[50,50,50]';[0 0 0 1]];

%% Z based on sensible position of camera
Cam_0 = [eye(3),[100 75 1000]';[0 0 0 1]];
Cam_to_base = Cam_0*Opt_to_flange*inv(robot_0);

iter = 360;

for l = 1:iter
  clear relPositions Robot_to_Flange;

  trans = randi([-10 10], 20, 3); %randi([-l l], 20, 3);
  rot = randi([-l l], 20, 3); %randi([-10 10], 20, 3);
  relPositions(:,1:3) = trans;
  relPositions(:,4:6) = rot;
  
%  relPositions = randi([-l l], 20, 6); 
   
for i = 1:n
  Robot_to_Flange(:, i*4-3:i*4) = CtoH(start_rob+relPositions(i,:));
end

clear Cam_to_opt;

%% compute ideal poses of Cam_to_opt 
for i = 1:n 
  Cam_to_opt(:, i*4-3:i*4) = Cam_to_base*Robot_to_Flange(:, i*4-3:i*4)*inv(Opt_to_flange); 
  Err = inv(Cam_to_base*Robot_to_Flange(:, i*4-3:i*4))*Cam_to_opt(:, i*4-3:i*4)*Opt_to_flange; % should be identity matrix
end


%%%%%%%%%%%%%%%%
%% addition of gaussian noise to the data
%%%%%%%%%%%%%%%%
  
err_rob_t = 0.5;% sigma for robot
err_rob_r = 0.0005; % creates rotation error between 0 and 3.4 degrees



for k = 1:length(err_rob_t)
  for m = 1:100
    
   clear Robot_to_Flange_noise Cam_to_opt_noise Robot_to_Flange_test Robot_to_Flange_training Cam_to_opt_test Cam_to_opt_training;
    
    %% noise on robot data
   Robot_to_Flange_noise = add_noise(Robot_to_Flange, err_rob_t, err_rob_r);
    
    %% noise on optical data
   Cam_to_opt_noise = add_noise(Cam_to_opt, err_rob_t, err_rob_r);

  %%%%%%%%%%%%%%%%
  %% define training and test data
  %%%%%%%%%%%%%%%%
  
  Robot_to_Flange_test = Robot_to_Flange_noise(:,1:n_HEC*4);
  Robot_to_Flange_training = Robot_to_Flange_noise(:,4*n_HEC+1:4*n);
  Cam_to_opt_test = Cam_to_opt_noise(:,1:n_HEC*4);
  Cam_to_opt_training = Cam_to_opt_noise(:,4*n_HEC+1:4*n);
  
  %%%%%%%%%%%%%%%%
  %% perform HEC
  %%%%%%%%%%%%%%%%
  
  clear Opt_to_flange_shah_noise Cam_to_base_shah_noise;
  
  %% optical/robot loop forward
  [Opt_to_flange_shah_noise, Cam_to_base_shah_noise] = shah(Cam_to_opt_test, Robot_to_Flange_test);
  
  norm_Ai(m) = norm(Cam_to_opt_test(1:3,4));
  norm_Bi(m) = norm(Robot_to_Flange_test(1:3,4));
  
  %%%%%%%%%%%%%%%%
  %% calculate errors
  %%%%%%%%%%%%%%%%
  
  %% optical/robot loop forward
  [ang_opt_to_flange(m), trans_opt_to_flange(m)] = delta_matrix(Opt_to_flange, Opt_to_flange_shah_noise);
  [ang_cam_to_base(m), trans_cam_to_base(m)] = delta_matrix(Cam_to_base, Cam_to_base_shah_noise);

  err_trans_opt(m) = norm(RMSE_trans(Cam_to_opt_training, Robot_to_Flange_training, Opt_to_flange_shah_noise, Cam_to_base_shah_noise));
  err_rot_opt(m) = RMSE_rot(Cam_to_opt_training, Robot_to_Flange_training, Opt_to_flange_shah_noise, Cam_to_base_shah_noise);
  
  end
  ang_opt_to_flange_mean(l) = mean(ang_opt_to_flange); % k, l
  trans_opt_to_flange_mean(l) = mean(trans_opt_to_flange);
  ang_cam_to_base_mean(l) = mean(ang_cam_to_base);
  trans_cam_to_base_mean(l) = mean(trans_cam_to_base);
  
  err_trans_opt_mean(l) = mean(err_trans_opt);
  err_rot_opt_mean(l) = mean(err_rot_opt);
  
  norm_A(l) = mean(norm_Ai);
  norm_B(l) = mean(norm_Bi);
  
end
end

%%%%%%%%%%%%%%%%
%% plot errors
%%%%%%%%%%%%%%%%

%% error of each transformation matrix 'X' and 'Z'
%% for changes in transformation matrices
figure(13);
subplot (2, 2, 1);
plot(1:iter, ang_opt_to_flange_mean, 'x');
title('Rotational error of X', 'fontsize', 15);
ylim([0 4]);
xlim([0 360]);
%xlabel('Increasing rotation of robot in degrees');
%xlabel('Increasing translational range of robot');
set(gca,'fontsize',12);
xlabel({'Increasing rotational range', 'of robot in degrees'}, 'fontsize', 14);
%xlabel({'Increasing rotational and', 'translational range of robot'}, 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 2, 2);
plot(1:iter, trans_opt_to_flange_mean, 'x');
title('Translational error of X', 'fontsize', 15);
ylim([0 25]);
xlim([0 360]);
%xlabel('Increasing rotation of robot in degrees');
%xlabel('Increasing translational range of robot');
set(gca,'fontsize',12);
xlabel({'Increasing rotational range', 'of robot in degrees'}, 'fontsize', 14);
%xlabel({'Increasing rotational and', 'translational range of robot'}, 'fontsize', 14);
ylabel({'Translational error', 'in mm'}, 'fontsize', 14);

subplot (2, 2, 3);
plot(1:iter, ang_cam_to_base_mean, 'x');
title('Rotational error of Z', 'fontsize', 15);
ylim([0 4]);
xlim([0 360]);
%xlabel('Increasing rotation of robot in degrees');
%xlabel('Increasing translational range of robot');
set(gca,'fontsize',12);
xlabel({'Increasing rotational range', 'of robot in degrees'}, 'fontsize', 14);
%xlabel({'Increasing rotational and', 'translational range of robot'}, 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 2, 4);
plot(1:iter, trans_cam_to_base_mean, 'x');
title('Translational error of Z', 'fontsize', 15);
ylim([0 25]);
xlim([0 360]);
%xlabel('Increasing rotation of robot in degrees');
%xlabel('Increasing translational range of robot');
set(gca,'fontsize',12);
xlabel({'Increasing rotational range', 'of robot in degrees'}, 'fontsize', 14);
%xlabel({'Increasing rotational and', 'translational range of robot'}, 'fontsize', 14);
ylabel({'Translational error', 'in mm'}, 'fontsize', 14);

figure(14);
subplot(2,1,1);
plot(1:iter, err_rot_opt_mean, ['x', 'r']);
title('Rotational error of closed loop', 'fontsize', 18);
ylim([0 4]);
xlim([0 360]);
%xlabel('Increasing rotation of robot in degrees');
%xlabel('Increasing translational range of robot in mm');
set(gca,'fontsize',13);
xlabel({'Increasing rotational range', 'of robot in degrees'}, 'fontsize', 14);
%xlabel({'Increasing rotational and', 'translational range of robot'}, 'fontsize', 14);
ylabel('Rotational error in degrees', 'fontsize', 14);

subplot (2, 1, 2);
plot(1:iter, err_trans_opt_mean, ['s', 'b']);
title('Translational error of closed loop', 'fontsize', 18);
ylim([0 25]);
xlim([0 360]);
%xlabel('Increasing rotation of robot in degrees');
xlabel({'Increasing rotational range', 'of robot in degrees'}, 'fontsize', 14);
set(gca,'fontsize',13);
%xlabel({'Increasing rotational and', 'translational range of robot'}, 'fontsize', 14);
ylabel('Translational error in mm', 'fontsize', 14);

%figure(40);
%plot(1:iter, norm_A, 'r');
%hold on;
%plot(1:iter, norm_B, 'b');
%legend('A', 'B');
%title('Distances of camera to marker and robot to flange', 'fontsize', 18);
%set(gca,'fontsize',16);
%xlabel({'Increasing rotational and', 'translational range of robot'}, 'fontsize', 18);
%ylabel('Norm of translation vector', 'fontsize', 18);