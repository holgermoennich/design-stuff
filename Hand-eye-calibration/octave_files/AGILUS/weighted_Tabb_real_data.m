%% Iterative method with weighted residuals applied on real data (analogous to HEC_XRay)

clc;
clear all;

%%%%%%%
%% Read transformation matrices
%%%%%%%

XRay_to_IBRMarker_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\XRay_to_IBRMarker_all_pos.txt');
Camera_to_Marker_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker_all_pos.txt');
Robot_to_Flange_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange_all_pos.txt');

[r0,c0] = size(XRay_to_IBRMarker_all_sorted_0);
n0 = c0/4;

%%%%%%%
%% Prepare transformation matrices
%%%%%%%

p = 1;

%% remove measurement where XRay to IBR marker is identity matrix
for i = 1:n0
  if XRay_to_IBRMarker_all_sorted_0(:, i*4-3:i*4) == eye(4)
    disp('Measurement number '); disp(i); disp(' removed');
  else
    XRay_to_IBRMarker_all_sorted(:, p*4-3:p*4) = XRay_to_IBRMarker_all_sorted_0(:, i*4-3:i*4);
    Camera_to_Marker_all_sorted(:, p*4-3:p*4) = Camera_to_Marker_all_sorted_0(:, i*4-3:i*4);
    Robot_to_Flange_all_sorted(:, p*4-3:p*4) = Robot_to_Flange_all_sorted_0(:, i*4-3:i*4);
    p +=1;
  end
end

[r,c] = size(XRay_to_IBRMarker_all_sorted);
n = c/4;

%% shuffle data 
idx = randperm(n); % random order of indices

for i = 1:n
  XRay_to_IBRMarker_all(:, i*4-3:i*4) = XRay_to_IBRMarker_all_sorted(:,idx(i)*4-3:idx(i)*4);
  Camera_to_Marker_all(:, i*4-3:i*4) = Camera_to_Marker_all_sorted(:,idx(i)*4-3:idx(i)*4);
  Robot_to_Flange_all(:, i*4-3:i*4) = Robot_to_Flange_all_sorted(:,idx(i)*4-3:idx(i)*4);
end

%% for cross validation
f = 6; % k-fold cross-validation
size_subset = n/f; % 6

for m = 1:f 
  
  e = size_subset*m; % end of test data (6,12,18,24)
  b = e-(size_subset-1); % begin of test data (1,7,13,19)
  
  clear As_XRay_eval Bs_eval As_XRay Bs;
  
  %% test data -> contains only one subset
  As_XRay_eval = XRay_to_IBRMarker_all(:, b*4-3:e*4);
  As_opt_eval = Camera_to_Marker_all(:, b*4-3:e*4);
  Bs_eval = Robot_to_Flange_all(:, b*4-3:e*4);
  
  
  %% training data -> contains the remaining subsets
  if b == 1
    As_XRay = XRay_to_IBRMarker_all(:,e*4+1:n*4);
    As_opt = Camera_to_Marker_all(:,e*4+1:n*4);
    Bs = Robot_to_Flange_all(:,e*4+1:n*4);
  elseif e == n
    As_XRay(:,1:4*(n-size_subset)) = XRay_to_IBRMarker_all(:,1:(b-1)*4);
    As_opt(:,1:4*(n-size_subset)) = Camera_to_Marker_all(:,1:(b-1)*4);
    Bs(:,1:4*(n-size_subset)) = Robot_to_Flange_all(:,1:(b-1)*4);
  else
    As_XRay(:,1:(b-1)*4) = XRay_to_IBRMarker_all(:,1:(b-1)*4);
    As_XRay(:,b*4-3:4*(n-size_subset)) = XRay_to_IBRMarker_all(:,e*4+1:n*4);
    As_opt(:,1:(b-1)*4) = Camera_to_Marker_all(:,1:(b-1)*4);
    As_opt(:,b*4-3:4*(n-size_subset)) = Camera_to_Marker_all(:,e*4+1:n*4);
    Bs(:,1:(b-1)*4) = Robot_to_Flange_all(:,1:(b-1)*4);
    Bs(:,b*4-3:4*(n-size_subset)) = Robot_to_Flange_all(:,e*4+1:n*4);
  end

  [r1,c1] = size(As_XRay);
  numRobotPoints = c1/4;

  [r2,c2] = size(Bs);

  [r3,c3] = size(As_opt);

  if c1!=c2 && c1!=c3
    error('Size of transformation matrices differ!');
  end

  %% perform 6 combiations of hand eye calibration
  %% loop: Camera - Base - Flange - optical marker
  disp('----------------------------');
  disp(m);
  disp('----------------------------');
  disp('Geometrical results');
  
  disp('optical - robot loop');
  disp('forward');
  [Opt_to_flange, Cam_to_base] = tabb_weighted(As_opt, Bs);

  %% loop: Source - Base - Flange - IBR marker
  disp('XRay - robot loop');
  disp('forward');
  [IBR_to_flange, Source_to_base] = tabb_weighted(As_XRay, Bs); 
 
  %% loop: Camera - optical marker - IBR marker - Source
  disp('optical - XRay loop');
  disp('forward');
  [Opt_to_IBR, Cam_to_source] = tabb_weighted(As_opt, As_XRay);

  disp('----------------------------');
  disp('Evaluation');

  [r1,c1] = size(As_XRay_eval);
  [r2,c2] = size(Bs_eval);
  [r3,c3] = size(As_opt_eval);

  if c1!=c2 && c1!=c3
    error('Size of transformation matrices differ!');
  end
  
  disp('Optical/robot loop forward');
  disp('mathematical errors');
  disp('rmse_trans');disp(norm(RMSE_trans(As_opt_eval, Bs_eval, Opt_to_flange, Cam_to_base)));
  disp('rmse_rot');disp(RMSE_rot(As_opt_eval, Bs_eval, Opt_to_flange, Cam_to_base));
  disp('e_c');disp(e_c(As_opt_eval, Bs_eval, Opt_to_flange, Cam_to_base));
  
  trans_err_opt(1,m) = norm(RMSE_trans(As_opt_eval, Bs_eval, Opt_to_flange, Cam_to_base));
  rot_err_opt(1,m) = RMSE_rot(As_opt_eval, Bs_eval, Opt_to_flange, Cam_to_base);
  ec_opt(1,m) = e_c(As_opt_eval, Bs_eval, Opt_to_flange, Cam_to_base);

  disp('XRay/robot loop forward');
  disp('mathematical errors');
  disp('rmse_trans');disp(norm(RMSE_trans(As_XRay_eval, Bs_eval, IBR_to_flange, Source_to_base)));
  disp('rmse_rot');disp(RMSE_rot(As_XRay_eval, Bs_eval, IBR_to_flange, Source_to_base));
  disp('e_c');disp(e_c(As_XRay_eval, Bs_eval, IBR_to_flange, Source_to_base));
  
  trans_err_XRay(1,m) = norm(RMSE_trans(As_XRay_eval, Bs_eval, IBR_to_flange, Source_to_base));
  rot_err_XRay(1,m) = RMSE_rot(As_XRay_eval, Bs_eval, IBR_to_flange, Source_to_base);
  ec_XRay(1,m) = e_c(As_XRay_eval, Bs_eval, IBR_to_flange, Source_to_base);

  disp('Optical/XRay loop forward');
  disp('mathematical errors - Tabb');
  disp('rmse_trans');disp(norm(RMSE_trans(As_opt_eval, As_XRay_eval, Opt_to_IBR, Cam_to_source)));
  disp('rmse_rot');disp(RMSE_rot(As_opt_eval, As_XRay_eval, Opt_to_IBR, Cam_to_source));
  disp('e_c');disp(e_c(As_opt_eval, As_XRay_eval, Opt_to_IBR, Cam_to_source));
  
  trans_err_comb(1,m) =  norm(RMSE_trans(As_opt_eval, As_XRay_eval, Opt_to_IBR, Cam_to_source));
  rot_err_comb(1,m) = RMSE_rot(As_opt_eval, As_XRay_eval, Opt_to_IBR, Cam_to_source);
  ec_comb(1,m) = e_c(As_opt_eval, As_XRay_eval, Opt_to_IBR, Cam_to_source);
end

figure(3);
plot(1:f, trans_err_opt, ['x', 'r']);
hold on;
plot(1:f, trans_err_XRay, ['*', 'b']);
hold on;
plot(1:f, trans_err_comb, ['s', 'g']);
ylim([0,3]);
xlabel('Number of test run');
ylabel('Translational error in mm');
legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
title('Tabb - forward - translational error');

figure(4);
plot(1:f, rot_err_opt, ['x', 'r']);
hold on;
plot(1:f, rot_err_XRay, ['*', 'b']);
hold on;
plot(1:f, rot_err_comb, ['s', 'g']);
ylim([0,2]);
xlabel('Number of test run');
ylabel('Rotational error in degrees');
legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
title('Tabb - forward - rotational error');

more off;
disp('Crossvalidated error values: ');
disp('Tabb - forward - translational error');
disp('Translational error - optical/robot loop'); disp(mean(trans_err_opt));
disp('Translational error - XRay/robot loop'); disp(mean(trans_err_XRay));
disp('Translational error - optical/XRay loop'); disp(mean(trans_err_comb));

disp('Tabb - forward/backward - rotational error');
disp('Rotational error - optical/robot loop'); disp(mean(rot_err_opt));
disp('Rotational error - XRay/robot loop'); disp(mean(rot_err_XRay));
disp('Rotational error - optical/XRay loop'); disp(mean(rot_err_comb));

disp('Tabb - forward - combined error');
disp('Combined error - optical/robot loop'); disp(mean(ec_opt));
disp('Combined error - XRay/robot loop'); disp(mean(ec_XRay));
disp('Combined error - optical/XRay loop'); disp(mean(ec_comb));
