%% Simulation -- how inaccuracies influence the hand-eye calibration and errors
%% creation of ideal data (based on measurements of the robot)
%% addition of gaussian noise

clc;
clear all;

%%%%%%%%%%%%%%%%
% creation of ideal data
%%%%%%%%%%%%%%%%

n = 20; 
n_HEC = 15; % number of data sets for hand eye calibration

start_flange = [1000 100 500 0 0 0];
robot_0 = CtoH(start_flange);
start_rob = start_flange;

% X
Opt_to_flange = [eye(3),[50,50,50]';[0 0 0 1]];

%% Z based on sensible position of camera
Cam_0 = [eye(3),[100 75 1000]';[0 0 0 1]];
Cam_to_base = Cam_0*Opt_to_flange*inv(robot_0);

%relPositions = randi([-50 50], 20, 6);
%% range: [-50, 50]
relPositions = ...
[  34    3   -4   34   20  -15;
    1   49   32   41  -23  -27;
   44   30  -17    4   23   -7;
   34   48  -24  -27  -30  -17;
  -50   37  -45   12    7  -46;
  -43  -43   15  -33  -15    2;
   -3  -29   14   48   29  -29;
  -37   36  -47  -15   19  -18;
   32  -15  -11    8  -11   -5;
  -11   -5   -2   26   30   -5;
  -35   26   40  -35   47  -15;
    3    6   13  -10   22  -15;
   49   28   21   33  -33   29;
   31   40   12    9    6   10;
   35   50  -39   -5  -49    9;
    1   30  -11  -14  -47  -21;
  -37  -30   45   -6   42   45;
   39  -44    1   25   18    3;
   22   46   50   37  -47  -35;
    7  -38  -24   15   12  -38];
   
for i = 1:n
  Robot_to_Flange(:, i*4-3:i*4) = CtoH(start_rob+relPositions(i,:));
end

     
%% compute ideal poses of Cam_to_opt 
for i = 1:n 
  Cam_to_opt(:, i*4-3:i*4) = Cam_to_base*Robot_to_Flange(:, i*4-3:i*4)*inv(Opt_to_flange); 
  Err = inv(Cam_to_base*Robot_to_Flange(:, i*4-3:i*4))*Cam_to_opt(:, i*4-3:i*4)*Opt_to_flange; % should be identity matrix
end


%%%%%%%%%%%%%%%%
%% addition of gaussian noise to the data
%%%%%%%%%%%%%%%%
  
%err_opt_t = [0.07, 0.07, 0.7]; % z coordinate 10 times worse
%err_opt_r = [0.025, 0.025, 0.0025]; %[0.03375,0.03375, 0.03375]; %[0.025, 0.025, 0.0025]; %[0.5, 0.5, 0.05];%[0.05, 0.5, 0.5]; % alpha and beta 10 times worse
a = 0:0.01:10; % 0.05; %0.0001:0.01:0.3;
err_rob_t = 0.2; % robot translational inaccuracy
err_rob_r = [0.05,0.05,0.05]; %[0.03375,0.03375,0.03375]; %[0.05,0.05,0.05];%[0.1, 0.1, 0.1]; % ? no information about rotational error of robot available

for k = 1:length(a)
   noise_rot = 0.5/(2*a(k)+1); %0.10125/(2*a(k)+1); %0.001
   noise_trans = 1.0/(2+a(k)); %0.84/(2+a(k)); 0.9
   err_opt_t = [noise_trans, noise_trans, a(k)*noise_trans];
   err_opt_r = [a(k)*noise_rot,a(k)*noise_rot, noise_rot];
  
  for m = 1:100
    
   clear Robot_to_Flange_noise Cam_to_opt_noise Robot_to_Flange_test Robot_to_Flange_training Cam_to_opt_test Cam_to_opt_training;
    
    %% noise on robot data
   Robot_to_Flange_noise = add_noise_Euler(Robot_to_Flange, err_rob_t, err_rob_r);
    
    %% noise on optical data
   Cam_to_opt_noise = add_noise_Euler(Cam_to_opt, err_opt_t, err_opt_r);


  %%%%%%%%%%%%%%%%
  %% define training and test data
  %%%%%%%%%%%%%%%%
  
  Robot_to_Flange_training = Robot_to_Flange_noise(:,1:n_HEC*4);
  Robot_to_Flange_test = Robot_to_Flange_noise(:,4*n_HEC+1:4*n);
  Cam_to_opt_training = Cam_to_opt_noise(:,1:n_HEC*4);
  Cam_to_opt_test = Cam_to_opt_noise(:,4*n_HEC+1:4*n);
  
  %%%%%%%%%%%%%%%%
  %% perform HEC
  %%%%%%%%%%%%%%%%

  clear Opt_to_flange_shah_noise Cam_to_base_shah_noise;
  
  %% optical/robot loop forward
  [Opt_to_flange_shah_noise, Cam_to_base_shah_noise] = shah(Cam_to_opt_training, Robot_to_Flange_training);
  
  %%%%%%%%%%%%%%%%
  %% calculate errors
  %%%%%%%%%%%%%%%%
  
  %% optical/robot loop forward
  [ang_opt_to_flange(m), trans_opt_to_flange(m)] = delta_matrix(Opt_to_flange, Opt_to_flange_shah_noise);
  [ang_cam_to_base(m), trans_cam_to_base(m)] = delta_matrix(Cam_to_base, Cam_to_base_shah_noise);

  err_trans_opt(m) = norm(RMSE_trans(Cam_to_opt_test, Robot_to_Flange_test, Opt_to_flange_shah_noise, Cam_to_base_shah_noise));
  err_rot_opt(m) = RMSE_rot(Cam_to_opt_test, Robot_to_Flange_test, Opt_to_flange_shah_noise, Cam_to_base_shah_noise);
  

  end
  ang_opt_to_flange_mean(k) = mean(ang_opt_to_flange);
  trans_opt_to_flange_mean(k) = mean(trans_opt_to_flange);
  ang_cam_to_base_mean(k) = mean(ang_cam_to_base);
  trans_cam_to_base_mean(k) = mean(trans_cam_to_base);
  
  err_trans_opt_mean(k) = mean(err_trans_opt);
  err_rot_opt_mean(k) = mean(err_rot_opt);
  
end


%%%%%%%%%%%%%%%%
%% plot errors
%%%%%%%%%%%%%%%%

%% error of each transformation matrix 'X' and 'Z'
%% for changes in transformation matrices

figure(25);
subplot (2, 2, 1);
plot(a, ang_opt_to_flange_mean, 'x');%['x', 'r']
title('Rotational error of X', 'fontsize', 18);
ylim([0 1]);
set(gca,'fontsize',13);
%xlabel('Increasing noise on rotation');
%xlabel('Increasing weighting factor for \alpha and \beta');
%xlabel('Increasing weighting factor for z');
xlabel({'Increasing weighting factor', 'for \alpha, \beta and z'}, 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 2, 2);
plot(a, trans_opt_to_flange_mean, 'x');
title('Translational error of X', 'fontsize', 18);
ylim([0 4]);
set(gca,'fontsize',13);
%xlabel('Increasing noise on rotation');
%xlabel('Increasing weighting factor for \alpha and \beta');
%xlabel('Increasing weighting factor for z');
xlabel({'Increasing weighting factor', 'for \alpha, \beta and z'}, 'fontsize', 14);
ylabel({'Translational error', 'in mm'}, 'fontsize', 14);

subplot (2, 2, 3);
plot(a, ang_cam_to_base_mean, 'x');%err_rob_r
title('Rotational error of Z', 'fontsize', 18);
ylim([0 1]);
set(gca,'fontsize',13);
%xlabel('Increasing noise on rotation');
%xlabel('Increasing weighting factor for \alpha and \beta');
%xlabel('Increasing weighting factor for z');
xlabel({'Increasing weighting factor', 'for \alpha, \beta and z'}, 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 2, 4);
plot(a, trans_cam_to_base_mean, 'x');
title('Translational error of Z', 'fontsize', 18);
ylim([0 4]);
set(gca,'fontsize',13);
%xlabel('Increasing noise on rotation');
%xlabel('Increasing weighting factor for \alpha and \beta');
%xlabel('Increasing weighting factor for z');
xlabel({'Increasing weighting factor', 'for \alpha, \beta and z'}, 'fontsize', 14);
ylabel({'Translational error', 'in mm'}, 'fontsize', 14);


figure(26);
subplot(2,1,1);
plot(a, err_rot_opt_mean, ['s', 'r']);
title('Rotational error of closed loop', 'fontsize', 18);
ylim([0 1]);
set(gca,'fontsize',13);
%xlabel('Increasing noise on rotation');
%xlabel('Increasing weighting factor for \alpha and \beta');
%xlabel('Increasing weighting factor for z');
xlabel({'Increasing weighting factor', 'for \alpha, \beta and z'}, 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 1, 2);
plot(a, err_trans_opt_mean, ['s', 'b']);
title('Translational error of closed loop', 'fontsize', 18);
ylim([0 4]);
set(gca,'fontsize',13);
%xlabel('Increasing noise on rotation');
%xlabel('Increasing weighting factor for \alpha and \beta');
%xlabel('Increasing weighting factor for z');
xlabel({'Increasing weighting factor', 'for \alpha, \beta and z'}, 'fontsize', 14);
ylabel('Translational error in mm', 'fontsize', 14);

%disp(err_rot_opt_mean);
%disp(err_trans_opt_mean);