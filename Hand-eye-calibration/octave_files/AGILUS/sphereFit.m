%function [Center,Radius] = sphereFit(X)
%% this fits a sphere to a collection of data using a closed form for the
%% solution (opposed to using an array the size of the data set). 
%% Minimizes Sum((x-xc)^2+(y-yc)^2+(z-zc)^2-r^2)^2
%% x,y,z are the data, xc,yc,zc are the sphere's center, and r is the radius
%
%% Assumes that points are not in a singular configuration, real numbers, ...
%% if you have coplanar data, use a circle fit with svd for determining the
%% plane, recommended Circle Fit (Pratt method), by Nikolai Chernov
%% http://www.mathworks.com/matlabcentral/fileexchange/22643
%
%% Input:
%% X: n x 3 matrix of cartesian data
%% Outputs:
%% Center: Center of sphere 
%% Radius: Radius of sphere
%% Author:
%% Alan Jennings, University of Dayton
%
%A=[mean(X(:,1).*(X(:,1)-mean(X(:,1)))), ...
%    2*mean(X(:,1).*(X(:,2)-mean(X(:,2)))), ...
%    2*mean(X(:,1).*(X(:,3)-mean(X(:,3)))); ...
%    0, ...
%    mean(X(:,2).*(X(:,2)-mean(X(:,2)))), ...
%    2*mean(X(:,2).*(X(:,3)-mean(X(:,3)))); ...
%    0, ...
%    0, ...
%    mean(X(:,3).*(X(:,3)-mean(X(:,3))))];
%A=A+A.';
%B=[mean((X(:,1).^2+X(:,2).^2+X(:,3).^2).*(X(:,1)-mean(X(:,1))));...
%    mean((X(:,1).^2+X(:,2).^2+X(:,3).^2).*(X(:,2)-mean(X(:,2))));...
%    mean((X(:,1).^2+X(:,2).^2+X(:,3).^2).*(X(:,3)-mean(X(:,3))))];
%Center=(A\B).';
%Radius=sqrt(mean(sum([X(:,1)-Center(1),X(:,2)-Center(2),X(:,3)-Center(3)].^2,2)));

function [center,radius,residuals] = sphereFit(x,y,z)
% Fit a sphere to data using the least squares approach.
%
% Fits the equation of a sphere in Cartesian coordinates to a set of xyz
% data points by solving the overdetermined system of normal equations, i.e.
% x^2 + y^2 + z^2 + a*x + b*y + c*z + d = 0
% The least squares sphere has radius R = sqrt((a^2+b^2+c^2)/4-d) and
% center coordinates (x,y,z) = (-a/2,-b/2,-c/2).
%
% Input arguments:
% x,y,z:
%    Cartesian coordinates of noisy data points
%
% Output arguments:
% center:
%    coordinates of the least-squares fit sphere center
% radius:
%    least-squares fit sphere radius
% residuals:
%    residuals in the radial direction
%
% Examples:
% [center,radius,residuals] = shperefit(X)
% [center,radius,residuals] = spherefit(x,y,z);

% Copyright 2010 Levente Hunyadi

narginchk(1,3);
n = size(x,1);
switch nargin  % n x 3 matrix
    case 1
        validateattributes(x, {'numeric'}, {'2d','real','size',[n,3]});
        z = x(:,3);
        y = x(:,2);
        x = x(:,1);
    otherwise  % three x,y,z vectors
        validateattributes(x, {'numeric'}, {'real','vector'});
        validateattributes(y, {'numeric'}, {'real','vector'});
        validateattributes(z, {'numeric'}, {'real','vector'});
        x = x(:);  % force into columns
        y = y(:);
        z = z(:);
        validateattributes(x, {'numeric'}, {'size',[n,1]});
        validateattributes(y, {'numeric'}, {'size',[n,1]});
        validateattributes(z, {'numeric'}, {'size',[n,1]});
end

% need four or more data points
if n < 4
   error('spherefit:InsufficientData', ...
       'At least four points are required to fit a unique sphere.');
end

% solve linear system of normal equations
%b = (x.^2 + y.^2 + z.^2);
%fun = @(x_c,y_c,z_c,r) -2*x_c*x - 2*y_c*y -2*z_c*z + x_c^2 + y_c^2 + z_c^2 + r^2 + b; 
%x0 = [0 0 0 0];
%g = lsqnonlin(fun,x0);

A = [x, y, z, ones(size(x))];
b = -(x.^2 + y.^2 + z.^2);
a = A \ b;

% return center coordinates and sphere radius
center = -a(1:3)./2;
radius = realsqrt(sum(center.^2)-a(4));

%if nargout > 2
%	% calculate residuals
%   residuals = radius - sqrt(sum(bsxfun(@minus,[x y z],center.').^2,2));
%elseif nargout > 1
%	% skip
%else
%    % plot sphere
%    hold all;
%	%sphere_gd(6,radius,center);
%    hold off;
%end
residuals = radius - sqrt(sum(bsxfun(@minus,[x y z],center.').^2,2));
rms = sqrt((sum(residuals.^2))/n);

%disp('residuals: '); disp(residuals);
%disp('rms: ') ;disp(rms);
%disp('center: '); disp(center);
%disp('radius: '); disp(radius);


end