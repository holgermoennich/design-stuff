function robot = connectAgilus(port, ipAddr)

tControlRobot = "RobotAPI.ControlRobot";
backLog = 50; % Default 50

robot = javaObject(tControlRobot,port,backLog,ipAddr);

end