%% Simulation -- how inaccuracies influence the hand-eye calibration and errors
%% creation of ideal data (based on measurements of the robot)
%% addition of gaussian noise

clc;
clear all;

%%%%%%%%%%%%%%%%
% creation of ideal data
%%%%%%%%%%%%%%%%

n = 20; 
n_HEC = 15; % number of data sets for hand eye calibration

start_flange = [1000 100 500 0 0 0];
robot_0 = CtoH(start_flange);
start_rob = start_flange;% zeros(1,6);% start_flange;

%base_to_fl_0 = CtoH([400 400 400 0 0 0]); 

% X
Opt_to_flange = [eye(3),[50,50,50]';[0 0 0 1]];

%% Z based on sensible position of camera
Cam_0 = [eye(3),[100 75 1000]';[0 0 0 1]];
Cam_to_base = Cam_0*Opt_to_flange*inv(robot_0);

iter = 100; %400

%for l = 1:iter
  clear relPositions Robot_to_Flange;
  
%relPositions = randi([-l l], 20, 6);
%% range: [-50, 50]
relPositions = ...
[  34    3   -4   34   20  -15;
    1   49   32   41  -23  -27;
   44   30  -17    4   23   -7;
   34   48  -24  -27  -30  -17;
  -50   37  -45   12    7  -46;
  -43  -43   15  -33  -15    2;
   -3  -29   14   48   29  -29;
  -37   36  -47  -15   19  -18;
   32  -15  -11    8  -11   -5;
  -11   -5   -2   26   30   -5;
  -35   26   40  -35   47  -15;
    3    6   13  -10   22  -15;
   49   28   21   33  -33   29;
   31   40   12    9    6   10;
   35   50  -39   -5  -49    9;
    1   30  -11  -14  -47  -21;
  -37  -30   45   -6   42   45;
   39  -44    1   25   18    3;
   22   46   50   37  -47  -35;
    7  -38  -24   15   12  -38];
   
for i = 1:n
  Robot_to_Flange(:, i*4-3:i*4) = CtoH(start_rob+relPositions(i,:));
end

%% optical - robot loop

%for l = 1:iter

clear Cam_to_opt;
%R_fl = EtoR([100,100,100], 'Rob');
%R_base = EtoR([2,2,2], 'Rob');
%R_var = EtoR([l,l,l], 'Rob');
%Opt_to_flange = [eye(3),[50,50,50]';[0 0 0 1]];

%Cam_to_base = [eye(3),[l,200,200]';[0 0 0 1]];
%Z_norm(l) = norm(3*l);


%Cam_0 = [eye(3),[100 75 1000]';[0 0 0 1]];
%Cam_to_base = Cam_0*Opt_to_flange*inv(Robot_to_Flange(:,1:4));
%Cam_to_base = [eye(3),[500,500,500]';[0 0 0 1]];

     
%% compute ideal poses of Cam_to_opt 
for i = 1:n 
  Cam_to_opt(:, i*4-3:i*4) = Cam_to_base*Robot_to_Flange(:, i*4-3:i*4)*inv(Opt_to_flange); 
  Err = inv(Cam_to_base*Robot_to_Flange(:, i*4-3:i*4))*Cam_to_opt(:, i*4-3:i*4)*Opt_to_flange; % should be identity matrix
end


%%%%%%%%%%%%%%%%
%% addition of gaussian noise to the data
%%%%%%%%%%%%%%%%
  
err_rob_t = 0.01:0.01:1; %0.5;% sigma for robot
err_rob_r = linspace(0.0000001, 0.001, length(err_rob_t)); % 0.01:0.01:0.04; %0.0005; % 0.01; %0.0000001:0.00001:0.001; %0.0005; % creates rotation error between 0 and 3.4 degrees
%err_rob_r = 0.5/length(err_rob_t):0.5/length(err_rob_t):0.5; %0.01:0.005:0.5;
err_opt_t = 0.01:0.01:1; %0.1; % sigma for optical tracker
%err_opt_r = 0.5/length(err_rob_t):0.5/length(err_rob_t):0.5;



for k = 1:length(err_rob_t)
  for m = 1:2
    
   clear Robot_to_Flange_noise Cam_to_opt_noise Robot_to_Flange_test Robot_to_Flange_training Cam_to_opt_test Cam_to_opt_training;
    
    %% noise on robot data
   Robot_to_Flange_noise = add_noise(Robot_to_Flange, err_rob_t(k), err_rob_r(k));%err_rob_t(k) err_rob_r(k)
    
    %% noise on optical data
   Cam_to_opt_noise = add_noise(Cam_to_opt, err_rob_t(k), err_rob_r(k)); %err_rob_t(k)


  %%%%%%%%%%%%%%%%
  %% define training and test data
  %%%%%%%%%%%%%%%%
  
  Robot_to_Flange_test = Robot_to_Flange_noise(:,1:n_HEC*4);
  Robot_to_Flange_training = Robot_to_Flange_noise(:,4*n_HEC+1:4*n);
  Cam_to_opt_test = Cam_to_opt_noise(:,1:n_HEC*4);
  Cam_to_opt_training = Cam_to_opt_noise(:,4*n_HEC+1:4*n);
  
  %%%%%%%%%%%%%%%%
  %% perform HEC
  %%%%%%%%%%%%%%%%

%  for i = 1:n_HEC
%    A_i = Cam_to_opt_test(:,4*i-3:4*i);
%    Opt_to_cam_test(:,4*i-3:4*i) = inv(A_i);
%    B_i = Robot_to_Flange_test(:,4*i-3:4*i);
%    Flange_to_robot_test(:,4*i-3:4*i) = inv(B_i);
%  end
  
  clear Opt_to_flange_shah_noise Cam_to_base_shah_noise Cam_to_base_shah_noise2 Opt_to_flange_shah_noise2 Flange_to_opt_shah_noise Base_to_cam_shah_noise Base_to_cam_shah_noise2 Flange_to_opt_shah_noise2;
  
  %% optical/robot loop forward
  [Opt_to_flange_shah_noise, Cam_to_base_shah_noise] = shah(Cam_to_opt_test, Robot_to_Flange_test);
  
%  Cam_to_base_shah_noise = Cam_to_base_shah_noise*inv(base_to_fl_0);
%  if (! is_real_array (Opt_to_flange_shah_noise))
%    disp('complex numbers');
%  end
%  [Cam_to_base_shah_noise2, Opt_to_flange_shah_noise2] = shah(Opt_to_cam_test, Flange_to_robot_test); % inverse input of A and B
  %% optical/robot loop backward
%  [Flange_to_opt_shah_noise, Base_to_cam_shah_noise] = shah(Robot_to_Flange_test, Cam_to_opt_test);
%  [Base_to_cam_shah_noise2, Flange_to_opt_shah_noise2] = shah(Flange_to_robot_test, Opt_to_cam_test); % inverse input of A and B

  %%%%%%%%%%%%%%%%
  %% calculate errors
  %%%%%%%%%%%%%%%%
  
  %% optical/robot loop forward
  [ang_opt_to_flange(m), trans_opt_to_flange(m)] = delta_matrix(Opt_to_flange, Opt_to_flange_shah_noise);
  [ang_cam_to_base(m), trans_cam_to_base(m)] = delta_matrix(Cam_to_base, Cam_to_base_shah_noise);

  %%
%  [ang_opt_to_flange2(k), trans_opt_to_flange2(k)] = delta_matrix(Opt_to_flange, Opt_to_flange_shah_noise2);
%  [ang_cam_to_base2(k), trans_cam_to_base2(k)] = delta_matrix(Cam_to_base, Cam_to_base_shah_noise2);
  %%
  
  err_trans_opt(m) = norm(RMSE_trans(Cam_to_opt_training, Robot_to_Flange_training, Opt_to_flange_shah_noise, Cam_to_base_shah_noise));
  err_rot_opt(m) = RMSE_rot(Cam_to_opt_training, Robot_to_Flange_training, Opt_to_flange_shah_noise, Cam_to_base_shah_noise);
  
  %% optical/robot loop backward

%  [ang_flange_to_opt(l), trans_flange_to_opt(l)] = delta_matrix(Opt_to_flange, inv(Flange_to_opt_shah_noise));
%  [ang_base_to_cam(l), trans_base_to_cam(l)] = delta_matrix(Cam_to_base, inv(Base_to_cam_shah_noise));
%  %%
%  [ang_flange_to_opt2(k), trans_flange_to_opt2(k)] = delta_matrix(Opt_to_flange, inv(Flange_to_opt_shah_noise2));
%  [ang_base_to_cam2(k), trans_base_to_cam2(k)] = delta_matrix(Cam_to_base, inv(Base_to_cam_shah_noise2));
%  %%
%  err_trans_opt_b(k) = norm(RMSE_trans(Robot_to_Flange_training, Cam_to_opt_training, Flange_to_opt_shah_noise, Base_to_cam_shah_noise));
%  err_rot_opt_b(k) = RMSE_rot(Robot_to_Flange_training, Cam_to_opt_training, Flange_to_opt_shah_noise, Base_to_cam_shah_noise);
%%  

  end
  ang_opt_to_flange_mean(k) = mean(ang_opt_to_flange); % k, l
  trans_opt_to_flange_mean(k) = mean(trans_opt_to_flange);
  ang_cam_to_base_mean(k) = mean(ang_cam_to_base);
  trans_cam_to_base_mean(k) = mean(trans_cam_to_base);
  
  err_trans_opt_mean(k) = mean(err_trans_opt);
  err_rot_opt_mean(k) = mean(err_rot_opt);
  
end
%end
%%%%%%%%%%%%%%%%
%% display errors
%%%%%%%%%%%%%%%%

%disp('optical/robot loop, forward, translational error: '); disp(mean(err_trans_opt));
%disp('optical/robot loop, forward, rotational error: '); disp(mean(err_rot_opt));
%disp('optical/robot loop, backward, translational error: '); disp(mean(err_trans_opt_b));
%disp('optical/robot loop, backward, rotational error: '); disp(mean(err_rot_opt_b));

%%%%%%%%%%%%%%%%
%% plot errors
%%%%%%%%%%%%%%%%

%% error of each transformation matrix 'X' and 'Z'
%% for changes in transformation matrices
%figure(1);
%subplot (2, 2, 1);
%plot(1:iter, ang_opt_to_flange_mean, 'x');%1:400
%%hold on;
%%plot(1:l*m, ang_flange_to_opt, 'o');
%%title({'shah - optical/robot loop'; 'Rotation error with increasing noise'});
%%title('Rotational error with increasing rotation of Z');
%title('Rotational error of X');
%%legend('Optical marker to flange'); %, 'Flange to optical marker'
%ylim([0 4]);
%%xlabel('Amount of deflection');
%xlabel('|Robot volume| in mm');%char(177) 
%%xlabel('Increasing translation t_x of Z');
%%xlabel('Euler angles of Z');
%ylabel('Rotational error in degrees');
%
%subplot (2, 2, 2);
%plot(1:iter, trans_opt_to_flange_mean, 'x');
%%hold on;
%%plot(1:l*m, trans_flange_to_opt, 'o');
%%title({'shah - optical/robot loop'; 'Translational error with increasing noise'});
%title('Translational error of X');
%%legend('Optical marker to flange'); %'Flange to optical marker'
%ylim([0 0.2]);
%%xlabel('Standard deviation in mm');
%%xlabel('Euler angles of Z');
%xlabel('|Robot volume| in mm');
%%xlabel('Increasing translation t_x of Z');
%ylabel('Translational error level');
%
%subplot (2, 2, 3);
%plot(1:iter, ang_cam_to_base_mean, 'x');
%%hold on;
%%plot(1:l*m, ang_base_to_cam, 'o');
%%title({'shah - optical/robot loop'; 'Rotation error with increasing noise'});
%title('Rotational error of Z');
%%legend('Optical tracker to base'); %, 'Base to optical tracker'
%ylim([0 4]);
%%xlabel('Standard deviation in degrees');
%%xlabel('Euler angles of Z');
%xlabel('|Robot volume| in mm');
%%xlabel('Increasing translation t_x of Z');
%ylabel('Rotational error in degrees');
%
%subplot (2, 2, 4);
%plot(1:iter, trans_cam_to_base_mean, 'x');
%%hold on;
%%plot(1:l*m, trans_base_to_cam, 'o');
%hold on;
%%title({'shah - optical/robot loop'; 'Translational error with increasing noise'});
%title('Translational error of Z');
%%legend('Optical tracker to base'); %, 'Base to optical tracker'
%ylim([0 0.2]);
%%xlabel('Standard deviation in mm');
%%xlabel('Euler angles of Z');
%xlabel('|Robot volume| in mm');
%%xlabel('Increasing translation t_x of Z');
%ylabel('Translational error level');

figure(1);
subplot (2, 2, 1);
plot(err_rob_t, ang_opt_to_flange_mean, 'x');%err_rob_r
%hold on;
%plot(err_rob_r, ang_flange_to_opt, 'o');
%title({'Shah - optical/robot loop'; 'Rotation error with increasing noise'});
title('Rotational error of X');
%legend('Optical marker to flange'); %'Flange to optical marker'
ylim([0 4]);
%xlabel('Standard deviation in degrees');
xlabel('Standard deviation in mm');
ylabel('Rotational error in degrees');

subplot (2, 2, 2);
plot(err_rob_t, trans_opt_to_flange_mean, 'x');
%hold on;
%plot(err_rob_t, trans_flange_to_opt, 'o');
%title({'shah - optical/robot loop'; 'Translational error with increasing noise'});
title('Translational error of X');
%legend('Optical marker to flange');, %'Flange to optical marker'
ylim([0 0.1]);
xlabel('Standard deviation in mm');
ylabel('Translational error in mm');

subplot (2, 2, 3);
plot(err_rob_t, ang_cam_to_base_mean, 'x');%err_rob_r
%hold on;
%plot(err_rob_r, ang_base_to_cam, 'o');
%title({'shah - optical/robot loop'; 'Rotation error with increasing noise'});
title('Rotational error of Z');
%legend('Optical tracker to base');%'Base to optical tracker'
ylim([0 4]);
%xlabel('Standard deviation in degrees');
xlabel('Standard deviation in mm');
ylabel('Rotational error in degrees');

subplot (2, 2, 4);
plot(err_rob_t, trans_cam_to_base_mean, 'x');
%hold on;
%plot(err_rob_t, trans_base_to_cam, 'o');
%title({'shah - optical/robot loop'; 'Translational error with increasing noise'});
title('Translational error of Z');
%legend('Optical tracker to base');%, 'Base to optical tracker'
ylim([0 0.1]);
xlabel('Standard deviation in mm');
ylabel('Translational error in mm');

%%%%%
%%%%%%% X as second and Z as first unknown
%figure(2);
%subplot (2, 2, 1);
%plot(err_rob_r, ang_opt_to_flange2, 'x');
%hold on;
%plot(err_rob_r, ang_flange_to_opt2, 'o');
%%title({'shah - optical/robot loop'; 'Rotation error with increasing noise'});
%title({'Rotational error with increasing noise'});
%legend('Optical marker to flange', 'Flange to optical marker');
%ylim([0 2]);
%xlabel('Standard deviation in degrees');
%ylabel('Rotational error in degrees');
%
%subplot (2, 2, 2);
%plot(err_rob_t, trans_opt_to_flange2, 'x');
%hold on;
%plot(err_rob_t, trans_flange_to_opt2, 'o');
%%title({'shah - optical/robot loop'; 'Translational error with increasing noise'});
%title({'Translational error with increasing noise'});
%legend('Optical marker to flange', 'Flange to optical marker');
%ylim([0 7]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%
%subplot (2, 2, 3);
%plot(err_rob_r, ang_cam_to_base2, 'x');
%hold on;
%plot(err_rob_r, ang_base_to_cam2, 'o');
%%title({'shah - optical/robot loop'; 'Rotation error with increasing noise'});
%title({'Rotational error with increasing noise'});
%legend('Optical tracker to base', 'Base to optical tracker');
%ylim([0 2]);
%xlabel('Standard deviation in degrees');
%ylabel('Rotation in degrees');
%
%subplot (2, 2, 4);
%plot(err_rob_t, trans_cam_to_base2, 'x');
%hold on;
%plot(err_rob_t, trans_base_to_cam2, 'o');
%%title({'shah - optical/robot loop'; 'Translational error with increasing noise'});
%title({'Translational error with increasing noise'});
%legend('Optical tracker to base', 'Base to optical tracker');
%ylim([0 7]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%%

%figure(2);
%subplot (2, 2, 1);
%plot(err_rob_t, ang_IBR_to_flange, 'x');
%hold on;
%plot(err_rob_t, ang_flange_to_IBR, 'o');
%title({'shah - XRay/robot loop'; 'Rotation error with increasing noise'});
%legend('IBR marker to flange', 'flange to IBR marker');
%ylim([0 2]);
%xlabel('Standard deviation in degrees');
%ylabel('Rotation in degrees');
%
%subplot (2, 2, 2);
%plot(err_rob_t, trans_IBR_to_flange, 'x');
%hold on;
%plot(err_rob_t, trans_flange_to_IBR, 'o');
%title({'shah - XRay/robot loop'; 'Translational error with increasing noise'});
%legend('IBR marker to flange', 'flange to IBR marker');
%ylim([0 7]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%
%subplot (2, 2, 3);
%plot(err_rob_t, ang_source_to_base, 'x');
%hold on;
%plot(err_rob_t, ang_base_to_source, 'o');
%title({'shah - XRay/robot loop'; 'Rotation error with increasing noise'});
%legend('source to base', 'base to source');
%ylim([0 2]);
%xlabel('Standard deviation in degrees');
%ylabel('Rotation in degrees');
%
%subplot (2, 2, 4);
%plot(err_rob_t, trans_source_to_base, 'x');
%hold on;
%plot(err_rob_t, trans_base_to_source, 'o');
%title({'shah - XRay/robot loop'; 'Translational error with increasing noise'});
%legend('source to base', 'base to source');
%ylim([0 7]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%
%figure(3);
%subplot (2, 2, 1);
%plot(err_rob_t, ang_opt_to_IBR, 'x');
%hold on;
%plot(err_rob_t, ang_IBR_to_opt, 'o');
%title({'shah - optical/XRay loop'; 'Rotation error with increasing noise'});
%legend('optical marker to IBR marker', 'IBR marker to optical marker');
%ylim([0 2]);
%xlabel('Standard deviation in degrees');
%ylabel('Rotation in degrees');
%
%subplot (2, 2, 2);
%plot(err_rob_t, trans_opt_to_IBR, 'x');
%hold on;
%plot(err_rob_t, trans_IBR_to_opt, 'o');
%title({'shah - optical/XRay loop'; 'Translational error with increasing noise'});
%legend('optical marker to IBR marker', 'IBR marker to optical marker');
%ylim([0 7]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%
%subplot (2, 2, 3);
%plot(err_rob_t, ang_cam_to_source, 'x');
%hold on;
%plot(err_rob_t, ang_source_to_cam, 'o');
%title({'shah - optical/XRay loop'; 'Rotation error with increasing noise'});
%legend('optical marker to source', 'source to optical marker');
%ylim([0 2]);
%xlabel('Standard deviation in degrees');
%ylabel('Rotation in degrees');
%
%subplot (2, 2, 4);
%plot(err_rob_t, trans_cam_to_source, 'x');
%hold on;
%plot(err_rob_t, trans_source_to_cam, 'o');
%title({'shah - optical/XRay loop'; 'Translational error with increasing noise'});
%legend('optical marker to source', 'source to optical marker');
%ylim([0 7]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%
%%%% plot of the error of closed loop
%figure(4);
%plot(err_rob_t, err_trans_opt, ['x', 'r']);
%ylim([0,3]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%legend('optical/robot loop');
%title('Shah - forward - translational error');
%
%figure(4);
%plot(err_rob_t, err_trans_opt, ['x', 'r']);
%hold on;
%plot(err_rob_t, err_trans_rob, ['*', 'b']);
%hold on;
%plot(err_rob_t, err_trans_comb, ['s', 'g']);
%ylim([0,3]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
%title('Shah - forward - translational error');
%
%figure(5);
%plot(err_rob_t, err_trans_opt_b, ['x', 'r']);
%hold on;
%plot(err_rob_t, err_trans_rob_b, ['*', 'b']);
%hold on;
%plot(err_rob_t, err_trans_comb_b, ['s', 'g']);
%ylim([0,3]);
%xlabel('Standard deviation in mm');
%ylabel('Translational error in mm');
%legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
%title('Shah - backward - translational error');
%
%figure(6);
%plot(err_rob_t, err_rot_opt, ['x', 'r']);
%hold on;
%plot(err_rob_t, err_rot_rob, ['*', 'b']);
%hold on;
%plot(err_rob_t, err_rot_comb, ['s', 'g']);
%ylim([0,2]);
%xlabel('Standard deviation in degrees');
%ylabel('Rotational error in degrees');
%legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
%title('Shah - forward - rotational error');
%
%figure(7);
%plot(1:length(err_rob_t), err_rot_opt_b, ['x', 'r']);
%hold on;
%plot(1:length(err_rob_t), err_rot_rob_b, ['*', 'b']);
%hold on;
%plot(1:length(err_rob_t), err_rot_comb_b, ['s', 'g']);
%ylim([0,2]);
%xlabel('Standard deviation in degrees');
%ylabel('Rotational error in degrees');
%legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
%title('Shah - backward - rotational error');

figure(8);
subplot(2,1,1);
plot(1:iter, err_rot_opt_mean, ['x', 'r']);
title('Rotational error of closed loop');
ylim([0 4]);
%xlabel('Amount of deflection');
xlabel('|Robot volume| in mm');%char(177) 
%xlabel('Increasing translation t_x of Z');
%xlabel('Euler angles of Z');
ylabel('Rotational error in degrees');

subplot (2, 1, 2);
plot(1:iter, err_trans_opt_mean, ['s', 'b']);
title('Translational error of closed loop');
ylim([0 15]);
%xlabel('Standard deviation in mm');
%xlabel('Euler angles of Z');
xlabel('|Robot volume| in mm');
%xlabel('Increasing translation t_x of Z');
ylabel('Translational error in mm');