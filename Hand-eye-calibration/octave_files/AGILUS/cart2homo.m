
%% Cartesian Points to Homogeneous Points 

function [ H ] = cart2homo( C )

[ totalCartPoints, n ] = size( C );

H = zeros( 4, 4 );

	for i = 1:totalCartPoints
		tranXYZ = [ C(i,1); C(i,2); C(i,3) ];
		rotmZYX = eul2rot( [ C(i,4), C(i,5), C(i,6) ], 'Rob' ); 
		H = [rotmZYX, tranXYZ;
			 0, 0, 0, 1];
	end

end

