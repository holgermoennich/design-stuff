%% converts manual values of robot cartesian coordinates into one matrix
clc;
clear all;

%% Robot to Flange
cart_pos_rob = ...
[ 590.4,  382.46, 634.06, -42.95,  73.32, -108.99;
  622.52, 450.76, 642.89, -20.68,  56.4,  -84.77;
  640.16, 429.93, 642.85, -36.29,  56.37, -84.8;
  640.15, 508.5,  642.62, -52.3,   65.49, -102.98;
  640.02, 468.71, 660.32, -21.61,  27.62, -63.81;
  640.19, 468.71, 711.59, -32.68,  11.36, -96.42;
  640.02, 486.53, 645.57, -31.96,  -2.03, -87.96;
  595.85, 477.19, 647.29, -48.25,  5.5,   -93.35;
  595.77, 477.27, 647.34, -62.15,  20.91, -86.12;
  595.81, 477.37, 696.91, -75.43,  30.34, -113.42;
  595.89, 482.24, 729.27, -79.0,   29.01, -124.0;
  595.93, 509.38, 729.24, -85.92,  30.6,  -137.88;
  595.91, 509.28, 729.25, -118.01, 30.32, -156.17;
  595.86, 509.29, 729.18, -119.36, 39.74, -139.33;
  595.92, 569.67, 728.98, -105.59, 42.04, -129.61;
  648.72, 417.81, 692.94, -34.28,  62.83, -113.07;
  648.74, 419.32, 692.84, -23.96,  43.21, -92.81;
  648.73, 455.18, 692.71, -27.21,  29.24, -100.08;
  648.73, 458.87, 704.0,  -34.34,  44.88, -111.7;
  664.31, 458.93, 729.7,  -28.21,  44.9,  -111.69;
  664.19, 458.84, 671.47, -15.49,  44.94, -111.66;
  664.25, 458.86, 671.5,  15.85,   45.97, -63.81;
  664.18, 486.55, 623.52, 35.94,   45.98, -63.73;
  664.09, 456.2,  623.58, -16.72,  43.82, -111.8];

[n,c] = size(cart_pos_rob);
Robot_to_Flange = zeros(4,4*n); 
 
for i = 1:n  
  Robot_to_Flange(:, 4*i-3:4*i) = CtoH(cart_pos_rob(i,:));
end

% Save trafo matrices of flange position
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange_all_pos.txt', Robot_to_Flange);

num_HEC = 15; %% use first num_HEC entries for HEC
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange.txt', Robot_to_Flange(:,1:4*num_HEC));

%% use remaining entries for the evaluation
if n > num_HEC
  dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange_for_eval.txt', Robot_to_Flange(:,(4*num_HEC+1):4*n));
end

%% optical tracker to IR-marker
cart_pos_marker = ...
[ 22.61,  15.34,  -1253.02,  7.75,  6.52,  12.7;
  20.7,   -47.9,  -1226.02,  12.32, 23.86, 6.42;
  32.68,  -54.2,  -1264.6,   28.6,  21.6,  17.3;
  28.04,  -134.39, -1252.93, 23.73, 10.97, 20.81;
  29.12,  -73.13, -1209.54,  40.09, 49.91, 11.52;
  35.53,  -87.54, -1200.23,  32.58, 66.34, 35.93;
  116.89, -101.58, -1171.9,  69.2,  75.5,  64.7;
  100.52, -101.9, -1159.3,   64.9,  58.3,  68.3;
  74.26,  -120.35, -1197.6,  66.1,  36.9,  59.9;
  31.74,  -127.93, -1223.65, 41.6,  25.5,  57.9;
  7.96,   -134.33, -1227.8,  33.5,  24.5,  60.2;
  8.42,   -160.53, -1219.84, 22.7,  19.7,  60.8;
  27.19,  -166.13, -1268.8,  17.9,  -5.4,  70.2;
  10.52,  -159.6, -1292.95,  36.8,  -10.8, 62.9;
  -6.89,  -216.8, -1253.98,  39.6,  -1.5,  58.1;
  -14.57, -34.37, -1275.6,   -4.5,  18.4,  10.3;
  0.42,   -31.36, -1248.8,   8.5,   37.7,  8.3;
  23.98,  -66.5,  -1214.4,   6.5,   51.7,  13.0;
  -5.46,  -76.4,  -1234.8,   -0.7,  36.2,  14.9;
  -27.86, -72.77, -1250.3,   -7.9,  37.3,  8.8;
  24.6,   -46.77, -1225.65,  -23.5, 37.1,  -4.2;
  -1.26,  -17.35, -1252.2,   -5.5,  26.4,  -22.5;
  43.5,   -9.45,  -1234.76,  -23.1, 17.1,  -35.0;
  72.7,   -44.59, -1211.45,  -22.3, 38.7,  -3.3];
  

[n2,c] = size(cart_pos_marker);

if n != n2
   error('number of measurements are not equal');
end

Camera_to_Marker = zeros(4,4*n2); 
 
for i = 1:n  
  Camera_to_Marker(:, 4*i-3:4*i) = CtoH_NDI(cart_pos_marker(i,:));
end

% Save trafo matrices of flange position
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker_all_pos.txt', Camera_to_Marker);

num_HEC = 15; %% use first num_HEC entries for HEC
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker.txt', Camera_to_Marker(:,1:4*num_HEC));

%% use remaining entries for the evaluation
if n > num_HEC
  dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker_for_eval.txt', Camera_to_Marker(:,(4*num_HEC+1):4*n));
end