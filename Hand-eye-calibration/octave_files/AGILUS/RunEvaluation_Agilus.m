%% Calculation of the translational and rotational error

clc;
clear all;
Marker_Positions = dlmread('D:\Agilus\AGILUS\results\Evaluation\Marker_Positions.txt');
Flange_Positions = dlmread('D:\Agilus\AGILUS\results\Evaluation\Flange_Positions.txt');
Expected_Base_to_Marker = dlmread('D:\Agilus\AGILUS\results\Evaluation\Base_to_Marker.txt');

Flange_home = dlmread('D:\Agilus\AGILUS\results\Evaluation\Flange_home.txt'); % Set as ground truth
Marker_home = dlmread('D:\Agilus\AGILUS\results\Evaluation\Marker_home.txt'); % Set as ground truth

%% 'X' from 'EvaluateMethodComparison_Agilus.m'
X = dlmread('D:\Agilus\AGILUS\results\Daniilidis\X.txt');
Z = dlmread('D:\Agilus\AGILUS\results\Daniilidis\Z.txt');


%Marker_Positions = dlmread('D:\Agilus\AGILUS\results\Evaluation\14-08-2017\Tabb\slerp_20deg\Marker_Positions.txt');
%Flange_Positions = dlmread('D:\Agilus\AGILUS\results\Evaluation\14-08-2017\Tabb\slerp_20deg\Flange_Positions.txt');
%%Expected_Base_to_Marker = dlmread('D:\Agilus\AGILUS\results\Evaluation\Base_to_Marker.txt');
%
%Flange_home = dlmread('D:\Agilus\AGILUS\results\Evaluation\14-08-2017\Tabb\slerp_20deg\Flange_home.txt'); % Set as ground truth
%Marker_home = dlmread('D:\Agilus\AGILUS\results\Evaluation\14-08-2017\Tabb\slerp_20deg\Marker_home.txt'); % Set as ground truth
%
%%% 'X' from 'EvaluateMethodComparison_Agilus.m'
%X = dlmread('D:\Agilus\AGILUS\results\Tabb\14-08-2017\X.txt');
%Z = dlmread('D:\Agilus\AGILUS\results\Tabb\14-08-2017\Z.txt');

[m,n] = size(Marker_Positions); % m = 4, n = 200
num_pos = n/4;

%%%% errors based on the evaluation (pivot movement)
%% Translational rms error of the marker deviation
rms_trans = transDevOfTip(Marker_home, Marker_Positions);
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Trans_rms_error.txt', rms_trans);
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Trans_rms_error.txt', norm(rms_trans), " ", "-append");

%% rms error calculated from expected orientation and measured orientation in camera frame
[rms_t, rms_angle] = rmsTransRot(Marker_Positions, Flange_Positions, X, Z);
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Rot_rms_error_includ_Z.txt', rms_angle);

%% Rotational rms error
%rot_m0 = RtoE(Marker_home(1:3,1:3), 'Rob'); % alpha, beta, gamma in degrees

%fl_ang_home = acos((trace(Flange_home(1:3,1:3))-1)/2)*180/pi;
%m_ang_home = acos((trace(Marker_home(1:3,1:3))-1)/2)*180/pi;

%% plotting
t = zeros(3,num_pos);

for j = 1:num_pos  
  s(:,j) = Flange_Positions(1:3,4*j);
  t(:,j) = Marker_Positions(1:3,4*j);
end

%% plot of translations
%% plot of marker translation -> should be at one position
figure(1);
m0 = Marker_home(1:3,4);
scatter3 (t(1,:), t(2,:), t(3,:), "b", "filled");
hold on;
scatter3 (m0(1), m0(2), m0(3), "r", "filled"); % plot marker origin
xlabel ("x");
ylabel ("y");
zlabel ("z");
title ("Marker translation");

%% plot of flange translation -> should be on a sphere around the marker
figure(2);
f0 = Flange_home(1:3,4);
scatter3 (s(1,:), s(2,:), s(3,:), "b", "filled");
hold on;
scatter3 (f0(1), f0(2), f0(3), "r", "filled"); % plot marker origin
xlabel ("x"); 
ylabel ("y");
zlabel ("z");
title ("Flange translation");

%% fit flange positions on a sphere 
[center, radius] = sphereFit(s');
exp_c = Flange_home*inv(X);
disp('expected center: '); disp(exp_c(1:3,4));
disp('calculated center: '); disp(center);
disp('expected radius: '); disp(norm(X(1:3,4)));
disp('calculated radius: '); disp(radius);


% pivotise
[ cCen, cRad] = pivotise2(s');
disp('cCen: '); disp(cCen);
disp('cRad: '); disp(cRad);


for j = 1:num_pos
  %% Trafo matrices
  Traf_Marker = Marker_Positions(1:4, 4*j-3:4*j);
  Traf_Flange = Flange_Positions(1:4, 4*j-3:4*j);
 
  %% Rotation matrices
  Rot_Marker = Traf_Marker(1:3, 1:3);
  Rot_Flange = Traf_Flange(1:3, 1:3);
  
  %% angles
  %rot_mj(:,j) = RtoE(Rot_Marker, 'Rob'); %Euler angle
  %rot_mj(j) = acos((trace(Rot_Marker)-1)/2)*180/pi; % rotation angle of each marker position
  [w,a] = to_axis(Rot_Marker);
  rot_mj(j) = a;
  axes_mj(1:3,j) = w;
  
  [z,b] = to_axis(Rot_Flange);
  rot_flj = b;
  axes_fl(1:3,j) = z;
  
  figure(3);
  title("Coordinate frames of Marker w.r.t. Camera");
  hold on;
  scale = 10;
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,1))', "r");
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,2))', "g");
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,3))', "b");
  xlabel ("x"); 
  ylabel ("y");
  zlabel ("z");
  view(3);
  
  figure(4);
  title("Coordinate frames of Flange w.r.t. Base");
  hold on;
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,1))', "r");
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,2))', "g");
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,3))', "b");
  xlabel ("x"); 
  ylabel ("y");
  zlabel ("z");
  view(3);
end

figure(5);
arrow3(zeros(num_pos,3), axes_fl');
title("Rotation axes Base to Flange");

figure(6);
arrow3(zeros(num_pos,3), axes_mj');
title("Rotation axes Camera to Marker");

%figure(7);
%n = 2*pi/num_pos:2*pi/num_pos:2*pi;
%plot(n, rot_flj);
%xlabel("Evaluation points");
%ylabel("Rotation angle");
%title("Rotation angles Base to Flange");
%
%figure(8);
%n = 2*pi/num_pos:2*pi/num_pos:2*pi;
%plot(n, rot_mj);
%xlabel("Evaluation points");
%ylabel("Rotation angle");
%title("Rotation angles Base to Flange");

%%% errors based on independent measurements

Camera_to_Marker_fE = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Camera_to_Marker_for_eval.txt');
Robot_to_Flange_fE = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Robot_to_Flange_for_eval.txt');

%% mean rotational error in degrees
err_R2 = e_R2(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\e_R2.txt', err_R2);

%% mean translational error
err_t = e_t(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\e_t.txt', err_t);

%% mean combined rotation and translation error without units
err_c = e_c(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
dlmwrite('D:\Agilus\AGILUS\results\Evaluation\e_c.txt', err_c);