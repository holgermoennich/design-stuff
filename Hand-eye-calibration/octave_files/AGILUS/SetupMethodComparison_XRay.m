clc
clear all

numPositions = 26;

homeJoint = jointPosition(-17, -56.6, 74, -100, -14, 153); %where the camera can see the marker, positions from each axes

%% OCATVE_SLICER OpenIGTLink PATH
addpath D:\Agilus\AGILUS;

port = 59152; % Defined by XML inside on the KUKA smartpad
addr = '172.31.1.140'; % Defined by XML inside the KUKA smartpad

%% Confirming Robot Connection
robot = connectAgilus(port,addr);

%% First motion is PTP motion to Home Position
robot.move(homeJoint);

markerPosition = [];
flangePosition = [];

%% possible volume of 300x200x500

%relPositions = ...
%   [  0.0000      0.0000      0.0000      0.0000      0.0000      0.0000;
%    32.12000    68.30000     8.83000   -63.63000   -16.92000    24.22000;
%    17.64000   -20.83000    -0.04000   -15.61000    -0.03000    -0.03000;
%    -0.01000    78.57000    -0.23000   -16.01000     9.12000   -18.18000;
%    -0.13000   -39.79000    17.70000    30.69000   -37.87000    39.17000;
%     0.17000     0.00000    51.27000   -11.07000   -16.26000   -32.61000;
%    -0.17000    17.82000   -66.02000     0.72000   -13.39000     8.46000;
%   -44.17000    -9.34000     1.72000   -16.29000     7.53000    -5.39000;
%    -0.08000     0.08000     0.05000   -13.90000    15.41000     7.23000;
%     0.04000     0.10000    49.57000   -13.28000     9.43000   -27.30000;
%     0.08000     4.87000    32.36000    -3.57000    -1.33000   -10.58000;
%     0.04000    27.14000    -0.03000    -6.92000     1.59000   -13.88000;
%    -0.02000    -0.10000     0.01000   -32.09000    -0.28000   -18.29000;
%    -0.05000     0.01000    -0.07000    -1.35000     9.42000    16.84000;
%     0.06000    60.38000    -0.20000    13.77000     2.30000     9.72000;
%    52.80000  -151.86000   -36.04000    71.31000    20.79000    16.54000;
%     0.02000     1.51000    -0.10000    10.32000   -19.62000    20.26000;
%    -0.01000    35.86000    -0.13000    -3.25000   -13.97000    -7.27000;
%     0.00000     3.69000    11.29000    -7.13000    15.64000   -11.62000;
%    15.58000     0.06000    25.70000     6.13000     0.02000     0.01000;
%    -0.12000    -0.09000   -58.23000    12.72000     0.04000     0.03000;
%     0.06000     0.02000     0.03000    31.34000     1.03000    47.85000;
%    -0.07000    27.69000   -47.98000    20.09000     0.01000     0.08000;
%    -0.09000   -30.35000     0.06000   -52.66000    -2.16000   -48.07000;
%    -10.0000    -5.00000     20.0000     8.00000    -20.0000    43.00000];

relPositions = ...
   [  0.0000      0.0000      0.0000      0.0000      0.0000      0.0000;
    -3.12000     6.30000     8.83000   -3.63000   -16.92000    24.22000;
    -17.64000   -20.83000    -0.04000   -15.61000    -0.03000    -0.03000;
    -0.01000    8.57000    -0.23000   -16.01000     9.12000    -18.18000;
    -0.13000   -39.79000    17.70000    30.69000   -37.87000    19.17000;
     0.17000     0.00000    51.27000   -11.07000   -16.26000   -32.61000;
    -0.17000    17.82000   -66.02000     0.72000   -13.39000     8.46000;
   -44.17000    -9.34000     1.72000   -16.29000     7.53000    -5.39000;
    -0.08000     20.08000    0.05000   23.90000    15.41000     7.23000;
     0.04000     0.10000    9.57000    13.28000     9.43000     7.30000;
     0.08000    -12.87000   32.36000    -3.57000    -1.33000   -10.58000;
    40.04000    -5.14000    -50.03000   40.92000     1.59000    23.88000;
    -0.02000    -0.10000     0.01000   -32.09000    -0.28000    18.29000;
    -0.05000     0.01000    -5.07000    -1.35000     9.42000    16.84000;
     0.06000    60.38000     14.20000    13.77000     2.30000   -9.72000;
    52.80000   -51.86000     6.04000     1.31000    20.79000   -16.54000;
     0.02000     1.51000    -0.10000    10.32000   -19.62000    20.26000;
    -0.01000    35.86000    -0.13000    -3.25000   -13.97000    -7.27000;
     0.00000     3.69000    11.29000    -7.13000    15.64000   -11.62000;
    15.58000     5.06000    -5.70000    -6.13000     5.02000     0.01000;
    -0.12000     5.09000    8.23000    -12.72000     20.04000     -10.03000;
     0.06000     0.02000     0.03000    31.34000     1.03000    47.85000;
    -0.07000    27.69000   -7.98000    20.09000     0.01000     0.08000;
    -0.09000   -30.35000     0.06000   -52.66000    -2.16000   -48.07000;
    -10.0000    -5.00000     20.0000     8.00000    -20.0000    43.00000];
    
%% Slow down the robot speed
robot.setSpeed(10);   
k = 1;
     
%% Collect data to perform AX = ZB
% Different Flange and Marker positions relative to Robot and Camera respectively
for i=1:numPositions
  disp(i);
  robot.moveRel( frame(relPositions(i,1), 
                       relPositions(i,2), 
                       relPositions(i,3), 
                       relPositions(i,4), 
                       relPositions(i,5), 
                       relPositions(i,6)) );                
  
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
  
  % Pause to allow the Robot to settle down
  pause(5);
  
  % Make 2D acquisition
  
  markerPosition = getCameraData2();

if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  %% save position only when the marker can be seen by the camera 
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
   end

end

disp('Data Collection Ended...')

%% Check if measurements are complete
[ l, m, n ] = size( mpHomoData );
numCamPoints = n;

[ p, q, r ] = size( fpCartData );
numRobotPoints = r

if numCamPoints ~= numRobotPoints
    
    error('Number of position data from the Camera and Robot is NOT equal')

else
  
  for i=1:numCamPoints
    
    fpHomoData(:,:,i) = CtoH(fpCartData(:,:,i));
  
  end

end 

%% Reshape for 'dornaika' and 'shah'
Camera_to_Marker = reshape(mpHomoData, [4, 4*numCamPoints]);
Robot_to_Flange = reshape(fpHomoData, [4, 4*numCamPoints]);

dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker_all_pos.txt', Camera_to_Marker);
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange_all_pos.txt', Robot_to_Flange);

% Save absolute trafo matrices 
n = 15;
%% use first n entries for HEC
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker.txt', Camera_to_Marker(:,1:4*n));
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange.txt', Robot_to_Flange(:,1:4*n));

%% use remaining entries for the evaluation
if numCamPoints > n
  dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker_for_eval.txt', Camera_to_Marker(:,(4*n+1):4*numCamPoints));
  dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange_for_eval.txt', Robot_to_Flange(:,(4*n+1):4*numCamPoints));
end

robot.closeConnection;