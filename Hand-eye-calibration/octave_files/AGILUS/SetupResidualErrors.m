%% residual error 
%% B*inv(X)*inv(A) = inv(Z)

%% first run SetupMethodComparison_Agilus.m with different relative positions

clc;
clear all;

A_i = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Camera_to_Marker.txt');
B_i = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Robot_to_Flange.txt');

X = dlmread('D:\Agilus\AGILUS\results\Shah\X2_Vicra.txt');
Z = dlmread('D:\Agilus\AGILUS\results\Shah\Z2_Vicra.txt');

% rotation angle of Z
rot_angle_Z = acos((trace(Z(1:3,1:3))-1)/2)*180/pi;

%% Number of measurements 
[m, n] = size(A_i);
num_meas = n/4;

for i = 1:num_meas
  A = A_i(:,4*i-3:4*i);
  B = B_i(:,4*i-3:4*i);   
  residuum = inv(Z)-B*inv(X)*inv(A);
  resid_trans(:,i) = residuum(1:3,4);
  R = residuum(1:3,1:3);
  % rotation angle
  diff_angle(i) = rot_angle_Z-acos((trace(R)-1)/2)*180/pi;
end
  mean_trans_error_x = mean(resid_trans(1,:));
  mean_trans_error_y = mean(resid_trans(2,:));
  mean_trans_error_z = mean(resid_trans(3,:));
  mean_rot_error = mean(diff_angle(:)); % in degree
  
  disp('mean translational error: '); disp(mean_trans_error_x); disp(mean_trans_error_y); disp(mean_trans_error_z);
  disp('mean rotational error: '); disp(mean_rot_error);