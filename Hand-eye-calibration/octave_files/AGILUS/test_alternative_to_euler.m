%% test equality
HMarker_home = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\HMarker_home.txt');
HFlange_home = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\HFlange_home.txt');

X = dlmread('D:\Agilus\AGILUS\results\Shah\X.txt');

 %Euler approach
Base_to_Marker_home = HFlange_home*inv(X);
home_angle = RtoE(Base_to_Marker_home(1:3,1:3), 'Rob'); %alpha, beta, gamma in degrees

r = 10;
phi = 10*2*pi/50;
alpha = home_angle(1) + r*cos(phi);
beta = home_angle(2) + r*sin(phi);
gamma = home_angle(3);

rot = EtoR([alpha; beta; gamma], 'Rob');
  % Homogeneous coordinates of the new marker position
newPos_of_tip = [rot, Base_to_Marker_home(1:3,4); 0, 0, 0, 1];
newPos_of_Flange = newPos_of_tip*X;
  % Rotation
rot_flange = RtoE(newPos_of_Flange(1:3,1:3), 'Rob') %alpha, beta, gamma of flange
  % Translation
t = newPos_of_Flange(1:3,4)

%Rotation angle approach
rot_single_angle = EtoR([r*cos(phi), r*sin(phi), 0], 'Rob');
mat_single_angle = [rot_single_angle, [0,0,0]';0,0,0,1]; 
newPos_of_tip2 = Base_to_Marker_home*mat_single_angle;
newPos_of_Flange2 = newPos_of_tip2*X;
% Rotation
rot_flange2 = RtoE(newPos_of_Flange2(1:3,1:3), 'Rob') %alpha, beta, gamma of flange
  % Translation
t2 = newPos_of_Flange2(1:3,4)