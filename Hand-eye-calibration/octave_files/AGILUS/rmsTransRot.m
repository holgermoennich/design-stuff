function [ rms_trans, rms_angle ] = rmsTransRot(As, Bs, X, Z)
  %% calculates the RMS error of the translational and rotational error
  %% the marker pose is always set as "ground truth"
  
  [r,c] = size(As); 
  num_meas = c/4;
  
  sum_angle = 0;
  sum_trans = 0;

  for i = 1:num_meas
    Ai = As(:, 4*i-3:4*i); % measured Camera to Marker Transformation
    C_H_M = Z*Bs(:,4*i-3:4*i)*inv(X); % expected Camera to Marker Transformation
    
    % rotation
    expec_angle = acos((trace(C_H_M(1:3,1:3))-1)/2)*180/pi;
    
    R_Ai = Ai(1:3,1:3);
    meas_angle = acos((trace(R_Ai)-1)/2)*180/pi;
    
    sqdiff_angle = (expec_angle - meas_angle)^2;
    sum_angle += sqdiff_angle;
    
    % translation
    expec_trans = C_H_M(1:3,4);
    meas_trans = Ai(1:3,4);
    
    sqdiff = (expec_trans - meas_trans).^2;
    sum_trans += sqdiff;
  end

  rms_angle = sqrt(sum_angle/num_meas);
  rms_trans = sqrt(sum_trans/num_meas);
end