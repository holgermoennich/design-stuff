function [ err_t ] = RMSE_trans(As, Bs, X, Z)
  %% calculates the RMS error of the translational error

  [r,c] = size(As); 
  num_meas = c/4;
  
  sum_trans = 0;
  
  for i = 1:num_meas
    A_i = As(:,4*i-3:4*i); % measured Camera to Marker Transformation
    B_i = Bs(:,4*i-3:4*i);
    E_i = inv(Z*B_i)*A_i*X; % error matrix
    t_i = E_i(1:3,4);
%    sum_trans += t_i.^2;
    sum_trans += t_i'*t_i;
    
%% alternative
%    R_A = A_i(1:3,1:3);
%    t_X = X(1:3,4);
%    t_A = A_i(1:3,4);
%    R_Z = Z(1:3,1:3);
%    t_B = B_i(1:3,4);
%    t_Z = Z(1:3,4);
%    
%    sum_trans += (R_A*t_X+t_A-R_Z*t_B-t_Z).^2;
    
  end
  
  err_t = sqrt(sum_trans/num_meas);
end