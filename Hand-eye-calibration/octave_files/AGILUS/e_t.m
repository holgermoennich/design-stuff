function [ err_t ] = e_t(As, Bs, X, Z)
  %% calculates mean translational error in mm
  
  t_Z = Z(1:3,4);
  t_X = X(1:3,4);
  R_Z = Z(1:3,1:3);
  sum_trans = 0;

  [r,c] = size(As); 
  num_meas = c/4;
  
  for i = 1:num_meas
    T_Bi = Bs(1:4, 4*i-3:4*i);
    R_Bi = T_Bi(1:3,1:3);
    t_Bi = T_Bi(1:3,4);
    
    T_Ai = As(1:4, 4*i-3:4*i);
    R_Ai = T_Ai(1:3,1:3);
    t_Ai = T_Ai(1:3,4);
    
    trans = (R_Ai*t_X + t_Ai) - (R_Z*t_Bi + t_Z);
    n_trans = norm(trans);
    sum_trans += n_trans;
  end

  err_t = sum_trans/num_meas;
end