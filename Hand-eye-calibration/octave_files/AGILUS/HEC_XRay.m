%% HEC with X-Ray 

clc;
clear all;

%%%%%%%
%% Read transformation matrices
%%%%%%%

XRay_to_IBRMarker_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\XRay_to_IBRMarker_all_pos.txt');
Camera_to_Marker_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker_all_pos.txt');
Robot_to_Flange_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange_all_pos.txt');
%XRay_to_IBRMarker_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\13-09-2017\XRay_to_IBRMarker_all_pos.txt');
%Camera_to_Marker_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\13-09-2017\Camera_to_Marker_all_pos.txt');
%Robot_to_Flange_all_sorted_0 = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\13-09-2017\Robot_to_Flange_all_pos.txt');
[r0,c0] = size(XRay_to_IBRMarker_all_sorted_0);
n0 = c0/4;

%%%%%%%
%% Prepare transformation matrices
%%%%%%%

p = 1;

%% remove measurement where XRay to IBR marker is identity matrix
for i = 1:n0
  if XRay_to_IBRMarker_all_sorted_0(:, i*4-3:i*4) == eye(4)
    disp('Measurement number '); disp(i); disp(' removed');
  else
    XRay_to_IBRMarker_all_sorted(:, p*4-3:p*4) = XRay_to_IBRMarker_all_sorted_0(:, i*4-3:i*4);
    Camera_to_Marker_all_sorted(:, p*4-3:p*4) = Camera_to_Marker_all_sorted_0(:, i*4-3:i*4);
    Robot_to_Flange_all_sorted(:, p*4-3:p*4) = Robot_to_Flange_all_sorted_0(:, i*4-3:i*4);
    p +=1;
  end
end

[r,c] = size(XRay_to_IBRMarker_all_sorted);
n = c/4;

%% shuffle data 
idx = randperm(n); % random order of indices

for i = 1:n
  XRay_to_IBRMarker_all(:, i*4-3:i*4) = XRay_to_IBRMarker_all_sorted(:,idx(i)*4-3:idx(i)*4);
  Camera_to_Marker_all(:, i*4-3:i*4) = Camera_to_Marker_all_sorted(:,idx(i)*4-3:idx(i)*4);
  Robot_to_Flange_all(:, i*4-3:i*4) = Robot_to_Flange_all_sorted(:,idx(i)*4-3:idx(i)*4);
end

%% for cross validation
f = 6; % k-fold cross-validation
size_subset = n/f; % 6

for m = 1:f 
  
  e = size_subset*m; % end of test data (6,12,18,24)
  b = e-(size_subset-1); % begin of test data (1,7,13,19)
  
  clear As_XRay_eval As_opt_eval Bs_eval As_XRay As_opt Bs;
  
  %% test data -> contains only one subset
  As_XRay_eval = XRay_to_IBRMarker_all(:, b*4-3:e*4);
  As_opt_eval = Camera_to_Marker_all(:, b*4-3:e*4);
  Bs_eval = Robot_to_Flange_all(:, b*4-3:e*4);
  
  
  %% training data -> contains the remaining subsets
  if b == 1
    As_XRay = XRay_to_IBRMarker_all(:,e*4+1:n*4);
    As_opt = Camera_to_Marker_all(:,e*4+1:n*4);
    Bs = Robot_to_Flange_all(:,e*4+1:n*4);
  elseif e == n
    As_XRay(:,1:4*(n-size_subset)) = XRay_to_IBRMarker_all(:,1:(b-1)*4);
    As_opt(:,1:4*(n-size_subset)) = Camera_to_Marker_all(:,1:(b-1)*4);
    Bs(:,1:4*(n-size_subset)) = Robot_to_Flange_all(:,1:(b-1)*4);
  else
    As_XRay(:,1:(b-1)*4) = XRay_to_IBRMarker_all(:,1:(b-1)*4);
    As_XRay(:,b*4-3:4*(n-size_subset)) = XRay_to_IBRMarker_all(:,e*4+1:n*4);
    As_opt(:,1:(b-1)*4) = Camera_to_Marker_all(:,1:(b-1)*4);
    As_opt(:,b*4-3:4*(n-size_subset)) = Camera_to_Marker_all(:,e*4+1:n*4);
    Bs(:,1:(b-1)*4) = Robot_to_Flange_all(:,1:(b-1)*4);
    Bs(:,b*4-3:4*(n-size_subset)) = Robot_to_Flange_all(:,e*4+1:n*4);
  end

  [r1,c1] = size(As_XRay);
  numRobotPoints = c1/4;

  [r2,c2] = size(Bs);

  [r3,c3] = size(As_opt);

  if c1!=c2 && c1!=c3
    error('Size of transformation matrices differ!');
  end
  
  %% perform 6 combiations of hand eye calibration
  %% loop: Camera - Base - Flange - optical marker
  disp('----------------------------');
  disp(m);
  disp('----------------------------');
  disp('Geometrical results');
  
  disp('optical - robot loop');
  disp('forward');
%  [Opt_to_flange_dor, Cam_to_base_dor] = dornaika(As_opt, Bs);
  [Opt_to_flange_shah, Cam_to_base_shah] = dornaika(As_opt, Bs);
%  disp('dornaika: distance flange-optical_marker');disp(norm(Opt_to_flange_dor(1:3,4)));
  disp('shah: distance flange-optical_marker');disp(norm(Opt_to_flange_shah(1:3,4)));

%  disp('dornaika: distance base-opt_tracker');disp(norm(Cam_to_base_dor(1:3,4)));
  disp('shah: distance base-opt_tracker');disp(norm(Cam_to_base_shah(1:3,4)));

  disp('backward');
%  [Flange_to_opt_dor, Base_to_cam_dor] = dornaika(Bs, As_opt);
  [Flange_to_opt_shah, Base_to_cam_shah] = dornaika(Bs, As_opt);
%  disp('dornaika: distance flange-optical_marker');disp(norm(Flange_to_opt_dor(1:3,4)));
%  disp('shah: distance flange-optical_marker');disp(norm(Flange_to_opt_shah(1:3,4)));

%  disp('dornaika: distance base-opt_tracker');disp(norm(Base_to_cam_dor(1:3,4)));
%  disp('shah: distance base-opt_tracker');disp(norm(Base_to_cam_shah(1:3,4)));

  %% loop: Source - Base - Flange - IBR marker
  disp('XRay - robot loop');
  disp('forward');
%  [IBR_to_flange_dor, Source_to_base_dor] = dornaika(As_XRay, Bs);
  [IBR_to_flange_shah, Source_to_base_shah] = dornaika(As_XRay, Bs);
%  disp('dornaika: distance flange-ibr_marker');disp(norm(IBR_to_flange_dor(1:3,4)));
  disp('shah: distance flange-ibr_marker');disp(norm(IBR_to_flange_shah(1:3,4)));

%  disp('dornaika: distance base-XRay');disp(norm(Source_to_base_dor(1:3,4)));
  disp('shah: distance base-XRay');disp(norm(Source_to_base_shah(1:3,4)));
  
    disp('backward');
%  [Flange_to_IBR_dor, Base_to_source_dor] = dornaika(Bs, As_XRay);
  [Flange_to_IBR_shah, Base_to_source_shah] = dornaika(Bs, As_XRay);
%  disp('dornaika: distance flange-ibr_marker');disp(norm(Flange_to_IBR_dor(1:3,4)));
%  disp('shah: distance flange-ibr_marker');disp(norm(Flange_to_IBR_shah(1:3,4)));

%  disp('dornaika: distance base-XRay');disp(norm(Base_to_source_dor(1:3,4)));
%  disp('shah: distance base-XRay');disp(norm(Base_to_source_shah(1:3,4)));

  %% loop: Camera - optical marker - IBR marker - Source
  disp('optical - XRay loop');
  disp('forward');
%  [Opt_to_IBR_dor, Cam_to_source_dor] = dornaika(As_opt, As_XRay);
  [Opt_to_IBR_shah, Cam_to_source_shah] = dornaika(As_opt, As_XRay);
%  disp('dornaika: distance ibr-optical_marker');disp(norm(Opt_to_IBR_dor(1:3,4)));
  disp('shah: distance ibr-optical_marker');disp(norm(Opt_to_IBR_shah(1:3,4)));

%  disp('dornaika: distance optical tracker - source of X-Ray');disp(norm(Cam_to_source_dor(1:3,4)));
  disp('shah: distance optical tracker - source of X-Ray');disp(norm(Cam_to_source_shah(1:3,4)));
  
  disp('backward');
%  [IBR_to_opt_dor, Source_to_cam_dor] = dornaika(As_XRay, As_opt);
  [IBR_to_opt_shah, Source_to_cam_shah] = dornaika(As_XRay, As_opt);
%  disp('dornaika: distance flange-ibr_marker');disp(norm(IBR_to_opt_dor(1:3,4)));
%  disp('shah: distance flange-ibr_marker');disp(norm(IBR_to_opt_shah(1:3,4)));

%  disp('dornaika: distance base-XRay');disp(norm(Source_to_cam_dor(1:3,4)));
%  disp('shah: distance base-XRay');disp(norm(Source_to_cam_shah(1:3,4)));

  %% small loop: optical marker - IBR marker - flange
%  IBR_to_opt_dor_SL = IBR_to_flange_dor*inv(Opt_to_flange_dor);
%  disp('dornaika: distance ibr-optical_marker'); disp(norm(IBR_to_opt_dor_SL(1:3,4)));

  IBR_to_opt_shah_SL = IBR_to_flange_shah*inv(Opt_to_flange_shah);
  disp('shah: distance ibr-optical_marker'); disp(norm(IBR_to_opt_shah_SL(1:3,4)));

  %op_H_ibr_loop = inv(As_opt(1:4,5:8))*Cam_to_base_shah*Source_to_base_shah*As_XRay(:,5:8);
  %disp('shah: distance ibr-optical_marker_big loop');norm(op_H_ibr_loop(1:3,4))

  disp('----------------------------');
  disp('Evaluation');
  %% transformation matrices for evaluation
  %As_XRay_eval = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\XRay_to_IBRMarker_for_eval.txt');
  %Bs_eval = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange_for_eval.txt');
  %As_opt_eval = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker_for_eval.txt');

  %As_XRay_eval = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\XRay_to_IBRMarker.txt');
  %Bs_eval = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Robot_to_Flange.txt');
  %As_opt_eval = dlmread('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\Camera_to_Marker.txt');

  [r1,c1] = size(As_XRay_eval);
  [r2,c2] = size(Bs_eval);
  [r3,c3] = size(As_opt_eval);

  if c1!=c2 && c1!=c3
    error('Size of transformation matrices differ!');
  end

  disp('Optical/robot loop forward');
  disp('mathematical errors - Shah');
  disp('rmse_trans');disp(norm(RMSE_trans(As_opt_eval, Bs_eval, Opt_to_flange_shah, Cam_to_base_shah)));
  disp('rmse_rot');disp(RMSE_rot(As_opt_eval, Bs_eval, Opt_to_flange_shah, Cam_to_base_shah));
  disp('e_c');disp(e_c(As_opt_eval, Bs_eval, Opt_to_flange_shah, Cam_to_base_shah));
  
  trans_err_opt(1,m) = norm(RMSE_trans(As_opt_eval, Bs_eval, Opt_to_flange_shah, Cam_to_base_shah));
  rot_err_opt(1,m) = RMSE_rot(As_opt_eval, Bs_eval, Opt_to_flange_shah, Cam_to_base_shah);
  ec_opt(1,m) = e_c(As_opt_eval, Bs_eval, Opt_to_flange_shah, Cam_to_base_shah);

%  disp('mathematical errors - Dornaika');
%  disp('rmse_trans');disp(norm(RMSE_trans(As_opt_eval, Bs_eval, Opt_to_flange_dor, Cam_to_base_dor)));
%  disp('rmse_rot');disp(RMSE_rot(As_opt_eval, Bs_eval, Opt_to_flange_dor, Cam_to_base_dor));
%  disp('e_c');disp(e_c(As_opt_eval, Bs_eval, Opt_to_flange_dor, Cam_to_base_dor));

  disp('Optical/robot loop backward');
  disp('mathematical errors - Shah');
  disp('rmse_trans');disp(norm(RMSE_trans(Bs_eval, As_opt_eval, Flange_to_opt_shah, Base_to_cam_shah)));
  disp('rmse_rot');disp(RMSE_rot(Bs_eval, As_opt_eval, Flange_to_opt_shah, Base_to_cam_shah));

  trans_err_opt_b(1,m) = norm(RMSE_trans(Bs_eval, As_opt_eval, Flange_to_opt_shah, Base_to_cam_shah));
  rot_err_opt_b(1,m) = RMSE_rot(Bs_eval, As_opt_eval, Flange_to_opt_shah, Base_to_cam_shah);
  ec_opt_b(1,m) = e_c(Bs_eval, As_opt_eval, Flange_to_opt_shah, Base_to_cam_shah);
  
%  disp('e_c');disp(e_c(Bs_eval, As_opt_eval, Flange_to_opt_shah, Base_to_cam_shah));
%  disp('mathematical errors - Dornaika');
%  disp('rmse_trans');disp(norm(RMSE_trans(Bs_eval, As_opt_eval, Flange_to_opt_dor, Base_to_cam_dor)));
%  disp('rmse_rot');disp(RMSE_rot(Bs_eval, As_opt_eval, Flange_to_opt_dor, Base_to_cam_dor));
%  disp('e_c');disp(e_c(Bs_eval, As_opt_eval, Flange_to_opt_dor, Base_to_cam_dor));

  disp('XRay/robot loop forward');
  disp('mathematical errors - Shah');
  disp('rmse_trans');disp(norm(RMSE_trans(As_XRay_eval, Bs_eval, IBR_to_flange_shah, Source_to_base_shah)));
  disp('rmse_rot');disp(RMSE_rot(As_XRay_eval, Bs_eval, IBR_to_flange_shah, Source_to_base_shah));
  disp('e_c');disp(e_c(As_XRay_eval, Bs_eval, IBR_to_flange_shah, Source_to_base_shah));
  
  trans_err_XRay(1,m) = norm(RMSE_trans(As_XRay_eval, Bs_eval, IBR_to_flange_shah, Source_to_base_shah));
  rot_err_XRay(1,m) = RMSE_rot(As_XRay_eval, Bs_eval, IBR_to_flange_shah, Source_to_base_shah);
  ec_XRay(1,m) = e_c(As_XRay_eval, Bs_eval, IBR_to_flange_shah, Source_to_base_shah);

%  disp('mathematical errors - Dornaika');
%  disp('rmse_trans');disp(norm(RMSE_trans(As_XRay_eval, Bs_eval, IBR_to_flange_dor, Source_to_base_dor)));
%  disp('rmse_rot');disp(RMSE_rot(As_XRay_eval, Bs_eval, IBR_to_flange_dor, Source_to_base_dor));
%  disp('e_c');disp(e_c(As_XRay_eval, Bs_eval, IBR_to_flange_dor, Source_to_base_dor));

  disp('XRay/robot loop backward');
  disp('mathematical errors - Shah');
  disp('rmse_trans');disp(norm(RMSE_trans(Bs_eval, As_XRay_eval, Flange_to_IBR_shah, Base_to_source_shah)));
  disp('rmse_rot');disp(RMSE_rot(Bs_eval, As_XRay_eval, Flange_to_IBR_shah, Base_to_source_shah));

  trans_err_XRay_b(1,m) = norm(RMSE_trans(Bs_eval, As_XRay_eval, Flange_to_IBR_shah, Base_to_source_shah));
  rot_err_XRay_b(1,m) = RMSE_rot(Bs_eval, As_XRay_eval, Flange_to_IBR_shah, Base_to_source_shah);
  ec_XRay_b(1,m) = e_c(Bs_eval, As_XRay_eval, Flange_to_IBR_shah, Base_to_source_shah);
  
%  disp('e_c');disp(e_c(Bs_eval, As_XRay_eval, Flange_to_IBR_shah, Base_to_source_shah));
%  disp('mathematical errors - Dornaika');
%  disp('rmse_trans');disp(norm(RMSE_trans(Bs_eval, As_XRay_eval, Flange_to_IBR_dor, Base_to_source_dor)));
%  disp('rmse_rot');disp(RMSE_rot(Bs_eval, As_XRay_eval, Flange_to_IBR_dor, Base_to_source_dor));
%  disp('e_c');disp(e_c(Bs_eval, As_XRay_eval, Flange_to_IBR_dor, Base_to_source_dor));

  disp('Optical/XRay loop forward');
  disp('mathematical errors - Shah');
  disp('rmse_trans');disp(norm(RMSE_trans(As_opt_eval, As_XRay_eval, Opt_to_IBR_shah, Cam_to_source_shah)));
  disp('rmse_rot');disp(RMSE_rot(As_opt_eval, As_XRay_eval, Opt_to_IBR_shah, Cam_to_source_shah));
  disp('e_c');disp(e_c(As_opt_eval, As_XRay_eval, Opt_to_IBR_shah, Cam_to_source_shah));
  
  trans_err_comb(1,m) =  norm(RMSE_trans(As_opt_eval, As_XRay_eval, Opt_to_IBR_shah, Cam_to_source_shah));
  rot_err_comb(1,m) = RMSE_rot(As_opt_eval, As_XRay_eval, Opt_to_IBR_shah, Cam_to_source_shah);
  ec_comb(1,m) = e_c(As_opt_eval, As_XRay_eval, Opt_to_IBR_shah, Cam_to_source_shah);

%  disp('mathematical errors - Dornaika');
%  disp('rmse_trans');disp(norm(RMSE_trans(As_opt_eval, As_XRay_eval, Opt_to_IBR_dor, Cam_to_source_dor)));
%  disp('rmse_rot');disp(RMSE_rot(As_opt_eval, As_XRay_eval, Opt_to_IBR_dor, Cam_to_source_dor));
%  disp('e_c');disp(e_c(As_opt_eval, As_XRay_eval, Opt_to_IBR_dor, Cam_to_source_dor));

  disp('Optical/XRay loop backward');
  disp('mathematical errors - Shah');
  disp('rmse_trans');disp(norm(RMSE_trans(As_XRay_eval, As_opt_eval, IBR_to_opt_shah, Source_to_cam_shah)));
  disp('rmse_rot');disp(RMSE_rot(As_XRay_eval, As_opt_eval, IBR_to_opt_shah, Source_to_cam_shah));

  trans_err_comb_b(1,m) = norm(RMSE_trans(As_XRay_eval, As_opt_eval, IBR_to_opt_shah, Source_to_cam_shah));
  rot_err_comb_b(1,m) = RMSE_rot(As_XRay_eval, As_opt_eval, IBR_to_opt_shah, Source_to_cam_shah);
  ec_comb_b(1,m) = e_c(As_XRay_eval, As_opt_eval, IBR_to_opt_shah, Source_to_cam_shah);
  
%  disp('e_c');disp(e_c(As_XRay_eval, As_opt_eval, IBR_to_opt_shah, Source_to_cam_shah));
%  disp('mathematical errors - Dornaika');
%  disp('rmse_trans');disp(norm(RMSE_trans(As_XRay_eval, As_opt_eval, IBR_to_opt_dor, Source_to_cam_dor)));
%  disp('rmse_rot');disp(RMSE_rot(As_XRay_eval, As_opt_eval, IBR_to_opt_dor, Source_to_cam_dor));
%  disp('e_c');disp(e_c(As_XRay_eval, As_opt_eval, IBR_to_opt_dor, Source_to_cam_dor));
end

figure(8);
plot(1:f, trans_err_opt, ['x', 'r']);
hold on;
plot(1:f, trans_err_XRay, ['*', 'b']);
hold on;
plot(1:f, trans_err_comb, ['s', 'g']);
ylim([0,3]);
xlabel('Number of test run');
ylabel('Translational error in mm');
legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
title('Shah - forward - translational error');

figure(9);
plot(1:f, trans_err_opt_b, ['x', 'r']);
hold on;
plot(1:f, trans_err_XRay_b, ['*', 'b']);
hold on;
plot(1:f, trans_err_comb_b, ['s', 'g']);
ylim([0,3]);
xlabel('Number of test run');
ylabel('Translational error in mm');
legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
title('Shah - backward - translational error');

figure(10);
plot(1:f, rot_err_opt, ['x', 'r']);
hold on;
plot(1:f, rot_err_XRay, ['*', 'b']);
hold on;
plot(1:f, rot_err_comb, ['s', 'g']);
ylim([0,2]);
xlabel('Number of test run');
ylabel('Rotational error in degrees');
legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
title('Shah - forward - rotational error');

figure(11);
plot(1:f, rot_err_opt_b, ['x', 'r']);
hold on;
plot(1:f, rot_err_XRay_b, ['*', 'b']);
hold on;
plot(1:f, rot_err_comb_b, ['s', 'g']);
ylim([0,2]);
xlabel('Number of test run');
ylabel('Rotational error in degrees');
legend('optical/robot loop', 'XRay/robot loop', 'optical/XRay loop');
title('Shah - backward - rotational error');

disp('Crossvalidated error values: ');
disp('Shah - forward - translational error');
disp('Translational error - optical/robot loop'); disp(mean(trans_err_opt)); disp(std(trans_err_opt));
disp('Translational error - optical/XRay loop'); disp(mean(trans_err_comb)); disp(std(trans_err_comb));
disp('Translational error - XRay/robot loop'); disp(mean(trans_err_XRay)); disp(std(trans_err_XRay));

disp('Shah - backward - translational error');
disp('Translational error - optical/robot loop'); disp(mean(trans_err_opt_b)); disp(std(trans_err_opt_b));
disp('Translational error - optical/XRay loop'); disp(mean(trans_err_comb_b)); disp(std(trans_err_comb_b));
disp('Translational error - XRay/robot loop'); disp(mean(trans_err_XRay_b)); disp(std(trans_err_XRay_b));

disp('Shah - forward - rotational error');
disp('Rotational error - optical/robot loop'); disp(mean(rot_err_opt)); disp(std(rot_err_opt));
disp('Rotational error - optical/XRay loop'); disp(mean(rot_err_comb)); disp(std(rot_err_comb));
disp('Rotational error - XRay/robot loop'); disp(mean(rot_err_XRay)); disp(std(rot_err_XRay));

disp('Shah - backward - rotational error');
disp('Rotational error - optical/robot loop'); disp(mean(rot_err_opt_b)); disp(std(rot_err_opt_b));
disp('Rotational error - optical/XRay loop'); disp(mean(rot_err_comb_b)); disp(std(rot_err_comb_b));
disp('Rotational error - XRay/robot loop'); disp(mean(rot_err_XRay_b)); disp(std(rot_err_XRay_b));

disp('Shah - forward - combined error');
disp('Combined error - optical/robot loop'); disp(mean(ec_opt)); disp(std(ec_opt));
disp('Combined error - optical/XRay loop'); disp(mean(ec_comb)); disp(std(ec_comb));
disp('Combined error - XRay/robot loop'); disp(mean(ec_XRay)); disp(std(ec_XRay));

disp('Shah - backward - combined error');
disp('Combined error - optical/robot loop'); disp(mean(ec_opt_b)); disp(std(ec_opt_b));
disp('Combined error - optical/XRay loop'); disp(mean(ec_comb_b)); disp(std(ec_comb_b));
disp('Combined error - XRay/robot loop'); disp(mean(ec_XRay_b)); disp(std(ec_XRay_b));