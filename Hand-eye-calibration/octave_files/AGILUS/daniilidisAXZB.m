function [X, Z] = daniilidisAXZB(A_abs,B_abs)
% Calculates the least squares solution of
% AX = XB
%
% The dual quaternion approach to hand-eye calibration
% Daniildis, Bayro-Corrochano
%
% Mili Shah
% July 2014
%
% Uses hom2quar.m and quar2hom.m

[m,n] = size(A_abs); n = n/4;
T = zeros(6*(n-1),8);

%% Change the direction of Camera_to_Marker (like the setup in Daniilidis)
for i = 1:n
  A_i = A_abs(:,4*i-3:4*i);
  Marker_to_Camera(:,4*i-3:4*i) = inv(A_i);
end

%A = A_{i+1}*inv(A_{i}), B = inv(B{i+1})*B{i} 
for i=1:n-1
  A_rel(:,:,i) = Marker_to_Camera(:,4*(i+1)-3:4*(i+1))*inv(Marker_to_Camera(:,4*(i)-3:4*(i)));
  B_rel(:,:,i) = inv(B_abs(:,4*(i+1)-3:4*(i+1)))*B_abs(:,4*(i)-3:4*(i));
end

%%% Reshape
A = reshape(A_rel, [4,4*(n-1)]);
B = reshape(B_rel, [4,4*(n-1)]);

%%%% start with normal method
for i = 1:n-1
  A1 = (A(:,4*i-3:4*i));
  B1 = (B(:,4*i-3:4*i));
  a = hom2quar(A1);
  b = hom2quar(B1);
  T(6*i-5:6*i,:) = ...
  [a(2:4,1)-b(2:4,1) skew(a(2:4,1)+b(2:4,1)) zeros(3,4);...
  a(2:4,2)-b(2:4,2) skew(a(2:4,2)+b(2:4,2)) a(2:4,1)-b(2:4,1) skew(a(2:4,1)+b(2:4,1))];
end

[u,s,v]=svd(T);
u1 = v(1:4,7);
v1 = v(5:8,7);
u2 = v(1:4,8);
v2 = v(5:8,8);
a = u1'*v1;
b = u1'*v2+u2'*v1;
c = u2'*v2;
s1 = (-b+sqrt(b^2-4*a*c))/2/a;
s2 = (-b-sqrt(b^2-4*a*c))/2/a;
s = [s1; s2];
[val,in] = max(s.^2*(u1'*u1) + 2*s*(u1'*u2) + u2'*u2);
s = s(in);
L2 = sqrt(1/val);
L1 = s*L2;
q = L1*v(:,7) + L2*v(:,8);
X = quar2hom([q(1:4) q(5:8)]);

t_Zi = 0;
Q = zeros(n,4);

%% calculation of Z
for i = 1:n
  T_A = A_abs(:,4*i-3:4*i);
  T_B = B_abs(:,4*i-3:4*i);
  R_A = T_A(1:3,1:3);
  R_B = T_B(1:3,1:3);
  t_A = T_A(1:3,4);
  t_B = T_B(1:3,4);

  Camera_to_Robot = T_A*X*inv(T_B);

  q_Zi = rotm2q(Camera_to_Robot(1:3,1:3));

  Q(i,1) = q_Zi.w;
  Q(i,2) = q_Zi.x;
  Q(i,3) = q_Zi.y;
  Q(i,4) = q_Zi.z;

  t_Zi += Camera_to_Robot(1:3,4);
end

weights = ones(n,1);
q_aver = quatWAvgMarkley(Q, weights);

q_Z = quaternion(q_aver(1), q_aver(2), q_aver(3), q_aver(4));
[v, theta] = q2rot(q_Z);
Z = rotv(v', theta);
t_Z = t_Zi/n;
Z = [Z', t_Z; 0 0 0 1];
end

function dq = hom2quar(H)
% Converts 4x4 homogeneous matrix H
% to a dual quaternion dq represented as
% dq(:,1) + e*dq(:,2)
%
% Mili Shah
R = H(1:3,1:3);
t = H(1:3,4);
R = logm(R);
r = [R(3,2) R(1,3) R(2,1)]';
theta = norm(r);
l = r/norm(theta);
q = [cos(theta/2); sin(theta/2)*l];
qprime = .5*qmult_modified([0;t],q);
dq=[q qprime];
end

function H = quar2hom(dq)
% Converts dual quaternion dq represented as
% dq(:,1) + e*dq(:,2)
% to a 4x4 homogeneous matrix H
%
% Uses the m-file
% qmult.m
%
% Mili Shah
q = dq(:,1); qe = dq(:,2);
R = [1-2*q(3)^2-2*q(4)^2 2*(q(2)*q(3)-q(4)*q(1)) 2*(q(2)*q(4)+q(3)*q(1));...
2*(q(2)*q(3)+q(4)*q(1)) 1-2*q(2)^2-2*q(4)^2 2*(q(3)*q(4)-q(2)*q(1));...
2*(q(2)*q(4)-q(3)*q(1)) 2*(q(3)*q(4)+q(2)*q(1)) 1-2*q(2)^2-2*q(3)^2];
q(2:4) = -q(2:4);
t = 2*qmult_modified(qe,q);
H = [R t(2:4);0 0 0 1];
end