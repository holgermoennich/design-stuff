function s = qmult1(q,r)

%  QMULT Multiplies 2 quaternions
%
%    S = QMULT(Q,R) returns the quaternion product of Q and R. Q (resp. R,
%      S)is a vector of size 4 represeting a quaternion or an array of size
%      4*N (column i represents quaternion i) where N is the number of
%      quaternions. There are several combinations possible:
%       - Q is a 4-vector and R is an array 4*N ==> S is an array 4*N. Q is
%       multiplied with each column (quaternion) of R.
%       - Q is an array 4*N and R is a 4-vector ==> S is an array 4*N. Each
%       column (quaternion) of Q is multiplied with R.
%       - Q and R are both arrays of size 4*N  ==> S is an array 4*N. The
%       quaternion muliplication is carried out on corresponding columns.
%      THE ORDER MATTERS: the quaternion muliplication is NOT commutative.
%      QMULT(Q,R) is different from QMULT(R,Q)
%
% See also DQMULT, QCONJ

sq = size(q);
sr = size(r);
if sq == [1 4]
    q = q.'; 
    sq = size(q); 
end
if sr == [1 4]
    r = r.'; 
    sr = size(r);
end

% wrong size
if sq(1) ~= 4 || sr(1) ~= 4 
    error('DualQuaternion:Qmult:wrongsize',...
        '%d rows in array q and %d rows in array r. It should be 4 for both.',...
        sq(1),sq(2));
end

nq = sq(2);
nr = sr(2);

% wrong format for the inputs
if nq ~= nr && nq > 1 && nr > 1 
    error('DualQuaternion:Qmult:wrongFormat',...
        '%d columns in array q and %d columns in array r. It should be the same number for both, or one of these should be 1.',...
        nq,nr);
end

% Implementation of the quaternion multiplication
s(1,:) = q(1,:).*r(1,:) - q(2,:).*r(2,:) - q(3,:).*r(3,:) - q(4,:).*r(4,:);
s(2,:) = q(1,:).*r(2,:) + q(2,:).*r(1,:) + q(3,:).*r(4,:) - q(4,:).*r(3,:);
s(3,:) = q(1,:).*r(3,:) - q(2,:).*r(4,:) + q(3,:).*r(1,:) + q(4,:).*r(2,:);
s(4,:) = q(1,:).*r(4,:) + q(2,:).*r(3,:) - q(3,:).*r(2,:) + q(4,:).*r(1,:);