%% Simulation -- how inaccuracies influence the hand-eye calibration and errors
%% creation of ideal data (based on measurements of the robot)
%% addition of gaussian noise

clc;
clear all;

%%%%%%%%%%%%%%%%
% creation of ideal data
%%%%%%%%%%%%%%%%

n = 20; 
n_HEC = 15; % number of data sets for hand eye calibration

start_flange = [1000 100 500 0 0 0];
robot_0 = CtoH(start_flange);
start_rob = start_flange;

% X
Opt_to_flange = [eye(3),[50,50,50]';[0 0 0 1]];

%% Z based on sensible position of camera
Cam_0 = [eye(3),[100 75 1000]';[0 0 0 1]];
Cam_to_base = Cam_0*Opt_to_flange*inv(robot_0);

clear relPositions Robot_to_Flange;
  
%% range: [-50, 50]
relPositions = ...
[  34    3   -4   34   20  -15;
    1   49   32   41  -23  -27;
   44   30  -17    4   23   -7;
   34   48  -24  -27  -30  -17;
  -50   37  -45   12    7  -46;
  -43  -43   15  -33  -15    2;
   -3  -29   14   48   29  -29;
  -37   36  -47  -15   19  -18;
   32  -15  -11    8  -11   -5;
  -11   -5   -2   26   30   -5;
  -35   26   40  -35   47  -15;
    3    6   13  -10   22  -15;
   49   28   21   33  -33   29;
   31   40   12    9    6   10;
   35   50  -39   -5  -49    9;
    1   30  -11  -14  -47  -21;
  -37  -30   45   -6   42   45;
   39  -44    1   25   18    3;
   22   46   50   37  -47  -35;
    7  -38  -24   15   12  -38];
   
for i = 1:n
  Robot_to_Flange(:, i*4-3:i*4) = CtoH(start_rob+relPositions(i,:));
end


clear Cam_to_opt;

%% compute ideal poses of Cam_to_opt 
for i = 1:n 
  Cam_to_opt(:, i*4-3:i*4) = Cam_to_base*Robot_to_Flange(:, i*4-3:i*4)*inv(Opt_to_flange); 
  Err = inv(Cam_to_base*Robot_to_Flange(:, i*4-3:i*4))*Cam_to_opt(:, i*4-3:i*4)*Opt_to_flange; % should be identity matrix
end


%%%%%%%%%%%%%%%%
%% addition of gaussian noise to the data
%%%%%%%%%%%%%%%%
  
err_rob_t = 0.01:0.01:1; %sigma for robot
err_rob_r = linspace(0.0000001, 0.001, length(err_rob_t)); % 0.01:0.01:0.04; %0.0005; % 0.01; %0.0000001:0.00001:0.001; %0.0005; % creates rotation error between 0 and 3.4 degrees


for k = 1:length(err_rob_t)
  for m = 1:100
    
   clear Robot_to_Flange_noise Cam_to_opt_noise Robot_to_Flange_test Robot_to_Flange_training Cam_to_opt_test Cam_to_opt_training;
    
    %% noise on robot data
   Robot_to_Flange_noise = add_noise(Robot_to_Flange, err_rob_t(k), err_rob_r(k));
    
    %% noise on optical data
   Cam_to_opt_noise = add_noise(Cam_to_opt, err_rob_t(k), err_rob_r(k)); 


  %%%%%%%%%%%%%%%%
  %% define training and test data
  %%%%%%%%%%%%%%%%
  
  Robot_to_Flange_test = Robot_to_Flange_noise(:,1:n_HEC*4);
  Robot_to_Flange_training = Robot_to_Flange_noise(:,4*n_HEC+1:4*n);
  Cam_to_opt_test = Cam_to_opt_noise(:,1:n_HEC*4);
  Cam_to_opt_training = Cam_to_opt_noise(:,4*n_HEC+1:4*n);
  
  %%%%%%%%%%%%%%%%
  %% perform HEC
  %%%%%%%%%%%%%%%%

  for i = 1:n
    A_i = Cam_to_opt_noise(:,4*i-3:4*i);
    Opt_to_cam(:,4*i-3:4*i) = inv(A_i);
    B_i = Robot_to_Flange_noise(:,4*i-3:4*i);
    Flange_to_robot(:,4*i-3:4*i) = inv(B_i);
  end
  Opt_to_cam_test = Opt_to_cam(:,1:n_HEC*4);
  Flange_to_robot_test = Flange_to_robot(:,1:n_HEC*4);
  Opt_to_cam_training = Opt_to_cam(:,4*n_HEC+1:4*n);
  Flange_to_robot_training = Flange_to_robot(:,4*n_HEC+1:4*n);
  
  
  
  clear Opt_to_flange_shah_noise Cam_to_base_shah_noise Cam_to_base_shah_noise2 Opt_to_flange_shah_noise2;
  
  %% optical/robot loop forward
  [Opt_to_flange_shah_noise, Cam_to_base_shah_noise] = shah(Cam_to_opt_test, Robot_to_Flange_test);
  
  [Cam_to_base_shah_noise2, Opt_to_flange_shah_noise2] = shah(Opt_to_cam_test, Flange_to_robot_test); % inverse input of A and B

  %%%%%%%%%%%%%%%%
  %% calculate errors
  %%%%%%%%%%%%%%%%
  
  %% optical/robot loop forward
  [ang_opt_to_flange(m), trans_opt_to_flange(m)] = delta_matrix(Opt_to_flange, Opt_to_flange_shah_noise);
  [ang_cam_to_base(m), trans_cam_to_base(m)] = delta_matrix(Cam_to_base, Cam_to_base_shah_noise);

  [ang_opt_to_flange2(m), trans_opt_to_flange2(m)] = delta_matrix(Opt_to_flange, Opt_to_flange_shah_noise2);
  [ang_cam_to_base2(m), trans_cam_to_base2(m)] = delta_matrix(Cam_to_base, Cam_to_base_shah_noise2);

  
  err_trans_opt(m) = norm(RMSE_trans(Cam_to_opt_training, Robot_to_Flange_training, Opt_to_flange_shah_noise, Cam_to_base_shah_noise));
  err_rot_opt(m) = RMSE_rot(Cam_to_opt_training, Robot_to_Flange_training, Opt_to_flange_shah_noise, Cam_to_base_shah_noise);
  
  err_trans_opt2(m) = norm(RMSE_trans(Opt_to_cam_training, Flange_to_robot_training, Cam_to_base_shah_noise2, Opt_to_flange_shah_noise2));
  err_rot_opt2(m) = RMSE_rot(Opt_to_cam_training, Flange_to_robot_training, Cam_to_base_shah_noise2, Opt_to_flange_shah_noise2);
 

  end
  ang_opt_to_flange_mean(k) = mean(ang_opt_to_flange);
  trans_opt_to_flange_mean(k) = mean(trans_opt_to_flange);
  ang_cam_to_base_mean(k) = mean(ang_cam_to_base);
  trans_cam_to_base_mean(k) = mean(trans_cam_to_base);
  
  err_trans_opt_mean(k) = mean(err_trans_opt);
  err_rot_opt_mean(k) = mean(err_rot_opt);

  ang_opt_to_flange_mean2(k) = mean(ang_opt_to_flange2);
  trans_opt_to_flange_mean2(k) = mean(trans_opt_to_flange2);
  ang_cam_to_base_mean2(k) = mean(ang_cam_to_base2);
  trans_cam_to_base_mean2(k) = mean(trans_cam_to_base2);  
  
  err_trans_opt_mean2(k) = mean(err_trans_opt2);
  err_rot_opt_mean2(k) = mean(err_rot_opt2);
end


%%%%%%%%%%%%%%%%
%% plot errors
%%%%%%%%%%%%%%%%

%% error of each transformation matrix 'X' and 'Z'
%% for changes in transformation matrices

figure(19);
subplot (2, 2, 1);
plot(err_rob_r, ang_opt_to_flange_mean, 'x');%err_rob_r
title('Rotational error of X', 'fontsize', 18);
ylim([0 4]);
%set(gca,'fontsize',13);
xlabel('Amount of deflection', 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 2, 2);
plot(err_rob_t, trans_opt_to_flange_mean, 'x');
title('Translational error of X', 'fontsize', 18);
ylim([0 60]);
%set(gca,'fontsize',13);
xlabel('Standard deviation in mm', 'fontsize', 14);
ylabel({'Translational error', 'in mm'}, 'fontsize', 14);

subplot (2, 2, 3);
plot(err_rob_r, ang_cam_to_base_mean, 'x');
title('Rotational error of Z', 'fontsize', 18);
ylim([0 4]);
%set(gca,'fontsize',13);
xlabel('Amount of deflection', 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 2, 4);
plot(err_rob_t, trans_cam_to_base_mean, 'x');
title('Translational error of Z', 'fontsize', 18);
ylim([0 60]);
%set(gca,'fontsize',13);
xlabel('Standard deviation in mm', 'fontsize', 14);
ylabel({'Translational error', 'in mm'}, 'fontsize', 14);

%%%%%
%%%%%%% X as second and Z as first unknown
figure(20);
subplot (2, 2, 1);
plot(err_rob_r, ang_opt_to_flange_mean2, 'x');
title('Rotational error of X', 'fontsize', 18);
ylim([0 4]);
%set(gca,'fontsize',13);
xlabel('Amount of deflection', 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 2, 2);
plot(err_rob_t, trans_opt_to_flange_mean2, 'x');
title('Translational error of X', 'fontsize', 18);
ylim([0 60]);
%set(gca,'fontsize',13);
xlabel('Standard deviation in mm', 'fontsize', 14);
ylabel({'Translational error', 'in mm'}, 'fontsize', 14);

subplot (2, 2, 3);
plot(err_rob_r, ang_cam_to_base_mean2, 'x');
title('Rotational error of Z', 'fontsize', 18);
ylim([0 4]);
%set(gca,'fontsize',13);
xlabel('Amount of deflection', 'fontsize', 14);
ylabel({'Rotational error', 'in degrees'}, 'fontsize', 14);

subplot (2, 2, 4);
plot(err_rob_t, trans_cam_to_base_mean2, 'x');
title('Translational error of Z', 'fontsize', 18);
ylim([0 60]);
%set(gca,'fontsize',13);
xlabel('Standard deviation in mm', 'fontsize', 14);
ylabel({'Translational error', 'in mm'}, 'fontsize', 14);
%%

figure(21);
subplot(2,1,1);
plot(err_rob_r, err_rot_opt_mean, ['x', 'r']);
title('Rotational error of closed loop', 'fontsize', 18);
ylim([0 4]);
set(gca,'fontsize',13);
xlabel('Amount of deflection', 'fontsize', 14);
ylabel('Rotational error in degrees', 'fontsize', 14);

subplot (2, 1, 2);
plot(err_rob_t, err_trans_opt_mean, ['s', 'b']);
title('Translational error of closed loop', 'fontsize', 18);
ylim([0 60]);
set(gca,'fontsize',13);
xlabel('Standard deviation in mm', 'fontsize', 14);
ylabel('Translational error in mm', 'fontsize', 14);

figure(22);
subplot(2,1,1);
plot(err_rob_r, err_rot_opt_mean2, ['x', 'r']);
title('Rotational error of closed loop', 'fontsize', 18);
ylim([0 4]); 
set(gca,'fontsize',13);
xlabel('Amount of deflection', 'fontsize', 14);
ylabel('Rotational error in degrees', 'fontsize', 14);

subplot (2, 1, 2);
plot(err_rob_t, err_trans_opt_mean2, ['s', 'b']);
title('Translational error of closed loop', 'fontsize', 18);
ylim([0 60]);
set(gca,'fontsize',13);
xlabel('Standard deviation in mm', 'fontsize', 14);
ylabel('Translational error in mm', 'fontsize', 14);