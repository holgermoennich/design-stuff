% 
% Agilus Control using JAVA API
% Written by: Florian Schiffers, 02/02/2017
%
% This is an example script how to use the AGILUS Java API
% within the OCTAVE Framework
%
% Prerequisites:
% 1. JAR of the Java Agilus API
%    If this is not available, you can create it with Eclipse using
%    ECLIPSE -> Export -> Runnable JAR File
%    Make sure to have following option enabled, to save all required KUKA
%    libraries into the JAR
%    -> Extract required libraries into generated JAR
% 2. Set AGILUS-JAR Variable in OCTAVE PATH
%    e.g. create a javaclasspath.txt containig the path in the folder
%    where javaclasspath.m is located
%    e.g. C:\Octave\Octave-4.0.3\share\octave\4.0.3\m\java
%    and add e.g.: X:\repository\philip\source\java\Agilus_KRL\agilus.jar
%
%

clear
clc

%
% JAVA KUKA Library provides two classes for positions
% JointPosition and FramePosition
% As an example we create the homeposition in frame and joint coordinates
%

homeFrame = frame(920,-230,995,0,90,0);
%homeJoint = jointPosition(0,-90,90,0,0,0);
homeJoint = jointPosition(105,-90,111,23,-1.6,-85.35);

%%
%% Define port number and ip addr
%% See EthernetKRL Guide for more information
%%
port = 59152; % Defined by XML inside on the KUKA smartpad
addr = '172.31.1.140'; % Defined by XML inside the KUKA smartpad

% Create agilus control object
robot = connectAgilus(port,addr);

robot.move(homeJoint);

% Some functions you can call
curFrame = robot.getCartesian();
disp(curFrame');

curJointPos = robot.getJointPosition();
disp(curJointPos');

sensorData = robot.getSensorData();
sensorData.getPosition.getAxis.getCoordinates;
sensorData.getPosition.getCartesian.getCoordinates;

% Get current velocity
curSpeed = sensorData.getSpeed();
disp(curSpeed);

robot.setSpeed(80);
robot.setSpeed(20);

%sensorData.printPretty();

%method moveRel expects double x, double y, double z and evtl. motiontype LIN/PTP
robot.moveRel(10,0,0); %back x

robot.move(homeJoint);

robot.moveRel(0,10,0); %torsion y

robot.move(homeJoint);

robot.moveRel(0,0,10); % up z

robot.move(homeJoint);

robot.moveRel(frame(-10, 0, 0, 0, 5, 0));

robot.move(homeJoint);

robot.moveRel(frame(0,0,20,10, 5,5));

%numPoints =10
%r = randi([-50 50],3,numPoints);
%
%for k = 1:numPoints
%robot.moveRel(r(1,k),r(2,k),r(3,k));
%end

robot.move(homeJoint);

robot.closeConnection;
