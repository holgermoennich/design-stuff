function [ H ] = CtoH_NDI( C )
%CTOH Cartesian Points to Homogeneous Points 

[totalCartPoints, n] = size(C);

H = zeros(4, 4);

for i=1:totalCartPoints
    tranXYZ = [C(i,1); C(i,2); C(i,3)];
    rotmZYX = EtoR([C(i,6), C(i,5), C(i,4)], 'Rob');
    H = [rotmZYX, tranXYZ;
         0, 0, 0, 1];
end

end