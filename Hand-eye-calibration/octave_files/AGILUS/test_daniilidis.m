clc
clear all

Camera_to_Marker = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Camera_to_Marker.txt');
Robot_to_Flange = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Robot_to_Flange.txt');

HMarker_home = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\HMarker_home.txt');
HFlange_home = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\HFlange_home.txt');

%% number of measurements 
[m, n] = size(Camera_to_Marker);
numCamPoints = n/4;

[q, r] = size(Robot_to_Flange);
numRobotPoints = r/4;

%A(:,:,1) = Camera_to_Marker(:,1:4)*inv(HMarker_home);
%B(:,:,1) = Robot_to_Flange(:,1:4)*inv(HFlange_home);
%for i=2:numCamPoints
%  A(:,:,i) = Camera_to_Marker(:,4*i-3:4*i)*inv(Camera_to_Marker(:,4*(i-1)-3:4*(i-1)));
%  B(:,:,i) = Robot_to_Flange(:,4*i-3:4*i)*inv(Robot_to_Flange(:,4*(i-1)-3:4*(i-1)));
%end

%% change the direction of Camera_to_Marker (like the setup in Daniilidis)
for i = 1:numCamPoints
  A_i = Camera_to_Marker(:, 4*i-3:4*i);
  Marker_to_Camera(:, 4*i-3:4*i) = inv(A_i);
end

%% relative movements with ten entries; A = A_{i+1}*inv(A_{i}), B = inv(B{i+1})*B{i} 
A(:,:,1) = Marker_to_Camera(:,1:4)*HMarker_home;
B(:,:,1) = inv(Robot_to_Flange(:,1:4))*HFlange_home;
for i=2:numCamPoints
  A(:,:,i) = Marker_to_Camera(:,4*i-3:4*i)*inv(Marker_to_Camera(:,4*(i-1)-3:4*(i-1)));
  B(:,:,i) = inv(Robot_to_Flange(:,4*i-3:4*i))*Robot_to_Flange(:,4*(i-1)-3:4*(i-1));
end

%A(:,:,1) = inv(HMarker_home)*inv(Camera_to_Marker_inv(:,1:4)); % Inverse of HMarker because of the other direction
%B(:,:,1) = inv(HFlange_home)*Robot_to_Flange(:,1:4);
%for i=2:numCamPoints
%  A(:,:,i) = Camera_to_Marker_inv(:,4*i-3:4*i)*inv(Camera_to_Marker_inv(:,4*(i-1)-3:4*(i-1)));
%  B(:,:,i) = inv(Robot_to_Flange(:,4*i-3:4*i))*Robot_to_Flange(:,4*(i-1)-3:4*(i-1));
%end

%%Reshape for 'daniiliids'
Camera_Movements = reshape(A, [4,4*numCamPoints]);
Flange_Movements = reshape(B, [4,4*numRobotPoints]);


disp( 'saving Daniilidis Results' )
Marker_to_Flange3 = daniilidis(Camera_Movements, Flange_Movements)
disp( 'Marker_to_Flange - NORM: ' ); disp( norm( Marker_to_Flange3( 1:3, 4 ) ) );