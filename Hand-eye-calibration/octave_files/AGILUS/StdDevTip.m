function [ tip_dev ] = StdDevTip(As)
  %% calculates the RMS error of the marker deviation after a pivot movement
  %% input: marker positions of the evaluation (pivot movement)
  %% output rms error in x,y,z direction
  
  [r,c] = size(As); 
  num_meas = c/4;
  
  sum_trans = 0; %zeros(3,1);
  
  for i = 1:num_meas
    t(:,i) = As(1:3,i*4);
  end
  
  t_m = mean(t');
  
  for j = 1:num_meas
    t_j = As(1:3,j*4);
%    sqdiff = (t_j - t_m').^2;
    sqdiff = (t_j - t_m')'*(t_j - t_m');
    sum_trans += sqdiff;
  end

  tip_dev = sqrt(sum_trans/num_meas);
  
end