clc;
clear all;

Marker_Positions = dlmread('D:\Agilus\AGILUS\results\Evaluation\14-08-2017\Daniilidis\slerp_20deg\Marker_Positions.txt');
Flange_Positions = dlmread('D:\Agilus\AGILUS\results\Evaluation\14-08-2017\Daniilidis\slerp_20deg\Flange_Positions.txt');


Flange_home = dlmread('D:\Agilus\AGILUS\results\Evaluation\14-08-2017\Daniilidis\slerp_20deg\Flange_home.txt');
Marker_home = dlmread('D:\Agilus\AGILUS\results\Evaluation\14-08-2017\Daniilidis\slerp_20deg\Marker_home.txt');

X = dlmread('D:\Agilus\AGILUS\results\Daniilidis\X.txt');
Z = dlmread('D:\Agilus\AGILUS\results\Daniilidis\Z.txt');


[m,n] = size(Marker_Positions); % m = 4, n = 200

num_pos = n/4;

%% Translational rms error
sum_trans = zeros(3,1);
sum_angle = 0;
t = zeros(3,num_pos);

for i = 1:num_pos
  t(:,i) = Marker_Positions(1:3,i*4);
  sqdiff = (Marker_home(1:3,4) - t(:,i)).^2;
  sum_trans += sqdiff;
end

rms_trans = sqrt(sum_trans/num_pos);
%dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Trans_rms_error.txt', rms_trans);

%% rms error calculated from expected orientation and measured orientation in camera frame
sum_angle = 0;

for k = 1:num_pos
  Expected_Marker_Trafo = Z*Flange_Positions(1:4,4*k-3:4*k)*inv(X);
  expec_angle = acos((trace(Expected_Marker_Trafo(1:3,1:3))-1)/2)*180/pi;
  
  Calc_rotation = Marker_Positions(1:3, 4*k-3:4*k-1);
  calc_angle = acos((trace(Calc_rotation)-1)/2)*180/pi;
  
  sqdiff_angle = (expec_angle - calc_angle)^2;
  sum_angle += sqdiff_angle;
end

rms_angle = sqrt(sum_angle/num_pos);
%dlmwrite('D:\Agilus\AGILUS\results\Evaluation\Rot_rms_error_includ_Z.txt',rms_angle);

%% Rotational rms error
%rot_m0 = RtoE(Marker_home(1:3,1:3), 'Rob'); % alpha, beta, gamma in degrees

%fl_ang_home = acos((trace(Flange_home(1:3,1:3))-1)/2)*180/pi;
%m_ang_home = acos((trace(Marker_home(1:3,1:3))-1)/2)*180/pi;

%% plotting

for j = 1:num_pos  
  s(:,j) = Flange_Positions(1:3, 4*j);
end

%% plot of translations
%% plot of marker translation -> should be at one position
figure(1);
m0 = Marker_home(1:3,4);
scatter3 (t(1,:), t(2,:), t(3,:), "b", "filled");
hold on;
scatter3 (m0(1), m0(2), m0(3), "r", "x"); % plot marker origin
xlabel ("x in mm");
ylabel ("y in mm");
zlabel ("z in mm");
legend('Marker positions', 'Ground truth');
title ("Marker Translation");

%% plot of flange translation -> should be on a sphere around the marker and on an ellipse in a plane
figure(2);
f0 = Flange_home(1:3,4);
scatter3 (s(1,:), s(2,:), s(3,:), "b", "filled");
hold on;
scatter3 (f0(1), f0(2), f0(3), "r", "filled"); % plot marker origin
xlabel ("x in mm"); 
ylabel ("y in mm");
zlabel ("z in mm");
title ("Flange Translation");

%% fit flange positions on a sphere 
[center, radius] = sphereFit(s');
exp_c = Flange_home*inv(X);
disp('expected center: '); disp(exp_c(1:3,4));
disp('calculated center: '); disp(center);
disp('expected radius: '); disp(norm(X(1:3,4)));
disp('calculated radius: '); disp(radius);


% pivotise
[ cCen, cRad] = pivotise2(s');
disp('cCen: '); disp(cCen);
disp('cRad: '); disp(cRad);


for j = 1:num_pos
  %% Trafo matrices
  Traf_Marker = Marker_Positions(1:4, 4*j-3:4*j);
  Traf_Flange = Flange_Positions(1:4, 4*j-3:4*j);
 
  %% Rotation matrices
  Rot_Marker = Traf_Marker(1:3, 1:3);
  Rot_Flange = Traf_Flange(1:3, 1:3);
  
  %% angles
  %rot_mj(:,j) = RtoE(Rot_Marker, 'Rob'); %Euler angle
  %rot_mj(j) = acos((trace(Rot_Marker)-1)/2)*180/pi; % rotation angle of each marker position
  [w,a] = to_axis(Rot_Marker);
  rot_mj(j) = a;
  axes_mj(1:3,j) = w;
  
  [z,b] = to_axis(Rot_Flange);
  rot_flj = b;
  axes_fl(1:3,j) = z;
  
%  fl_t = logm(Rot_Flange);
%  t_x = fl_t(3,2)/a;
%  t_y = fl_t(1,3)/a;
%  t_z = fl_t(2,1)/a;
%  w(:,j) = [t_x, t_y, t_z]';
 
  %rot_fj(:,j) = RtoE(Traf_Flange(1:3,1:3), 'Rob'); %Euler angle
  %rot_fj(j) = acos((trace(Traf_Flange(1:3,1:3))-1)/2)*180/pi;
  
  figure(3);
  title("Coordinate frames of Marker w.r.t. Camera");
  hold on;
  scale = 10;
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,1))', "r");
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,2))', "g");
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,3))', "b");
  view(3);
  
  figure(4);
  title("Coordinate frames of Flange w.r.t. Base");
  hold on;
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,1))', "r");
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,2))', "g");
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,3))', "b");
  view(3);
end

figure(5);
arrow3(zeros(num_pos,3), axes_fl');
title("Rotation axes Base to Flange");

figure(6);
arrow3(zeros(num_pos,3), axes_mj');
title("Rotation axes Camera to Marker");

figure(7);
n = 2*pi/num_pos:2*pi/num_pos:2*pi;
plot(n, rot_flj);
xlabel("Evaluation points");
ylabel("Rotation angle");
title("Rotation angles Base to Flange");

figure(8);
n = 2*pi/num_pos:2*pi/num_pos:2*pi;
plot(n, rot_mj);
xlabel("Evaluation points");
ylabel("Rotation angle");
title("Rotation angles Base to Flange");

%%% errors based on independent measurements

Camera_to_Marker_fE = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Camera_to_Marker_for_eval.txt');
Robot_to_Flange_fE = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\Robot_to_Flange_for_eval.txt');

[r,s] = size(Camera_to_Marker_fE); 
num_meas = s/4;

%% mean rotational error in degrees
R_Z = Z(1:3,1:3);
R_X = X(1:3,1:3);
sum_rot2 = 0;

for k = 1:num_meas
  T_Bi = Robot_to_Flange_fE(1:4, 4*k-3:4*k);
  R_Bi = T_Bi(1:3,1:3);
  T_Ai = Camera_to_Marker_fE(1:4, 4*k-3:4*k);
  R_Ai = T_Ai(1:3,1:3);
  R_i = (R_Z*R_Bi)'*(R_Ai*R_X); 
  
  sum_rot2 += acos((trace(R_i(1:3,1:3))-1)/2)*180/pi;
end

e_R2 = sum_rot2/num_meas;
%dlmwrite('D:\Agilus\AGILUS\results\Evaluation\e_R2.txt', e_R2);

%% mean translational error
t_Z = Z(1:3,4);
t_X = X(1:3,4);
sum_trans = 0;

for l = 1:num_meas
  T_Bi = Robot_to_Flange_fE(1:4, 4*l-3:4*l);
  R_Bi = T_Bi(1:3,1:3);
  t_Bi = T_Bi(1:3,4);
  T_Ai = Camera_to_Marker_fE(1:4, 4*l-3:4*l);
  R_Ai = T_Ai(1:3,1:3);
  t_Ai = T_Ai(1:3,4);
  
  trans = (R_Ai*t_X + t_Ai) - (R_Z*t_Bi+t_Z);
  n_trans = norm(trans);
  sum_trans += n_trans;
end

e_t = sum_trans/num_meas;
%dlmwrite('D:\Agilus\AGILUS\results\Evaluation\e_t.txt', e_t);

%% mean combined rotation and translation error without units
sum_dN = 0;

for u = 1:num_meas
  Ai = Camera_to_Marker_fE(:,4*u-3:4*u);
  Bi = Robot_to_Flange_fE(:,4*u-3:4*u);
  dH = Ai*X - Z*Bi; % 4x4 difference matrix
  dN = norm(dH, "fro"); % frobenius norm of the difference matrix
  sum_dN += dN^2;
end
e_c = sum_dN/num_meas;
%dlmwrite('D:\Agilus\AGILUS\results\Evaluation\e_c.txt', e_c);