%% Dependency between number of positions and error

clc;
clear all;

Camera_to_Marker = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\number_of_pos\Camera_to_Marker.txt');
Robot_to_Flange = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\number_of_pos\Robot_to_Flange.txt');

%Camera_to_Marker = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\small_volume\Camera_to_Marker.txt');
%Robot_to_Flange = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\small_volume\Robot_to_Flange.txt');

[u,v] = size(Camera_to_Marker); 
num_pos = v/4; %20

Camera_to_Marker_fE = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\number_of_pos\Camera_to_Marker_for_eval.txt');
Robot_to_Flange_fE = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\number_of_pos\Robot_to_Flange_for_eval.txt');

%Camera_to_Marker_fE = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\small_volume\Camera_to_Marker_for_eval.txt');
%Robot_to_Flange_fE = dlmread('D:\Agilus\AGILUS\results\TrafoMatrices\small_volume\Robot_to_Flange_for_eval.txt');

[r,s] = size(Camera_to_Marker_fE); 
num_meas = s/4; %10

eR_j = zeros(1,num_pos);
et_j = zeros(1, num_pos);
ec_j = zeros(1, num_pos);

iter = 100;
for j = 1:iter
  clear idx Camera_to_Marker_mixed Robot_to_Flange_mixed;
  
  idx = randperm(num_pos);
  
  for k = 1:num_pos
    Camera_to_Marker_mixed(:, k*4-3:k*4) = Camera_to_Marker(:,idx(k)*4-3:idx(k)*4);
    Robot_to_Flange_mixed(:, k*4-3:k*4) = Robot_to_Flange(:,idx(k)*4-3:idx(k)*4);
  end  
  
  for i = num_pos:-1:3

    X = zeros(4,4);
    Z = zeros(4,4);
    
    [X,Z] = dornaika(Camera_to_Marker_mixed(:,1:4*i), Robot_to_Flange_mixed(:,1:4*i));
    
    eR_i(i) = RMSE_rot(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
    ec_i(i) = e_c(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
    rms_trans = RMSE_trans(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
    et_i(i) = norm(rms_trans);
   end
   
   eR_j += eR_i;
   et_j += et_i;
   ec_j += ec_i;
end
  et = et_j/iter;
  eR = eR_j/iter;
  ec = ec_j/iter; 

figure(1);
plot(3:num_pos, et(3:num_pos), 'LineWidth', 2); 
xlabel("Number of measurements", 'FontSize',18);
ylabel("Translation error in mm", 'FontSize',18);
xlim([3 num_pos]);
title('e_{trans}', 'FontSize',18);

figure(2);
plot(3:num_pos, eR(3:num_pos), 'LineWidth', 2); 
xlabel("Number of measurements", 'FontSize',18);
ylabel("Rotation error in degree", 'FontSize',18);
xlim([3 num_pos]);
title('e_{rot}', 'FontSize',18);

figure(3);
plot(3:num_pos, ec(3:num_pos), 'LineWidth', 2);
xlabel("Number of measurements", 'FontSize',18);
ylabel("Combined error w/o unit", 'FontSize',18);
xlim([3 num_pos]);
title('e_c', 'FontSize',18);

%% mean rotational error in degrees
  
%for i = num_pos:-1:3
%  sum_rot2 = 0;
%  sum_trans = 0;
%  sum_dN = 0;
%  X = zeros(4,4);
%  Z = zeros(4,4);
%  
%  [X,Z] = dornaika(Camera_to_Marker(:,1:4*i), Robot_to_Flange(:,1:4*i));
%  R_X = X(1:3,1:3);
%  R_Z = Z(1:3,1:3);
%
%  t_X = X(1:3,4);
%  t_Z = Z(1:3,4);
%  
%  
%  for k = 1:num_meas
%    T_Bi = Robot_to_Flange_fE(1:4, 4*k-3:4*k);
%    R_Bi = T_Bi(1:3,1:3);
%    T_Ai = Camera_to_Marker_fE(1:4, 4*k-3:4*k);
%    R_Ai = T_Ai(1:3,1:3);
%    R_i = (R_Z*R_Bi)'*(R_Ai*R_X); 
%  
%    sum_rot2 += acos((trace(R_i(1:3,1:3))-1)/2)*180/pi;
%  end
%
%  e_R2(i) = sum_rot2/num_meas;
%
%  %% mean translational error
%  for l = 1:num_meas
%    T_Bi = Robot_to_Flange_fE(1:4, 4*l-3:4*l);
%    R_Bi = T_Bi(1:3,1:3);
%    t_Bi = T_Bi(1:3,4);
%    T_Ai = Camera_to_Marker_fE(1:4, 4*l-3:4*l);
%    R_Ai = T_Ai(1:3,1:3);
%    t_Ai = T_Ai(1:3,4);
%    
%    trans = (R_Ai*t_X + t_Ai) - (R_Z*t_Bi+t_Z);
%    n_trans = norm(trans);
%    sum_trans += n_trans;
%  end
%
%  e_t(i) = sum_trans/num_meas;
%
%  %% mean combined rotation and translation error without units
%
%  for u = 1:num_meas
%    Ai = Camera_to_Marker_fE(:,4*u-3:4*u);
%    Bi = Robot_to_Flange_fE(:,4*u-3:4*u);
%    dH = Ai*X - Z*Bi; % 4x4 difference matrix
%    dN = norm(dH, "fro"); % frobenius norm of the difference matrix
%    sum_dN += dN^2;
%  end
%  e_c(i) = sum_dN/num_meas;
%  
%end
%
%figure(1);
%plot(3:num_pos, e_t(3:num_pos), 'LineWidth', 2);
%xlabel("Number of positions");
%ylabel("Translation error in mm");
%xlim([3 num_pos]);
%title('e_t', 'FontSize',14);
%
%figure(2);
%plot(3:num_pos, e_R2(3:num_pos), 'LineWidth', 2);
%xlabel("Number of positions");
%ylabel("Rotation error in degree");
%xlim([3 num_pos]);
%title('e_{R2}', 'FontSize',14);
%
%figure(3);
%plot(3:num_pos, e_c(3:num_pos), 'LineWidth', 2);
%xlabel("Number of positions");
%ylabel("Combined error w/o unit");
%xlim([3 num_pos]);
%title('e_c', 'FontSize',14);