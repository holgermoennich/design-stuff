%% mono tracking
%% converts dicom into png, computes 6D marker pose from 2D file, saves transformation matrices into one single matrix needed for HEC

clc;
clear all;

path_marker = 'D:\XRay\Mono\Modelgeometry';
path_flouro = 'D:\XRay\Mono\As\Flouros';
path_png = 'D:\XRay\Mono\As\Images';
path_erg = 'D:\XRay\Mono\As\TrafoMatrices';
%% processed images
path_morph = 'D:\Temp\img_morph.png';
path_sat = 'D:\Temp\img_sat.png';
path_overlay = 'D:\Temp\overlay.png';
path_proc = 'D:\XRay\Mono\As\proc_Images';

marker_size = 40;

switch (marker_size)
  case 20
    path_model = strcat(path_marker, '\Modelgeometry_20mm.txt');
   case 30
    path_model = strcat(path_marker, '\Modelgeometry_30mm.txt');
   case 40
    path_model = strcat(path_marker, '\Modelgeometry_40mm.txt');
   otherwise
    path_model = strcat(path_marker, '\Modelgeometry_old.txt');
endswitch

% number of flouros
n = numel(dir(path_flouro))-2;

%% via spyder convert all .ima to .png

py_command = cstrcat('python convert_dcm_to_png.py ', path_flouro, ' ', path_png);
current_dir = pwd();
cd('D:\Git\Lemmium\Lemmium\Hand-eye-calibration\Python');
system(py_command);
cd(current_dir);

% for testing - based on one 3D run
%dos('python D:\XRay\Python\convert_dcm_to_png.py D:\XRay\TestData\X-RAY_Marker.IMA D:\XRay\TestData\images1');

%% via C++ convert .png to transformation matrix
%dos('D:\XRay\Mono\Pose_from_Points_Mono_CLI.exe --input D:\XRay\TestData\images\0.png --model D:\XRay\TestData\Modelgeometry.txt --output D:\XRay\Mono\As\TrafoMatrices\0.txt');
%--input  D:\XRay\Mono\As\Images\0.png --model D:\XRay\Mono\Modelgeometry\Modelgeometry_40mm.txt --output D:\XRay\Mono\As\TrafoMatrices\0.txt --morph_size 20  --grey_max 255  --dot_max 50 --dot_size 20 --px 0.16
for i = 0:n-1
   Filename = strcat(path_png, '\', num2str(i), '.png');
   ErgName = strcat(path_erg, '\', num2str(i), '.txt');
   command = cstrcat('D:\XRay\Mono\Build\Pose_from_Points_Mono\Release\Pose_from_Points_Mono_CLI.exe --input ', Filename, ' --model ', path_model, ' --output ', ErgName, ' --px 0.16 --morph_size 40 --grey_min 0 --grey_max 255 --dot_max 30 --dot_size 20');
   %command = cstrcat('D:\XRay\Mono\Build\Pose_from_Points_Mono\Release\Pose_from_Points_Mono_CLI.exe --input ', Filename, ' --model ', path_model, ' --output ', ErgName);
   dos(command);
   %% saving temporary images
   path_tmp_morph = strcat(path_proc, '\', num2str(i),'_img_morph.png');
   path_tmp_sat = strcat(path_proc, '\', num2str(i),'_img_sat.png');
   path_tmp_overlay = strcat(path_proc, '\', num2str(i),'_overlay.png');
   imwrite(imread(path_morph), path_tmp_morph);
   imwrite(imread(path_sat), path_tmp_sat); 
   imwrite(imread(path_overlay), path_tmp_overlay); 
end

%% save txt files in one matrix 
filelist = readdir(path_erg);

if n!= (numel(filelist)-2)/3 % if 2d3d.txt and residuals.txt are saved for each image
  error('Number of input and output data is NOT equal');
end 

XRay_to_IBR = zeros(4,4*n);

for i = 0:n-1
   path = strcat(path_erg, '\', num2str(i), '.txt');
   XRay_to_IBR(:,4*(i+1)-3:4*(i+1)) = dlmread(path);
end

% Save trafo matrices 
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\XRay_to_IBRMarker_all_pos.txt', XRay_to_IBR);

num_HEC = 15; %% use first num_HEC entries for HEC
dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\XRay_to_IBRMarker.txt', XRay_to_IBR(:,1:4*num_HEC));

%% use remaining entries for the evaluation
if n > num_HEC
  dlmwrite('D:\Agilus\AGILUS\results\XRay\TrafoMatrices\XRay_to_IBRMarker_for_eval.txt', XRay_to_IBR(:,(4*num_HEC+1):4*n));
end