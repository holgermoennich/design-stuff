function E = RtoE( R, str )
%RTOE computes euler angles from rotation matrix
%
E=zeros(3,1);
%str
if strcmp(str, 'Rob')
    aa=atan2(R(2,1),R(1,1));
    b= -asin(R(3,1));
    c=atan2(R(3,2),R(3,3));
end

if strcmp(str, 'DIN')
%    str
    b = asin(R(1,3));
    c = -atan2(R(1,2),R(1,1));
    Rhelp=R*[cos(c) sin(c) 0;-sin(c) cos(c) 0;0 0 1]*[cos(b) 0 -sin(b);0 1 0;sin(b) 0 cos(b)];
    aa =-atan2(Rhelp(2,3),Rhelp(2,2));
end

if strcmp(str, 'y')
%    str
% not implemented
end


E(1)=aa*180/pi;  %a=E(1)*pi/180;
E(2)=b*180/pi;  %b=E(2)*pi/180;
E(3)=c*180/pi;  %c=E(3)*pi/180;