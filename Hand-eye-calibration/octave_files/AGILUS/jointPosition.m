function joint = jointPosition(varargin)
 
 joint = 0;
 
 switch (length(varargin))
  case 1
    if(length(varargin{1})==6)
      joint = varargin{1};
    else
    error("Requires 6 parameters or an array of parameter");
    end
  case 6
    joint = [varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6}];
  otherwise
    error("Requires either 6 parameters or an array of parameter");
endswitch

newJointPosition = "com.kuka.roboticsAPI.deviceModel.JointPosition";
joint  = javaObject(newJointPosition,joint);

end