%% Get Camera Data i.e. markerPosition, pointerPosition from 3D Slicer
function [ markerPosition, pointerPosition ] = getCameraData()

  % Obtain markerposition
  sd = igtlopen('localhost', 1004);
  if sd == -1
    error('Could not connect to the server.');
  else
    disp('Connected to the server.');
  end
  markerPosition = igtlreceive(sd);
  markerPosition = markerPosition.Trans;
  igtlclose(sd);
  
  % Obtain pointerposition
  sd = igtlopen('localhost', 1006);
  if sd == -1
    error('Could not connect to the server.');
  else
    disp('Connected to the server.');
  end
  pointerPosition = igtlreceive(sd);
  pointerPosition = pointerPosition.Trans;
  igtlclose(sd);
  
  
  