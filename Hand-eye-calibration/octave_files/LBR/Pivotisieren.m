function varargout = Pivotisieren(varargin)
% Pivotisieren M-file for Pivotisieren.fig
%      Pivotise Object by pointer: track pointer, add points etc
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Pivotisieren

% Last Modified by GUIDE v2.5 22-Jul-2010 10:01:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Pivotisieren_OpeningFcn, ...
                   'gui_OutputFcn',  @Pivotisieren_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Pivotisieren is made visible.
function Pivotisieren_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Pivotisieren (see VARARGIN)

% Choose default command line output for Pivotisieren
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Pivotisieren wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Pivotisieren_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in StopButton.
function StopButton_Callback(hObject, eventdata, handles)
% hObject    handle to StopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global caller
    if (isa(caller, 'timer'))
         stop(caller);
    end
    
    global ARTPointerList
    global ARTPatientList
    global ARTPatientRotList
    
    DispData(:,1:3)=ARTPointerList;
    DispData(:,4:6)=ARTPatientList;
    DispData(:,7:10)=ARTPatientRotList;
%    DispData(:,7:15)=reshape(QtoR(ARTPatientRotList),1,9);
    
    set(handles.ARTTable,'DATA',DispData)


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in StartButton.
function StartButton_Callback(hObject, eventdata, handles)
% hObject    handle to StartButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Starte Timer um ART Daten auzuzeichnen
    global ARTPointer
    global ARTPatient
    
%    ARTPointer=2
%    ARTPatient=4
       
    clear global ARTPointerList
    clear global ARTPointerRotList
    clear global ARTPatientList
    clear global ARTPatientRotList
    
    timedCallerHM(handles,0.05)


% --- Executes on slider movement.
function ARTPointer_Callback(hObject, eventdata, handles)
% hObject    handle to ARTPointer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global ARTPointer
ARTPointer = get(hObject,'Value');



% --- Executes during object creation, after setting all properties.
function ARTPointer_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ARTPointer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global ARTPointer
global ipNamingService;
ARTPointer = get(hObject,'Value');
[pos_p rot_p]=mex_art(ipNamingService,ARTPointer)

% --- Executes on slider movement.
function ARTPatient_Callback(hObject, eventdata, handles)
% hObject    handle to ARTPatient (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global ARTPatient
ARTPatient = get(hObject,'Value')
% --- Executes during object creation, after setting all properties.
function ARTPatient_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ARTPatient (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global ARTPatient
global ipNamingService
ARTPatient = get(hObject,'Value');
[pos rot]=mex_art(ipNamingService,ARTPatient)

% --- Executes on button press in addButton.
function addButton_Callback(hObject, eventdata, handles)
% hObject    handle to addButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ARTPointerList
global ARTPatientList
global ARTPatientRotList
global regPunkte



[cCen,cRad,npoints]=pivotise(ARTPointerList,ARTPatientList,ARTPatientRotList)
pos_matrix=size(regPunkte)
regPunkte(pos_matrix(1)+1,:)=cCen;

set(handles.PivTable,'DATA',regPunkte)


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global regPunkte
regPunkte
save('reg.mat','regPunkte')


% --- Executes on button press in loesche.
function loesche_Callback(hObject, eventdata, handles)
% hObject    handle to loesche (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear global regPunkte
global regPunkte
set(handles.PivTable,'DATA',regPunkte)


% --- Executes on button press in saveDaten.
function saveDaten_Callback(hObject, eventdata, handles)
% hObject    handle to saveDaten (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ARTPointerList
global ARTPointerRotList
ARTPointerList
save('DanielPointer.mat','ARTPointerList','ARTPointerRotList')


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
