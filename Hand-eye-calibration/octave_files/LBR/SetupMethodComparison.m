clc
clear all

numPositions = 25;

%% OCATVE_SLICER OpenIGTLink PATH

addpath D:\3D\Tool\MatlabOpenIGT;


%% Confirming Robot Connection
robot = javaObject('lbrApplications.ControlViaOctave');

%robotToFlangeFrame = robot.getCurrentCartesianPosition();

%% First motion is PTP motion to Home Position
home_axes = [4, -5.7, -42, 68, 23.3, -70, 1.8];
homePosition = robot.moveToJointPositionInDeg(home_axes(1), home_axes(2), home_axes(3), home_axes(4), home_axes(5), home_axes(6), home_axes(7));
%homePosition = robot.moveToPtp(12.66,55.78,-82.37,-108.99,144.43,39.68,-97.30);


markerPosition = [];
flangePosition = [];
mpHomoData = [];

%relPositions = randi([-6 6], numPositions, 6);
%relPositions = randi([-10 10], numPositions, 7);

%relPositions = ...
%[  -10   -1   -1   10   -8    2;
%   -1    0    2   -6   -3     1;
%    6   -5   -3    4    0     -4;
%   10    9    0   -4    6     1;
%   -5    6    3    3   -9     5;
%    0    1   -5   -6    2     2;
%   -2   -2    6    2   10     -4;
%    1   -2    0   -1   -3     2;
%    0   -5    7    8    2     1;
%   -7    6    9    3    1     1];
   
%relPositions = ...
%[  -5   -1   -1   1   -3    1;
%   -1    6    2   -2   2    1;
%    6   -5   -9    4    0   2;
%   10    9    0   -2    3   -5;
%   -5    -6    3    3   -2   2;
%    0    1   -5   -6    2   6;
%   -2   -2    6    2    4   -7;
%    1   -2    0   -7   -3   2;
%    0   -5    9    5    2   1;
%   -7    3    9    3    1   8];

%% small volume of 20x20x20, angles in a range between -35 and 35 degrees

%relPositions = [  5   -4   -2     2    9    -2;
%                  7   10   -18   -10   -6   -8;
%                 -4   6     5    -8    7    -24;
%                  5   -8    13    12   20   12;
%                  7   -18   2    -30   -3   -13;
%                 -5    7    7    -1    8    10;
%                 -4   -13   4    18   -40   20;
%                 -14   10   9   -18   -10   -14;
%                 -3    5    -1   5    -15    5;
%                 -10   -8   -15  15    5    29;  
%                  -4   -4   -8   35    9    20;
%                  7    10   -2   15    22   -10;
%                  4    20   -11  -7    3    -20;
%                  15   7    -3   -20   -16   10;
%                  1    -8    12  -8    -10  -23; %% end of training data
%                  -8   -8    3    -2    5   -6;
%                 -10  -7   10    10   -7   -6;
%                 -7   -10  -7    10    4   -13;
%                  10    6    8    7     7    9;
%                   8     3   11    -6    7    10;
%                  15    10  -5    -13   9    15;
%                  7     9   -10   -15  -13   9;
%                  10     10  -15   -3   -12    7;
%                  -7     4   -15   -11  -14   -10;
%                  -7     8   -5    -2   -11   -2]; %% end of test data
                 
%% large volume of 50x50x50, angles in a range between -35 and 35 degrees                 
relPositions = [  15   -4   -2     2    9   -2;
                  27   10   -48   -10   -6  -8;
                  8    16    5    -8    7   -24;
                 -15   -8    13    12   20   12;
                 -12   -18   22   -30   -3  -13;
                 -5    47    7    -1     8   10;
                 -4    -13   4     18   -40  20;
                 -14   20    9    -18   -10 -14;
                 -3    -35   -1    5    -20   5;
                 -30   -8    -15   15    5   29;  
                 -17   -14   -8    35    9   20;
                  7    -30   50    15    22  -10;
                  14   -13   14    -7    3   -20;
                  15    7    -3    -20  -16   10;
                  1     25   -12   -8   -10  -23; %% end of training data
                  -8   -8    3    -2    5    -6;
                  10    7    10    7    7    -6;
                   7   -5    -7    10     4    -13;
                  10    6    -8    7     7     9;
                   8     3   -11    -6   7    10;
                   5    10   -5    -13   9    15;
                  -7     9    -10   -15  -13   9;
                  10     10  -15   -3   -12    7;
                  -7     4   15   -11   10   -10;
                  -7     8   -5    -2   -11   -2];  %% end of test data
                  

% Home position
Flange_home = robot.getCurrentCartesianPosition();
home = [Flange_home.getX() Flange_home.getY() Flange_home.getZ() Flange_home.getAlphaRad() Flange_home.getBetaRad() Flange_home.getGammaRad()];
HFlange_home = CtoH(home);
HMarker_home = getCameraData2();         
                 
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\HFlange_home.txt', HFlange_home);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\HMarker_home.txt', HMarker_home);

%% test if random positions have sufficient rotation
cart_pos = []; 
tmp = zeros(1,6);
for i = 1:numPositions
  tmp += relPositions(i,:);
  cart_pos(:,:,i) = tmp;
end

min_alpha = min(cart_pos(:,4,:));
max_alpha = max(cart_pos(:,4,:));
disp('Range of alpha - min: '); disp(min_alpha); disp(' max:'); disp(max_alpha);
min_beta = min(cart_pos(:,5,:));
max_beta = max(cart_pos(:,5,:));
disp('Range of beta - min: '); disp(min_beta); disp(' max:'); disp(max_beta);
min_gamma = min(cart_pos(:,6,:));
max_gamma = max(cart_pos(:,6,:));
disp('Range of gamma - min: '); disp(min_gamma); disp(' max:'); disp(max_gamma);

min_x = min(cart_pos(:,1,:));
max_x = max(cart_pos(:,1,:));
disp('Range of x - min: '); disp(min_x); disp(' max:'); disp(max_x);
min_y = min(cart_pos(:,2,:));
max_y = max(cart_pos(:,2,:));
disp('Range of y - min: '); disp(min_y); disp(' max:'); disp(max_y);
min_z = min(cart_pos(:,3,:));
max_z = max(cart_pos(:,3,:));
disp('Range of z - min: '); disp(min_z); disp(' max:'); disp(max_z);
                 
%% Collect data to perform AX = ZB
% Different Flange and Marker positions relative to Robot and Camera respectively

k = 1;
for i=1:numPositions

%robot.moveToJointPositionInDeg(home_axes(1)+relPositions(i,1), home_axes(2)+relPositions(i,2), home_axes(3)+relPositions(i,3), home_axes(4)+relPositions(i,4), home_axes(5)+relPositions(i,5), home_axes(6)+relPositions(i,6), home_axes(7)+relPositions(i,7));
  disp(i);
  robot.moveRelativeLinToCurrentPosition( 
                 relPositions(i,1), 
                 relPositions(i,2), 
                 relPositions(i,3), 
                 relPositions(i,4), 
                 relPositions(i,5), 
                 relPositions(i,6) );
               
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
  
  % Pause to allow the Robot to settle down
  pause(5);
  
  markerPosition = getCameraData2();
  
%  [ markerPosition, pointerPosition ] = getCameraData();
 

  if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  %% save position only when the marker can be seen by the camera 
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
   end
end


disp('Data Collection Ended...')

%% Perform Marker-Flange and Camera-Robot Calibration 
[ l, m, n ] = size( mpHomoData );
numCamPoints = n;

[ p, q, r ] = size( fpCartData );
numRobotPoints = r;


if numCamPoints ~= numRobotPoints
    
    error('Number of position data from the Camera and Robot is NOT equal')

else
  
  for i=1:numCamPoints
    
    fpHomoData(:,:,i) = CtoH(fpCartData(:,:,i));
  
  end

end

disp('Number of saved positions: '); disp(numRobotPoints);

%% Reshape for 'dornaika' and 'shah'
Camera_to_Marker = reshape(mpHomoData, [4,4*numCamPoints]);
Robot_to_Flange = reshape(fpHomoData, [4,4*numCamPoints]);

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker_all_pos.txt', Camera_to_Marker);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange_all_pos.txt', Robot_to_Flange);


% Save absolute trafo matrices 
n = 15;
%% use first n entries for HEC
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker.txt', Camera_to_Marker(:,1:4*n));
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange.txt', Robot_to_Flange(:,1:4*n));

%% use remaining entries for the evaluation
if numCamPoints > n
  dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker_for_eval.txt', Camera_to_Marker(:,(4*n+1):4*numCamPoints));
  dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange_for_eval.txt', Robot_to_Flange(:,(4*n+1):4*numCamPoints));
end