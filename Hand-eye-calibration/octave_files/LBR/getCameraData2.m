%% Get Camera Data i.e. markerPosition, pointerPosition from 3D Slicer
function [ markerPosition ] = getCameraData2()

  % Obtain markerPosition
  sd = igtlopen('localhost', 1006);
  if sd == -1
    error('Could not connect to the server.');
  else
%    disp('Successfully got markerPosition from 3D SLICER.');
  end
  markerPosition = igtlreceive(sd);
  markerPosition = markerPosition.Trans;
  igtlclose(sd);
  