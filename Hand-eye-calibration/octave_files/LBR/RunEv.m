clc;
clear all;

%% Training data
Marker_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\17-08-2017\Camera_to_Marker.txt');
Flange_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\17-08-2017\Robot_to_Flange.txt');

%% Test data
Camera_to_Marker_fE = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\17-08-2017\Camera_to_Marker_for_eval.txt');
Robot_to_Flange_fE = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\17-08-2017\Robot_to_Flange_for_eval.txt');

%% Evaluation data
Marker_Positions_ev = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Tabb\slerp20deg\Marker_Positions.txt');
Flange_Positions_ev = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Tabb\slerp20deg\Flange_Positions.txt');

X = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Tabb\17-08-2017\X.txt');
Z = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Tabb\17-08-2017\Z.txt');

%% pivot movement evaluation
disp('pivot');
sigma = StdDevTip(Marker_Positions_ev)
norm_sigma = norm(sigma)
rot_piv = RMSE_rot(Marker_Positions_ev, Flange_Positions_ev, X, Z)

disp('all data');

t_tr = RMSE_trans(Marker_Positions, Flange_Positions, X, Z)
norm(t_tr)
ang_tr = RMSE_rot(Marker_Positions, Flange_Positions, X, Z)
t_test = RMSE_trans(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z)
norm(t_test)
ang_test = RMSE_rot(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z)
t_ev = RMSE_trans(Marker_Positions_ev, Flange_Positions_ev, X, Z)
norm(t_ev)
ang_ev = RMSE_rot(Marker_Positions_ev, Flange_Positions_ev, X, Z)

%disp('e_t');
%e_t(Marker_Positions, Flange_Positions, X, Z)
%e_t(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z)
%e_t(Marker_Positions_ev, Flange_Positions_ev, X, Z)

%disp('e_R2');
%e_R2(Marker_Positions, Flange_Positions, X, Z)
%e_R2(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z)
%e_R2(Marker_Positions_ev, Flange_Positions_ev, X, Z)

disp('e_c');
c_tr = e_c(Marker_Positions, Flange_Positions, X, Z)
c_test = e_c(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z)
c_ev = e_c(Marker_Positions_ev, Flange_Positions_ev, X, Z)