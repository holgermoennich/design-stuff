clc;
clear all;

Marker_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\slerp20deg\Marker_Positions.txt');
Flange_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\slerp20deg\Flange_Positions.txt');

Flange_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\slerp20deg\Flange_home.txt');
Marker_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\slerp20deg\Marker_home.txt');

X = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Dornaika\17-08-2017\X.txt');
Z = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Dornaika\17-08-2017\Z.txt');

[m,n] = size(Marker_Positions); % m = 4, n = 200

num_pos = n/4;

%% plotting

for j = 1:num_pos  
  s(:,j) = Flange_Positions(1:3, 4*j);
  t(:,j) = Marker_Positions(1:3,j*4);
end

%% plot of translations
%% plot of marker translation -> should be at one position
figure(1);
m0 = Marker_home(1:3,4);
scatter3 (t(1,:), t(2,:), t(3,:), "b", "filled");
hold on;
scatter3 (m0(1), m0(2), m0(3), "r", "x"); % plot marker origin
xlabel ("x in mm", 'fontsize', 14);
ylabel ("y in mm", 'fontsize', 14);
zlabel ("z in mm", 'fontsize', 14);
set(gca,'fontsize',12);
legend('Marker positions', 'Start position');
title ("Marker Translation", 'fontsize', 18);

%% plot of flange translation -> should be on a sphere around the marker and on an ellipse in a plane
figure(2);
f0 = Flange_home(1:3,4);
scatter3 (s(1,:), s(2,:), s(3,:), "b", "filled");
hold on;
%scatter3 (f0(1), f0(2), f0(3), "r", "filled"); % plot marker origin
xlabel ("x in mm", 'FontSize',16); 
ylabel ("y in mm", 'FontSize',16);
zlabel ("z in mm", 'FontSize',16);
title ("Flange Positions", 'FontSize',16);

