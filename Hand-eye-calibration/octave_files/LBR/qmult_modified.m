%% function multiplies two quaternions
%% input: 4 values of each quaternion 

function retval = qmult_modified (a, b)
  
  n = size(a);
  m = size(b);
 
  x0 = a(1);
  x1 = a(2);
  x2 = a(3);
  x3 = a(4);
  
  y0 = b(1); 
  y1 = b(2); 
  y2 = b(3); 
  y3 = b(4); 
 
  r0 = x0*y0-x1*y1-x2*y2-x3*y3;
  ri = x0*y1+x1*y0+x2*y3-x3*y2;
  rj = x0*y2-x1*y3+x2*y0+x3*y1;
  rk = x0*y3+x1*y2-x2*y1+x3*y0;
  
  retval = [r0, ri, rj, rk]';

end
