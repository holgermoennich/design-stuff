%% Takes the X (transformation between marker and robot flange) 
%% and rotates around the calculated tip/marker origin
%% -> marker origin should not move in an ideal case

clc;
clear all;

addpath D:\3D\Tool\MatlabOpenIGT;

%% Confirming Robot Connection
robot = javaObject('lbrApplications.ControlViaOctave');

%% X and Z from the hand-eye method 
X = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Tabb\X.txt');
Z = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Z.txt');

%% Home position where camera can see the marker
homePosition = robot.moveToJointPositionInDeg(4, -5.7, -42, 68, 23.3, -70, 1.8); % angles of axes

%% Homogeneous coordinates from home position
Flange_home = robot.getCurrentCartesianPosition();
home = [Flange_home.getX() Flange_home.getY() Flange_home.getZ() Flange_home.getAlphaRad() Flange_home.getBetaRad() Flange_home.getGammaRad()];
HFlange_home = CtoH(home);

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Flange_home.txt', HFlange_home);

Base_to_Marker_home = HFlange_home*inv(X);

%% Home position of the marker
HMarker_home = getCameraData2(); 

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Marker_home.txt', HMarker_home);

%% Number of measurements
num_meas = 50; 

i = 1;
k = 1;

Rot_angle = eye(3);

r = 10; 
%rand1 = randi([-r, r], num_meas, 3);

rand1 = ...
  [-5    5   -7;
    3   -4    6;
    1   -8   -1;
   -8   -3   -8;
    4    0    4;
   -6    4   -5;
   -4   -2   -1;
    9    8   -1;
    7    0    2;
    2   -8   -2;
   -9  -10   -6;
   10   -9    8;
    6   -5    0;
  -10    3   -5;
    1   -4    9;
    1    8    0;
   -2    1   -4;
    6   -6  -10;
   10   -1   10;
    0    2    3;
   -1    9    1;
   -5    8    7;
    9   -8    4;
   -3    9   -9;
   -9    8    3;
   -8    5  -10;
   -5   -9   -6;
   -6    9    1;
    3   10    5;
    1    9    0;
    3   -6    9;
    4   10    4;
    0    2    4;
    2   -3   -4;
    9  -10    8;
   -1   -2    9;
   -2   -6    2;
  -10    0   -5;
   -8   -8   -2;
   -3    6    0;
    3    1    3;
    4   -3    3;
   -8   10   10;
    0   -5    8;
    1   -2   -2;
    5    9    1;
    9   -4    0;
  -10    2  -10;
    4    1    7;
    8    6   -1];

%% Move robot around the marker and track the marker
for phi = (2*pi/num_meas):(2*pi/num_meas):(2*pi)
    
  %% random positions on a sphere
  
  Rot_angle = EtoR([rand1(i,1)*pi/180, rand1(i,2)*pi/180 , rand1(i,3)*pi/180], 'Rob');
  Matrix_angle = [Rot_angle, [0,0,0]';0,0,0,1]; % change of rotation as transformation matrix
  NewPos_of_Tip = Base_to_Marker_home*Matrix_angle;
  Base_to_Marker(:,:,i) = NewPos_of_Tip; % expected orientation of the marker in robot frame
  NewPos_of_Flange = NewPos_of_Tip*X;
  
  B_t_P = NewPos_of_Flange(1:3,4); % Base to Flange translation
  R = NewPos_of_Flange(1:3,1:3);
  
  rot_flange = RtoE(R, 'Rob');
  
  robot.moveToCartesianPosition(frame(B_t_P(1), B_t_P(2), B_t_P(3), rot_flange(1)*pi/180, rot_flange(2)*pi/180, rot_flange(3)*pi/180));
 
  % Pause to allow the Robot to settle down
  pause(5);
  
  robotToFlangeFrame = robot.getCurrentCartesianPosition();
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
  
  % Camera data
  markerPosition = getCameraData2();
  
  
  if i == 1
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    k += 1;
  end
  
  previous = mpHomoData(:,:,k-1);
  
  if (i > 1) && (markerPosition(1:3,1:3) != previous(1:3,1:3))
    mpHomoData(:,:,k) = markerPosition;
    fpCartData(:,:,k) = flangePosition;
    
    k += 1;
   end
  
  i += 1;
end

[ l, m, n ] = size( mpHomoData );
numCamPoints = n;

[ p, q, r ] = size( fpCartData );
numRobotPoints = r;


if numCamPoints ~= numRobotPoints
    
    error('Number of position data from the Camera and Robot is NOT equal')

else
  
  for i=1:numCamPoints
    
    fpHomoData(:,:,i) = CtoH(fpCartData(:,:,i));
    Base_to_Marker(:,:,i) = fpHomoData(:,:,i)*inv(X);
  end

end


Marker_Positions = reshape(mpHomoData, [4, 4*numCamPoints]);
Flange_Positions = reshape(fpHomoData, [4, 4*numCamPoints]);
Base_to_Marker_Positions = reshape(Base_to_Marker, [4, 4*numCamPoints]);

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Marker_Positions.txt', Marker_Positions);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Flange_Positions.txt', Flange_Positions); 
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Base_to_Marker.txt', Base_to_Marker_Positions);