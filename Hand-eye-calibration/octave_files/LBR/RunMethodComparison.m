%% Perform Marker-Flange and Camera-Robot Calibration

clc;
clear all;

HFlange_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\HFlange_home.txt');
HMarker_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\HMarker_home.txt');

Camera_to_Marker = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker.txt');
Robot_to_Flange = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange.txt');

%% Number of measurements 
[ m, n ] = size( Camera_to_Marker );
numCamPoints = n/4;

[ q, r ] = size( Robot_to_Flange );
numRobotPoints = r/4;

if numCamPoints ~= numRobotPoints    
    error('Number of position data from the Camera and Robot is NOT equal')
end

%% Change the direction of Camera_to_Marker (like the setup in Daniilidis)
for i = 1:numCamPoints
  A_i = Camera_to_Marker(:, 4*i-3:4*i);
  Marker_to_Camera(:, 4*i-3:4*i) = inv(A_i);
end

%% Relative movements with ten entries; A = A{i+1}*inv(A{i}), B = inv(B{i+1})*B{i} 
A(:,:,1) = Marker_to_Camera(:,1:4)*HMarker_home;
B(:,:,1) = inv(Robot_to_Flange(:,1:4))*HFlange_home;
for i=2:numCamPoints
  A(:,:,i) = Marker_to_Camera(:,4*i-3:4*i)*inv(Marker_to_Camera(:,4*(i-1)-3:4*(i-1)));
  B(:,:,i) = inv(Robot_to_Flange(:,4*i-3:4*i))*Robot_to_Flange(:,4*(i-1)-3:4*(i-1));
end

%% Reshape for 'daniiliids'
Camera_Movements = reshape(A, [4,4*numCamPoints]);
Flange_Movements = reshape(B, [4,4*numRobotPoints]);

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_Movements.txt', Camera_Movements);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Flange_Movements.txt', Flange_Movements);

%% Calculating results with absolute movement
disp( 'absolute movement...' )
disp( 'saving Dornaika Results' )
[Marker_to_Flange, Camera_to_Robot] = dornaika(Camera_to_Marker, Robot_to_Flange);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Dornaika\X.txt', Marker_to_Flange);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Dornaika\Z.txt', Camera_to_Robot);
disp( 'Marker_to_Flange - NORM: ' ); disp( norm( Marker_to_Flange( 1:3, 4 ) ) );
disp( 'Camera_to_Robot - NORM: ' ); disp( norm( Camera_to_Robot( 1:3, 4 ) ) );

disp( 'saving Shah Results' )
[Marker_to_Flange2, Camera_to_Robot2] = shah(Camera_to_Marker, Robot_to_Flange);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Shah\X.txt', Marker_to_Flange2);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Shah\Z.txt', Camera_to_Robot2);
disp( 'Marker_to_Flange - NORM: ' ); disp( norm( Marker_to_Flange2( 1:3, 4) ) );
disp( 'Camera_to_Robot - NORM: ' ); disp( norm( Camera_to_Robot2( 1:3, 4 ) ) );

disp( 'saving Daniilidis Results' )
Marker_to_Flange3 = daniilidis(Camera_Movements, Flange_Movements);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Daniilidis\X.txt', Marker_to_Flange3);

t_Zi = 0;

Q = zeros(numCamPoints,4);

 for i = 1:numCamPoints
  T_A = Camera_to_Marker(:,4*i-3:4*i);
  T_B = Robot_to_Flange(:,4*i-3:4*i);
  R_A = T_A(1:3, 1:3);
  R_B = T_B(1:3, 1:3);
  t_A = T_A(1:3,4);
  t_B = T_B(1:3,4);

  Camera_to_Robot3 = T_A*Marker_to_Flange3*inv(T_B);

  q_Zi = rotm2q(Camera_to_Robot3(1:3,1:3));

  Q(i,1) = q_Zi.w;
  Q(i,2) = q_Zi.x;
  Q(i,3) = q_Zi.y;
  Q(i,4) = q_Zi.z;

  t_Zi += Camera_to_Robot3(1:3,4);
end

weights = ones(numCamPoints,1);
q_aver = quatWAvgMarkley(Q, weights);

q_Z = quaternion(q_aver(1), q_aver(2), q_aver(3), q_aver(4));

[v, theta] = q2rot(q_Z);
Z = rotv(v', theta);
t_Z = t_Zi/numCamPoints;
Z = [Z', t_Z; 0 0 0 1];

%Camera_to_Robot3 = Camera_to_Marker(1:4,1:4)*Marker_to_Flange3*inv(Robot_to_Flange(1:4,1:4));
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Daniilidis\Z.txt', Z);
disp( 'Marker_to_Flange - NORM: ' ); disp( norm( Marker_to_Flange3( 1:3, 4) ) );
disp( 'Camera_to_Robot - NORM: ' ); disp( norm( Z( 1:3, 4 ) ) );

%% Relative movements
%% Calculating results with relative movement for 'dornaika' and 'shah'
%% Each position is thought to start from the home position (multiplication with the inverse of the home position)
disp( 'relative movement...' )
for i = 1: numCamPoints
  Camera_to_Marker_rel(:,i*4-3:i*4) = inv(HMarker_home)*Camera_to_Marker(:,i*4-3:i*4);
  Robot_to_Flange_rel(:,i*4-3:i*4) = inv(HFlange_home)*Robot_to_Flange(:,i*4-3:i*4);
end

%% Save relative trafo matrices
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker_rel.txt', Camera_to_Marker_rel);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange_rel.txt', Robot_to_Flange_rel);

disp( 'saving Dornaika Results' )
[Marker_to_Flange4, Camera_to_Robot4] = dornaika( Camera_to_Marker_rel, Robot_to_Flange_rel );
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Dornaika\X_rel.txt', Marker_to_Flange4);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Dornaika\Z_rel.txt', HMarker_home*Camera_to_Robot4*inv(HFlange_home));

disp( 'saving Shah Results' )
[Marker_to_Flange5, Camera_to_Robot5] = shah( Camera_to_Marker_rel, Robot_to_Flange_rel );
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Shah\X_rel.txt', Marker_to_Flange5);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Shah\Z_rel.txt', HMarker_home*Camera_to_Robot5*inv(HFlange_home));

%% delete the old result directory and create a new one
 
rmdir('D:\3D\Tool\MatlabOpenIGT\results\Tabb\results', "s");
mkdir('D:\3D\Tool\MatlabOpenIGT\results\Tabb\results');

%% calculating results of iterative methods (Tabb)
%% Converts the TrafoMatrices for the iterative method in C++ 
l = 1;
for i = 1:numRobotPoints
  for j = 1:4
    for k = i*4-3:i*4
    conv_Robot_to_Flange(l) = Robot_to_Flange(j,k);
    l++;
    end
  end
end

%% save Bs as robot_cali.txt (required as input for C++)
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp\robot_cali.txt', numRobotPoints);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp\robot_cali.txt', conv_Robot_to_Flange, " ", "-append");

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp\num_meas.txt', numRobotPoints);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp\As.txt', Camera_to_Marker);

%% run cmd with C++ Code
dos('D:\iterative_method\Tabb\bin\Debug\Tabb_AhmadYousef_RWHEC_Jan2017_Project.exe D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp D:\3D\Tool\MatlabOpenIGT\results\Tabb\results 1');

%% saving results in the same convention as the other methods
Transformations = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Tabb\results\rwhe_Q_c1_separable\transformations.txt');
[m, n] = size(Transformations);

X = Transformations(2:5, 1:4);
Z = Transformations(7:10, 1:4);

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\X.txt',X);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Z.txt',Z);