%% Pivot calibration 
%% reads the transformation matrizes "NeedleToTracker" from slicer
%% 
clc;
clear all;

num_of_meas = 50;

trans_needle = zeros(4*num_of_meas,4);

for x = 0:num_of_meas-1
  sd = igtlopen('localhost', 18944);
  r = igtlreceive(sd, 100);
  disp(r);
  trans_needle(x*4+1:x*4+4,:) = r.Trans;
  pause(1);
  igtlclose(sd);
end


k = 1;
xyz = zeros(num_of_meas,3);

%% matrix needed for sphereFit
for i = 1:4:4*num_of_meas
  xyz_as_col = trans_needle(i:i+2,4);
  xyz(k,:) = xyz_as_col';
  k++;
end

erg_sphere = sphereFit(xyz);

%% Matrix for Least square fit (s.IGSTK S.227)
R = zeros(3*num_of_meas,6);
i = 0;
for x = 1:4:4*num_of_meas
  R(x-i:x-i+2,1:3) = trans_needle(x:x+2,1:3); 
  R(x-i:x-i+2,4:6) = [[-1,0,0];[0,-1,0];[0,0,-1]];
  i++;
end

xyz_c = reshape(xyz, [1,3*num_of_meas]); % alle Koordinaten der Messungen als Spaltenvektor
erg_pivot_igstk = R\(-1*xyz_c)';
t_tip = sqrt(sum(erg_pivot_igstk(1:3).^2)); %radius
center_igstk = erg_pivot_igstk(4:6); 
disp('center/t_fixed: '); disp(center_igstk); %% noch falsch
disp('radius: '); disp(t_tip); %% erwartet: 120
%% rms
residuals_igstk = t_tip - sqrt(sum(bsxfun(@minus,[xyz(:,1) xyz(:,2) xyz(:,3)],center_igstk.').^2,2));
rms_igstk = sqrt((sum(residuals_igstk.^2))/num_of_meas);
