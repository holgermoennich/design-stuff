clc
clear all

%%
numPositions = 10;

%% OCATVE_SLICER OpenIGTLink PATH

addpath D:\3D\Tool\MatlabOpenIGT;
%addpath D:\Camera_Integration\008_CameraRobotCalibration\tryLBR\OctaveOpenIGT;

%% Confirming Robot Connection
robot = javaObject('lbrExampleApplications.Octave');
robot.Position;
initialPosition = robot.Position.get();

robotToFlangeFrame = robot.getCurrentCartesianPos();


%% First motion is PTP motion to Home Position
%homePosition = robot.moveToPtp(79.03,34.20,-15.86,-52.28,53.30,31.70,-40.11);
%homePosition = robot.moveToPtp(12.66,55.78,-82.37,-108.99,144.43,39.68,-97.30);
%homePosition = robot.moveToPtp(11.78,1.24,-13.98,-109.64,160.22,18.50,-162.69);
homePosition = robot.moveToPtp(26,-30.4,0.4,100,50.3,27.3,-25.4);
%robot.moveRel(3,    9,   -2,    5,   -4,   -2 );
%pause(2);
%robot.moveRel( 5,    6,  -10,    7,   10,   -1);
%pause(2);
%robot.moveRel(8,    9,    4,    4,    6,    0);
%pause(2);
%robot.moveRel(-2,   -6,    0,    5,   -8,   -3);
%pause(2);
%robot.moveRel(-8,    1,   -3,    1,    8,    2);
%pause(2);
%robot.moveRel(7,   -4,  -10,   -5,    2,   -2);
%pause(2);
%robot.moveRel(-8,   -4,   -2,   -4,    2,   -4);
%pause(2);
%robot.moveRel(-3,    3,    4,    4,    8,    3);
%pause(2);
%robot.moveRel(9,  -10,    5,   -3,    1,    1);
%pause(2);
%robot.moveRel(-4,    2,   -1,   10,   -8,   -5);


markerPosition = [];
flangePosition = [];


%relPositions=[ -60 0 0 0 0 0;
%                  0 -60 0 0 0 0;
%                  0 0 -60 0 0 0;
%                  0 50 0 20 0 0;
%                  0 0 50 0 0 20;
%                  50 0 0 0 20 0;
%                  20 20 20 -10 -10 -10;
%                  -30 -30 -30 0 0 0;
%                  60 0 -50 0 0 0;
%                  50 10 50 0 0 0];


%relPositions = randi([-10 10], numPositions, 6);
relPositions = [  3    9   -2    5   -4   -2;
                  5    6  -10    7   10   -1;
                  8    9    4    4    6    0;
                 -2   -6    0    5   -8   -3;
                 -8    1   -3    1    8    2;
                  7   -4  -10   -5    2   -2;
                 -8   -4   -2   -4    2   4;
                 -3    3    4    4    -2    3;
                  9  -10    5   -3    -7    1;
                 -4    2   -1   10   -8   -5  ];

%relPositions = [  2    9   -2    5   -4   -2;
%                  -3   -6  -8    7   10   -1;
%                  8    7    4    4    6    0;
%                 -6   -5    2    5   -8   -3;
%                 -1   -3   -3    1    8    2;
%                  7   2    10   -5    2   -2;
%                 -8   -1   -2   -4    2   4;
%                 2    4     4    4    -2    3;
%                 -5   -2    5   -3    5    1;
%                 5    -1   -1   10   -8   -5  ];

%relPositions = [  3    9   -2    5   -4   -2;
%                  5    6  -10    7   10   -1;
%                  8    9    4    4    6    0;
%                 -2   -6    0    5   -8   -3;
%                 -8    1   -3    1    8    2;
%                  7   -4  -10   -5    2   -2;
%                 -8   -4   -2   -4    2   -4;
%                 -3    3    4    4    8    3;
%                  9   10    5   -3   -8    1;
%                 -4    2   -1   10   -8   -5  ];

%relPositions = ...
%[  -10   -1   -1   10   -8    2;
%   -1    0    2   -6   -3     1;
%    6   -5   -3    4    0     -4;
%   10    9    0   -4    6     1;
%   -5    6    3    3   -9     5;
%    0    1   -5   -6    2     2;
%   -2   -2    6    2   10     -4;
%    1   -2    0   -1   -3     2;
%    0   -5    7    8    2     1;
%   -7    6    9    3    1     1];

%% Collect data to perform AX = ZB
% Different Flange and Marker positions relative to Robot and Camera respectively
for i=1:numPositions

  robot.moveRel( relPositions(i,1), 
                 relPositions(i,2), 
                 relPositions(i,3), 
                 relPositions(i,4), 
                 relPositions(i,5), 
                 relPositions(i,6) );
               
  robotToFlangeFrame = robot.getCurrentCartesianPos();
  
  flangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];
  
  % Pause to allow the Robot to settle down
  pause(3)
  
   markerPosition = getCameraData2();
  
%  [ markerPosition, pointerPosition ] = getCameraData();

  mpHomoData(:,:,i) = markerPosition;
  fpCartData(:,:,i) = flangePosition;

end


disp('Data Collection Ended...')

%% Perform Marker-Flange and Camera-Robot Calibration 
[ l, m, n ] = size( mpHomoData );
numCamPoints = n;

[ p, q, r ] = size( fpCartData );
numRobotPoints = r

if numCamPoints ~= numRobotPoints
    
    error('Number of position data from the Camera and Robot is NOT equal')

else
  
  for i=1:numCamPoints
    
    fpHomoData(:,:,i) = CtoH(fpCartData(:,:,i));
  
  end

end


%% Reshape for 'dornaika'
Camera_to_Marker = reshape( mpHomoData, [ 4, 4*numCamPoints ] );
Robot_to_Flange = reshape( fpHomoData, [ 4, 4*numCamPoints ] );

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker.txt', Camera_to_Marker);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange.txt', Robot_to_Flange);

disp( 'Dornaika Results:' )
[Marker_to_Flange, Camera_to_Robot] = dornaika( Camera_to_Marker, Robot_to_Flange );

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Dornaika\X.txt', Marker_to_Flange);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Dornaika\Z.txt', Camera_to_Robot);

disp( 'Camera_to_Robot: ' ); disp( Camera_to_Robot ); 
disp( 'Camera_to_Robot - XYZ: ' ); disp( Camera_to_Robot( 1:3, 4 ) ); 
disp( 'Camera_to_Robot - NORM: ' ); disp( norm( Camera_to_Robot( 1:3, 4 ) ) );
Robot_to_Camera = inv( Camera_to_Robot );

disp( 'Marker_to_Flange: ' ); disp( Marker_to_Flange ); 
disp( 'Marker_to_Flange - XYZ: ' ); disp( Marker_to_Flange( 1:3, 4 ) ); 
disp( 'Marker_to_Flange - NORM: ' ); disp( norm( Marker_to_Flange( 1:3, 4) ) );
Flange_to_Marker = inv( Marker_to_Flange );



%% Get Marker_to_ToolTip and ToolTip_to_Flange transform 
%[ Marker_to_ToolTip, ToolTip_to_Flange ] = getRobotToolTip( Camera_to_Robot, robot );
%
%disp( 'Marker_to_ToolTip: ' ); disp( Marker_to_ToolTip ); 
%disp( 'Marker_to_ToolTip - XYZ: ' ); disp( Marker_to_ToolTip( 1:3, 4)); 
%disp( 'Marker_to_ToolTip - NORM: ' ); disp( norm( Marker_to_ToolTip( 1:3, 4) ) );
%
%disp( 'ToolTip_to_Flange: ' ); disp( ToolTip_to_Flange ); 
%disp( 'ToolTip_to_Flange - XYZ: ' ); disp( ToolTip_to_Flange( 1:3, 4) ); 
%disp( 'ToolTip_to_Flange - NORM: ' ); disp( norm( ToolTip_to_Flange( 1:3, 4) ) );
%
%save fixedTransforms.mat 'Camera_to_Robot' 'Robot_to_Camera' 'Marker_to_Flange' 'Flange_to_Marker' 'Marker_to_ToolTip' 'ToolTip_to_Flange'


