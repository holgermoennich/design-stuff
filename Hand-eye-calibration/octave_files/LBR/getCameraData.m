%% Get Camera Data i.e. markerPosition, pointerPosition from 3D Slicer
function [ markerPosition, pointerPosition, Pointer_to_PointerTip ] = getCameraData()

  % Obtain markerPosition
  sd = igtlopen('localhost', 1006);
  if sd == -1
    error('Could not connect to the server.');
  else
%    disp('Successfully got markerPosition from 3D SLICER.');
  end
  markerPosition = igtlreceive(sd);
  markerPosition = markerPosition.Trans;
  igtlclose(sd);
  
  % Obtain pointerPosition
  sd = igtlopen('localhost', 1007);
  if sd == -1
    error('Could not connect to the server.');
  else
%    disp('Successfully got pointerPosition from 3D SLICER.');
  end
  pointerPosition = igtlreceive(sd);
  pointerPosition = pointerPosition.Trans;
  igtlclose(sd);
  
  % Obtain Pointer_to_PointerTip
  sd = igtlopen('localhost', 1008);
  if sd == -1
    error('Could not connect to the server.');
  else
%    disp('Successfully got Pointer_to_PointerTip from 3D SLICER.');
  end
  Pointer_to_PointerTip = igtlreceive(sd);
  Pointer_to_PointerTip = Pointer_to_PointerTip.Trans;
  igtlclose(sd);
  
  
  
  
