function [ cCen, cRad, npoints ] = pivotise( points, coordrelPos, coordrelOr )
%PIVOTISE the points with respect to the bodies coordinate system coordrel
%   row vectors, orientation in quaternions

rows=size(points, 1);
npoints=zeros(rows,3);
r=zeros(rows,1);
D=zeros(rows,4);
niter=20;
res=zeros(niter,4);

% transformation in local body coordinate system
for i=1:rows
    coordrelOr(i,:);
    points(i,:);
    coordrelPos(i,:);
    npoints(i,:)=quat2rot(coordrelOr(i,:))'*(points(i,:)'-coordrelPos(i,:)'); 
end

% starting values for sphere parameters
cCen=mean(npoints)';
%cRad=norm(std(npoints));
cRad=160;

% iterate values for radius and center of sphere
for i=1:niter
    %iterations of Gauss-Newton-algorithm
    for n=1:rows        %build residual function and Jacobi-matrix
        PosX=npoints(n,1);
        PosY=npoints(n,2);
        PosZ=npoints(n,3);
        
        r(n,1)=cRad^2-(PosX-cCen(1))^2-(PosY-cCen(2))^2-(PosZ-cCen(3))^2;
        D(n,:)=2*[cRad PosX-cCen(1) PosY-cCen(2) PosZ-cCen(3)];
    end
    
   s=pinv(D'*D)*D'*r;
   cCen=cCen-s(2:4);
   %cRad=cRad-s(1);
   %res noch zu, dann toll
   res(i,:)=[cCen; cRad'];
end
plot(res);
  