%% Calculation of translational and rotational errors

clc;
clear all;

%Marker_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Marker_Positions.txt');
%Flange_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Flange_Positions.txt');
%Expected_Base_to_Marker = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Base_to_Marker.txt');
%
%Flange_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Flange_home.txt'); % Set as ground truth
%Marker_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Marker_home.txt'); % Set as ground truth
%
%X = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\X.txt');
%Z = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Z.txt');

%% Evaluation data
Marker_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Marker_Positions.txt');
Flange_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Flange_Positions.txt');
Expected_Base_to_Marker = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Base_to_Marker.txt');

Flange_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Flange_home.txt'); % Set as ground truth
Marker_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Marker_home.txt'); % Set as ground truth

%Marker_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\random\Marker_Positions.txt');
%Flange_Positions = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\random\Flange_Positions.txt');
%Expected_Base_to_Marker = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\random\Base_to_Marker.txt');
%
%Flange_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\random\Flange_home.txt'); % Set as ground truth
%Marker_home = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\17-08-2017\Dornaika\random\Marker_home.txt'); 


%% 'X' from 'EvaluateMethodComparison.m'
X = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Tabb\X.txt');
Z = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Z.txt');

[m,n] = size(Marker_Positions); % m = 4, n = 200
num_pos = n/4;

%%%% errors based on the evaluation (pivot movement)
%% Translational rms error of the marker deviation
rms_trans = transDevOfTip(Marker_home, Marker_Positions);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Trans_rms_error.txt', rms_trans);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Trans_rms_error.txt', norm(rms_trans), " ", "-append");

%% rms error calculated from expected orientation and measured orientation in camera frame
[rms_t, rms_angle] = rmsTransRot(Marker_Positions, Flange_Positions, X, Z);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Rot_rms_error_includ_Z.txt', rms_angle);

%% plotting
%b_H_m = Flange_home*inv(X);
for j = 1:num_pos  
  s(:,j) = Flange_Positions(1:3, 4*j);
  t(:,j) = Marker_Positions(1:3,4*j);
  
%  %% for pivot fit
%  F_H_b = inv(Flange_Positions(:, 4*j-3:4*j));
%  F_H_m = F_H_b*b_H_m;
%  m_H_F = inv(F_H_m);
%  m_t_F(:,j) = m_H_F(1:3,4);
end

%% plot of translations
%% plot of marker translation -> should be at one position
figure(1);
m0 = Marker_home(1:3,4);
scatter3 (t(1,:), t(2,:), t(3,:), "b", "filled");
hold on;
scatter3 (m0(1), m0(2), m0(3), "r", "filled"); % plot marker origin
xlabel ("x");
ylabel ("y");
zlabel ("z");
title ("Marker translation");

%% plot of flange translation -> should be on a sphere around the marker
figure(2);
f0 = Flange_home(1:3,4);
scatter3 (s(1,:), s(2,:), s(3,:), "b", "filled");
hold on;
scatter3 (f0(1), f0(2), f0(3), "r", "filled"); % plot marker origin
xlabel ("x"); 
ylabel ("y");
zlabel ("z");
title ("Flange translation");

%% fit flange positions on a sphere 
[center, radius] = sphereFit(s');
exp_c = Flange_home*inv(X);
disp('Expected center w.r.t. base: '); disp(exp_c(1:3,4));
disp('Calculated center w.r.t. base: '); disp(center);
disp('Expected radius: '); disp(norm(X(1:3,4)));
disp('Calculated radius: '); disp(radius);

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\SphereFit\Expected_center.txt', exp_c(1:3,4));
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\SphereFit\Calculated_center.txt', center);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\SphereFit\Expected_radius.txt', norm(X(1:3,4)));
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\SphereFit\Calculated_radius.txt', radius);

[ cCen, cRad] = pivotise2(s'); 
disp('cCen: '); disp(cCen);
disp('cRad: '); disp(cRad);
disp('norm of translation of X: '); disp(norm(X(1:3,4)));

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\PivotFit\center.txt', cCen);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\PivotFit\radius.txt', cRad);


%% plot frames and rotation axes of flange and marker
for m = 1:num_pos
  %% Trafo matrices
  Traf_Marker = Marker_Positions(1:4, 4*m-3:4*m);
  Traf_Flange = Flange_Positions(1:4, 4*m-3:4*m);
 
  %% Rotation matrices
  Rot_Marker = Traf_Marker(1:3, 1:3);
  Rot_Flange = Traf_Flange(1:3, 1:3);
  
  [w,a] = to_axis(Rot_Marker);
  rot_mj(m) = a;
  axes_mj(1:3,m) = w;
  
  [z,b] = to_axis(Rot_Flange);
  rot_flj(m) = b;
  axes_fl(1:3,m) = z;
   
  figure(3);
  title("Coordinate frames of Marker w.r.t. Camera");
  hold on;
  scale = 10;
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,1))', "r");
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,2))', "g");
  drawVector3d((Traf_Marker(1:3,4))', scale*(Rot_Marker(:,3))', "b");
  xlabel ("x"); 
  ylabel ("y");
  zlabel ("z");
  view(3);
  
  figure(4);
  title("Coordinate frames of Flange w.r.t. Base");
  hold on;
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,1))', "r");
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,2))', "g");
  drawVector3d((Traf_Flange(1:3,4))', scale*(Rot_Flange(:,3))', "b");
  xlabel ("x"); 
  ylabel ("y");
  zlabel ("z");
  view(3);
end

figure(5);
arrow3(zeros(num_pos,3), axes_fl');
title("Rotation axes Base to Flange");

figure(6);
arrow3(zeros(num_pos,3), axes_mj');
title("Rotation axes Camera to Marker");

figure(7);
n = 2*pi/num_pos:2*pi/num_pos:2*pi;
plot(n, rot_flj);
xlabel("Evaluation points");
ylabel("Rotation angle");
title("Rotation angles Base to Flange");

figure(8);
n = 2*pi/num_pos:2*pi/num_pos:2*pi;
plot(n, rot_mj);
xlabel("Evaluation points");
ylabel("Rotation angle");
title("Rotation angles Base to Flange");

%%%% errors based on independent measurements (Test Data)
Camera_to_Marker_fE = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker_for_eval.txt');
Robot_to_Flange_fE = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange_for_eval.txt');

%% mean rotational error in degrees
err_R2 = e_R2(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_R2_testData.txt', err_R2);

%% mean translational error in mm
%err_t = e_t(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
%dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_t_testData.txt', err_t);

%% mean combined rotation and translation error without units
err_c = e_c(Camera_to_Marker_fE, Robot_to_Flange_fE, X, Z);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_c_testData.txt', err_c);

%% errors based on Evaluation Data
err_R2 = e_R2(Marker_Positions, Flange_Positions, X, Z);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_R2_evalData.txt', err_R2);

%% mean translational error in mm
%err_t = e_t(Marker_Positions, Flange_Positions, X, Z);
%dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_t_evalData.txt', err_t);

%% mean combined rotation and translation error without units
err_c = e_c(Marker_Positions, Flange_Positions, X, Z);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_c_evalData.txt', err_c);

%% errors based on Training Data
Marker_Positions_TrDat = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker.txt');
Flange_Positions_TrDat = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange.txt');

err_R2 = e_R2(Marker_Positions_TrDat, Flange_Positions_TrDat, X, Z);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_R2_trainData.txt', err_R2);

%% mean translational error in mm
%err_t = e_t(Marker_Positions_TrDat, Flange_Positions_TrDat, X, Z);
%dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_t_trainData.txt', err_t);

%% mean combined rotation and translation error without units
err_c = e_c(Marker_Positions_TrDat, Flange_Positions_TrDat, X, Z);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\e_c_trainData.txt', err_c);