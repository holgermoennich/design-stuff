function [w,a] = to_axis(R)
% [w,a] = to_axis(R) calculates the rotation axis
% w and the angle of rotation a about this axis (in degrees)
% from the rotation matrix R. The axis is a 3-element column vector
% of unit length. The angle of rotation is in the range [0,180 deg].
% The rotation appears counterclockwise when light from the object
% to the observer moves in the direction of the axis.
% 
% Author: J. M. Fitzpatrick, Vanderbilt University,
% Nashville TN, US

if ndims(R) ~= 2 || size(R,1) ~=3 || size(R,2) ~= 3
    error('R must be 3x3');
end
abs_diff_from_I = abs(R'*R-eye(3));
if sum(abs_diff_from_I)> 10^(-5)%100*eps
    error('R must be orthogonal (R''R = eye(3))');
end

w = zeros(3,1); % Force it to a column vector
% Check to see whether R is approximiately the identity:
a = acos((trace(R) - 1)/2); % NOTE: a is in [0,pi] here
if a < 100*eps
    w = [0;0;1]; a = 0;
    return
end

% Two cases remain: 
%   (1)  a ~= pi, so sina not = 0
%   (2)  a == pi, so sina == 0
sina = sin(a); 
if sina > 100*eps    % Note: sin(a) > 0 for a in [0,pi].
    % Case (1) 
    Rd = R-R'; % Since sina not = 0, Rd not = 0.
    w(1) = Rd(3,2);
    w(2) = Rd(1,3);
    w(3) = Rd(2,1);
    w = w/norm(w);
else
    % Case (2) a = pi, so R-R' = 0, and we use R itself to find w:
    [Y,I] = max(diag(R));
    w(I) = sqrt( (R(I,I)+1)/2 );
    J = mod(I , 3) + 1;
    K = mod(I + 1, 3) + 1;
    w(J) = R(I,J)/w(I);
    w(K) = R(I,K)/w(I);
end

% The axis can almost be found (in both cases) by utlizing the eigenvector 
% of R whose eigenvalue has the largest real part (= 1):
%     [V,D] = eig(R);
%     [Y,I] = max(real(diag(D)));  % Find the eigenvalue that equals 1
%     if abs(D(I,I) - 1) > 100*eps % Threshold of 10*eps is too small.
%         error('Max real eigenvalue not equal to 1!');
%     end
%     w = V(:,I);
% The only problem is that the sense is arbitrary.

    to_degrees = 180/pi;
    a = a*to_degrees;
