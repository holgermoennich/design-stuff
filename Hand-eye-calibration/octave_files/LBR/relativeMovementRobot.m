function [ relFlangePoseDiff ] = relativeMovementRobot( newMarkerPosition, Camera_to_Robot, Marker_to_Flange, robot)

% Calculate the Position of the Robot from the Camera Body
targetFlangePose = inv(inv(newMarkerPosition) * Camera_to_Robot) * Marker_to_Flange; % newRobot_to_Flange = inv(inv(Camera_to_startMarker) * Camera_to_Robot) * Marker_to_Flange
robotToFlangeFrame = robot.getCurrentCartesianPos();
startFlangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];

% Calculate the error
x = robotToFlangeFrame.getX();
y = robotToFlangeFrame.getY();
z = robotToFlangeFrame.getZ();
a = robotToFlangeFrame.getAlphaRad();
b = robotToFlangeFrame.getBetaRad();
c = robotToFlangeFrame.getGammaRad();
currentFlangePose = EtoR([a b c],'Rob');
currentFlangePose(1:3,4) = [x y z];
currentFlangePose(4,1:4) = [0 0 0 1];

relFlangePoseDiff = inv(targetFlangePose) * currentFlangePose;

errorNorm = norm(relFlangePoseDiff(1:3, 4));
[errorAngle] = ScalDreh(targetFlangePose(1:3,1:3), currentFlangePose(1:3,1:3), pi, 2 * pi);

%disp('errorNorm: '); disp(errorNorm);
%disp('errorAngle: '); disp(errorAngle);

% Move the robot by relFlangePoseDiff, relative to currentFlangePose 
% Convert to Robot again :-(
robotToFlangeFrame.setX(relFlangePoseDiff(1,4));
robotToFlangeFrame.setY(relFlangePoseDiff(2,4));
robotToFlangeFrame.setZ(relFlangePoseDiff(3,4));

euler = RtoE(relFlangePoseDiff(1:3,1:3),'Rob');
euler = euler*pi/180;
robotToFlangeFrame.setAlphaRad(euler(1));
robotToFlangeFrame.setBetaRad(euler(2));
robotToFlangeFrame.setGammaRad(euler(3));

robot.moveRel(robotToFlangeFrame);
targetFlangePosition = [robotToFlangeFrame.getX() robotToFlangeFrame.getY() robotToFlangeFrame.getZ() robotToFlangeFrame.getAlphaRad() robotToFlangeFrame.getBetaRad() robotToFlangeFrame.getGammaRad()];

c=0;
