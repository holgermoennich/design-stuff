%% Dependency between number of positions and error

clc;
clear all;

Camera_to_Marker = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange_20_pos.txt');
Robot_to_Flange = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker_20_pos.txt');

Camera_to_Marker_fE = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker.txt');
Robot_to_Flange_fE = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange.txt');

[r,s] = size(Camera_to_Marker_fE); 
num_meas = s/4;

%% mean rotational error in degrees
  
  
for i = 20:-1:3
  sum_rot2 = 0;
  sum_trans = 0;
  sum_dN = 0;
  X = zeros(4,4);
  Z = zeros(4,4);
  
  [X,Z] = dornaika(Camera_to_Marker(:,1:4*i), Robot_to_Flange(:,1:4*i));
  R_X = X(1:3,1:3);
  R_Z = Z(1:3,1:3);

  t_X = X(1:3,4);
  t_Z = Z(1:3,4);
  
  
  for k = 1:num_meas
    T_Bi = Robot_to_Flange_fE(1:4, 4*k-3:4*k);
    R_Bi = T_Bi(1:3,1:3);
    T_Ai = Camera_to_Marker_fE(1:4, 4*k-3:4*k);
    R_Ai = T_Ai(1:3,1:3);
    R_i = (R_Z*R_Bi)'*(R_Ai*R_X); 
  
    sum_rot2 += acos((trace(R_i(1:3,1:3))-1)/2);
  end

  e_R2(i) = sum_rot2/num_meas;

  %% mean translational error
  for l = 1:num_meas
    T_Bi = Robot_to_Flange_fE(1:4, 4*l-3:4*l);
    R_Bi = T_Bi(1:3,1:3);
    t_Bi = T_Bi(1:3,4);
    T_Ai = Camera_to_Marker_fE(1:4, 4*l-3:4*l);
    R_Ai = T_Ai(1:3,1:3);
    t_Ai = T_Ai(1:3,4);
    
  trans = (R_Ai*t_X + t_Ai) - (R_Z*t_Bi+t_Z);
  n_trans = norm(trans);
  sum_trans += n_trans;
  end

  e_t(i) = sum_trans/num_meas;

  %% mean combined rotation and translation error without units

  for u = 1:num_meas
    Ai = Camera_to_Marker_fE(:,4*u-3:4*u);
    Bi = Robot_to_Flange_fE(:,4*u-3:4*u);
    dH = Ai*X - Z*Bi; % 4x4 difference matrix
    dN = norm(dH, "fro"); % frobenius norm of the difference matrix
    sum_dN += dN;
  end
  e_c(i) = sum_dN/num_meas;
  
end

figure(1);
plot(3:20, e_t(3:20));
title('e_t');

figure(2);
plot(3:20, e_R2(3:20));
title('e_R2');

figure(3);
plot(3:20, e_c(3:20));
title('e_c');