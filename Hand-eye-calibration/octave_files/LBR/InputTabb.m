%% Converts the TrafoMatrices for the iterative method in C++ 

clc;
clear all;

%% Read transformation matrices A und B from file
Camera_to_Marker = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Camera_to_Marker.txt');
Robot_to_Flange = dlmread('D:\3D\Tool\MatlabOpenIGT\results\TrafoMatrices\Robot_to_Flange.txt');

%% Number of measurements 
[m, n] = size(Camera_to_Marker);
numCamPoints = n/4;

[q, r] = size(Robot_to_Flange);
numRobotPoints = r/4;

if numCamPoints ~= numRobotPoints    
    error('Number of position data from the Camera and Robot is NOT equal')
end


l = 1;
for i = 1:numRobotPoints
  for j = 1:4
    for k = i*4-3:i*4
    conv_Robot_to_Flange(l) = Robot_to_Flange(j,k);
    l++;
    end
  end
end

%% save Bs as robot_cali.txt (required as input for C++)
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp\robot_cali.txt', numRobotPoints);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp\robot_cali.txt', conv_Robot_to_Flange, " ", "-append");

dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp\num_meas.txt', numRobotPoints);
dlmwrite('D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp\As.txt', Camera_to_Marker);

%% run cmd -> D:\iterative_method\Tabb\bin\Debug>:
%%Tabb_AhmadYousef_RWHEC_Jan2017_Project.exe D:\3D\Tool\MatlabOpenIGT\results\Tabb\Input_Cpp D:\3D\Tool\MatlabOpenIGT\results\Tabb\results 1