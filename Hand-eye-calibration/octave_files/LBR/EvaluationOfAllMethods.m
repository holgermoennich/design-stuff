clc;
clear all;

%% Evaluation of the pivot movement
%% Daniilidis
Marker_Positions_Dan = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Marker_Positions.txt');
Flange_Positions_Dan = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Flange_Positions.txt');
Expected_Base_to_Marker_Dan = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Base_to_Marker.txt');

Flange_home_Dan = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Flange_home.txt'); % Set as ground truth
Marker_home_Dan = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Marker_home.txt'); % Set as ground truth

X_Dan = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\X.txt');
Z_Dan = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Daniilidis_25-07-2017\Z.txt');

%% Dornaika
Marker_Positions_Dor = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Dornaika_25-07-2017\Marker_Positions.txt');
Flange_Positions_Dor = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Dornaika_25-07-2017\Flange_Positions.txt');
Expected_Base_to_Marker_Dor = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Dornaika_25-07-2017\Base_to_Marker.txt');

Flange_home_Dor = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Dornaika_25-07-2017\Flange_home.txt'); % Set as ground truth
Marker_home_Dor = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Dornaika_25-07-2017\Marker_home.txt'); % Set as ground truth

X_Dor = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Dornaika_25-07-2017\X.txt');
Z_Dor = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Dornaika_25-07-2017\Z.txt');

%% Shah
Marker_Positions_Shah = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Shah_25-07-2017\Marker_Positions.txt');
Flange_Positions_Shah = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Shah_25-07-2017\Flange_Positions.txt');
Expected_Base_to_Marker_Shah = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Shah_25-07-2017\Base_to_Marker.txt');

Flange_home_Shah = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Shah_25-07-2017\Flange_home.txt'); % Set as ground truth
Marker_home_Shah = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Shah_25-07-2017\Marker_home.txt'); % Set as ground truth

X_Shah = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Shah_25-07-2017\X.txt');
Z_Shah = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Shah_25-07-2017\Z.txt');

%% Tabb
Marker_Positions_Tabb = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Tabb_c2_separable_25-07-2017\Marker_Positions.txt');
Flange_Positions_Tabb = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Tabb_c2_separable_25-07-2017\Flange_Positions.txt');
Expected_Base_to_Marker_Tabb = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Tabb_c2_separable_25-07-2017\Base_to_Marker.txt');

Flange_home_Tabb = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Tabb_c2_separable_25-07-2017\Flange_home.txt'); % Set as ground truth
Marker_home_Tabb = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Tabb_c2_separable_25-07-2017\Marker_home.txt'); % Set as ground truth

X_Tabb = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Tabb_c2_separable_25-07-2017\X.txt');
Z_Tabb = dlmread('D:\3D\Tool\MatlabOpenIGT\results\Evaluation\Tabb_c2_separable_25-07-2017\Z.txt');


[m,n] = size(Marker_Positions_Dan); % m = 4, n = 200

num_pos = n/4;

for i = 1:num_pos
  t(:,i) = Marker_Positions_Dan(1:3,i*4);
end

for j = 1:num_pos
  %% Trafo matrices
  Traf_Marker = Marker_Positions_Dan(1:4, 4*j-3:4*j);
  Traf_Flange = Flange_Positions_Dan(1:4, 4*j-3:4*j);
 
  %% Rotation matrices
  Rot_Marker = Traf_Marker(1:3, 1:3);
  Rot_Flange = Traf_Flange(1:3, 1:3);
  
  [w,a] = to_axis(Rot_Marker);
  rot_mj(j) = a;
  axes_mj(1:3,j) = w;
  
  [z,b] = to_axis(Rot_Flange);
  rot_flj = b;
  axes_fl(1:3,j) = z;
  
  s(:,j) = Flange_Positions_Dan(1:3, 4*j); % for the plot afterwards
end

%
%%% plot of translations
%%% plot of marker translation
m0 = Marker_home_Dan(1:3,4);
scatter3 (t(1,:), t(2,:), t(3,:), "b", 'filled');
hold on;
scatter3 (m0(1), m0(2), m0(3), "r"); % plot marker origin
xlabel ("x");
ylabel ("y");
zlabel ("z");
title ("Marker translation");

%% plot of flange translation
figure(2);
f0 = Flange_home_Dan(1:3,4);
scatter3 (s(1,:), s(2,:), s(3,:), "b");
hold on;
scatter3 (f0(1), f0(2), f0(3), "r"); % plot marker origin
hold on;
xlim([-600 -500]);
ylim([-100 100]);
zlim([400 500]);
fl_ang = acos((trace(Flange_home_Dan(1:3,1:3))-1)/2);
disp('Rotation angle'); disp(fl_ang*180/pi);
fl_t = logm(Flange_home_Dan(1:3,1:3));
t_x = fl_t(3,2)/fl_ang;
t_y = fl_t(1,3)/fl_ang;
t_z = fl_t(2,1)/fl_ang;
exp_c = Flange_home_Dan*inv(X_Dan);
scale = 0.1;
%plot3([f0(1);f0(1)-scale*t_x],[f0(2);f0(2)-scale*t_y],[f0(3);f0(3)-scale*t_z], 'LineWidth',3);
plot3([f0(1);(f0(1)+scale*exp_c(1,4))],[f0(2);(f0(2)+scale*exp_c(2,4))],[f0(3);(f0(3)+scale*exp_c(3,4))], 'LineWidth',3);
xlabel ("x"); 
ylabel ("y");
zlabel ("z");
title ("Flange translation");

figure(3);
%arrow3(zeros(50,3), axes_mj');
arrow3(zeros(50,3), axes_fl');
