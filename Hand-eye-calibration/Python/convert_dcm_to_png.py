# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import png
import os
import dicom
import numpy as np
import sys


def write_png(img, filename, force_8bit=False):
    bitdepth = 8
    if img.dtype == np.float or img.dtype == np.float32:
        img = img - img.min()
        img = ((img.astype(np.double) / img.ptp()) * (2**16-1)).astype(np.uint16)
        bitdepth = 16
    elif img.dtype == np.uint32:
        img = img - img.min()
        img = ((img.astype(np.double) / img.ptp()) * (2 ** 16 - 1)).astype(np.uint16)
        bitdepth = 16
    elif img.dtype == np.uint16:
        bitdepth = 16
    if force_8bit and bitdepth == 16:
        img = img - img.min()
        img = ((img.astype(np.double) / img.ptp()) * (2 ** 8 - 1)).astype(np.uint8)
        bitdepth = 8
    with open(filename, 'wb') as f:
        pngWriter = png.Writer(img.shape[1], img.shape[0], bitdepth=bitdepth, greyscale=True)
        pngWriter.write(f, img)

input_path = sys.argv[1] #"D:\\XRay\\Mono\\As\\Flouros" # saved .ima's
out_path = sys.argv[2] #"D:\\XRay\\Mono\\As\\Images" # directory for the png's

content = os.listdir(input_path)
i = 0

# for directory with several .ima's
for root, dirs, files in os.walk(input_path):
    for name in files:
        FileName = name
        dcm = dicom.read_file(root + "\\" + FileName)
        img = dcm.pixel_array
        if img.ndim == 3:
            img = img[0]
        num = str(i)
        path = out_path + "\\" + num + ".png"
        # path = out_path + "\\" + FileName[0:-4] + ".png"
        write_png(img, path, force_8bit=True)
        i += 1
        
 # for a 3D run  
#dcm = dicom.read_file(input_path)   # "D:\\XRay\\TestData\\X-RAY_Marker.IMA"
#for i in range(len(content)): #397 for 3D run       
 #   img = dcm.pixel_array[i]  # first frame in a multi-frame series…                   
#    Filename = str(i)
#    path = out_path + "\\" + Filename + ".png"
#    write_png(img, path)
    
# for single flouro
#dcm = dicom.read_file("D:\\XRay\\Mono\\As\\Flouros\\M_.XA.RIGHT_NEURO.0006.0049.2016.05.20.15.43.16.952113.6333901.IMA")
#img = dcm.pixel_array
#out_path = "D:\\XRay\\Mono\\As\\test.png"
#write_png(img, out_path, force_8bit=True)