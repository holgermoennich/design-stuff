function [rRobPos,rRobOr] = SollPosRob(ArtPose, model,R_ArtRob,t_ArtRob,R_ModellObjekt,t_ModellObjekt,tg)
% This block supports the Embedded MATLAB subset.
% See the help menu for details. 

mcoord=zeros(3,1);
skuPos=zeros(3,1);
mcoord(1:3,1)=model(1:3);
skuPos(1:3,1)=model(4:6);

%R=Trans(1:3,1:3)
%tg=Trans(1:3,4)

%position und orientation in ART-coordinates
ArtPos=ArtPose(1:3);
ArtOr =EtoR(ArtPose(4:6),'DIN');

%modell coordinates to local ART coordinates
lcoord  = t_ModellObjekt + R_ModellObjekt*mcoord;
lskuPos = t_ModellObjekt + R_ModellObjekt*skuPos;

%local ART coordinates to ART coordinates
acoord  = ArtPos+ArtOr*lcoord;
askuPos = ArtPos+ArtOr*lskuPos;

%position and direction of beam in robot coordiantes
rskuPos = R_ArtRob*askuPos+t_ArtRob;
rcoord  = R_ArtRob*acoord+t_ArtRob;
rdirec  = rskuPos-rcoord;

%Where should the distance sensor be in robot coordinates
x = -rdirec/norm(rdirec);
y = -cross(rcoord, rskuPos);
if all(y) == 0
    z = [rdirec(1) rdirec(2) 0];
    z = z/norm(z);
    y = -cross(z,x)';
else
    y = y/norm(y);
    z = cross(x,y)';
end
rDisOr  = [-y x z'];

rDisPos = R_ArtRob*acoord+t_ArtRob;

%Where should the robot be in his coordinates
%rRobOr2  =rDisOr;

rRobOr2 = rDisOr;
rRobPos = rDisPos+rDisOr*tg;
%rRobOr  = RtoE(rRobOr2 ,'Rob');
%rRobOr  = RtoE(rRobOr2 ,'Rob');
rRobOr=rRobOr2;
