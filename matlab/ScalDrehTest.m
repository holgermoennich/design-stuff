function ScalDrehTest(nr)

    pos_s=eye(3)
    pos_s(1:3,4)=[0 0 1]

    orBodySetTransform(orEnvGetBody('box3'),pos_s)
    pos_s=reshape(orBodyGetTransform(orEnvGetBody('box3')),3,4)
    
    % Irgendeine Drehung
    rot=EtoR([22.5 45 34.5],'DIN')
    
    pos_e=pos_s;
    pos_e(1:3,1:3)=rot;
    pos_e(1:3,4)=[0 0 1.3];
    pos_e;
    
    drehung=0;
    for i=1:nr
        drehung=drehung+0.01;
        [relE,Ist1,Soll1]=ScalDreh(pos_s(1:3,1:3),pos_e(1:3,1:3),drehung,1);
        pos_n=pos_s;
        pos_n(1:3,1:3)=relE;
        pos_n=pos_s+i*(pos_e-pos_s)/100;
        orBodySetTransform(orEnvGetBody('box3'),pos_n);
        pause(0.05);
    end
    
    %Scalierungstest
    pos_s=eye(3)
    pos_s(1:3,4)=[0 0 1]

    pause(1)
    orBodySetTransform(orEnvGetBody('box3'),pos_s)
    pause(1)
    rot=EtoR([0 90 0],'DIN')
    [relE,Ist1,Soll1]=ScalDreh(pos_s(1:3,1:3),pos_e(1:3,1:3),2,10);
    pos_s(1:3,1:3)=relE
    orBodySetTransform(orEnvGetBody('box3'),pos_s);

    
end