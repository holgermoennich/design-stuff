function [R,relEuler,angle, Ist1, Soll1]   = ScalDreh( Ist, Soll, maxangle, scal )
%INFDREH Summary of this function goes here
%   Detailed explanation goes here

Ist1=Ist;
Soll1=Soll;

%istR=EtoR(Ist, 'Rob');
%sollR=EtoR(Soll, 'Rob');

istR=Ist;
sollR=Soll;

Rot=sollR*istR';

% angle = acos(( m00 + m11 + m22 - 1)/2)
% x = (m21 - m12)/?((m21 - m12)2+(m02 - m20)2+(m10 - m01)2)
% y = (m02 - m20)/?((m21 - m12)2+(m02 - m20)2+(m10 - m01)2)
% z = (m10 - m01)/?((m21 - m12)2+(m02 - m20)2+(m10 - m01)2)

angle=acos((trace(Rot)-1)/2);
angle=angle/scal;
if angle>(pi/2) 
    angle=angle-pi; 
end
axis=[Rot(3,2)-Rot(2,3);Rot(1,3)-Rot(3,1);Rot(2,1)-Rot(1,2)];
axis=axis/norm(axis);
if abs(angle)>maxangle
    angle=sign(angle)*maxangle;
end

%[R] = [I] + s*[~axis] + t*[~axis]2
%# c =cos(angle)
%# s = sin(angle)
%# t =1 - c
%axis;
Skew=skew(axis);
R=eye(3)+sin(angle)*Skew+(1-cos(angle))*Skew^2;

%new version
relEuler=RtoE(R,'Rob');

%old version
    % sollR=R*istR
    % E=RtoE(sollR,'Rob')
    % 
    % relE=E-Ist
    % %relE=-relE
    % 
    % if relE(1)>180
    %     relE(1)=relE(1)-360; 
    % end
    % %if relE(1)<-180
    % %    relE(1)=relE(1)+360; 
    % %end
    % if relE(2)>90
    %     relE(2)=relE(2)-180; 
    % end
    % %if relE(2)<-90
    % %    relE(2)=relE(2)+180; 
    % %end
    % relE(3)
    % if relE(3)>180
    %     relE(3)=relE(3)-360
    % end
    % %if relE(3)<-180
    % %    relE(3)=relE(3)+360
    % %end
    % end