############################
## Siemens, GE
## Created on June 13th 2017
## Auhor: R. Mebarki
##
## Function: 1. generate a path for LBR 
##           2. apply the path ; sending commands to the real robot  
##           3. replicate the real robot configuration onto the simulated one; emulation 
##  
##############################

function planned_path_matrix = or_robotpath_gen(sLBR_robot, sLBR_problem,
						rLBR_robot,
						rLBR_pose_mat_target)


  ## #####################
  ##   GENERATE PATH
  ## #####################
  disp("*********  Path planning  **************");
  

  ## Joint Angles
  if(0)
    sLBR_joints_cmd  = pi/180*rLBR_joints_target_deg.';
    planned_path_string = orProblemSendCommand(['MoveManipulator goal ' sprintf('%f ',sLBR_joints_cmd(:)) ' outputtraj' ], sLBR_problem);
  end
  
  ## Cartesian
  if(1)
  %%T = reshape(linkk(:,1),[3 4]);
    T = rLBR_pose_mat_target;
    planned_path_string = orProblemSendCommand(['MoveToHandPosition matrix ' sprintf('%f ',T(1:3,1:4))  ' outputtraj' ],
			 sLBR_problem)
    
 end
  
  orEnvWait(sLBR_robot.id);

  printf("\n\n ********* Planning done. \n\n");

  

  ## #####################################
  ## CONVERT STRING PATH INTO MATRIX PATH
  ## ######################################

  if( ~isempty(planned_path_string))
    planned_path = str2num(planned_path_string)';
    
			    % Unterscheidung zwischen 6 DOF und 7 DOF!
    if ( mod((length(planned_path)-4),13)==0 )
      planned_path_matrix =reshape(planned_path(5:end),13,planned_path(1));
    end
    if (mod((length(planned_path)-4),14)==0)
      planned_path_matrix =reshape(planned_path(5:end),14,planned_path(1));
    end
    if (mod((length(planned_path)-4),15)==0)
      planned_path_matrix =reshape(planned_path(5:end),15,planned_path(1));
    end
    if (mod((length(planned_path)-4),16)==0)
      planned_path_matrix =reshape(planned_path(5:end),16,planned_path(1));
    end
  else
    fprintf('gave a problem, got nothing in return...\n');
    
  end

  ## ############
  
 
  [nrows, ncols] = size(planned_path_matrix);
  itr =1;

  while( itr<=ncols)
    sLBR_joint_ref = planned_path_matrix(1:7, itr);
    rLBR_joints_target = 180/pi*sLBR_joint_ref.';


    ## TODO: check which one of the two below commnds applies
    if(0)
      homePosition = rLBR_robot.moveToPtp( rLBR_joints_target(1), rLBR_joints_target(2), rLBR_joints_target(3), rLBR_joints_target(4), rLBR_joints_target(5), rLBR_joints_target(6), rLBR_joints_target(7) ); % in [deg] TODO: check this command!!
    else
	homePosition = rLBR_robot.moveToJointPosition( rLBR_joints_target(1), rLBR_joints_target(2), rLBR_joints_target(3), rLBR_joints_target(4), rLBR_joints_target(5), rLBR_joints_target(6), rLBR_joints_target(7) ); % in [deg] TODO: check this command!!
    endif

    ##  reading current rLBR joint configuration
    %% rLBR_current_joints_rad = rLBR_robot.Position.get(); % in [rad] TODO: check this too!! OLD
    rLBR_current_joints_rad = rLBR_robot.getCurrentJointPositionInRad();
    
    ## TRANSFER as cmds to Virtual LBR
    sLBR_joints_cmd  = rLBR_current_joints_rad';
    orRobotSetDOFValues(sLBR_robot.id, sLBR_joints_cmd(1:7), 0:6);

    

    ##
    itr+= 10; ## to seed up the process
  end
  
end
