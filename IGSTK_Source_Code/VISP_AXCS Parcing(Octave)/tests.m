###############################################################
# Siemens, Forcheim, June 2017
# By R. Mebarki
# Created on June 2017. Last update on June 2017.
#
# Script for emulating Zeego's C-ARM in Koje environment using OpenRave on Octave.
#  - Using an UPDATED version of the CAD models.
#    -- Zeego Pheno model!! 
#  - Reading/parsing C-ARM pose from AXCS msg bus and MONACO.
#  -- Parsing achieved in a modular fashion with `axcs_msg_parse_func.m` function.
#  -- STRINGS used in `axcs_msg_parse_func.m` need to match those in `x:/monaco/sources/cpp/LabDaemonPlugins/LabFrameGrabber/LabFrameGrabber.cpp`.
#
# 
# File originally copied from `emulation_carm_only_updated.m`.
# Use insated `axcs_msg_modular_parsing_tester.m` to test only parsing.
#
################################################################

clear all
more off
global probs


langname = 'octave';

##############
# PATHS
##############

addpath('C:\Program Files\openrave\share\openrave\octave')
addpath('D:\WORKSPACE\lab-machine\DEMOS\DemoAlpha')


## This seems optional. TODO: check!!
addpath ("c:/Octave/Octave-4.2.1/share/octave/packages/quaternion-2.4.0/")
addpath ("c:/Octave/Octave-4.2.1/lib/octave/packages/quaternion-2.4.0/x86_64-w64-mingw32-api-v51/")

## ######################
## #  SCENE
## ######################
## disp("Loading openrave environment..");
## orEnvLoadScene('D:\WORKSPACE\lab-machine\DEMOS\DemoAlpha\or-module\data\safros_carm_LBR_new.env.xml',1); 




## ###########################
## #   sLBR
## ###########################
## disp("*** Creating Virtual LBR robot*****");
## sLBR_robot_name = 'sLBR';
## sLBR_robot.id = orEnvGetBody(sLBR_robot_name);
## sLBR_robot.name = sLBR_robot_name;
## sLBR_robot.totaldof = orBodyGetDOF(sLBR_robot.id)
## sLBR_robot.ikreachability = ''; % ik reachability file

## disp("*** Creating LBR OpenRave problem****");
## sLBR_problem = orEnvCreateProblem('BaseManipulation', 'sLBR', 0); #
## SLBR_manips =  orRobotGetManipulators(sLBR_robot.id);

## s = orProblemSendCommand(['LoadIKFastSolver ' sLBR_robot.name ' Transform6D'], sLBR_problem) ; # not sure about this one??




angle= -pi/2;
  vect=[1, 0, 0];
  quat = [cos(angle/2), sin(angle/2)*vect];
 

######################################################
##################        MAIN LOOP     ##############
######################################################


disp("That is all folks.");
