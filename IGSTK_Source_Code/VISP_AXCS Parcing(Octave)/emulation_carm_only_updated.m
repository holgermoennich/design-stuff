##############################
# Siemens, Forcheim, May 2017
# By R. Mebarki
# Created on June 2017.
#
# Using cad models UPDATED version
#  
##############################


more off
global probs


langname = 'octave';

##############
# PATHS
##############

addpath('C:\Program Files\openrave\share\openrave\octave')
addpath('D:\WORKSPACE\lab-machine\DEMOS\DemoAlpha')


addpath ("c:/Octave/Octave-4.2.1/share/octave/packages/quaternion-2.4.0/")
addpath ("c:/Octave/Octave-4.2.1/lib/octave/packages/quaternion-2.4.0/x86_64-w64-mingw32-api-v51/")

######################
#  SCENE
######################
disp("Loading openrave environment..");
orEnvLoadScene('D:\WORKSPACE\lab-machine\DEMOS\DemoAlpha\or-module\data\safros_carm_original_updated.env.xml',1); 




###########################
#   sLBR
###########################
disp("*** Creating Virtual LBR robot*****");
sLBR_robot_name = 'sLBR';
sLBR_robot.id = orEnvGetBody(sLBR_robot_name);
sLBR_robot.name = sLBR_robot_name;
sLBR_robot.totaldof = orBodyGetDOF(sLBR_robot.id)
sLBR_robot.ikreachability = ''; % ik reachability file

disp("*** Creating LBR OpenRave problem****");
sLBR_problem = orEnvCreateProblem('BaseManipulation', 'sLBR', 0); #
SLBR_manips =  orRobotGetManipulators(sLBR_robot.id);

s = orProblemSendCommand(['LoadIKFastSolver ' sLBR_robot.name ' Transform6D'], sLBR_problem) ; # not sure about this one??


###########################
#   sCARM
###########################
disp("*** Creating Virtual ZEEGO CARM*****");
sCARM_robot_name = 'sCARM';
sCARM_robot.id = orEnvGetBody(sCARM_robot_name);
sCARM_robot.name = sCARM_robot_name;
sCARM_robot.totaldof = orBodyGetDOF(sCARM_robot.id)
sCARM_robot.ikreachability = ''; % ik reachability file

## disp("*** Creating LBR OpenRave problem****");
## sLBR_problem = orEnvCreateProblem('BaseManipulation', 'sLBR', 0); #
## SLBR_manips =  orRobotGetManipulators(sCARM_robot.id);

## s = orProblemSendCommand(['LoadIKFastSolver ' sCARM_robot.name ' Transform6D'], sLBR_problem) ; # not sure about this one??





######################################################
##################        MAIN LOOP     ##############
######################################################
itr=0;
while(1)

  ## #############
  ## REAL ZEEGO ROBOT
  ## #############
  
  ## ### INNER LOOP TO EMPTY THE BUGGER --> TO GET THE LAST possible MESSAGE
  for inner_itr=1:1
    str = mexGetMessage();
    

    char_array= strsplit(str, "\n");
    [row, col] = size(char_array);

    if(col >= 13)

      ## Angle 1 ?? x axis
      str_Aangul = char_array{1,1};
      str_Aangul2 = strsplit(str_Aangul, "=");
      cAangul = str_Aangul2{1,2};
      Aangul =  pi/180*str2num(cAangul);
            
      ## Angle 2 ?? y axis
      str_Acran = char_array{1,3};
      str_Acran2 = strsplit(str_Acran, "=");
      cAcran = str_Acran2{1,2};
      Acran =  -pi/180*str2num(cAcran);
      
      ## 3D coordinates
      str_coordi_x = char_array{1,11};
      str_coordi_y = char_array{1,12};
      str_coordi_z = char_array{1,13};

    
      ## PARSE again each component
      str_coordi_x2 = strsplit(str_coordi_x, "=");
      ccoordi_x = str_coordi_x2{1,2};
      coordi_x =  str2num(ccoordi_x);

      str_coordi_y2 = strsplit(str_coordi_y, "=");
      ccoordi_y = str_coordi_y2{1,2};
      coordi_y =  str2num(ccoordi_y);

      str_coordi_z2 = strsplit(str_coordi_z, "=");
      ccoordi_z = str_coordi_z2{1,2};
      coordi_z =  str2num(ccoordi_z);
      
      disp("**** DISPLAY COORDINATES ****")
      coordi_x
      coordi_y
      coordi_z
      
      
      ##
      ##  EMULATED ZEEGO CARM: 
      ##
      px = coordi_x/1000.0 -2.9;
      py = coordi_y/1000.0;
      pz = coordi_z/1000.0;# -0.85;
      %% pz = 0.8;

      ## ROTATION MATRIX

      theta = Aangul;
      Rotx = [ 1  0    0
	       0 cos(theta) -sin(theta)
	       0 sin(theta)  cos(theta) ];

      phi = Acran;
      Roty = [cos(phi) 0 sin(phi)
	      0 1 0
	      -sin(phi)   0  cos(phi)];

      Rotxy   = Rotx*Roty;

      ## conversion to quaternion
      quat = rotm2q(Rotxy);
      
      ## #############################
      ## Send coordi to the simulator
      ## #############################
      if(1)
	orBodySetTransform(sCARM_robot.id, [px, py, pz],
			   [quat.w,quat.x,quat.y,quat.z]);
      end
      
      if(0)
      TransH = [Rotxy(1, 1:3) coordi_x
		Rotxy(2, 1:3) coordi_y
		Rotxy(3, 1:3) coordi_z
	       ];
      orBodySetTransform(sCARM_robot.id, reshape(TransH,[1 12])  );

      end
      
      
 
      
    end
    save("-append","myfile.dat",'px', 'py');
  end
  ## ###############
  
  

  itr++;
  
end

disp("That is all folks.");
