############################
## Siemens, DE
## Created on June 13th 2017
## Auhor: R. Mebarki
##
## Function: 1. send command to real LBR 
##           2. emulate sLBR (simulated one)  
##          
##  
##############################

function or_ref2lbr_emulate(sLBR_robot,
			    rLBR_robot, sLBR_problem,
			    joints_ref_rad)




  printf("\n\n*** Moving real robot...");
  homePosition = rLBR_robot.moveToJointPositionInRad(joints_ref_rad(1), joints_ref_rad(2), joints_ref_rad(3), joints_ref_rad(4), joints_ref_rad(5), joints_ref_rad(6), joints_ref_rad(7));
  
  printf("\n\n*** We read this from the real robot:")
  joint_rad = rLBR_robot.getCurrentJointPositionInRad()
  pause(5);
  
  %%zero_joint_rad(7) -= 3*pi/2;

  ## ##### ALIGN virtual robot to real one
  printf("\n\n*** Aligning sLBR...");
  pause(2);
  %%orRobotSetDOFValues(sLBR_robot.id,  joints_ref_rad(1:7), 0:6);
  orProblemSendCommand(['MoveManipulator goal ' sprintf('%f ', joints_ref_rad(:))], sLBR_problem);

  
end
