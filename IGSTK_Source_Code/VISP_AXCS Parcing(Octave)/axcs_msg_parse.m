##############################
# Siemens, Forcheim, May 2017
# By R. Mebarki
# Code used for the demo of June 6th 2017.
# 
##############################


more off
global probs


langname = 'octave';

##############
# PATHS
##############

addpath('C:\Program Files\openrave\share\openrave\octave')
addpath('D:\WORKSPACE\lab-machine\DEMOS\DemoAlpha')


addpath ("c:/Octave/Octave-4.2.1/share/octave/packages/quaternion-2.4.0/")
addpath ("c:/Octave/Octave-4.2.1/lib/octave/packages/quaternion-2.4.0/x86_64-w64-mingw32-api-v51/")



######################################################
##################        MAIN LOOP     ##############
######################################################
itr=0;

while(1)

  ## #############
  ## REAL ZEEGO ROBOT
  ## #############
  
  ## ### INNER LOOP TO EMPTY THE BUFFER --> TO GET THE LAST possible MESSAGE
  
  str = mexGetMessage();
  

  char_array= strsplit(str, "\n");
  [row, col] = size(char_array);

  
  for i=1:col
    [name, right_side] = extrName(char_array{1,i});

    ## MATRICES
    if( strcmpi(name, "Dicom2Table_A")  )
      dicom2table_A = string2mat(right_side, "&#0a;"); %%ok
      
    elseif( strcmpi(name, "CameraCV2ImageCV_A")  )
      cameraCV2ImageCV_A = string2mat(right_side, "&#0a;"); %%ok

      ## Issue with Table2World. incomplete string obtained. Strange!!
    elseif( strcmpi(name, "Table2World_A")  ) 
      table2World_A  = string2mat(right_side, "&#0a;"); ## no
      
    elseif( strcmpi(name, "ImageCV2ImageNormGL_A")  )
      imageCV2ImageNormGL_A = string2mat(right_side, "&#0a;");  %%no
      
      
   
    endif
    
    ## COORDI in m and deg
    if( strcmpi(name, "A.IsoWorldAxcsX")  )
      coordi_x =  str2num(right_side)/1000.0;

    elseif( strcmpi(name, "A.IsoWorldAxcsY")  )
      coordi_y =  str2num(right_side)/1000.0
      
    elseif( strcmpi(name, "A.IsoWorldAxcsZ")  )
      coordi_z =  str2num(right_side)/1000.0;

      
    elseif( strcmpi(name, "A.Angul")  )
      Aangul =  pi/180*str2num(right_side); # in deg
      
    elseif( strcmpi(name, "A.Cran")  )
      Acran =  pi/180*str2num(right_side); # in deg
      
      

      
    endif
    
  end #<-- for i




  ## ###############



  itr++;

end  ## <- while()

disp("That is all folks.");
