## Siemens Forcheim
## June 2017
## By R. Mebarki
##
## Returns 4x4 matrix
## 

function mat = string2mat(line, del)

  if(0)
    line_carr  = strsplit(line, "=");
    rows_str =line_carr{1,2};
  else
    rows_str = line;
  end
  rows_carr = strsplit(rows_str, del);
  row1_str = rows_carr{1,2};
  row2_str = rows_carr{1,3};
  row3_str = rows_carr{1,4};
  row4_str = rows_carr{1,5};

  row1_carr = strsplit(row1_str, " ");
  row2_carr = strsplit(row2_str, " ");
  row3_carr = strsplit(row3_str, " ");
  row4_carr = strsplit(row4_str, " ");
  
  m11 = str2num(row1_carr{1,1});
  m12 = str2num(row1_carr{1,2});
  m13 = str2num(row1_carr{1,3});
  m14 = str2num(row1_carr{1,4});

  m21 = str2num(row2_carr{1,1});
  m22 = str2num(row2_carr{1,2});
  m23 = str2num(row2_carr{1,3});
  m24 = str2num(row2_carr{1,4});

  m31 = str2num(row3_carr{1,1});
  m32 = str2num(row3_carr{1,2});
  m33 = str2num(row3_carr{1,3});
  m34 = str2num(row3_carr{1,4});

  m41 = str2num(row4_carr{1,1});
  m42 = str2num(row4_carr{1,2});
  m43 = str2num(row4_carr{1,3});
  m44 = str2num(row4_carr{1,4});
  
  mat = [m11 ,m12 ,m13 ,m14
	 m21, m22, m23, m24
	 m31 ,m32 ,m33, m34 
	 m41 ,m42 ,m43, m44];
   
end
