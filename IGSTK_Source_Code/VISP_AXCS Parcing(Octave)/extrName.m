## Siemens, Forcheim 2017.				#
##
## Author: R. Mebarki
  
function [name, val] = extrName(line_str)
  line_carr = strsplit(line_str, "=");
  name = line_carr{1,1};
  [row, col] = size(line_carr);
  if(col>1)
    val = line_carr{1,2};
  else
    val=-777; %% TODO: check this!!
  endif
end
