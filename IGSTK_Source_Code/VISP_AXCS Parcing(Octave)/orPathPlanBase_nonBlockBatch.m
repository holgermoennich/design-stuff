############################
## Siemens, DE
## Created on June 13th 2017
## Auhor: R. Mebarki
##
## Function: 1. generate a path for LBR 
##           2. apply the path ; sending commands to the real robot  
##           3. replicate the real robot configuration onto the simulated one; emulation 
##  
##############################

function [newPathMat, status] =  orPathPlanBase_nonBlockBatch(sLBR_robot, sLBR_problem,
							      rLBR_robot,
							      H_target,
							      status)


  ## #######################
  ##   GENERATE PATH STRING
  ## #######################
  ## disp("*********  Path planning  **************");
  

  ## Cartesian
  planned_path_string = orProblemSendCommand(['MoveToHandPosition matrix ' sprintf('%f ',H_target(1:3,1:4))  ' outputtraj' ],
					     sLBR_problem);
  
  orEnvWait(sLBR_robot.id);


  ## ###########################
  ## SAVE PATH
  ## ############################
  save  orPath.mat 'planned_path_string';
  rLBR_current_joints_rad = rLBR_robot.getCurrentJointPositionInRad();
  save rLBRinitJoints.mat 'rLBR_current_joints_rad';

  
  ## #####################################
  ## CONVERT STRING PATH INTO MATRIX PATH
  ## ######################################
  clear planned_path_matrix; 

  err=1; 
  if( ~isempty(planned_path_string))
    planned_path = str2num(planned_path_string)';
    [row, col] = size(planned_path);
    
    ## 15=7+8
    if (mod((length(planned_path)-4),15)==0)
      ## planned_path_matrix =reshape(planned_path(5:end),15,planned_path(1));
      new_lg = (row-4)/15;
      planned_path_matrix =reshape(planned_path(5:end),15,new_lg);
      err=0; 
    end
    
  else
    planned_path_matrix =-1;
    err=1;
    
  end

  ## ################
  ## SEND PATH TO LBR
  ## #################

  if(!err)
    [nrows, ncols] = size(planned_path_matrix);
    ## ************
    ##   SUBSAMPLE
    ## ************
    if(0)
      clear newPathMat;
      
      itr=1; i=1;
      N=19; ## subsampling factor
      while( itr<=ncols)
	newPathMat(1:7, i) = planned_path_matrix(1:7, itr);
	i++;
	## JUMP
	if( itr<ncols - N)
	  itr+= N; 
	else
	  itr++;
	end
	
      end
      
      ## **** LBR BATCH-CONTROL  ******
      status.sentCmd =1;
      status.rLBR_motion = runMotionBatch( rLBR_robot, newPathMat);
    else
      status.sentCmd =1;
      status.rLBR_motion = runMotionBatch( rLBR_robot, planned_path_matrix(1:7, 1:ncols));
    endif
  endif
  
end
