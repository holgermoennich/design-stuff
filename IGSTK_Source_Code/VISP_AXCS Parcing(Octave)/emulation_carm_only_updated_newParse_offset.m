###############################################################
# Siemens, Forcheim, June 2017
# By R. Mebarki
# Created on June 2017. Last update on June 2017.
#
# Script for emulating Zeego's C-ARM in Koje environment using OpenRave on Octave.
#  - Using an UPDATED version of the CAD models.
#    -- Zeego Pheno model!! 
#  - Reading/parsing C-ARM pose from AXCS msg bus and MONACO.
#  -- Parsing achieved in a modular fashion with `axcs_msg_parse_func.m` function.
#  -- STRINGS used in `axcs_msg_parse_func.m` need to match those in `x:/monaco/sources/cpp/LabDaemonPlugins/LabFrameGrabber/LabFrameGrabber.cpp`.
#
# 
# File originally copied from `emulation_carm_only_updated.m`.
# Use insated `axcs_msg_modular_parsing_tester.m` to test only parsing.
#
################################################################

clear all
more off
global probs


langname = 'octave';

##############
# PATHS
##############

addpath('C:\Program Files\openrave\share\openrave\octave')
addpath('D:\WORKSPACE\lab-machine\DEMOS\DemoAlpha')


## This seems optional. TODO: check!!
addpath ("c:/Octave/Octave-4.2.1/share/octave/packages/quaternion-2.4.0/")
addpath ("c:/Octave/Octave-4.2.1/lib/octave/packages/quaternion-2.4.0/x86_64-w64-mingw32-api-v51/")

######################
#  SCENE
######################
disp("Loading openrave environment..");
orEnvLoadScene('D:\WORKSPACE\lab-machine\DEMOS\DemoAlpha\or-module\data\safros_carm_original_updated.env.xml',1); 




###########################
#   sLBR
###########################
disp("*** Creating Virtual LBR robot*****");
sLBR_robot_name = 'sLBR';
sLBR_robot.id = orEnvGetBody(sLBR_robot_name);
sLBR_robot.name = sLBR_robot_name;
sLBR_robot.totaldof = orBodyGetDOF(sLBR_robot.id)
sLBR_robot.ikreachability = ''; % ik reachability file

disp("*** Creating LBR OpenRave problem****");
sLBR_problem = orEnvCreateProblem('BaseManipulation', 'sLBR', 0); #
SLBR_manips =  orRobotGetManipulators(sLBR_robot.id);

s = orProblemSendCommand(['LoadIKFastSolver ' sLBR_robot.name ' Transform6D'], sLBR_problem) ; # not sure about this one??


###########################
#   sCARM
###########################
disp("*** Creating Virtual ZEEGO CARM*****");
sCARM_robot_name = 'sCARM';
sCARM_robot.id = orEnvGetBody(sCARM_robot_name);
sCARM_robot.name = sCARM_robot_name;
sCARM_robot.totaldof = orBodyGetDOF(sCARM_robot.id)
sCARM_robot.ikreachability = ''; % ik reachability file

## disp("*** Creating LBR OpenRave problem****");
## sLBR_problem = orEnvCreateProblem('BaseManipulation', 'sLBR', 0); #
## SLBR_manips =  orRobotGetManipulators(sCARM_robot.id);

## s = orProblemSendCommand(['LoadIKFastSolver ' sCARM_robot.name ' Transform6D'], sLBR_problem) ; # not sure about this one??




## ########### FIX TRANSFROM FROM ZEEGO ROBOT BASE FRAME TO LINK6
## ########### Values taken from the .trf file accompanying the .stl cad file 
Rb6 = eye(3); ## check this!!
tb6 = [2.790; 0 ; 1.784]; ## <- from the CAD model

Hb6 = eye(4);
Hb6(1:3, 1:3) = Rb6(1:3, 1:3);
Hb6(1:3, 4) = tb6(1:3);

######################################################
##################        MAIN LOOP     ##############
######################################################
itr=0;
Twp = eye(4); ## TODO: be careful of this!!
table2World_A = eye(4);
world2CArm_A =eye(4);
carm2CameraPhys_A = eye(4);
carm2CameraCV_A = eye(4);
coordi.x = 0;
coordi.y = 0;
coordi.z = 0;
coordi.angul=0;
coordi.cran=0;

while(1)

  ## ##########################################
  ## READIG FROM ZEEGO/MONACO AXCS MESSAGE BUS
  ## NOTE: matrices in [mm]!!
  ## ##########################################
  for inner_itr=1:30
    
    
    [ str_, dicom2table_A_,    table2World_A_,    cameraCV2ImageCV_A_,    imageCV2ImageNormGL_A_,    cameraPhys2CArm_A_,    world2CArm_A_,    carm2CameraPhys_A_,    coordi_, carm2CameraCV_A_] = axcs_msg_parse_func();

    if(dicom2table_A_.up==1) dicom2table_A = dicom2table_A_.mat; endif
    if(table2World_A_.up==1)  table2World_A = table2World_A_.mat; endif
    if(cameraCV2ImageCV_A_.up==1) cameraCV2ImageCV_A = cameraCV2ImageCV_A_.mat; endif
    if(imageCV2ImageNormGL_A_.up==1) imageCV2ImageNormGL_A = imageCV2ImageNormGL_A_.mat; endif
    if(cameraPhys2CArm_A_.up==1)    cameraPhys2CArm_A = cameraPhys2CArm_A_.mat;  endif
    if(world2CArm_A_.up==1)    world2CArm_A = world2CArm_A_.mat;  endif
    if(carm2CameraPhys_A_.up==1)    carm2CameraPhys_A = carm2CameraPhys_A_.mat;  endif
    if(carm2CameraCV_A_.up==1)    carm2CameraCV_A = carm2CameraCV_A_.mat;  endif # new
    if(coordi_.up==1)    coordi = coordi_;  endif  
  end ## <- inner loop
  
  ## #############################
  ## Send coordi to the simulator
  ## #############################
  if(0)

    Twp = world2CArm_A * carm2CameraPhys_A; ##TODO: check this!
    ## Ttp = table2World_A*world2CArm_A * carm2CameraPhys_A; ##TODO: check this!
    
    ##Twp = world2CArm_A *carm2CameraCV_A;

    ## Tow = [ 0  0 -1  0 
    ## 	    -1 0  0  0
    ## 	    0  1  0  0
    ## 	    0  0  0  1];

    Tow = [ 0  0  1  0 
    	    1  0  0  0
    	    0  1  0  0
    	    0  0  0  1];

    Tot = [ 0  0 1  0 
	    1 0  0  0
	    0  -1  0  0
	    0  0  0  1];

    Tpv = [0 -1  0  0
	   0  0  1  0
	   1  0  0  0
	   0  0  0  1];

    Top  = Tow*Twp;
    ## Twp = Tow *Twp;
    ##Top = Tot *Ttp;
    ## Top = Tot *Ttp*Tpv;
    
    ## coordi in [m]
    px =  Top(1,4)/1000; 
    py =  Top(2,4)/1000;
    pz =  Top(3,4)/1000;
    
    Rot_op = Top(1:3, 1:3);
    quat_op = rotm2q(Rot_op);
    orBodySetTransform(sCARM_robot.id, [px, py, pz],
		       [quat_op.w,quat_op.x,quat_op.y,quat_op.z]);
  endif
  ## ################
  ## ################ Using ISO data from AXCS
  ## ################
  if(1) ## this as before!!

    ## TRANSLATION VECTOR
    px = coordi.x; ##-1.05   ;
    py = coordi.y ;
    pz = coordi.z; ##+6;# -0.85;
    p_vect = [px; py; pz];
    
   
    ## ROTATION MATRIX

    theta = coordi.angul; ## TODO: check!
    Rotx = [ 1  0    0
	     0 cos(theta) -sin(theta)
	     0 sin(theta)  cos(theta) ];

    phi = -coordi.cran; ## TODO: check.
    Roty = [cos(phi) 0 sin(phi)
	    0 1 0
	    -sin(phi)   0  cos(phi)];

    Rotxy   = Rotx*Roty;
    
    ## ###### DESIRED

    Ho_iso= eye(4);
    Ho_iso(1:3, 1:3) = Rotxy(1:3, 1:3);
    Ho_iso(1:3, 4) = p_vect(1:3);

    
    ## Transfrom from link6 to ISO constant for the time being; in the future, it could varry if C-arm radius changes!!
    H6iso = eye(4);
    H6iso(1, 4) = 1.05; ##1.05 but should be 1.26
    H6iso(2, 4) = 0;
    H6iso(3, 4) = -0.; #.05-0.3; #-0.15; ##TODO: adjust this value!
    
   
    ##Hob  = Ho6*inv(Hb6);
    #Hob  = Ho6*inv(H6iso)*inv(Hb6);
    Hob  = Ho_iso*inv(H6iso)*inv(Hb6);
    

    ##  ########
    orBodySetTransform(sCARM_robot.id, reshape(Hob(1:3,:),[1 12])  );
    
    ## conversion to quaternion
    ##quat = rotm2q(Rotxy);
    ##orBodySetTransform(sCARM_robot.id, [px, py, pz],
    ##		       [quat.w,quat.x,quat.y,quat.z]);
    
  endif
  
   
  save("-append","myfile.dat",'px', 'py');
  
  ## ###############
  
  

  itr++;
  
end  ## <- while()

disp("That is all folks.");
