##############################
# Siemens, Forcheim, May 2017
# 
# Code for parsing data and matrices published on Monaco/AXCS message bus.
#
# Created on June 2017.
# Author: R. Mebarki
##############################


function  [ str, dicom2table_A,	    table2World_A,	    cameraCV2ImageCV_A,	    imageCV2ImageNormGL_A,	    cameraPhys2CArm_A,	    world2CArm_A,	    carm2CameraPhys_A, coordi, cArm2CameraCV_A,  dicom2CameraCV_A, xrayState] = axcs_msg_parse_func()



  ## INIT
  dicom2table_A.up=0;
  table2World_A.up=0;
  cameraCV2ImageCV_A.up=0;
  imageCV2ImageNormGL_A.up=0;
  cameraPhys2CArm_A.up=0;
  world2CArm_A.up=0;
  carm2CameraPhys_A.up=0;
  cArm2CameraCV_A.up =0; 
  dicom2CameraCV_A.up=0;
  xrayState.up=0; 
  Twp.up=0;
  coordi.up=0;
  
  
  world2CArm_A.mat= eye(4);
  carm2CameraPhys_A.mat = eye(4);

  ##
  str = mexGetMessage();
  char_array= strsplit(str, "\n");
  [row, col] = size(char_array);

  ##
  msg4.a=0;
  msg4.b=0;
  for i=1:col
    [name, right_side] = extrName(char_array{1,i});

    ##disp("**** NAME: "); name ## TODO: commented!! R.

    ## MATRICES
    if( strcmpi(name, "Dicom2Table_A")  )
      dicom2table_A.mat = string2mat(right_side, "&#0a;");
      dicom2table_A.up=1;
      
      ## Issue with Table2World. incomplete string obtained. Strange!!
    elseif( strcmpi(name, "Table2World_A")  ) 
      table2World_A.mat  = string2mat(right_side, "&#0a;");
      table2World_A.up   = 1;
      
   
      
      
    elseif( strcmpi(name, "ImageCV2ImageNormGL_A")  )
      imageCV2ImageNormGL_A.mat = string2mat(right_side, "&#0a;");  %%no
      imageCV2ImageNormGL_A.up = 1; 

      
    elseif( strcmpi(name, "CArm2World_A")  )
      carm2World_A.mat = string2mat(right_side, "&#0a;");
      carm2World_A.up = 1;
      
    elseif( strcmpi(name, "CameraPhys2CArm_A")  )
      cameraPhys2CArm_A.mat = string2mat(right_side, "&#0a;");  
      cameraPhys2CArm_A.up = 1; 

      
    elseif( strcmpi(name, "World2CArm_A")  )
      world2CArm_A.mat = string2mat(right_side, "&#0a;");
      world2CArm_A.up = 1;
     
      
    elseif( strcmpi(name, "CArm2CameraPhys_A")  )
      carm2CameraPhys_A.mat = string2mat(right_side, "&#0a;");
      carm2CameraPhys_A.up = 1;

      
    elseif( strcmpi(name, "CArm2CameraCV_A")  )
      cArm2CameraCV_A.mat = string2mat(right_side, "&#0a;");
      cArm2CameraCV_A.up = 1;
      
          
    ## *** COORDI in m and deg
    elseif( strcmpi(name, "A.IsoWorldAxcsX")  )
      coordi_x =  str2num(right_side)/1000.0;

    elseif( strcmpi(name, "A.IsoWorldAxcsY")  )
      coordi_y =  str2num(right_side)/1000.0;
      
    elseif( strcmpi(name, "A.IsoWorldAxcsZ")  )
      coordi_z =  str2num(right_side)/1000.0;
      coordi.up=1;
      
    elseif( strcmpi(name, "A.Angul")  )
      Aangul =  pi/180*str2num(right_side); # in deg
      
    elseif( strcmpi(name, "A.Cran")  )
      Acran =  pi/180*str2num(right_side); # in deg

    elseif( strcmpi(name, "CameraCV2ImageCV_A")  )
      cameraCV2ImageCV_A.mat = string2mat(right_side, ";");
      cameraCV2ImageCV_A.up = 1; 

    elseif( strcmpi(name, "Dicom2CameraCV_A")  )
      dicom2CameraCV_A.mat = string2mat(right_side, ";"); ##NEW
      dicom2CameraCV_A.up = 1; 
    
    
    elseif( strcmpi(name, "State")  )
      xrayState.val = right_side;
      xrayState.up = 1; 
    endif
    
  end #<-- for i

  if(coordi.up==1)
    coordi.x = coordi_x;
    coordi.y = coordi_y;
    coordi.z = coordi_z;
    coordi.angul = Aangul;
    coordi.cran = Acran;
  endif


end
