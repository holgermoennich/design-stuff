/*! \file   
 *  \brief      LabFrameGrabber plugin class implementation.
 *  \author     Mario Koerner - 2012 February: first implementation
 *  \detail
 *
 *  Copyright (C) Siemens AG 2006-2012 All Rights Reserved
 */

#include <Base/Log.h>
#include <Base/KeyValueSet.h>
#include <OverlayStand/LogID.h>
#include <Communication/DataStream/DataStream.h>
#include <OverlayStand/LabCom/LabImageStream.h>
#include <AppEngine/Messages.h>

#include "LabFrameGrabber.h"

#ifdef VISION_FG
#include "LabFrameGrabberVisionSource.h"
#elif EPIPHAN_FG
#include "LabFrameGrabberEpiphanSource.h"
#elif FAKE_FG
#include "LabFrameGrabberFakeSource.h"
#endif

#include <gbl/axcscom.h>
#include <ctime>
#include <sstream>

#include "FrameGrabberPluginLogID.h" 

#include <CArmGeometry/StandGeoTransform.h>  



namespace AX { namespace FrameGrabberPlugin {

    std::string padded(int i)
    {
      std::stringstream sstr;
      if (i < 10) sstr << "0";
      sstr << i;
      return sstr.str();
    }

    struct LabFrameGrabber::LabFrameGrabberPrivate
    {
      std::wstring examFolder;
      std::wstring tempFolder;

      std::wstring generateTimestampedFileName(char planeName)
      {
	time_t rawtime;
	struct tm ptm;

	time ( &rawtime );
	gmtime_s(&ptm,  &rawtime );
		
	ptm.tm_year += 1900; 
	ptm.tm_mon += 1;

		
	std::stringstream sstr;
	sstr << ptm.tm_year << padded(ptm.tm_mon) << padded(ptm.tm_mday) <<"_"<< padded(ptm.tm_hour) << padded(ptm.tm_min) << padded(ptm.tm_sec) << "_" << planeName <<".raw";
		
		
	std::string filename = sstr.str();
	std::wstring wfilename(filename.length()+1, 0);
	std::copy(filename.begin(), filename.end(), wfilename.begin());
	return wfilename;
      }
    };


    LabFrameGrabber::LabFrameGrabber(Stand::ILabDaemon* labDaemon_in, Base::XMLNode const& config_in)
      : mLabDaemon(labDaemon_in)
    {
      Base::XMLNode LabFrameGrabberConfig = config_in.getChildNode(L"labFrameGrabber");
      mP = new LabFrameGrabberPrivate();
      mGrabbingEnabled = true;
	
      int n;
      for (n=0; n<LabFrameGrabberConfig.nChildNode(L"stream"); n++)
	{
	  Base::XMLNode streamConfig = LabFrameGrabberConfig.getChildNode(L"stream", n);

	  const wchar_t* streamLabel = streamConfig.getAttribute(L"label");
	  const wchar_t* streamType = streamConfig.getAttribute(L"type");
	  const wchar_t* plane = streamConfig.getAttribute(L"plane");

	  //buffer size is currently hardcoded. if you uncomment this line, make sure to adapht the *Source files to check against
	  //this size
	  //const wchar_t* bufferSize = streamConfig.getAttribute(L"bufferSize");

	  if (streamLabel && streamType)
	    {
	      bool isLive = false;
	      if (wcscmp(streamType, L"live") == 0)
		{
		  isLive = true;
		}


	      if (mLabFrameGrabberSources.find(streamLabel) != mLabFrameGrabberSources.end())
		{
		  LOG_ERROR("LabFrameGrabber::LabFrameGrabber  Duplicate stream label '%S'", streamLabel);
		}
	      else
		{
		  LOG_INFO("LabFrameGrabber::LabFrameGrabber  Connecting stream '%S'", streamLabel);

		  try 
		    {
		      LabFrameGrabberSource* lfgs  = 0;
		      if (isLive)
			{
#ifdef VISION_FG
			  lfgs = new LabFrameGrabberVisionSource(streamConfig);
#elif EPIPHAN_FG
			  lfgs = new LabFrameGrabberEpiphanSource(streamConfig);
#elif FAKE_FG
			  lfgs = new LabFrameGrabberFakeSource(streamConfig);
#endif
			}
		      else
			{
			  LOG_ERROR("LabFrameGrabber::LabFrameGrabber Offline-FG is not supported anymore.");
			  continue;
			}

		      mLabFrameGrabberSources[streamLabel] = lfgs;
		      Base::KeyValueSet FrameGrabberInitializedMessage;
		      FrameGrabberInitializedMessage[L"cmd"] = L"Framegrabber.Initialized";
		      FrameGrabberInitializedMessage[L"planeName"] = plane;
		      if (isLive)
			{
			  FrameGrabberInitializedMessage[L"type"] = L"Live";
			}
		      else
			{
			  FrameGrabberInitializedMessage[L"type"] = L"Offline";
			}
		      mLabDaemon->publishCommand(FrameGrabberInitializedMessage);
		    }
		  catch (...)
		    {
		      LOG_ERROR("LabFrameGrabber::LabFrameGrabber  Error creating frame grabber instance for stream config #%d", n);
		    }
		}
			
	    }
	  else
	    {
	      LOG_ERROR("LabFrameGrabber::LabFrameGrabber  Error reading stream config #%d", n);
	    }
	}

      const wchar_t* tempFolder = LabFrameGrabberConfig.getAttribute(L"tempFolder");
      if (tempFolder)
	{
	  mP->tempFolder = tempFolder; 
	}
      else
	{
	  LOG_ERROR("LabFrameGrabber::LabFrameGrabber No temp folder specified, using x:\\data\\temp");
	  mP->tempFolder = L"x:\\data\\temp";
	}

    }

    LabFrameGrabber::~LabFrameGrabber()
    {
      // shut down all lab image sources
      std::map<Base::String, LabFrameGrabberSource*>::iterator it;
      for (it=mLabFrameGrabberSources.begin(); it != mLabFrameGrabberSources.end(); it++)
	{
	  delete it->second;
	}
      mLabFrameGrabberSources.clear();
      delete mP;
    }

    void LabFrameGrabber::onProcessCommand(Base::KeyValueSet &cmd_in)
    {
      if (Stand::Messages::StandGeometryChanged::checkType(cmd_in))
	{

	  // Rafik MODIFIED
	  CArmGeometry::StandGeoParam geoParam;
	  geoParam.fromKav(cmd_in, L"A.");

	  CArmGeometry::StandGeoTransform geoTransform;
	  geoTransform.setParams(geoParam, CArmGeometry::PlaneA);

	  //R.
	  std::wstring coucou( L"777" );
	  const wchar_t* szCoucou = coucou.c_str();
	  //.

	  if(1){
	    AX::AppEngine::Messages::MessageBase msg(L"Stand.AllMatrices");
	    msg.set(L"id", szCoucou );
	    msg.set(L"Dicom2Table_A", geoTransform.Dicom2Table().toString() );
	    //  msg.set(L"CameraCV2ImageCV_A", geoTransform.CameraCV2ImageCV().toString() );
	    msg.set(L"Table2World_A",geoTransform.Table2World().toString() );
	    msg.set(L"ImageCV2ImageNormGL_A", geoTransform.ImageCV2ImageNormGL().toString() );
	    msg.set(L"CArm2CameraPhys_A", geoTransform.CArm2CameraPhys().toString() );
	    msg.set(L"World2CArm_A", geoTransform.World2CArm().toString() );
	    msg.set(L"CArm2CameraCV_A", geoTransform.CArm2CameraCV().toString() );
	    msg.set(L"id", szCoucou );
	    mLabDaemon->publishCommand(msg);
	  }
	  else{
 AX::AppEngine::Messages::MessageBase msg(L"Stand.AllMatrices");
	    msg.set(L"id", szCoucou );
	    msg.set(L"Dicom2Table_A", geoTransform.Dicom2Table().toString() );
	    msg.set(L"CameraCV2ImageCV_A", geoTransform.CameraCV2ImageCV().toString() );
	    //!! msg.set(L"Table2World_A",geoTransform.Table2World().toString() );
	    //!! msg.set(L"ImageCV2ImageNormGL_A", geoTransform.ImageCV2ImageNormGL().toString() ); //not enough space left on the msg, it seems...!?  Had to add other messages!!//R.
	    msg.set(L"id", szCoucou );
	    mLabDaemon->publishCommand(msg);
	  
	    AX::AppEngine::Messages::MessageBase msg2(L"Stand.AllMatrices2");
	    msg2.set(L"Table2World_A",geoTransform.Table2World().toString() );
	    //!!  msg2.set(L"ImageCV2ImageNormGL_A", geoTransform.ImageCV2ImageNormGL().toString() ); did not work nether. Had to add another msg (msg3 below)!! //R.
	    mLabDaemon->publishCommand(msg2);
	  
	    AX::AppEngine::Messages::MessageBase msg3(L"Stand.AllMatrices3");
	    msg3.set(L"ImageCV2ImageNormGL_A", geoTransform.ImageCV2ImageNormGL().toString() );
	    mLabDaemon->publishCommand(msg3);	   

	    AX::AppEngine::Messages::MessageBase msg4(L"Stand.AllMatrices4");
	    //msg4.set(L"CArm2World_A", geoTransform.CArm2World().toString() );
	    // msg4.set(L"CameraPhys2CArm_A", geoTransform.CameraPhys2CArm().toString() );
	    msg4.set(L"CArm2CameraPhys_A", geoTransform.CArm2CameraPhys().toString() );
	    msg4.set(L"World2CArm_A", geoTransform.World2CArm().toString() );
	    mLabDaemon->publishCommand(msg4);

	    AX::AppEngine::Messages::MessageBase msg5(L"Stand.AllMatrices5");
	    msg5.set(L"CArm2CameraCV_A", geoTransform.CArm2CameraCV().toString() );
	    mLabDaemon->publishCommand(msg5);
	  }
	
	
	  //////////////

	  //*************
	  Stand::Messages::StandGeometryChanged m(cmd_in);

	  // set current parameters to all lab image sources
	  std::map<Base::String, LabFrameGrabberSource*>::iterator it;
	  for (it=mLabFrameGrabberSources.begin(); it != mLabFrameGrabberSources.end(); it++)
	    {
	      using std::cout; using std::endl; //R.
	      Math::Matrix4D dicom2cam;
	      Math::Matrix4D cam2img;
	      //  Math::Matrix4D testmat; //R.
	      
	      if (m.hasDicom2CameraCV_A() && m.hasCameraCV2ImageCV_A())
		{
		  dicom2cam = Math::Matrix4D::fromString(m.getDicom2CameraCV_A(), L";");
		  cam2img = Math::Matrix4D::fromString(m.getCameraCV2ImageCV_A(), L";");

		  //cam2img = Math::Matrix4D::fromString(m.getCameraCV2ImageCV_A(), L";");
		  //msg.set(L"Table2World_A",geoTransform.Table2World().toString() );

		   //  testmat = Math::Matrix4D::fromString()
		  it->second->setStandGeometry(cmd_in, AX::CArmGeometry::PlaneA, &dicom2cam, &cam2img);
		  cout << "here1" << endl; //R.

		  //R.
		  it->second->setStandGeometry(cmd_in, AX::CArmGeometry::PlaneA, &dicom2cam, &cam2img);
		  //.
		}
	      else
		{
		  it->second->setStandGeometry(cmd_in, AX::CArmGeometry::PlaneA, 0, 0);
		  cout  << "here2" << endl;
		}

	      if (m.hasDicom2CameraCV_B() && m.hasCameraCV2ImageCV_B())
		{
		  dicom2cam = Math::Matrix4D::fromString(m.getDicom2CameraCV_B(), L";");
		  cam2img = Math::Matrix4D::fromString(m.getCameraCV2ImageCV_B(), L";");
		  it->second->setStandGeometry(cmd_in, AX::CArmGeometry::PlaneB, &dicom2cam, &cam2img);
		  cout  << "here3" << endl;
		}
	      else
		{
		  it->second->setStandGeometry(cmd_in, AX::CArmGeometry::PlaneB, 0, 0);
		  cout  << "here4" << endl;
		}
	    }
	


	}
      else if (cmd_in[L"cmd"] == L"Framegrabber.StartCapture")
	{
	  toggleGrabber(cmd_in[L"Stream"], true);
	}
      else if (cmd_in[L"cmd"] == L"Framegrabber.StopCapture")
	{
	  toggleGrabber(cmd_in[L"Stream"], false);
	}
      else if (cmd_in[L"cmd"] == L"Framegrabber.SetDumpFileName")
	{
	  LOG_ERROR("LabFrameGrabber: SetDumpFileName message is not supported anymore");
	}
      else if (cmd_in[L"cmd"] == L"FrameGrabber.DisableGrabbing")
	{
	  mGrabbingEnabled = false;
	}
      else if (cmd_in[L"cmd"] == L"FrameGrabber.EnableGrabbing")
	{
	  mGrabbingEnabled = true;
	}
      else if (cmd_in[L"cmd"] == L"Framegrabber.SetPlaneBOffset")
	{

	  using std::cout; using std::endl;
	  cout << endl << "****** HERE!! *******" << endl;
	  std::map<Base::String, LabFrameGrabberSource*>::iterator sourceIt;
	  for (sourceIt = mLabFrameGrabberSources.begin(); sourceIt != mLabFrameGrabberSources.end(); sourceIt++)
	    {

	      cout << "## okk ## " << endl;
	      float IsoTableX =  _wtof(cmd_in[L"IsoTableX"]);
	      float IsoTableY =  _wtof(cmd_in[L"IsoTableY"]);
	      float IsoTableZ =  _wtof(cmd_in[L"IsoTableZ"]);
	      sourceIt->second->setPlaneBOffset(IsoTableX, IsoTableY, IsoTableZ);
	      LOG_INFO("PlaneB offset changed: (%f,%f,%f)", IsoTableX, IsoTableY, IsoTableZ); 
	    }
	}

      else if (Overlay::Messages::ImageStreamSubscribe::checkType(cmd_in))
	{
	  Overlay::Messages::ImageStreamSubscribe m(cmd_in);
	  std::map<Base::String, LabFrameGrabberSource*>::iterator sourceIt;
	  for (sourceIt = mLabFrameGrabberSources.begin(); sourceIt != mLabFrameGrabberSources.end(); sourceIt++)
	    {
	      if (sourceIt->second->dataStream() == NULL)
		continue;

	      Overlay::Messages::PublishImageStream sis;
	      sis.setChannel(m.getChannel());
	      const wchar_t* name = sourceIt->second->dataStream()->name();
	      unsigned int bufferCnt = sourceIt->second->dataStream()->bufferCnt();
	      unsigned int bufferSize = sourceIt->second->dataStream()->bufferSize();
	      sis.setBufferName(sourceIt->second->dataStream()->name());
	      sis.setBufferCount(sourceIt->second->dataStream()->bufferCnt());
	      sis.setBufferSize(sourceIt->second->dataStream()->bufferSize());
	      sis.setStream(sourceIt->second->dataStream()->name());
	      sis.setHeaderSize(sourceIt->second->dataStream()->headerSize());
	      mLabDaemon->publishCommand(sis);
	    }

	  /****************************************************** HACK **************************************************************/
	  // The information about the 'OfflineA' should actually be published via the StreamRecorderPlugin-Class.
	  // Since the Overlay-Plugin request message currently can not be received by the StreamRecorderPlugin,
	  // we inform here about the existance of this stream.
	  // 
	  // TODO: Move the lines below to StreamRecorderPlugin, as soon as messages from OverlayPlugin can be received by StreamRecorder
	  Overlay::Messages::PublishImageStream sis;
	  sis.setChannel(m.getChannel());
	  const wchar_t* name = L"OfflineA";
	  unsigned int bufferCnt = 1000;
	  unsigned int bufferSize = 1048576;
	  sis.setBufferName(name);
	  sis.setBufferCount(bufferCnt);
	  sis.setBufferSize(bufferSize);
	  sis.setStream(name);
	  sis.setHeaderSize(568);
	  mLabDaemon->publishCommand(sis);
	}
      else if (cmd_in[L"cmd"] == L"FrameGrabber.SetExamFolder")
	{
	  mP->examFolder = cmd_in[L"path"];

	  if (mP->examFolder == L"")
	    {
	      LOG_ERROR("LabFrameGrabber:  No working folder specified in Framegrabber.SetWorkingFolder message; using default temp folder");
	      mP->examFolder = mP->tempFolder;
	    }
	}
      else if (cmd_in[L"cmd"] == L"FrameGrabber.LoadFileToOfflineStream")
	{
	  LOG_ERROR("LabFrameGrabber: Loading file to Offline Stream nor supported any more.");
	}

    }

    void LabFrameGrabber::toggleGrabber(const wchar_t* streamName, bool enabled)
    {
      std::map<Base::String, LabFrameGrabberSource*>::iterator it;
      it = mLabFrameGrabberSources.find(streamName);
      if (it != mLabFrameGrabberSources.end())
	{
	  LabFrameGrabberSource* theSource = it->second;
	  if (enabled && !theSource->isGrabbing())
	    {
	      std::wstring tempPath = mP->tempFolder + L"\\";
	      if (mP->examFolder == L"")
		{
		  LOG_ERROR("LabFrameGrabber:  No exam folder specified; using default temp folder");
		  mP->examFolder = L"x:\\data\\temp";
		}
	      char planeChar = 'A';
	      if (theSource->getPlane() == AX::CArmGeometry::PlaneB)
		{
		  planeChar = 'B';
		}
	      theSource->setDumpFileName(mP->generateTimestampedFileName(planeChar).c_str(), tempPath.c_str(), mP->examFolder.c_str());

	      theSource->startGrabbing();
	    }
	  else
	    {
	      theSource->stopGrabbing();
	    }
	}
      else
	{
	  A3D_LOG_INSTANCE.logMessage(Base::Log::LOG_TRACE, LogID, 0, "LabFrameGrabber::toggleGrabber: stream \"%S\" not found", streamName);
	}
    }

    void LabFrameGrabber::onAxcsMessage(unsigned short msgId_in, void* msg_in)
    {
      A3D_LOG_INSTANCE.logMessage(Base::Log::LOG_TRACE, LogID, 0, "Received AXCS telegram 0x%04x from LabDaemon", msgId_in);
      if (msgId_in == SH_PREP)
	{
	  sh_prep_t1* prepMsg = static_cast<sh_prep_t1*>(msg_in);
	  if (prepMsg->prepa == ACS_DONE && mGrabbingEnabled)
	    {
	      toggleGrabber(L"LiveA", true);
	    }
	  else if (prepMsg->prepa == ACS_XRAY_OFF)
	    {
	      toggleGrabber(L"LiveA", false);
	    }
	  else if (prepMsg->prepb == ACS_DONE && mGrabbingEnabled)
	    {
	      toggleGrabber(L"LiveB", true);
	    }
	  else if (prepMsg->prepb == ACS_XRAY_OFF)
	    {
	      toggleGrabber(L"LiveB", false);
	    }
	}
      else if (msgId_in == UI_PPP)
	{
	  ui_ppp_t1_tag* pppMsg = static_cast<ui_ppp_t1_tag*>(msg_in);

	  if (pppMsg->ppp_func == ACS_POS_REACHED)
	    {
	      //Need to send message pos reached to msg bus here
				
	      //in case of xray, labdaemon already sends message:
	      /*Plane=A
		State=On/Off
		cmd=Stand.XRay*/
	
	    }
	}
    }


    int LabFrameGrabber::getAxcsMessages(unsigned short *msgIds)
    {
      msgIds[0] = SH_STPAR;
      msgIds[1] = SH_PREP;
      msgIds[2] = UI_PPP;
      return 3;
    }

    void LabFrameGrabber::onShutdown()
    {
	
      std::map<Base::String, LabFrameGrabberSource*>::iterator it;
      for (it=mLabFrameGrabberSources.begin(); it != mLabFrameGrabberSources.end(); it++)
	{
	  unsigned int result;
	  it->second->join(&result, 500);
	  delete it->second;
	}
      mLabFrameGrabberSources.clear();
    }




  }} // namespaces
