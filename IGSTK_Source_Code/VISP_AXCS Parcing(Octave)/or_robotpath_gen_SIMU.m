############################
## Siemens, GE
## Created on June 13th 2017
##############################

function planned_path_matrix = or_robotpath_gen_SIMU(sLBR_robot, sLBR_problem,
						     rLBR_robot, rLBR_problem,
						     rLBR_joints_target_deg)


  sleep(2);
  disp("*********  Path planning  **************");
  sLBR_joints_cmd  = pi/180*rLBR_joints_target_deg.';

  planned_path_string = orProblemSendCommand(['MoveManipulator goal ' sprintf('%f ',sLBR_joints_cmd(:)) ' outputtraj' ], sLBR_problem);

  orEnvWait(sLBR_robot.id);

  printf("\n\n ********* Planning done. \n\n");

  sleep(2);

  ## ######### CONVERT STRING PATH INTO MATRIX PATH

  if( ~isempty(planned_path_string))
    planned_path = str2num(planned_path_string)';
    
			    % Unterscheidung zwischen 6 DOF und 7 DOF!
    if ( mod((length(planned_path)-4),13)==0 )
      planned_path_matrix =reshape(planned_path(5:end),13,planned_path(1));
    end
    if (mod((length(planned_path)-4),14)==0)
      planned_path_matrix =reshape(planned_path(5:end),14,planned_path(1));
    end
    if (mod((length(planned_path)-4),15)==0)
      planned_path_matrix =reshape(planned_path(5:end),15,planned_path(1));
    end
    if (mod((length(planned_path)-4),16)==0)
      planned_path_matrix =reshape(planned_path(5:end),16,planned_path(1));
    end
  else
    fprintf('gave a problem, got nothing in return...\n');
    
  end

  ## ############
  
  if(0)
    [nrows, ncols] = size(planned_path_matrix);
    itr =1;

    while( itr<=ncols)
      sLBR_joint_ref = planned_path_matrix(1:7, itr);
      rLBR_joints_target = 180/pi*sLBR_joint_ref.';
      
      homePosition = rLBR_robot.moveToPtp( rLBR_joints_target(1), rLBR_joints_target(2), rLBR_joints_target(3), rLBR_joints_target(4), rLBR_joints_target(5), rLBR_joints_target(6), rLBR_joints_target(7) ); % in [deg]

      ##  reading current rLBR joint configuration
      rLBR_current_joints_rad = rLBR_robot.Position.get(); % in [rad]
      
      ## TRANSFER as cmds to Virtual LBR
      sLBR_joints_cmd  = rLBR_current_joints_rad.';
      orRobotSetDOFValues(sLBR_robot.id, sLBR_joints_cmd(1:7), 0:6);

      

      ##
      itr+= 10; ## to seed up the process
    end
  end
  
######################
####################
  if(1)
    [nrows, ncols] = size(planned_path_matrix);

    for itr=1:ncols
      target_pose = planned_path_matrix(:, itr);
      ##orProblemSendCommand(['movetohandposition matrix ',num2str(reshape(sLBR_link8_final,1,12)), 'outputtraj'], sLBR_problem);
      orProblemSendCommand(['MoveManipulator goal ' sprintf('%f ',target_pose(1:7))], rLBR_problem);
      orEnvWait(rLBR_robot.id);
      printf("Replicated motion plan: %d\n", itr);
    end
    
  end


  
end  ## function end.
