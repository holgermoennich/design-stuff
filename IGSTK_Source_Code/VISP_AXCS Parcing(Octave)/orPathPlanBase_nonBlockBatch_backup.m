############################
## Siemens, DE
## Created on June 13th 2017
## Auhor: R. Mebarki
##
## Function: 1. generate a path for LBR 
##           2. apply the path ; sending commands to the real robot  
##           3. replicate the real robot configuration onto the simulated one; emulation 
##  
##############################

function [planned_path_matrix, status] =  orPathPlanBase_nonBlockBatch2(sLBR_robot, sLBR_problem,
							 rLBR_robot,
							 H_target,
							 status)


  ## #####################
  ##   GENERATE PATH
  ## #####################
  ## disp("*********  Path planning  **************");
  

  ## Cartesian
  planned_path_string = orProblemSendCommand(['MoveToHandPosition matrix ' sprintf('%f ',H_target(1:3,1:4))  ' outputtraj' ],
					     sLBR_problem);
  
  orEnvWait(sLBR_robot.id);

  ##printf("\n\n ********* Planning done. \n\n");

  

  ## #####################################
  ## CONVERT STRING PATH INTO MATRIX PATH
  ## ######################################
  clear planned_path_matrix; ## R. mew

  
  if( ~isempty(planned_path_string))
    
    planned_path = str2num(planned_path_string)';
    [row, col] = size(planned_path);
    
    ## Unterscheidung zwischen 6 DOF und 7 DOF!
    if ( mod((length(planned_path)-4),13)==0 )
      ##   planned_path_matrix =reshape(planned_path(5:end),13,planned_path(1));
      new_lg = (row-4)/13;
      planned_path_matrix =reshape(planned_path(5:end),13,new_lg);
    end
    if (mod((length(planned_path)-4),14)==0)
      ##    planned_path_matrix =reshape(planned_path(5:end),14,planned_path(1));
      new_lg = (row-4)/14;
      planned_path_matrix =reshape(planned_path(5:end),14, new_lg);
    end
    if (mod((length(planned_path)-4),15)==0)
      ## planned_path_matrix =reshape(planned_path(5:end),15,planned_path(1));
      new_lg = (row-4)/15;
      planned_path_matrix =reshape(planned_path(5:end),15,new_lg);
    end
    if (mod((length(planned_path)-4),16)==0)
      ## planned_path_matrix =reshape(planned_path(5:end),16,planned_path(1));
      new_lg = (row-4)/16;
      planned_path_matrix =reshape(planned_path(5:end),16,new_lg);
    end
  else
    ##fprintf('gave a problem, got nothing in return...\n');
    planned_path_matrix =-1;
    return;
    
  end

  ## ############
  
  
  [nrows, ncols] = size(planned_path_matrix);

   ## NEW
  status.sentCmd =1;
  runMotionBatch( rLBR_robot, planned_path_matrix(1:7, 1:ncols-1));

  rLBR_joints_target = planned_path_matrix(1:7, ncols);
  homePosition =  rLBR_robot.moveToJointPositionInRad_NB_Status(rLBR_joints_target(1),
  								rLBR_joints_target(2),
  								rLBR_joints_target(3),
  								rLBR_joints_target(4),
  								rLBR_joints_target(5),
  								rLBR_joints_target(6),
								rLBR_joints_target(7));

  
end
