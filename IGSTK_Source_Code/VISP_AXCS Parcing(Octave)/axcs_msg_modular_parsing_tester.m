##############################
# Siemens, Forcheim, June 2017
# Author: R. Mebarki
# Main code (executable) for parsing data and matrices from Monaco AXCS message bus.
# - Use it to test the parsing.
# 
# 
##############################

clear all
more off
global probs


langname = 'octave';

##############
# PATHS
##############

##addpath('C:\Program Files\openrave\share\openrave\octave')
##addpath('D:\SpineMonaco\AXCS_Octave') # peter removed
##addpath ('D:\SpineMonaco\OctaveOpenIGT')
addpath ('D:\dev\code_siemens_com_lemmium\Lemmium\orHardwLoop_SimCtrl\DemoAlpha\OctaveOpenIGT')

##addpath ("c:/Octave/Octave-4.2.1/share/octave/packages/quaternion-2.4.0/")
##addpath ("c:/Octave/Octave-4.2.1/lib/octave/packages/quaternion-2.4.0/x86_64-w64-mingw32-api-v51/")

xrayState="off"

######################################################
##################        MAIN LOOP     ##############
######################################################
camok=0;
itr=0;
activation=0;
flNbr=0;
ProcessPicture=0;
while(1)

  ## #############
  ## READIG FROM ZEEGO/MONACO AXCS MESSAGE BUS
  ## NOTE: matrices in [mm]!!  xrayState_
  ## #############
  
  
  [ str_, dicom2table_A_,    table2World_A_,    cameraCV2ImageCV_A_,    imageCV2ImageNormGL_A_,    cameraPhys2CArm_A_,    world2CArm_A_,    carm2CameraPhys_A_,    coordi_, carm2CameraCV_A_,  dicom2CameraCV_A_ , xrayState_] = axcs_msg_parse_func();

  if(dicom2table_A_.up==1) dicom2table_A = dicom2table_A_.mat; endif
  if(table2World_A_.up==1)  table2World_A = table2World_A_.mat; endif
  if(cameraCV2ImageCV_A_.up==1) cameraCV2ImageCV_A = cameraCV2ImageCV_A_.mat; endif
  if(imageCV2ImageNormGL_A_.up==1) imageCV2ImageNormGL_A = imageCV2ImageNormGL_A_.mat; endif
  if(cameraPhys2CArm_A_.up==1)    cameraPhys2CArm_A = cameraPhys2CArm_A_.mat;  endif
  if(world2CArm_A_.up==1)    world2CArm_A = world2CArm_A_.mat;  endif
  if(carm2CameraPhys_A_.up==1)    carm2CameraPhys_A = carm2CameraPhys_A_.mat;  endif
  if(carm2CameraCV_A_.up==1)    carm2CameraCV_A = carm2CameraCV_A_.mat;  endif # new
  if(dicom2CameraCV_A_.up==1)    dicom2CameraCV_A = dicom2CameraCV_A_.mat;  endif # new
  if(coordi_.up==1)    coordi = coordi_;  endif
  if(xrayState_.up==1)    xrayState = xrayState_.val endif   
  
 ##======= DETECT FLAG
 if(dicom2CameraCV_A_.up==1) 
 camok=1;
 endif
 
 if( strcmpi(xrayState, "On")  )
  activation++;
 else
 activation=0;
  ProcessPicture=0;
  endif

  %if( strcmpi(xrayState, "Off")  )
  %activation=0;
  %ProcessPicture=0;
 %endif
 
  ##if(dicom2CameraCV_A_.up==1) 
  if( activation>0 && camok==1)
  ProcessPicture++;
  endif
  
  
  
  ##================ PROCESS
  if(ProcessPicture > 0)
  ProcessPicture++;
  
  flNbr++
    sd = igtlopen('localhost', 1005);

    if sd == -1
     error ('Could not connect to the Server :('); 
    end 
    Camera2Marker = igtlreceive(sd);
    Camera2Marker = Camera2Marker.Trans;
    igtlclose(sd);

    #### Convert from cm to mm ######
    Camera2Marker(1:3, 4) = Camera2Marker(1:3, 4) * 10;
    
    Dicom2Camera = dicom2CameraCV_A;
    Dicom2Marker = inv(Camera2Marker) * Dicom2Camera;

    D2M.Type = 'TRANSFORM' ;
    D2M.Name = 'Dicom2Marker' ;
    D2M.Trans = Dicom2Marker;
    send = igtlopen('localhost', 1006);
    r = igtlsend(send , D2M)
    igtlclose(send)
    
    
    D2C.Type = 'TRANSFORM' ;
    D2C.Name = 'Dicom2Camera' ;
    D2C.Trans = Dicom2Camera ;
    send = igtlopen('localhost', 1007);
    r = igtlsend(send , D2C)
    igtlclose(send)
    
  endif
    
   
   
  itr++;
  activation=0;

end  
disp("\n\n\n********* That is all folks.");
