################################################
##
## Siemens, July 2017 fORCHEIM
## Author:     R. Mebarki 
##
## converting a homegeneous matrix to meter order.
##
##
##################################################



##
##
function mat = cvHmat2meter(mat_mm)
  
  mat = mat_mm;
  mat(1:3,4) = 1e-3* mat_mm(1:3,4);
end

