# IGSTK

The code inside the IGSTK Folder is the the latest IGSTK (V5.2) source code from http://www.igstk.org/IGSTK/resources/software.html

The source code was patched/changed to support the Atracsys FTk500 camera https://atracsys.com/web/documents/upload/fTk500-datasheet.pdf and the soure file igstkAtracsysFusionTrackTracker.cxx

The source code was also extended to support X Ray Mono Tracking via the VISP Library - igstkMonocularMarkerTracker.cxx


License
----

MIT
