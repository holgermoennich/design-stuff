/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkMonocularMarkerConfiguration.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah (Siemens Healthineers)

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "igstkMonocularMarkerConfiguration.h"

namespace igstk
{                  //highest referesh rate for the MonocularMarker tracker family (Sx60)
const double MonocularMarkerConfiguration::MAXIMAL_REFERESH_RATE = 300;


MonocularMarkerConfiguration::MonocularMarkerConfiguration() :
m_PixelSpacing(0)
, m_SDD(0)
{
	 cout<<"SDD 3 lll    " << m_SDD << endl;
  this->m_Frequency = this->MAXIMAL_REFERESH_RATE;
 
}


MonocularMarkerConfiguration::~MonocularMarkerConfiguration()
{
}

double
MonocularMarkerConfiguration::GetMaximalRefreshRate()
{
  return this->MAXIMAL_REFERESH_RATE;
}

void 
MonocularMarkerConfiguration::InternalAddTool( 
  const TrackerToolConfiguration *tool, 
  bool isReference )
{
  AddToolFailureEvent fe;

  const MonocularMarkerToolConfiguration *wirelessTool = 
    dynamic_cast<const MonocularMarkerToolConfiguration *>( tool );

  if( wirelessTool == NULL )
    {
    fe.Set( "Given tool configuration type not compatible with tracker type." );
    this->InvokeEvent( fe );
    return;
    }
 
  if( !isReference )
    {
    this->m_TrackerToolList.insert(std::pair<std::string, 
                                   TrackerToolConfiguration *>
      (wirelessTool->GetToolName(), 
       new MonocularMarkerToolConfiguration( *wirelessTool )) );
    }
  else
    {
    if (this->m_ReferenceTool)
      {
      delete this->m_ReferenceTool;
      }
    this->m_ReferenceTool = new MonocularMarkerToolConfiguration( *wirelessTool );
    }
  this->InvokeEvent( AddToolSuccessEvent() );
}


MonocularMarkerToolConfiguration::MonocularMarkerToolConfiguration() :m_Point()               
{
}


MonocularMarkerToolConfiguration::MonocularMarkerToolConfiguration( const 
  MonocularMarkerToolConfiguration &other ) : TrackerToolConfiguration( other )
{
	
	this->m_Point = other.m_Point;
}


MonocularMarkerToolConfiguration::~MonocularMarkerToolConfiguration() 
{
}


std::string 
MonocularMarkerToolConfiguration::GetToolTypeAsString()
{
  return "MonocularMarkerToolConfiguration";
}

} // end of name space
