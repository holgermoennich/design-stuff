/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkMonocularMarkerConfigurationXMLFileReader.h
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah (Siemens Healthineers)

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef __igstkMonocularMarkerConfigurationXMLFileReader_h
#define __igstkMonocularMarkerConfigurationXMLFileReader_h

#include <itkXMLFile.h>
#include "igstkTrackerConfigurationXMLFileReaderBase.h"

namespace igstk
{
/**
 * \class MonocularMarkerConfigurationXMLFileReader 
 *
 * \brief This class is used to read the xml
 *        configuration file for Claron Technology Inc. MonocularMarker tracker.
 *
 *        This class is used to read the xml
 *        configuration file for Claron Technology Inc. MonocularMarker tracker.
 */  
class MonocularMarkerConfigurationXMLFileReader : 
   public TrackerConfigurationXMLFileReaderBase 
{
public:

  //standard typedefs
  typedef MonocularMarkerConfigurationXMLFileReader          Self;
  typedef TrackerConfigurationXMLFileReaderBase     Superclass;
  typedef itk::SmartPointer<Self>                   Pointer;

  //run-time type information (and related methods)
  itkTypeMacro( MonocularMarkerConfigurationXMLFileReader, 
                TrackerConfigurationXMLFileReaderBase );

  itkNewMacro( Self );

  /**
   * Method called when a new xml tag start is encountered.
   */
  virtual void StartElement( const char * name, const char **atts ); 


  /**
   * Method called when an xml tag end is encountered.
   */
  virtual void EndElement( const char *name ); 


   /**
   * Method for checking if the configuration data has been read. When the xml 
   * file is empty or doesn't contain the relevant tags the read operation is 
   * successful, but we don't have the information we need.
   */
  virtual bool HaveConfigurationData(); 


  /**
   * Return a pointer to the object containing the configuration data.
   */
  virtual const igstk::TrackerConfiguration::Pointer 
    GetTrackerConfigurationData() throw ( FileFormatException );


protected:

  virtual double GetMaximalRefreshRate();
  virtual std::string GetSystemType();

  //this is the constructor that is called by the factory to 
  //create a new object
  MonocularMarkerConfigurationXMLFileReader() : TrackerConfigurationXMLFileReaderBase()
  { }
  virtual ~MonocularMarkerConfigurationXMLFileReader() {}

  void ProcessMonocularMarkerPointX() throw ( FileFormatException );
  void ProcessMonocularMarkerPointY() throw ( FileFormatException );
  void ProcessMonocularMarkerPointZ() throw ( FileFormatException );
  void ProcessMonocularMarkerPixelSpacing() throw ( FileFormatException );
  void ProcessMonocularMarkerSDD() throw ( FileFormatException );

  virtual void ProcessToolData() throw ( FileFormatException );

 
   bool m_ReadingMarker;
  
  float	        	        m_MonocularMarkerPixelSpacing;
  unsigned int		        m_MonocularMarkerSDD;

  std::vector<float> m_CurrentPointVector;
  std::vector< std::vector<float> > Markers;
  private:
  MonocularMarkerConfigurationXMLFileReader( 
    const MonocularMarkerConfigurationXMLFileReader & other );


  const MonocularMarkerConfigurationXMLFileReader & operator=( 
    const MonocularMarkerConfigurationXMLFileReader & right );
};


}
#endif //__igstkMonocularMarkerConfigurationXMLFileReader_h
