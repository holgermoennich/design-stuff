/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkAtracsysFusionTrackConfigurationXMLFileReader.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include <itksys/SystemTools.hxx>

#include "igstkAtracsysFusionTrackConfigurationXMLFileReader.h"
#include "igstkAtracsysFusionTrackTrackerConfiguration.h"

namespace igstk
{


void 
AtracsysFusionTrackConfigurationXMLFileReader::StartElement( 
  const char * name, 
  const char **atts )
{
  //let the super class try to analyze the current tag
  Superclass::StartElement( name, atts );
  if( itksys::SystemTools::Strucmp( name, "initialization_file" ) == 0 )
    {
		if(!m_ReadingToolConfiguration )
		{
         throw FileFormatException( "\"initialization_file\" tag not nested in \"tool\" tag." );
		}
    }  
}


void 
AtracsysFusionTrackConfigurationXMLFileReader::EndElement( 
  const char *name )
{
  //let the super class try to analyze the current tag
  Superclass::EndElement( name );
  
  if( m_ReadingToolConfiguration &&
           itksys::SystemTools::Strucmp( name, "initialization_file" ) == 0 )
    {
    ProcessAtracsysFusionTrackInitializationFile();
    }
}


std::string 
AtracsysFusionTrackConfigurationXMLFileReader::GetSystemType()
{
  return "Atracsys Fusion 500";
}


double 
AtracsysFusionTrackConfigurationXMLFileReader::GetMaximalRefreshRate()
{
  igstk::AtracsysFusionTrackTrackerConfiguration::Pointer trackerConfig =
    igstk::AtracsysFusionTrackTrackerConfiguration::New();

  return trackerConfig->GetMaximalRefreshRate();
}

void 
AtracsysFusionTrackConfigurationXMLFileReader::ProcessAtracsysFusionTrackInitializationFile() 
throw ( FileFormatException )
{
  if( !itksys::SystemTools::FileExists( this->m_CurrentTagData.c_str() ) ||
      itksys::SystemTools::FileIsDirectory( this->m_CurrentTagData.c_str()  ) )
    {
    throw FileFormatException( 
      "Invalid string given as initialization_file tag" );
    }
  this->m_CurrentInitializationFile = this->m_CurrentTagData;
}


void 
AtracsysFusionTrackConfigurationXMLFileReader::ProcessToolData() 
throw ( FileFormatException )
{
  if( this->m_CurrentToolName.empty() )
    {
    throw FileFormatException( "\"name\" missing for one of the tools." );
    }
  if( this->m_CurrentInitializationFile.empty() )
    {
    throw FileFormatException( "\"initialization_file\" missing for one of the tools." );
    }

  //if the tool section does not have a "calibration_file" tag 
  //we assume the calibration is identity
  igstk::AtracsysFusionTrackToolConfiguration *toolConfig = 
      new igstk::AtracsysFusionTrackToolConfiguration();
  
  toolConfig->SetToolName( this->m_CurrentToolName );
  toolConfig->SetInitializationFile( this->m_CurrentInitializationFile );
  toolConfig->SetCalibrationTransform( this->m_CurrentToolCalibration );  
  if( this->m_CurrentToolIsReference )
    {
    this->m_ReferenceTool = toolConfig;
    }
  else
    {
    this->m_TrackerToolList.push_back( toolConfig );
    }

  //reset all tool data to initial state
  this->m_CurrentToolIsReference = false;
  this->m_CurrentToolName.clear();
  this->m_CurrentInitializationFile.clear();
  this->m_CurrentToolCalibration.SetToIdentity( 
    igstk::TimeStamp::GetLongestPossibleTime() );
}


bool 
AtracsysFusionTrackConfigurationXMLFileReader::HaveConfigurationData()
{
  //check that we have a refresh rate for the tracking system, at least 
  //one tool
  return ( this->m_HaveRefreshRate &&
           this->m_TrackerToolList.size() != 0  );
}


const igstk::TrackerConfiguration::Pointer
AtracsysFusionTrackConfigurationXMLFileReader::GetTrackerConfigurationData()
throw ( FileFormatException )
{
  igstk::AtracsysFusionTrackTrackerConfiguration::Pointer trackerConfig = 
    igstk::AtracsysFusionTrackTrackerConfiguration::New();
    
  //this request is guaranteed to succeed as the refresh rate
  //is validated in the TrackerConfigurationXMLReaderBase
  trackerConfig->RequestSetFrequency( this->m_RefreshRate );
  
  //add the tools
  std::vector<TrackerToolConfiguration *>::iterator it, end; 
  it = this->m_TrackerToolList.begin();
  end = this->m_TrackerToolList.end();  
  
  AddToolFailureObserver::Pointer failureObserver =
    AddToolFailureObserver::New();
  trackerConfig->AddObserver( igstk::TrackerConfiguration::AddToolFailureEvent(),
                              failureObserver );
  for(; it!=end; it++ )
    {
    trackerConfig->RequestAddTool( *it );
    if( failureObserver->GotAddToolFailure() )
      {
      throw FileFormatException( failureObserver->GetAddToolFailure() );
      }
    }
  if( this->m_ReferenceTool != NULL )
    {
    trackerConfig->RequestAddReference( this->m_ReferenceTool );
    if( failureObserver->GotAddToolFailure() )
      {
      throw FileFormatException( failureObserver->GetAddToolFailure() );
      }
    }
 
  //explicitly upcast to avoid the compiler warning
  igstk::TrackerConfiguration::Pointer genericConfig;
  genericConfig  = trackerConfig;
  return genericConfig;
}

} //namespace
