/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkMonocularMarkerConfigurationXMLFileReader.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah (Siemens Healthineers)

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include <itksys/SystemTools.hxx>

#include "igstkMonocularMarkerConfigurationXMLFileReader.h"
#include "igstkMonocularMarkerConfiguration.h"
#include <vector>

namespace igstk
{


void 
MonocularMarkerConfigurationXMLFileReader::StartElement( 
  const char * name, 
  const char **atts )
{
  //let the super class try to analyze the current tag
  Superclass::StartElement( name, atts );

  if(  itksys::SystemTools::Strucmp( name,"pixel_spacing" ) == 0 ||
       itksys::SystemTools::Strucmp( name,"sdd" ) == 0  )
    {
    if( !m_ReadingTrackerConfiguration )
      {
      throw FileFormatException( "Tag not nested correctly in xml structure. (Pixel_Spacing , Sdd)" );
      }
    }

   if( itksys::SystemTools::Strucmp( name, "marker" ) == 0 ) 
    {
   
    this->m_ReadingMarker = true;
    }          //Tool Marker
  else if( itksys::SystemTools::Strucmp( name,"pointx" ) == 0 ||
           itksys::SystemTools::Strucmp( name,"pointy" ) == 0 ||
           itksys::SystemTools::Strucmp( name,"pointz" ) == 0 )
          
    {
    if( !m_ReadingMarker )
      {
      throw FileFormatException( "Tag not nested correctly in xml structure." );
      }
    }
}


void 
MonocularMarkerConfigurationXMLFileReader::EndElement( 
  const char *name )
{
  //let the super class try to analyze the current tag
  Superclass::EndElement( name );

   if( m_ReadingTrackerConfiguration &&
      itksys::SystemTools::Strucmp( name, "pixel_spacing" ) == 0 )
    {
    ProcessMonocularMarkerPixelSpacing();
    }
    if( m_ReadingTrackerConfiguration &&
      itksys::SystemTools::Strucmp( name, "sdd" ) == 0 )
    {
    ProcessMonocularMarkerSDD();
    }
  
  if( m_ReadingMarker &&
      itksys::SystemTools::Strucmp( name, "marker" ) == 0 )
    {
    this->m_ReadingMarker = false;
   
    }
  if( m_ReadingToolConfiguration &&
           itksys::SystemTools::Strucmp( name, "pointx" ) == 0 )
    {
    ProcessMonocularMarkerPointX();
    }
   else if( m_ReadingToolConfiguration &&
           itksys::SystemTools::Strucmp( name, "pointy" ) == 0 )
    {
    ProcessMonocularMarkerPointY();
    }
   else if( m_ReadingToolConfiguration &&
           itksys::SystemTools::Strucmp( name, "pointz" ) == 0 )
    {
    ProcessMonocularMarkerPointZ();
    }
}


std::string 
MonocularMarkerConfigurationXMLFileReader::GetSystemType()
{
  return "visp";
}


double 
MonocularMarkerConfigurationXMLFileReader::GetMaximalRefreshRate()
{
  igstk::MonocularMarkerConfiguration::Pointer trackerConfig =
    igstk::MonocularMarkerConfiguration::New();

  return trackerConfig->GetMaximalRefreshRate();
}

void 
MonocularMarkerConfigurationXMLFileReader::ProcessMonocularMarkerPixelSpacing()
throw ( FileFormatException )
{
  
	this->m_MonocularMarkerPixelSpacing = atof (this->m_CurrentTagData.c_str());
}


void 
MonocularMarkerConfigurationXMLFileReader::ProcessMonocularMarkerSDD()
throw ( FileFormatException )
{
 
  this->m_MonocularMarkerSDD =atoi( this->m_CurrentTagData.c_str());
  cout <<"the SDD 1  is :" << m_MonocularMarkerSDD << endl;
}

void 
MonocularMarkerConfigurationXMLFileReader::ProcessMonocularMarkerPointX()
throw ( FileFormatException )
{
  
  float m_CurrentPointX = atof (this->m_CurrentTagData.c_str());
  this->m_CurrentPointVector.push_back(m_CurrentPointX);
}
void 
MonocularMarkerConfigurationXMLFileReader::ProcessMonocularMarkerPointY()
throw ( FileFormatException )
{
	float m_CurrentPointY = atof(this->m_CurrentTagData.c_str()); 
	this->m_CurrentPointVector.push_back(m_CurrentPointY);
}
void 
MonocularMarkerConfigurationXMLFileReader::ProcessMonocularMarkerPointZ()
throw ( FileFormatException )
{
  
  float m_CurrentPointZ = atof(this->m_CurrentTagData.c_str());
  this->m_CurrentPointVector.push_back(m_CurrentPointZ);
  this->Markers.push_back(m_CurrentPointVector);
  this->m_CurrentPointVector.clear();
}

void 
MonocularMarkerConfigurationXMLFileReader::ProcessToolData() 
throw ( FileFormatException )
{
  if( this->m_CurrentToolName.empty() )
    {
    throw FileFormatException( "\"name\" missing for one of the tools." );
    }
  
  //if the tool section does not have a "calibration_file" tag 
  //we assume the calibration is identity
  igstk::MonocularMarkerToolConfiguration *toolConfig = 
      new igstk::MonocularMarkerToolConfiguration();
  
  toolConfig->SetToolName( this->m_CurrentToolName );
  toolConfig->SetPoint(this->Markers);
 
  toolConfig->SetCalibrationTransform( this->m_CurrentToolCalibration );  
  if( this->m_CurrentToolIsReference )
    {
    this->m_ReferenceTool = toolConfig;
    }
  else
    {
    this->m_TrackerToolList.push_back( toolConfig );
    }

  //reset all tool data to initial state
  this->m_CurrentToolIsReference = false;
  this->m_CurrentToolName.clear();

  this->m_CurrentToolCalibration.SetToIdentity( 
    igstk::TimeStamp::GetLongestPossibleTime() );
}


bool 
MonocularMarkerConfigurationXMLFileReader::HaveConfigurationData()
{
  //check that we have a refresh rate for the tracking system, at least 
  //one tool
  return ( this->m_HaveRefreshRate &&
           this->m_TrackerToolList.size() != 0 &&
		   this->m_MonocularMarkerPixelSpacing != 0 &&
		   this->m_MonocularMarkerSDD !=0);
}


const igstk::TrackerConfiguration::Pointer
MonocularMarkerConfigurationXMLFileReader::GetTrackerConfigurationData()
throw ( FileFormatException )
{
  igstk::MonocularMarkerConfiguration::Pointer trackerConfig = 
    igstk::MonocularMarkerConfiguration::New();
  cout<< "sdd 2 : "<<this->m_MonocularMarkerSDD << endl;
  //this request is guaranteed to succeed as the refresh rate
  //is validated in the TrackerConfigurationXMLReaderBase
  trackerConfig->RequestSetFrequency( this->m_RefreshRate );
  trackerConfig->SetPixelSpacing(this->m_MonocularMarkerPixelSpacing);
  trackerConfig->SetSDD(this->m_MonocularMarkerSDD);
    cout << "GetSDD    :  " << trackerConfig->GetSDD() << endl; 
  
  //add the tools
  std::vector<TrackerToolConfiguration *>::iterator it, end; 
  it = this->m_TrackerToolList.begin();
  end = this->m_TrackerToolList.end();  
  
  AddToolFailureObserver::Pointer failureObserver =
    AddToolFailureObserver::New();
  trackerConfig->AddObserver( igstk::TrackerConfiguration::AddToolFailureEvent(),
                              failureObserver );
  for(; it!=end; it++ )
    {
    trackerConfig->RequestAddTool( *it );
    if( failureObserver->GotAddToolFailure() )
      {
      throw FileFormatException( failureObserver->GetAddToolFailure() );
      }
    }
  if( this->m_ReferenceTool != NULL )
    {
    trackerConfig->RequestAddReference( this->m_ReferenceTool );
    if( failureObserver->GotAddToolFailure() )
      {
      throw FileFormatException( failureObserver->GetAddToolFailure() );
      }
    }
 
  //explicitly upcast to avoid the compiler warning
  igstk::TrackerConfiguration::Pointer genericConfig;
  genericConfig  = trackerConfig;
  return genericConfig;
}

} //namespace


