/*=========================================================================

Program:   Image Guided Surgery Software Toolkit
Module:    igstkMonocularMarkerTrackerConfiguration.h
Language:  C++
Date:      $Date$
Version:   $Revision$
Author:    Mazen Alharah (Siemens Healthineers)

Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef __igstkMonocularMarkerConfiguration_h
#define __igstkMonocularMarkerConfiguration_h

#include "igstkTrackerConfiguration.h"

namespace igstk
{


/**
 * \class MonocularMarkerToolConfiguration 
 * \brief This class represents the configuration data required by a MonocularMarker
 * tool. 
 *
 * This class represents the configuration data required by a MonocularMarker tool.
 * NOTE: The tool and marker names are two distinct entities. The tool name 
 *       is the name the user decides to assign the specific tool (US, bayonet
 *       probe, etc.). The marker name is the name of the tracked marker which
 *       is attached to the tool.
 */
class MonocularMarkerToolConfiguration : public TrackerToolConfiguration
{
public:
  MonocularMarkerToolConfiguration();
  MonocularMarkerToolConfiguration(const MonocularMarkerToolConfiguration &other);
  virtual ~MonocularMarkerToolConfiguration();

  
  igstkSetMacro( Point, std::vector< std::vector<float> > );
  igstkGetMacro( Point, std::vector< std::vector<float> > );

  virtual std::string GetToolTypeAsString();

protected:
 
	std::vector< std::vector<float> > m_Point;
};


/**
 * \class MonocularMarkerTrackerConfiguration This class represents the configuration 
 *        data required by the MonocularMarker tracker.
 */
class MonocularMarkerConfiguration : public TrackerConfiguration
{
public:
  //standard typedefs
  igstkStandardClassBasicTraitsMacro( MonocularMarkerConfiguration, 
                                      TrackerConfiguration )

  //method for creation through the object factory
  igstkNewMacro( Self );
   /**
   * Get the manufacturer specified maximal refresh rate.
   */
  virtual double GetMaximalRefreshRate();

  igstkSetMacro( PixelSpacing, float );
  igstkGetMacro( PixelSpacing, float );

  igstkSetMacro( SDD, double );             // Source-Dectector distance 
  igstkGetMacro( SDD, double );


protected:
  virtual void InternalAddTool( const TrackerToolConfiguration *tool,bool isReference );
  MonocularMarkerConfiguration();
  virtual ~MonocularMarkerConfiguration();
  
  
private:

  float m_PixelSpacing ;
  double m_SDD;

 //manufacturer specified maximal refresh rate [Hz]
  static const double MAXIMAL_REFERESH_RATE;
};

} // end of name space
#endif
