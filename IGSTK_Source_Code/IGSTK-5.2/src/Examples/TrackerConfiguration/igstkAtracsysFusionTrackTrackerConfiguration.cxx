/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkAtracsysFusionTrackTrackerConfiguration.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:	 Mazen Alharah

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "igstkAtracsysFusionTrackTrackerConfiguration.h"

namespace igstk
{                  //highest referesh rate for the AtracsysFusionTrack tracker family (Sx60)
const double AtracsysFusionTrackTrackerConfiguration::MAXIMAL_REFERESH_RATE = 300;


AtracsysFusionTrackTrackerConfiguration::AtracsysFusionTrackTrackerConfiguration() 
{
  this->m_Frequency = this->MAXIMAL_REFERESH_RATE;
}


AtracsysFusionTrackTrackerConfiguration::~AtracsysFusionTrackTrackerConfiguration()
{
}

double
AtracsysFusionTrackTrackerConfiguration::GetMaximalRefreshRate()
{
  return this->MAXIMAL_REFERESH_RATE;
}

void 
AtracsysFusionTrackTrackerConfiguration::InternalAddTool( 
  const TrackerToolConfiguration *tool, 
  bool isReference )
{
  AddToolFailureEvent fe;

  const AtracsysFusionTrackToolConfiguration *wirelessTool = 
    dynamic_cast<const AtracsysFusionTrackToolConfiguration *>( tool );

  if( wirelessTool == NULL )
    {
    fe.Set( "Given tool configuration type not compatible with tracker type." );
    this->InvokeEvent( fe );
    return;
    }
  if( wirelessTool->GetInitializationFile().empty() )
    {
    fe.Set( "ini file not specified for wireless tool." );
    this->InvokeEvent( fe );
    return;
    }
  if( !isReference )
    {
    this->m_TrackerToolList.insert(std::pair<std::string, 
                                   TrackerToolConfiguration *>
      (wirelessTool->GetToolName(), 
       new AtracsysFusionTrackToolConfiguration( *wirelessTool )) );
    }
  else
    {
    if (this->m_ReferenceTool)
      {
      delete this->m_ReferenceTool;
      }
    this->m_ReferenceTool = new AtracsysFusionTrackToolConfiguration( *wirelessTool );
    }
  this->InvokeEvent( AddToolSuccessEvent() );
}


AtracsysFusionTrackToolConfiguration::AtracsysFusionTrackToolConfiguration() : m_InitializationFile( "" )
{
}


AtracsysFusionTrackToolConfiguration::AtracsysFusionTrackToolConfiguration( const 
  AtracsysFusionTrackToolConfiguration &other ) : TrackerToolConfiguration( other )
{
  this->m_InitializationFile = other.m_InitializationFile;
}


AtracsysFusionTrackToolConfiguration::~AtracsysFusionTrackToolConfiguration() 
{
}


std::string 
AtracsysFusionTrackToolConfiguration::GetToolTypeAsString()
{
  return "AtracsysFusionTrackToolConfiguration";
}

} // end of name space
