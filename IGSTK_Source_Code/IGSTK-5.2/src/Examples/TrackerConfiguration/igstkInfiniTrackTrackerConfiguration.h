/*=========================================================================

Program:   Image Guided Surgery Software Toolkit
Module:    igstkMicronTrackerConfiguration.h
Language:  C++
Date:      $Date$
Version:   $Revision$

Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef __igstkInfiniTrackTrackerConfiguration_h
#define __igstkInfiniTrackTrackerConfiguration_h

#include "igstkTrackerConfiguration.h"

namespace igstk
{


/**
 * \class InfiniTrackToolConfiguration 
 * \brief This class represents the configuration data required by a InfiniTrack
 * tool. 
 *
 * This class represents the configuration data required by a InfiniTrack tool.
 * NOTE: The tool and marker names are two distinct entities. The tool name 
 *       is the name the user decides to assign the specific tool (US, bayonet
 *       probe, etc.). The marker name is the name of the tracked marker which
 *       is attached to the tool.
 */
class InfiniTrackToolConfiguration : public TrackerToolConfiguration
{
public:
  InfiniTrackToolConfiguration();
  InfiniTrackToolConfiguration(const InfiniTrackToolConfiguration &other);
  virtual ~InfiniTrackToolConfiguration();

  igstkSetMacro( InitializationFile, std::string );
  igstkGetMacro( InitializationFile, std::string );
  virtual std::string GetToolTypeAsString();

protected:
   std::string     m_InitializationFile;
};


/**
 * \class InfiniTrackTrackerConfiguration This class represents the configuration 
 *        data required by the InfiniTrack tracker.
 */
class InfiniTrackTrackerConfiguration : public TrackerConfiguration
{
public:
  //standard typedefs
  igstkStandardClassBasicTraitsMacro( InfiniTrackTrackerConfiguration, 
                                      TrackerConfiguration )

  //method for creation through the object factory
  igstkNewMacro( Self );
   /**
   * Get the manufacturer specified maximal refresh rate.
   */
  virtual double GetMaximalRefreshRate();

protected:
  virtual void InternalAddTool( const TrackerToolConfiguration *tool,bool isReference );
  InfiniTrackTrackerConfiguration();
  virtual ~InfiniTrackTrackerConfiguration();
  
  
private:
  //manufacturer specified maximal refresh rate [Hz]
  static const double MAXIMAL_REFERESH_RATE;
};


} // end of name space
#endif
