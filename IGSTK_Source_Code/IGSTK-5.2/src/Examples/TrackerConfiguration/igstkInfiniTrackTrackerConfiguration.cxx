/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkInfiniTrackTrackerConfiguration.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:	 Mazen Alharah

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "igstkInfiniTrackTrackerConfiguration.h"

namespace igstk
{                  //highest referesh rate for the InfiniTrack tracker family (Sx60)
const double InfiniTrackTrackerConfiguration::MAXIMAL_REFERESH_RATE = 300;


InfiniTrackTrackerConfiguration::InfiniTrackTrackerConfiguration() 
{
  this->m_Frequency = this->MAXIMAL_REFERESH_RATE;
}


InfiniTrackTrackerConfiguration::~InfiniTrackTrackerConfiguration()
{
}

double
InfiniTrackTrackerConfiguration::GetMaximalRefreshRate()
{
  return this->MAXIMAL_REFERESH_RATE;
}

void 
InfiniTrackTrackerConfiguration::InternalAddTool( 
  const TrackerToolConfiguration *tool, 
  bool isReference )
{
  AddToolFailureEvent fe;

  const InfiniTrackToolConfiguration *wirelessTool = 
    dynamic_cast<const InfiniTrackToolConfiguration *>( tool );

  if( wirelessTool == NULL )
    {
    fe.Set( "Given tool configuration type not compatible with tracker type." );
    this->InvokeEvent( fe );
    return;
    }
  if( wirelessTool->GetInitializationFile().empty() )
    {
    fe.Set( "ini file not specified for wireless tool." );
    this->InvokeEvent( fe );
    return;
    }
  if( !isReference )
    {
    this->m_TrackerToolList.insert(std::pair<std::string, 
                                   TrackerToolConfiguration *>
      (wirelessTool->GetToolName(), 
       new InfiniTrackToolConfiguration( *wirelessTool )) );
    }
  else
    {
    if (this->m_ReferenceTool)
      {
      delete this->m_ReferenceTool;
      }
    this->m_ReferenceTool = new InfiniTrackToolConfiguration( *wirelessTool );
    }
  this->InvokeEvent( AddToolSuccessEvent() );
}


InfiniTrackToolConfiguration::InfiniTrackToolConfiguration() : m_InitializationFile( "" )
{
}


InfiniTrackToolConfiguration::InfiniTrackToolConfiguration( const 
  InfiniTrackToolConfiguration &other ) : TrackerToolConfiguration( other )
{
  this->m_InitializationFile = other.m_InitializationFile;
}


InfiniTrackToolConfiguration::~InfiniTrackToolConfiguration() 
{
}


std::string 
InfiniTrackToolConfiguration::GetToolTypeAsString()
{
  return "InfiniTrackToolConfiguration";
}

} // end of name space
