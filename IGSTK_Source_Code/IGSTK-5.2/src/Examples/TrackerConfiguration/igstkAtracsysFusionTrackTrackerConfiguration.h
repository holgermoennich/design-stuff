/*=========================================================================

Program:   Image Guided Surgery Software Toolkit
Module:    igstkAtracsysFusionTrackTrackerConfiguration.h
Language:  C++
Date:      $Date$
Version:   $Revision$
Author:    Mazen Alharah

Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef __igstkAtracsysFusionTrackTrackerConfiguration_h
#define __igstkAtracsysFusionTrackTrackerConfiguration_h

#include "igstkTrackerConfiguration.h"

namespace igstk
{


/**
 * \class AtracsysFusionTrackToolConfiguration 
 * \brief This class represents the configuration data required by a AtracsysFusionTrack
 * tool. 
 *
 * This class represents the configuration data required by a AtracsysFusionTrack tool.
 * NOTE: The tool and marker names are two distinct entities. The tool name 
 *       is the name the user decides to assign the specific tool (US, bayonet
 *       probe, etc.). The marker name is the name of the tracked marker which
 *       is attached to the tool.
 */
class AtracsysFusionTrackToolConfiguration : public TrackerToolConfiguration
{
public:
  AtracsysFusionTrackToolConfiguration();
  AtracsysFusionTrackToolConfiguration(const AtracsysFusionTrackToolConfiguration &other);
  virtual ~AtracsysFusionTrackToolConfiguration();

  igstkSetMacro( InitializationFile, std::string );
  igstkGetMacro( InitializationFile, std::string );
  virtual std::string GetToolTypeAsString();

protected:
   std::string     m_InitializationFile;
};


/**
 * \class AtracsysFusionTrackTrackerConfiguration This class represents the configuration 
 *        data required by the AtracsysFusionTrack tracker.
 */
class AtracsysFusionTrackTrackerConfiguration : public TrackerConfiguration
{
public:
  //standard typedefs
  igstkStandardClassBasicTraitsMacro( AtracsysFusionTrackTrackerConfiguration, 
                                      TrackerConfiguration )

  //method for creation through the object factory
  igstkNewMacro( Self );
   /**
   * Get the manufacturer specified maximal refresh rate.
   */
  virtual double GetMaximalRefreshRate();

protected:
  virtual void InternalAddTool( const TrackerToolConfiguration *tool,bool isReference );
  AtracsysFusionTrackTrackerConfiguration();
  virtual ~AtracsysFusionTrackTrackerConfiguration();
  
  
private:
  //manufacturer specified maximal refresh rate [Hz]
  static const double MAXIMAL_REFERESH_RATE;
};


} // end of name space
#endif
