#if defined(_MSC_VER)
//  Warning about: identifier was truncated to '255' characters 
// in the debug information (MVC6.0 Debug)
#pragma warning( disable : 4786 )
#endif

#include "igstkMonocularMarkerTrackerTool.h"
#include "igstkMonocularMarkerTracker.h"
#include <string>

namespace igstk
{

//z** Constructor (configures AtracsysFusionTrack-specific tool values)  */
MonocularMarkerTrackerTool::MonocularMarkerTrackerTool()
                      : m_StateMachine(this)
                      , m_TrackerToolConfigured (false)
{

  // States
  igstkAddStateMacro (Idle);
  igstkAddStateMacro (MarkerPointsSpecified);

  // Set the input descriptors
  igstkAddInputMacro (ValidMarkerPoints);
  igstkAddInputMacro (InValidMarkerPoints);

  // Add transitions
  //
  // Transitions from idle state
  igstkAddTransitionMacro (Idle,
                           ValidMarkerPoints,
                           MarkerPointsSpecified,
                           SetMarkerPoints);

  igstkAddTransitionMacro (Idle,
                           InValidMarkerPoints,
                           Idle,
                           ReportInvalidMarkerPointsSpecified);

  // Transitions from MarkerName specified
  igstkAddTransitionMacro (MarkerPointsSpecified,
                           ValidMarkerPoints,
                           MarkerPointsSpecified,
                           ReportInvalidRequest);

  igstkAddTransitionMacro (MarkerPointsSpecified,
                           InValidMarkerPoints,
                           MarkerPointsSpecified,
                           ReportInvalidRequest);

  // Inputs to the state machine
  igstkSetInitialStateMacro (Idle);

  this->m_StateMachine.SetReadyToRun ();
}

/** Destructor */
MonocularMarkerTrackerTool::~MonocularMarkerTrackerTool()
{
}


/** Set tracker tool marker name */
void MonocularMarkerTrackerTool::RequestSetMarkerPoints (
   const std::vector< std::vector<float>> & markerPoints )
{
  igstkLogMacro(DEBUG, 
   "igstk::MonocularMarkerTrackerTool::RequestSetMarkerName called ...\n");

  
    this->m_MarkerPointsToBeSet = markerPoints;
	//this->m_MarkerNameToBeSetY = markerNameY;
	//this->m_MarkerNameToBeSetZ = markerNameZ;
    this->m_StateMachine.PushInput( this->m_ValidMarkerPointsInput );
    this->m_StateMachine.ProcessInputs();
    
}

/** Print Self function */
void MonocularMarkerTrackerTool::PrintSelf( 
   std::ostream& os, itk::Indent indent ) const
{
  Superclass::PrintSelf(os, indent);

 /* os << indent << "Marker name  : ";
  for (unsigned int i=0; i < this->m_MarkerName.size(); ++i)
  {
    os << this->m_MarkerName.at(i).at(0) << std::endl;
  //os << indent << "Marker name Y : "          << this->m_MarkerNameY << std::endl;
  //os << indent << "Marker name Z : "          << this->m_MarkerNameZ << std::endl;
  }*/
  os << indent << "TrackerToolConfigured:" 
     << this->m_TrackerToolConfigured << std::endl;
}

/** Report invalid marker specified */
void MonocularMarkerTrackerTool::ReportInvalidMarkerPointsSpecifiedProcessing ()
{
  igstkLogMacro( DEBUG, 
    "igstk::MonocularMarkerTrackerTool::ReportInvalidMarkerNameSpecifiedProcessing \
    called ...\n");

  igstkLogMacro( CRITICAL, "Invalid marker specified ");
}

/** Report any invalid request to the tool */
void MonocularMarkerTrackerTool::ReportInvalidRequestProcessing ()
{
  igstkLogMacro (WARNING, "ReportInvalidRequestProcessing() called ...\n");
}

/** Set the marker processing */
void MonocularMarkerTrackerTool::SetMarkerPointsProcessing ()
{
  igstkLogMacro (DEBUG, 
     "igstk::MonocularMarkerTrackerTool::SetMarkerNameProcessing called ...\n");

  this->m_MarkerPoints = this->m_MarkerPointsToBeSet;
  //this->m_MarkerNameY = this->m_MarkerNameToBeSetY;
  //this->m_MarkerNameZ = this->m_MarkerNameToBeSetZ;
  this->m_TrackerToolConfigured = true;

  // For MonocularMarkerTracker, marker name is used as a unique identifier
  SetTrackerToolIdentifier (this->m_MarkerPoints); 
}
void 
MonocularMarkerTrackerTool::SetTrackerToolIdentifier( const std::vector< std::vector<float>> & identifier) 
{
  igstkLogMacro( DEBUG, 
    "igstk::TrackerTool::SetTrackerToolIdentifier called...\n");
  this->m_TrackerToolIdentifier = identifier;
  //this->m_TrackerToolIdentifier2 = identifier2;
  //this->m_TrackerToolIdentifier3 = identifier3;


}

/** The "CheckIfTrackerToolIsConfigured" method returns true if 
 * the tracker tool is configured */
bool MonocularMarkerTrackerTool::CheckIfTrackerToolIsConfigured () const
{
  igstkLogMacro (DEBUG, 
   "igstk::MonocularMarkerTrackerTool::CheckIfTrackerToolIsConfigured called...\n");
  return this->m_TrackerToolConfigured;
}

}
