/*=========================================================================
  
  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkAtracsysFusionTrackTracker.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah
  
  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.
  
  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.
  
  =========================================================================*/

#if defined(_MSC_VER)
// Warning about: identifier was truncated to '255' characters in the
// debug information (MVC6.0 Debug)
#pragma warning( disable : 4786 )
#endif

#include "igstkAtracsysFusionTrackTracker.h"
#include "itksys/SystemTools.hxx"
#include "itkLoggerBase.h"
#include "geometryHelper.hpp"
#include "helpers.hpp"
#include "itkMatrix.h"
#include "iostream"
#include "fstream"
#include "vnl_svd.h"


namespace igstk
{
  
  
  
  
  void deviceEnumCallback (uint64 sn, void* user, ftkDeviceType type)
  {
    uint64* lastDevice = (uint64*) user;
    if (lastDevice)
      lastDevice = &sn;
  }
  
  
  /** Constructor */
  AtracsysFusionTrackTracker::AtracsysFusionTrackTracker (void) :
    m_StateMachine(this)
								, lib (NULL)
								, sn (0)
								 
  {
    
    
  }
  
  /** Destructor */
  AtracsysFusionTrackTracker::~AtracsysFusionTrackTracker(void)
  {
    
  }
  
  /** The "InternalOpen" method opens communication with a tracking device. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalOpen (void)
  {
    igstkLogMacro(DEBUG, "igstk::AtracsysFusionTrackTracker::InternalOpen called ...\n")
      
      lib = ftkInit(); 
    
    if(!lib)
      {
	igstkLogMacro(CRITICAL,"Cannot close library");
	
	return FAILURE;
      }
    
    if ( ftkEnumerateDevices (lib, 
			      deviceEnumerator,
			      &sn) != FTK_OK)
      {
        igstkLogMacro(CRITICAL,"Cannot enumerate devices");
	return FAILURE;
      }
    
    if (sn == 0)
      {
	igstkLogMacro(CRITICAL, "No device detected")
	  return FAILURE;
      }
    return SUCCESS;
  }
  
  /** 
      callback r.  
      The "InternalClose" method closes communication with a tracking device. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalClose( void )
  {  
    igstkLogMacro(DEBUG, "igstk::AtracsysFusionTrackTracker::InternalClose called ...\n")
      
      if (lib)
	{
	  ftkClose (&lib);
	  
	  lib = NULL;
	}
    
    return SUCCESS;
  }
  
  /** The "InternalReset" method resets tracker to a known configuration. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalReset( void )
  {
    igstkLogMacro(DEBUG, "igstk::AtracsysFusionTrackTracker::InternalReset called ...\n")
      ftkBuffer buffer;
    
    
    return SUCCESS;
  }
  
  /** The "InternalStartTracking" method starts tracking. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalStartTracking( void )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::InternalStartTracking called ...\n")
      
      
      
      frame = ftkCreateFrame();
    
    if ( frame == 0 )
      {
        error( "Cannot create frame instance" );
	igstkLogMacro(CRITICAL,"Cannot create frame instance");
	return FAILURE;
      }
    if ( ftkSetFrameOptions( false, false, 16u, 16u, 0u, 16u, frame ) != FTK_OK)
      {
	igstkLogMacro(CRITICAL,"Connot Start Tracking");
	return FAILURE;
      }
    
    return SUCCESS;
  }
  
  /** The "InternalStopTracking" method stops tracking. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalStopTracking( void )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::InternalStopTracking called ...\n")
      
      ftkDeleteFrame (frame);
    cout<<"Delete"<<endl;
    
    
    return SUCCESS;
  }
  
  
  /** The "InternalUpdateStatus" method updates tracker status. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalUpdateStatus( void )
  {
  igstkLogMacro(DEBUG, 
		"igstk::AtracsysFusionTrackTracker::InternalUpdateStatus called ...\n")
      
      
    TrackerToolsContainerType trackerToolContainer = GetTrackerToolContainer();
    
  uint32 counter( 1000u );
  for ( uint32 u( 0u ), i; u < 100u; ++u )
    {
      /* block up to 100 ms if next frame is not available*/  
	
      if (  ftkGetLastFrame( lib, sn, frame, 100u )!= FTK_OK )
	{
	  cout << ".";
	  continue;
	}
	
      // lib is an initialise ftkLibrary handle
      // sn is the serial number of the discovered fusionTrack
      ftkError err( ftkSetInt32( lib, sn, 103, 0 ) );
      if ( err != FTK_OK )
	{
	  // error handling
	  cout<<"Error handling "<< endl;
	}
	
	
      switch ( frame->markersStat )
	{
	case QS_WAR_SKIPPED:
	  ftkDeleteFrame( frame );
	  cerr << "marker fields in the frame are not set correctly" << endl; 
	  checkError( lib );
	    
	case QS_ERR_INVALID_RESERVED_SIZE:
	  ftkDeleteFrame( frame );
	  cerr << "frame -> markersVersionSize is invalid" << endl;
	  checkError( lib );
	    
	default:
	  ftkDeleteFrame( frame );
	  cerr << "invalid status" << endl;
	  checkError( lib );
	    
	case QS_OK:
	  break;
	}
	
      if ( frame->markersCount == 0u )
	{
	  cout << ".";
	  igstk::PulseGenerator::Sleep(500);
	    
	  // sleep( 1000L );
	  continue;
	}
	
      if ( frame->markersStat == QS_ERR_OVERFLOW )
	{
	  cerr <<
	    "WARNING: marker overflow. Please increase cstMarkersCount"
	       << endl;
	}
	
      cout << endl << "iteration " << u << endl;
      for ( i = 0u; i < frame->markersCount; ++i )
	{
	  cout.precision( 2u );
	  cout << "geometry " << frame->markers[ i ].geometryId
	       << ", tracking " << frame->markers[ i ].id
	       << ", trans (" << frame->markers[ i ].translationMM[ 0u ]
	       << " " << frame->markers[ i ].translationMM[ 1u ]
	       << " " << frame->markers[ i ].translationMM[ 2u ]
	       << "), error ";
	  cout.precision( 3u );
	  cout << frame->markers[ i ].registrationErrorMM << endl;
	}
	
      if ( --counter == 0uLL )
	{
	  break;
	}
      igstk::PulseGenerator::Sleep(500);
	
      //sleep( 1000L );
    }
    
  if ( counter != 0u )
    {
      cout << endl << "Acquisition loop aborted after too many vain trials"
	   << endl;
    }
  else
    {
      cout << "\tSUCCESS" << endl;
    }
    

  // GUI
  if(1){
	  for(size_t m = 0; m < frame->markersCount; m++ )
      {
    std::string tool="";
    // report to the tracker tool that the tracker is Visible
    this->ReportTrackingToolVisible(trackerToolContainer[tool]);
    
    // create the transform
    TransformType transform;
    
    typedef TransformType::VectorType TranslationType;
    TranslationType translation;
    
    typedef TransformType::VersorType RotationType;
    RotationType rotation;
    itk::Matrix<double> matrix;
    
    typedef TransformType::ErrorType  ErrorType;
    ErrorType errorValue;	
    
    
	translation[0] = frame->markers[m].translationMM[0];                   
	translation[1] = frame->markers[m].translationMM[1];                  
	translation[2] = frame->markers[m].translationMM[2];  
	matrix [0][0] = frame->markers[m].rotation [0][0];    
	matrix [0][1] = frame->markers[m].rotation [0][1];
	matrix [0][2] = frame->markers[m].rotation [0][2];
	matrix [1][0] = frame->markers[m].rotation [1][0];
	matrix [1][1] = frame->markers[m].rotation [1][1];
	matrix [1][2] = frame->markers[m].rotation [1][2];
	matrix [2][0] = frame->markers[m].rotation [2][0];
	matrix [2][1] = frame->markers[m].rotation [2][1];
	matrix [2][2] = frame->markers[m].rotation [2][2];
	errorValue =frame->markers[m].registrationErrorMM;


	//***********    from outside
	// make matrix orthogonal
	vnl_svd<double> svd(matrix.GetVnlMatrix());
	svd.W(0,0) = 1;
	svd.W(1,1) = 1;
	svd.W(2,2) = 1;
	itk::Matrix<double> matrixOrthogonal(svd.recompose());
	rotation.Set (matrixOrthogonal);
    
	long lTime = this->GetValidityTime (); // TODO: ??
    
	transform.SetToIdentity(this->GetValidityTime());
	transform.SetTranslationAndRotation(translation, 
					    rotation, 
					    errorValue,
					    this->GetValidityTime());
    
	// set the raw transform
	this->SetTrackerToolRawTransform(trackerToolContainer [tool], transform );
    
	this->SetTrackerToolTransformUpdate(trackerToolContainer [tool], true );
	
	///
    
      }
    
   
    
  }
  return SUCCESS;
}
  
  
  /** The "InternalThreadedUpdateStatus" method updates tracker status. */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::InternalThreadedUpdateStatus( void )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::InternalThreadedUpdateStatus called ...\n")
      
      cout<<" Hi"<< endl;
    
    return SUCCESS;
  }
  
  
  /** Print the object information in a stream. */
  void AtracsysFusionTrackTracker::PrintSelf( std::ostream& os, itk::Indent indent ) const
  {
    Superclass::PrintSelf(os, indent);
    
  
    
    if(!lib || !sn)
      {
	return;
      }
    
    // Declaration of the device structure
    DeviceData device( retrieveLastDevice( lib ) );
    uint64 sn( device.SerialNumber );
    uint32 uLeft  = ((uint32)(device.SerialNumber)),
      uRight = ((uint32)((device.SerialNumber)>>32));
    char strSerial [100];
    
    sprintf (strSerial, "0x%X%X", uLeft, uRight);
    os << indent << "Device serial number:" << strSerial << std::endl;
    
  }
  
  
  
  
  
  
  
  /** Verify if a tracker tool information is correct before attaching */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::VerifyTrackerToolInformation (
							    const TrackerToolType* trackerTool)
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::VerifyTrackerToolInformation called ...\n")
      
      return SUCCESS;
  }
  
  
  /** The "ValidateSpecifiedFrequency" method checks if the specified  */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::ValidateSpecifiedFrequency( double frequencyInHz )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::ValidateSpecifiedFrequency called ...\n")
      
      return SUCCESS;
  }
  
  
  
  
  
  
  /** This method will remove entries of the traceker tool 
   * from internal data containers */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::RemoveTrackerToolFromInternalDataContainers(
									  const TrackerToolType * trackerTool )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::RemoveTrackerToolFromInternalDataContainers \
   called ...\n")
      TrackerToolType* pTrackerToolNonConst = 
      const_cast<TrackerToolType *>(trackerTool);
    AtracsysFusionTrackTrackerTool* pAtracsysFusionTrackTrackerTool = 
      dynamic_cast<AtracsysFusionTrackTrackerTool *> (pTrackerToolNonConst);
    std::string strMarkerFileName = 
      pAtracsysFusionTrackTrackerTool->GetMarkerName (); 
    
    
    //ftkGeometry geom;
    
    if (loadGeometry (lib, 
		      sn,strMarkerFileName.c_str (),geom)!= FTK_OK)
      {
	igstkLogMacro(CRITICAL, "Error, cannot load geometry file ")
	  return FAILURE;
      }
    
    if (ftkClearGeometry (lib, 
			  this->sn, geom.geometryId))
      {
	igstkLogMacro(CRITICAL, "Cannot Clear geometry")
	  return FAILURE;
      }
    
    
    
    return SUCCESS;
  }
  
  
  
  
  
  
  /** Add tracker tool entry to internal containers */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::AddTrackerToolToInternalDataContainers (
								      const TrackerToolType * trackerTool)
  {

    static int myitr=0;
    printf("inside  AddTrackerToolToInternalDataContainers %d", myitr++);

    puts("fdsfdsfds");
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::AddTrackerToolToInternalDataContainers \
   called ...\n")
      
      TrackerToolType* pTrackerToolNonConst = 
      const_cast<TrackerToolType *>(trackerTool);
    AtracsysFusionTrackTrackerTool* pAtracsysFusionTrackTrackerTool = 
      dynamic_cast<AtracsysFusionTrackTrackerTool *> (pTrackerToolNonConst);

     std::string strMarkerFileName = pAtracsysFusionTrackTrackerTool->GetMarkerName (); // Load the Paths from XML parsing

    // std::string geomFile2("C://Program Files//Atracsys//Passive Tracking SDK//data//geometry011.ini");  
    // std::string geomFile1= "C://Program Files//Atracsys//Passive Tracking SDK//data//geometry006.ini";
    
    //   std::vector<std::string> vect2;
    // std::string array[2];
    // array[1] = "C://Program Files//Atracsys//Passive Tracking SDK//data//geometry006.ini";
    // array[0] = "C://Program Files//Atracsys//Passive Tracking SDK//data//geometry011.ini";
    
   
    // vect2.push_back(geomFile2);
    // vect2.push_back(geomFile1);
   
    // //############### ERROR 
    // if(0){ //Mazen original code 
    //   //ftkGeometry geom;
    //   for(int i=0; i< 2/*vect2.size()*/; ++i){
      
    // 	if (loadGeometry (lib,sn,strMarkerFileName.c_str ()/*array[i]*/ ,geom)!= FTK_OK){
    // 	  igstkLogMacro(CRITICAL, "Error, cannot load geometry file ")
    // 	    return FAILURE;
    // 	}
      
    // 	if (ftkSetGeometry (lib, 
    // 			    sn, &geom))
    // 	  {
    // 	    igstkLogMacro(CRITICAL, "Cannot set geometry")
    // 	      return FAILURE;
    // 	  }
      
      
    // 	printf("in loop");
    // 	getchar();
    // 	return SUCCESS;
     
    //   }
    // }
    //############################

    ////////////////////////////

    //ftkGeometry geom ;
    for(int i=0; i< /*(int)geomFileVec.size()*/ 1; ++i)
      {
	switch ( loadGeometry( lib, sn,strMarkerFileName.c_str () /*array[i] /**/, geom ) )
	  {
	  case 1:
	    cout << "Loaded from installation directory." << endl;

	  case 0:
	    CHECK_ERR( ftkSetGeometry( lib, sn, &geom ) );
	    break;

	  default:

	    cerr << "Error, cannot load geometry file '"
		 << geomFileVec[i] << "'." << endl;
	    CHECK_ERR( ftkClose( &lib ) );

#ifdef ATR_WIN
	    cout << endl << " *** Hit a key to exit ***" << endl;
	    waitForKeyboardHit();
#endif
	    return SUCCESS;
	  }
      }


    ///////////////////////
    
    
    
  }
  
} // end of namespace igstk
