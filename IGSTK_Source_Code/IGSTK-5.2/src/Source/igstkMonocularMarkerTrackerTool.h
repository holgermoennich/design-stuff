/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkMonocularMarkerTrackerTool.h
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah (Siemens Healthineers)

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#ifndef __igstkMonocularMarkerTrackerTool_h
#define __igstkMonocularMarkerTrackerTool_h

#ifdef _MSC_VER
#pragma warning ( disable : 4018 )
//Warning about: identifier was truncated to '255' characters in the debug
//information (MVC6.0 Debug)
#pragma warning( disable : 4284 )
#endif

#include "igstkTrackerTool.h"


namespace igstk
{

class igstkMonocularMarkerTracker;



class MonocularMarkerTrackerTool : public TrackerTool
{

public:

  /** Macro with standard traits declarations. */
  igstkStandardClassTraitsMacro (MonocularMarkerTrackerTool, TrackerTool )

  /** Get Tracker tool marker name */
  igstkGetMacro (MarkerPoints,std::vector< std::vector<float>>);
 

  /** Set tracker tool marker name */
  //void RequestSetMarkerName (const float & markerNameX, const float & markerNameY,const float & markerNameZ); 
  void RequestSetMarkerPoints (const std::vector< std::vector<float>> & markerPoints);
protected:

  /** Constructor (configures AtracsysFusionTrack-specific tool values)  */
  MonocularMarkerTrackerTool();

  /** Destructor */
  virtual ~MonocularMarkerTrackerTool();

  /** Print object information */
  virtual void PrintSelf( std::ostream& os, ::itk::Indent indent ) const;

 // void SetTrackerToolIdentifier( const float identifier1 , const float identifier2 , const float identifier3 );
  void SetTrackerToolIdentifier( const std::vector< std::vector<float>> & identifier);
private:
  MonocularMarkerTrackerTool(const Self&);   //purposely not implemented
  void operator=(const Self&);       //purposely not implemented

  /** Get boolean variable to check if the tracker tool is 
   *  configured or not.
   */
  virtual bool CheckIfTrackerToolIsConfigured () const;

  /** Report invalid marker specified */
  void ReportInvalidMarkerPointsSpecifiedProcessing ();

  /** Report any invalid request to the tool */
  void ReportInvalidRequestProcessing ();

  /** Set the marker processing */
  void SetMarkerPointsProcessing ();

  /** States for the State Machine */
  igstkDeclareStateMacro (Idle);
  igstkDeclareStateMacro (MarkerPointsSpecified);

  /** Inputs to the State Machine */
  igstkDeclareInputMacro (ValidMarkerPoints);
  igstkDeclareInputMacro (InValidMarkerPoints);

  /** The marker */
  std::vector< std::vector<float>> m_MarkerPoints ;
  std::vector< std::vector<float>> m_MarkerPointsToBeSet ;


  /** boolean to indicate if the tracker tool is configured */
  bool m_TrackerToolConfigured;

  std::vector< std::vector<float>> m_TrackerToolIdentifier;


};  

} // namespace igstk


#endif  // __igstk_MonocularMarkerTrackerTool_h_
