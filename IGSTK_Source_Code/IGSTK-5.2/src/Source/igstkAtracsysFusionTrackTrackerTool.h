/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkAtracsysFusionTrackTrackerTool.h
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#ifndef __igstkAtracsysFusionTrackTrackerTool_h
#define __igstkAtracsysFusionTrackTrackerTool_h

#ifdef _MSC_VER
#pragma warning ( disable : 4018 )
//Warning about: identifier was truncated to '255' characters in the debug
//information (MVC6.0 Debug)
#pragma warning( disable : 4284 )
#endif

#include "igstkTrackerTool.h"


namespace igstk
{

class igstkAtracsysFusionTrackTracker;

/** \class AtracsysFusionTrackTrackerTool
* \brief A AtracsysFusionTrack-specific TrackerTool class.
*
* This class is a for providing AtracsysFusionTrack-specific functionality
* for TrackerTools, and also to allow compile-time type enforcement
* for other classes and functions that specifically require
* an AtracsysFusionTrackTrackertool.
*
* \sa http://www.atracsys.com/
*
* \ingroup Tracker
*
*/

class AtracsysFusionTrackTrackerTool : public TrackerTool
{

public:

  /** Macro with standard traits declarations. */
  igstkStandardClassTraitsMacro (AtracsysFusionTrackTrackerTool, TrackerTool )

  /** Get Tracker tool marker name */
  igstkGetStringMacro (MarkerName);

  /** Set tracker tool marker name */
  void RequestSetMarkerName (const std::string & markerName); 

protected:

  /** Constructor (configures AtracsysFusionTrack-specific tool values)  */
  AtracsysFusionTrackTrackerTool();

  /** Destructor */
  virtual ~AtracsysFusionTrackTrackerTool();

  /** Print object information */
  virtual void PrintSelf( std::ostream& os, ::itk::Indent indent ) const;

private:
  AtracsysFusionTrackTrackerTool(const Self&);   //purposely not implemented
  void operator=(const Self&);       //purposely not implemented

  /** Get boolean variable to check if the tracker tool is 
   *  configured or not.
   */
  virtual bool CheckIfTrackerToolIsConfigured () const;

  /** Report invalid marker specified */
  void ReportInvalidMarkerNameSpecifiedProcessing ();

  /** Report any invalid request to the tool */
  void ReportInvalidRequestProcessing ();

  /** Set the marker processing */
  void SetMarkerNameProcessing ();

  /** States for the State Machine */
  igstkDeclareStateMacro (Idle);
  igstkDeclareStateMacro (MarkerNameSpecified);

  /** Inputs to the State Machine */
  igstkDeclareInputMacro (ValidMarkerName);
  igstkDeclareInputMacro (InValidMarkerName);

  /** The marker */
  std::string m_MarkerName;
  std::string m_MarkerNameToBeSet;

  /** boolean to indicate if the tracker tool is configured */
  bool m_TrackerToolConfigured;

};  

} // namespace igstk


#endif  // __igstk_AtracsysFusionTrackTrackerTool_h_
