/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkAtracsysFusionTrackTrackerTool.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#if defined(_MSC_VER)
//  Warning about: identifier was truncated to '255' characters 
// in the debug information (MVC6.0 Debug)
#pragma warning( disable : 4786 )
#endif

#include "igstkAtracsysFusionTrackTrackerTool.h"
#include "igstkAtracsysFusionTrackTracker.h"

namespace igstk
{

//z** Constructor (configures AtracsysFusionTrack-specific tool values)  */
AtracsysFusionTrackTrackerTool::AtracsysFusionTrackTrackerTool()
                      : m_StateMachine(this)
                      , m_TrackerToolConfigured (false)
{

  // States
  igstkAddStateMacro (Idle);
  igstkAddStateMacro (MarkerNameSpecified);

  // Set the input descriptors
  igstkAddInputMacro (ValidMarkerName);
  igstkAddInputMacro (InValidMarkerName);

  // Add transitions
  //
  // Transitions from idle state
  igstkAddTransitionMacro (Idle,
                           ValidMarkerName,
                           MarkerNameSpecified,
                           SetMarkerName);

  igstkAddTransitionMacro (Idle,
                           InValidMarkerName,
                           Idle,
                           ReportInvalidMarkerNameSpecified);

  // Transitions from MarkerName specified
  igstkAddTransitionMacro (MarkerNameSpecified,
                           ValidMarkerName,
                           MarkerNameSpecified,
                           ReportInvalidRequest);

  igstkAddTransitionMacro (MarkerNameSpecified,
                           InValidMarkerName,
                           MarkerNameSpecified,
                           ReportInvalidRequest);

  // Inputs to the state machine
  igstkSetInitialStateMacro (Idle);

  this->m_StateMachine.SetReadyToRun ();
}

/** Destructor */
AtracsysFusionTrackTrackerTool::~AtracsysFusionTrackTrackerTool()
{
}


/** Set tracker tool marker name */
void AtracsysFusionTrackTrackerTool::RequestSetMarkerName (
   const std::string & markerName)
{
  igstkLogMacro(DEBUG, 
   "igstk::AtracsysFusionTrackTrackerTool::RequestSetMarkerName called ...\n");

  if (markerName == "")
    {
    this->m_StateMachine.PushInput( this->m_InValidMarkerNameInput );
    this->m_StateMachine.ProcessInputs();
    } 
  else 
    {
    this->m_MarkerNameToBeSet = markerName;
    this->m_StateMachine.PushInput( this->m_ValidMarkerNameInput );
    this->m_StateMachine.ProcessInputs();
    }
}

/** Print Self function */
void AtracsysFusionTrackTrackerTool::PrintSelf( 
   std::ostream& os, itk::Indent indent ) const
{
  Superclass::PrintSelf(os, indent);

  os << indent << "Marker name : "         << this->m_MarkerName << std::endl;
  os << indent << "TrackerToolConfigured:" 
     << this->m_TrackerToolConfigured << std::endl;
}

/** Report invalid marker specified */
void AtracsysFusionTrackTrackerTool::ReportInvalidMarkerNameSpecifiedProcessing ()
{
  igstkLogMacro( DEBUG, 
    "igstk::AtracsysFusionTrackTrackerTool::ReportInvalidMarkerNameSpecifiedProcessing \
    called ...\n");

  igstkLogMacro( CRITICAL, "Invalid marker specified ");
}

/** Report any invalid request to the tool */
void AtracsysFusionTrackTrackerTool::ReportInvalidRequestProcessing ()
{
  igstkLogMacro (WARNING, "ReportInvalidRequestProcessing() called ...\n");
}

/** Set the marker processing */
void AtracsysFusionTrackTrackerTool::SetMarkerNameProcessing ()
{
  igstkLogMacro (DEBUG, 
     "igstk::AtracsysFusionTrackTrackerTool::SetMarkerNameProcessing called ...\n");

  this->m_MarkerName = this->m_MarkerNameToBeSet;
  this->m_TrackerToolConfigured = true;

  // For AtracsysFusionTrackTracker, marker name is used as a unique identifier
  SetTrackerToolIdentifier (this->m_MarkerName); 
}

/** The "CheckIfTrackerToolIsConfigured" method returns true if 
 * the tracker tool is configured */
bool AtracsysFusionTrackTrackerTool::CheckIfTrackerToolIsConfigured () const
{
  igstkLogMacro (DEBUG, 
   "igstk::AtracsysFusionTrackTrackerTool::CheckIfTrackerToolIsConfigured called...\n");
  return this->m_TrackerToolConfigured;
}

}
