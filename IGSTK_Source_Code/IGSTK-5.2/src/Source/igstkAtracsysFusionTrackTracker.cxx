/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkAtracsysFusionTrackTracker.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

  =========================================================================*/

#if defined(_MSC_VER)
// Warning about: identifier was truncated to '255' characters in the
// debug information (MVC6.0 Debug)
#pragma warning( disable : 4786 )
#endif

#include "igstkAtracsysFusionTrackTracker.h"
#include "itksys/SystemTools.hxx"
#include "itkLoggerBase.h"
#include "geometryHelper.hpp"
#include "helpers.hpp"
#include "itkMatrix.h"
#include "iostream"
#include "fstream"
#include "vnl_svd.h"


namespace igstk
{




  void deviceEnumCallback (uint64 sn, void* user, ftkDeviceType type)
  {
    uint64* lastDevice = (uint64*) user;
    if (lastDevice)
      lastDevice = &sn;
  }
 

  /** Constructor */
  AtracsysFusionTrackTracker::AtracsysFusionTrackTracker (void) :
    m_StateMachine(this)
    , ftklib (NULL)
    , sn (0)
    , m_BufferLock (NULL)
    , frame (NULL)
    , m_iProcessed (-1)
    , m_iAvailable (-1)
    , m_iInAcquisition (-1)
    , m_vecTrackerToolID ()

  {
	
    this->frame = new std::vector<ftkFrameQuery*>[3]; 

    this->m_vecTrackerToolID.clear ();

    // Lock for the data buffer that is used to transfer the
    // transformations from thread that is communicating
    // with the tracker to the main thread.
    this->m_BufferLock = itk::MutexLock::New();
  }

  /** Destructor */
  AtracsysFusionTrackTracker::~AtracsysFusionTrackTracker(void)
  {
    for (size_t t = 0; t < this->frame [0].size (); t++)
      {
	delete this->frame [0][t];
      }

    for (size_t t = 0; t < this->frame [1].size (); t++)
      {
	delete this->frame [1][t];
      }

    for (size_t t = 0; t < this->frame [2].size (); t++)
      {
	delete this->frame [2][t];
      }

    if (this->frame)
      {
	delete [] this->frame;
      }
  
    this->m_vecTrackerToolID.clear ();
  }

  /** The "InternalOpen" method opens communication with a tracking device. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalOpen (void)
  {
    igstkLogMacro(DEBUG, "igstk::AtracsysFusionTrackTracker::InternalOpen called ...\n")

      ftklib = ftkInit(); 

    if(!this->ftklib)
      {
	igstkLogMacro(CRITICAL,"Cannot close ftklibrary");
    
	return FAILURE;
      }

    if ( ftkEnumerateDevices (this->ftklib, 
			      deviceEnumerator,
			      &this->sn) != FTK_OK)
      {
        igstkLogMacro(CRITICAL,"Cannot enumerate devices");
	return FAILURE;
      }

    if (this->sn == 0)
      {
	igstkLogMacro(CRITICAL, "No device detected")
	  return FAILURE;
      }
    return SUCCESS;
  }

  /** The "InternalClose" method closes communication with a tracking device. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalClose( void )
  {  
    igstkLogMacro(DEBUG, "igstk::AtracsysFusionTrackTracker::InternalClose called ...\n")

      if (this->ftklib)
	{
	  ftkClose (&this->ftklib);
	
	  ftklib = NULL;
	}

    return SUCCESS;
  }

  /** The "InternalReset" method resets tracker to a known configuration. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalReset( void )
  {
    igstkLogMacro(DEBUG, "igstk::AtracsysFusionTrackTracker::InternalReset called ...\n")
      ftkBuffer buffer;


    return SUCCESS;
  }

  /** The "InternalStartTracking" method starts tracking. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalStartTracking( void )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::InternalStartTracking called ...\n")

  

    m_ReadFrame = ftkCreateFrame();

    if ( m_ReadFrame == 0 )
    {
		error( "Cannot create frame instance" );
		igstkLogMacro(CRITICAL,"Cannot create frame instance");
		return FAILURE;
    }
    if ( ftkSetFrameOptions( false, false, 16u, 16u, 0u, 16u, m_ReadFrame ) != FTK_OK)
    {
		igstkLogMacro(CRITICAL,"Connot Start Tracking");
		return FAILURE;
    }

    return SUCCESS;
  }

  /** The "InternalStopTracking" method stops tracking. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalStopTracking( void )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::InternalStopTracking called ...\n")

    ftkDeleteFrame (m_ReadFrame);
    cout<<"Delete"<<endl;
 
   
    return SUCCESS;
  }


  /** The "InternalUpdateStatus" method updates tracker status. */
  AtracsysFusionTrackTracker::ResultType AtracsysFusionTrackTracker::InternalUpdateStatus( void )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::InternalUpdateStatus called ...\n")

    this->m_BufferLock->Lock ();
    setNextArrayForUser ();
    this->m_BufferLock->Unlock ();

    if (m_iProcessed < 0)
      {
	igstkLogMacro(WARNING, "No frame available")
	  return FAILURE;
      }

    TrackerToolsContainerType trackerToolContainer = GetTrackerToolContainer();
    
	ftkFrameQuery* o_frame;
	for (size_t u = 0; u < this->m_vecTrackerToolID.size (); u++)
	{
		
			o_frame = m_ReadFrame; 

			// Select correct frame & store name & position
			std::string tool = "";

			for (size_t m = 0; m < o_frame->markersCount; m++)
			{
				if (this->m_vecTrackerToolID[u].m_u32GeometryID == 
					o_frame->markers[m].geometryId)
				{
					tool = this->m_vecTrackerToolID[u].m_TrackerToolName;

					typedef TransformType::VectorType TranslationType;
					TranslationType translation;
					translation[0] = o_frame->markers[m].translationMM[0];                   
					translation[1] = o_frame->markers[m].translationMM[1];                  
					translation[2] = o_frame->markers[m].translationMM[2];    

					itk::Matrix<double> matrix;
					matrix [0][0] = o_frame->markers[m].rotation[0][0];    
					matrix [0][1] = o_frame->markers[m].rotation[0][1];
					matrix [0][2] = o_frame->markers[m].rotation[0][2];
					matrix [1][0] = o_frame->markers[m].rotation[1][0];
					matrix [1][1] = o_frame->markers[m].rotation[1][1];
					matrix [1][2] = o_frame->markers[m].rotation[1][2];
					matrix [2][0] = o_frame->markers[m].rotation[2][0];
					matrix [2][1] = o_frame->markers[m].rotation[2][1];
					matrix [2][2] = o_frame->markers[m].rotation[2][2];
					// make matrix orthogonal
					typedef TransformType::VersorType RotationType;
					RotationType rotation;
					vnl_svd<double> svd(matrix.GetVnlMatrix());
					svd.W(0,0) = 1;
					svd.W(1,1) = 1;
					svd.W(2,2) = 1;
					itk::Matrix<double> matrixOrthogonal(svd.recompose());
					rotation.Set (matrixOrthogonal);
					
					typedef TransformType::ErrorType  ErrorType;
					ErrorType errorValue;
					errorValue = o_frame->markers[m].registrationErrorMM;

					// report to the tracker tool that the tracker is Visible
					this->ReportTrackingToolVisible(trackerToolContainer[tool]);

					// Create the transform object
					TransformType transform;

					long lTime = this->GetValidityTime ();
					transform.SetToIdentity(this->GetValidityTime());
					transform.SetTranslationAndRotation(translation, 
										rotation, 
										errorValue,
										this->GetValidityTime());

					// set the raw transform
					this->SetTrackerToolRawTransform(trackerToolContainer[tool], transform );

					this->SetTrackerToolTransformUpdate(trackerToolContainer[tool], true );
				}  
			}

     
		
	}

    return SUCCESS;
  }


  /** The "InternalThreadedUpdateStatus" method updates tracker status. */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::InternalThreadedUpdateStatus( void )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::InternalThreadedUpdateStatus called ...\n")

      this->m_BufferLock->Lock ();
    setNextArrayForAcquisition ();
    this->m_BufferLock->Unlock ();

  
	if (ftkGetLastFrame(this->ftklib, this->sn, m_ReadFrame, 100 ) != FTK_OK )
	{
		cout<<"Frame Missing..."<<endl;
	}
	ftkError err( ftkSetInt32(this->ftklib, this->sn, 103, 0 ) );
	if ( err != FTK_OK )
	{
		// error handling
		cout<<"Error handling "<< endl;
	}
	  
	if( m_ReadFrame->markersStat == QS_OK )
	{
	
		//this->frame[m_iInAcquisition].push_back(m_ReadFrame);
		for (unsigned int i = 0u; i < m_ReadFrame->markersCount; ++i )
		{
			cout.precision( 2u );
			cout << "geometry " << m_ReadFrame->markers[ i ].geometryId
				<< ", tracking " << m_ReadFrame->markers[ i ].id
					<< ", trans (" << m_ReadFrame->markers[ i ].translationMM[ 0u ]
					<< " " << m_ReadFrame->markers[ i ].translationMM[ 1u ]
					<< " " << m_ReadFrame->markers[ i ].translationMM[ 2u ]
					<< "), error ";
			cout.precision( 3u );
			cout << m_ReadFrame->markers[ i ].registrationErrorMM << endl;
		}
	}

    this->m_BufferLock->Lock ();
    acquisitionFinished ();
    this->m_BufferLock->Unlock ();

    return SUCCESS;
  }
  /** Print the object information in a stream. */
  void AtracsysFusionTrackTracker::PrintSelf( std::ostream& os, itk::Indent indent ) const
  {
    Superclass::PrintSelf(os, indent);

    //cout.self (std::ios_base::fixed, std::ios_base::floatfield);

    if(!this->ftklib || !this->sn)
      {
	return;
      }

    // Declaration of the device structure
    DeviceData device( retrieveLastDevice( this->ftklib ) );
    uint64 sn( device.SerialNumber );
    uint32 uLeft  = ((uint32)(device.SerialNumber)),//mu64SerialNumber
      uRight = ((uint32)((device.SerialNumber)>>32));
    char strSerial [100];

    sprintf (strSerial, "0x%X%X", uLeft, uRight);
    os << indent << "Device serial number:" << strSerial << std::endl;
  
  }







  /** Verify if a tracker tool information is correct before attaching */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::VerifyTrackerToolInformation (
							    const TrackerToolType* trackerTool)
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::VerifyTrackerToolInformation called ...\n")

      return SUCCESS;
  }
  

  /** The "ValidateSpecifiedFrequency" method checks if the specified  */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::ValidateSpecifiedFrequency( double frequencyInHz )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::ValidateSpecifiedFrequency called ...\n")

      return SUCCESS;
  }






  /** This method will remove entries of the traceker tool 
   * from internal data containers */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::RemoveTrackerToolFromInternalDataContainers(
									  const TrackerToolType * trackerTool )
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::RemoveTrackerToolFromInternalDataContainers \
   called ...\n")
      TrackerToolType* pTrackerToolNonConst = 
      const_cast<TrackerToolType *>(trackerTool);
    AtracsysFusionTrackTrackerTool* pAtracsysFusionTrackTrackerTool = 
      dynamic_cast<AtracsysFusionTrackTrackerTool *> (pTrackerToolNonConst);
    std::string strMarkerFileName = 
      pAtracsysFusionTrackTrackerTool->GetMarkerName (); 

  
   // ftkGeometry geom;
    
    if (loadGeometry (this->ftklib, 
		      this->sn,strMarkerFileName.c_str (),geom)!= FTK_OK)
      {
	igstkLogMacro(CRITICAL, "Error, cannot load geometry file ")
	  return FAILURE;
      }
    
    if (ftkClearGeometry (this->ftklib, 
			  this->sn, geom.geometryId))
      {
	igstkLogMacro(CRITICAL, "Cannot Clear geometry")
	  return FAILURE;
      }
    
    for (size_t t = 0; t < m_vecTrackerToolID.size (); t++)
      {
	if (this->m_vecTrackerToolID [t].m_TrackerToolName == 
	    pAtracsysFusionTrackTrackerTool->GetMarkerName ())
	  {
	    this->m_vecTrackerToolID.erase (this->m_vecTrackerToolID.begin () + t);
	    break;
	  }
      }

    return SUCCESS;
  }












  /** Add tracker tool entry to internal containers */
  AtracsysFusionTrackTracker::ResultType 
  AtracsysFusionTrackTracker::AddTrackerToolToInternalDataContainers (
								      const TrackerToolType * trackerTool)
  {
    igstkLogMacro(DEBUG, 
		  "igstk::AtracsysFusionTrackTracker::AddTrackerToolToInternalDataContainers \
   called ...\n")

      TrackerToolType* pTrackerToolNonConst = 
      const_cast<TrackerToolType *>(trackerTool);
    AtracsysFusionTrackTrackerTool* pAtracsysFusionTrackTrackerTool = 
      dynamic_cast<AtracsysFusionTrackTrackerTool *> (pTrackerToolNonConst);
    
    std::string strMarkerFileName = 
      pAtracsysFusionTrackTrackerTool->GetMarkerName (); 

    //ftkGeometry geom;
    
    if (loadGeometry (this->ftklib, 
		      this->sn,strMarkerFileName.c_str (),geom)!= FTK_OK)
      {
	igstkLogMacro(CRITICAL, "Error, cannot load geometry file ")
	  return FAILURE;
      }
    
    if (ftkSetGeometry (this->ftklib, 
			this->sn, &geom))
      {
	igstkLogMacro(CRITICAL, "Cannot set geometry")
	  return FAILURE;
      }

    TrackerToolIdentification TrackerID;

    TrackerID.m_TrackerToolName = pAtracsysFusionTrackTrackerTool->GetMarkerName ();
    TrackerID.m_u32GeometryID   = geom.geometryId;

    this->m_vecTrackerToolID.push_back (TrackerID);

    return SUCCESS;
  }
  

  /** Set the next available object array for acquisition */
  void AtracsysFusionTrackTracker::setNextArrayForAcquisition ()
  {
    if (this->m_iAvailable != 0 && this->m_iProcessed != 0)
      {
	this->m_iInAcquisition = 0;
      }
    else if (this->m_iAvailable != 1 && this->m_iProcessed != 1)
      {
	this->m_iInAcquisition = 1;
      }
    else
      {
	this->m_iInAcquisition = 2;
      }
  }

  /** Set the next object array for the user. True if a new array is available */
  bool AtracsysFusionTrackTracker::setNextArrayForUser ()
  {
    this->m_iProcessed = this->m_iAvailable;
    this->m_iAvailable = -1;

    if(this->m_iProcessed > -1)
      {
	return true;
      }
    else
      {
	return false;
      }
  }

  /** Called when the acquisition is finished */
  void AtracsysFusionTrackTracker::acquisitionFinished ()
  {
    this->m_iAvailable     = this->m_iInAcquisition;
    this->m_iInAcquisition = -1;
  }



} // end of namespace igstk
