/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkMonocularMarkerTracker.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah, Bastian Kuth (Siemens Heathineers)

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#if defined(_MSC_VER)
// Warning about: identifier was truncated to '255' characters in the
// debug information (MVC6.0 Debug)
#pragma warning( disable : 4786 )
#endif

#include "igstkMonocularMarkerTracker.h"
#include "itksys/SystemTools.hxx"
#include<opencv2\opencv.hpp>


namespace igstk
{

MonocularMarkerTracker::MonocularMarkerTracker (void):
m_StateMachine(this)
,device(0)
,computeCount (0)
,BigDotCount(1)
,cam(-m_SDD/m_PixelSpacing, m_SDD/m_PixelSpacing, frame.getWidth()/2, frame.getHeight()/2)
{
	

  // Lock for the data buffer that is used to transfer the
  // transformations from thread that is communicating
  // with the tracker to the main thread.
 
}

MonocularMarkerTracker::~MonocularMarkerTracker (void)
{ 

}

     

/** The "InternalOpen" method opens communication with a tracking device. */
MonocularMarkerTracker::ResultType MonocularMarkerTracker::InternalOpen (void)
{
  igstkLogMacro(DEBUG, "igstk::MonocularMarkerTracker::InternalOpen called ...\n")
  
	  cap=1;
      if(!cap.isOpened()) { // check if is succeeded
      std::cout << "Failed to open the camera" << std::endl;
      return FAILURE;
    }
	   
       int i=0 ;
	  
	   while ((i++ < 100) && !cap.read (I)) {};
	   cv::Mat temp;
       while ((i++ < 100) && !cap.read (temp)) {};
	  
	   cv::Rect myROI(0, 0, 1200, 1200);
	   temp = temp(myROI);
	  
	   cv::resize(temp, this->I, cv::Size(928, 928));
	   this->I = temp.clone();
       std::cout << "Image size: " <<this-> I.rows << " " << this->I.cols << std::endl;
       cout <<"Test1"<<endl;
	 
  return SUCCESS;
}


/** The "InternalClose" method closes communication with a tracking device. */
MonocularMarkerTracker::ResultType MonocularMarkerTracker::InternalClose( void )
{  
  igstkLogMacro(DEBUG, "igstk::MonocularMarkerTracker::InternalClose called ...\n")

	
 

  return SUCCESS;
}



/** The "InternalReset" method resets tracker to a known configuration. */
MonocularMarkerTracker::ResultType MonocularMarkerTracker::InternalReset( void )
{
  igstkLogMacro(DEBUG, "igstk::MonocularMarkerTracker::InternalReset called ...\n")



  return SUCCESS;
}



/** The "InternalStartTracking" method starts tracking. */
MonocularMarkerTracker::ResultType MonocularMarkerTracker::InternalStartTracking( void )
{
  igstkLogMacro(DEBUG, 
         "igstk::MonocularMarkerTracker::InternalStartTracking called ...\n")

 cout<<"test2"<<endl;
  
  vpImageConvert::convert(this->I, this->frame) ;
  vpDisplayOpenCV d (this->frame);
  this->cam.initPersProjWithoutDistortion(-this->m_SDD/this->m_PixelSpacing,
  this->m_SDD/this->m_PixelSpacing, 928/2, 928/2);
  cout << "SDD: " << this->m_SDD << " px: " << m_PixelSpacing << endl;
  cout << this->cam.get_K() << endl;

 
 
  return SUCCESS;
}



/** The "InternalStopTracking" method stops tracking. */
MonocularMarkerTracker::ResultType MonocularMarkerTracker::InternalStopTracking( void )
{
  igstkLogMacro(DEBUG, 
      "igstk::MonocularMarkerTracker::InternalStopTracking called ...\n")


	
   
  return SUCCESS;
}



/** The "InternalUpdateStatus" method updates tracker status. */
MonocularMarkerTracker::ResultType MonocularMarkerTracker::InternalUpdateStatus( void )
{
  igstkLogMacro(DEBUG, 
               "igstk::MonocularMarkerTracker::InternalUpdateStatus called ...\n")

  TrackerToolsContainerType trackerToolContainer = GetTrackerToolContainer();

			   //Dot Searching
	std::cout<<"Dot Searching start"<<std::endl;
	std::list<vpDot2> list_DotSearch;
	
	searchDotsVISP(list_DotSearch, this->frame);
	std::cout<<"Dot Searching finish"<<std::endl;

	// -----------------dot sorting-----------------
	std::cout<<"Dot Sorting start"<<std::endl;
	std::vector<vpDot2> DotBigs;
	std::vector<vpDot2> DotSmalls;

	sortDots (list_DotSearch,DotBigs,DotSmalls);
	std::cout<<"Dot Sorting finish"<<std::endl;

	//----------------dot processing-----------------
	std::cout << "---processing start---" << std::endl;

	vpHomogeneousMatrix cMo;
	double residual;
	std::vector<vpDot2> usedDots;
	std::cout << "DotBigsSize: " << DotBigs.size() << std::endl;
	std::cout << "DotSmallsSize: " << DotSmalls.size() << std::endl;
	std::cout << "model3dSize: " << model3d.size() << std::endl;
	processDots (DotBigs,DotSmalls,cMo,usedDots, residual, model3d,cam);
	
	std::cout << "---processing done---" << std::endl << std::endl;	
	std::cout<<"cMo Matrix"<< cMo<<std::endl;


	std::list<vpDot2>::const_iterator it = list_DotSearch.begin();
	while (it != list_DotSearch.end())
	{
		std::cout << (*it).getCog() << std::endl;
		++it;
	}

	std::string toolName = "";

	this->ReportTrackingToolVisible(trackerToolContainer[toolName]);

	TransformType transform ;

	// translation
	typedef TransformType::VectorType TranslationType;
    TranslationType translation;

	vpTranslationVector transVec = cMo.getTranslationVector();
	translation.SetNthComponent(0, transVec[0]);
	translation.SetNthComponent(1, transVec[1]);
	translation.SetNthComponent(2, transVec[2]);

	
	// rotation
	typedef TransformType::VersorType RotationType;
    RotationType rotation;

	vpQuaternionVector rotationQuat(cMo.getRotationMatrix());
	rotation.Set(rotationQuat.x(), rotationQuat.y(), rotationQuat.z(), rotationQuat.w());

    itk::Matrix<double> matrix;

	// report error value
    // Get error value from the tracker.
    typedef TransformType::ErrorType  ErrorType;
	ErrorType errorValue = residual;
	cout<< "Error" << residual<< endl;
	long lTime = this->GetValidityTime ();

    transform.SetToIdentity(this->GetValidityTime());
    transform.SetTranslationAndRotation(translation, 
                                        rotation, 
                                        errorValue,
                                        this->GetValidityTime());
	
	// set the raw transform
	this->SetTrackerToolRawTransform(trackerToolContainer [toolName], transform );

    this->SetTrackerToolTransformUpdate(trackerToolContainer [toolName], true );
	
  return SUCCESS;
}



/** The "InternalThreadedUpdateStatus" method updates tracker status. */
MonocularMarkerTracker::ResultType 
MonocularMarkerTracker::InternalThreadedUpdateStatus( void )
{
  igstkLogMacro(DEBUG, 
    "igstk::MonocularMarkerTracker::InternalThreadedUpdateStatus called ...\n")

  

  this->cap >>this->I  ;
  vpImageConvert::convert(this->I,this->frame);


 
 

  return SUCCESS;
}




/** Verify if a tracker tool information is correct before attaching */
MonocularMarkerTracker::ResultType 
MonocularMarkerTracker::VerifyTrackerToolInformation (
     const TrackerToolType* trackerTool)
{
  igstkLogMacro(DEBUG, 
     "igstk::MonocularMarkerTracker::VerifyTrackerToolInformation called ...\n")

  return SUCCESS;
}
  


/** The "ValidateSpecifiedFrequency" method checks if the specified  */
MonocularMarkerTracker::ResultType 
MonocularMarkerTracker::ValidateSpecifiedFrequency( double frequencyInHz )
{
  igstkLogMacro(DEBUG, 
   "igstk::MonocularMarkerTracker::ValidateSpecifiedFrequency called ...\n")

  return SUCCESS;
}


/** This method will remove entries of the traceker tool 
 * from internal data containers */
MonocularMarkerTracker::ResultType 
MonocularMarkerTracker::RemoveTrackerToolFromInternalDataContainers(
 const TrackerToolType * trackerTool )
{
  igstkLogMacro(DEBUG, 
   "igstk::MonocularMarkerTracker::RemoveTrackerToolFromInternalDataContainers \
   called ...\n")

 
  return SUCCESS;
}



/** Add tracker tool entry to internal containers */
MonocularMarkerTracker::ResultType 
MonocularMarkerTracker::AddTrackerToolToInternalDataContainers (
   const TrackerToolType * trackerTool)
{
  igstkLogMacro(DEBUG, 
  "igstk::MonocularMarkerTracker::AddTrackerToolToInternalDataContainers \
   called ...\n")

  TrackerToolType* pTrackerToolNonConst = 
                 const_cast<TrackerToolType *>(trackerTool);
  MonocularMarkerTrackerTool* pMonocularMarkerTrackerTool = 
                 dynamic_cast<MonocularMarkerTrackerTool *> (pTrackerToolNonConst);
  std::vector<std::vector<float>> markersVector =                                                     
             pMonocularMarkerTrackerTool->GetMarkerPoints ();

   if ( trackerTool == NULL )
    {
    return FAILURE;
    }

   for (unsigned int i=0; i < markersVector.size(); ++i)
   {
	this->model3d.push_back( vpPoint( markersVector.at(i).at(0), markersVector.at(i).at(1), markersVector.at(i).at(2)) );
   }

  return SUCCESS;
}
 


void MonocularMarkerTracker::go(int offset, int k, std::vector<std::vector<int>> &combinations, std::vector<int> &com, std::vector<int> &indicis) {
  if (k == 0) {
    combinations.push_back(com);
    return;
  }
  for (int i = offset; i <= indicis.size() - k; ++i) {
    com.push_back(indicis[i]);
    go(i+1, k-1, combinations, com, indicis);
    com.pop_back();
  }
}

std::vector<std::vector<int>> MonocularMarkerTracker::getCombinationVector(int n, int k)
{
	std::vector<int> com, indicis;

	std::vector<std::vector<int>> combinations;

	for (int i = 0; i < n; ++i) { indicis.push_back(i); }
	go(0, k, combinations, com, indicis);
	return combinations;
}

void MonocularMarkerTracker::processOrder(std::vector<vpPoint> model3d, std::vector<vpDot2> testDots, vpDot2 bigDot, double &bestResidual, vpHomogeneousMatrix &cMo, vpCameraParameters cam, std::vector<vpDot2> &usedDots)
{
	std::vector<int> v;
	std::vector<int>::iterator iter;
	for (int i=1; i <= 3; ++i)v.push_back(i);
	do 
	{	
		double cResidual = 0;
		vpPose pose_Test;
		std::vector<vpPoint> point_Test = this->model3d;
		std::vector<vpDot2> currentDots;
		vpHomogeneousMatrix cMo_Test;
		
		for (iter = v.begin(); iter != v.end(); ++iter)
		{
			int temp = *iter-1;
			currentDots.push_back(testDots[temp]);
		}
		currentDots.push_back(bigDot);
		double x=0, y=0;
		for (unsigned int i=0; i < point_Test.size(); i ++) {
			vpPixelMeterConversion::convertPoint(cam, currentDots[i].getCog(), x, y);
			point_Test[i].set_x(x);
			point_Test[i].set_y(y);
		
			pose_Test.addPoint(point_Test[i]);
		}
		pose_Test.computePose(vpPose::DEMENTHON, cMo_Test);
		cResidual = pose_Test.computeResidual(cMo_Test);

		if (bestResidual > cResidual)
		{	
			bestResidual = cResidual;
			currentDots.push_back(bigDot);
			usedDots = currentDots;
			pose_Test.computePose(vpPose::DEMENTHON_LOWE, cMo_Test);
			cMo = cMo_Test;

		}
	}while (next_permutation(v.begin(), v.end()));
}


void MonocularMarkerTracker::searchDotsVISP (std::list<vpDot2> &list_DotSearch, vpImage<unsigned char> frame)
{
	vpDot2 DotSearch;
	DotSearch.setWidth(30.0);
    DotSearch.setHeight(30.0);
	DotSearch.setArea(30*30*M_PI_4);
    DotSearch.setGrayLevelMin(0);
    DotSearch.setGrayLevelMax(50);
    DotSearch.setGrayLevelPrecision(0);
    DotSearch.setSizePrecision(0.3);
    DotSearch.setEllipsoidShapePrecision(0.9);
	DotSearch.setEllipsoidBadPointsPercentage(0.2);

	vpImage<unsigned char> temp;
	cv::Mat CVframe(this->frame.getWidth(), this->frame.getHeight(), CV_8UC3, cv::Scalar(0,0,0));
	vpImageConvert::convert(this->frame,CVframe);

	cv::Rect myROI(0, 0, 1200, 1200);
	CVframe = CVframe(myROI);
	cv::resize(CVframe, CVframe, cv::Size(928, 928));

	cv::imshow("",CVframe);
	cv::imwrite("D:\\Temp\\grabbed.png",CVframe);
	//cv::waitKey(0);
	vpImageConvert::convert(CVframe,temp);

	DotSearch.searchDotsInArea(temp, 0, 0, temp.getWidth(),temp.getHeight(), list_DotSearch);
	std::cout<<"Dot Searching is finish ------------"<<std::endl;
	//cv::waitKey(0);
}


void MonocularMarkerTracker::sortDots (std::list<vpDot2> list_DotSearch,std::vector<vpDot2> &DotBigs,std::vector<vpDot2> &DotSmalls)
{
	std::list<vpDot2>::const_iterator it = list_DotSearch.begin();

	while (it != list_DotSearch.end())
	{

		if (abs (((*it).getHeight()/(*it).getWidth())-1) > 0.2)
		{
			vpImagePoint cog = (*it).getCog();
			unsigned int i = (unsigned int)cog.get_i();
			unsigned int j = (unsigned int)cog.get_j();
			list_DotSearch.erase(it++);
		}else{
			++it;
		}
	}

	double average_size = avgSize(list_DotSearch);

	for (std::list<vpDot2>::const_iterator it = list_DotSearch.begin(); it != list_DotSearch.end(); ++ it)
    {		
		vpImagePoint cog = (*it).getCog();
		std::cout << cog << std::endl;
		unsigned int i = (unsigned int)cog.get_i();
		unsigned int j = (unsigned int)cog.get_j();
	
		if ((*it).getArea() < average_size*1.3)
		{
			DotSmalls.push_back(*it);
		}else{
			DotBigs.push_back(*it);
		}    
    }
}

void MonocularMarkerTracker::processDots (std::vector<vpDot2> DotBigs,
	std::vector<vpDot2> DotSmalls,
	vpHomogeneousMatrix &cMo,
	std::vector<vpDot2> &usedDots,
	double &bestResidual,
	std::vector<vpPoint> model3d,
	vpCameraParameters cam)
{
	bestResidual = 1e10;

	if (DotSmalls.size() >=  this->model3d.size()-1 && DotBigs.size() >= 1)
	{

		std::vector<std::vector<int>> CombinationVectorSmall = getCombinationVector(DotSmalls.size(), this->model3d.size() - 1);
		
		for (int selectBigIndex = 0; selectBigIndex < DotBigs.size(); ++selectBigIndex)
		{
			for (int CombinationVectorSmallIndex = 0; CombinationVectorSmallIndex < CombinationVectorSmall.size(); ++CombinationVectorSmallIndex)
			{
				std::vector<int> CombinationSmall = CombinationVectorSmall[CombinationVectorSmallIndex];
				std::vector<vpDot2> testDots;

				for (int addIndex=0; addIndex < CombinationSmall.size(); ++addIndex)
				{
						testDots.push_back(DotSmalls[CombinationSmall[addIndex]]);
				}
				processOrder(this->model3d, testDots, DotBigs[selectBigIndex], bestResidual, cMo, cam, usedDots);

			}
		}
		std::cout << "best residual: " << bestResidual << std::endl;
	}
}

double MonocularMarkerTracker::avgSize (std::list<vpDot2> list_dot)
{
	double sum = 0;
	for (std::list<vpDot2>::const_iterator it = list_dot.begin(); it != list_dot.end(); ++ it)
	{
		sum+=(*it).getArea();
	}
	return sum/list_dot.size();
}

} // end of namespace igstk