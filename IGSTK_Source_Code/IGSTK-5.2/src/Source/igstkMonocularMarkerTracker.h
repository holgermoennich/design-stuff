/*=========================================================================

  Program:   Image Guided Surgery Software Toolkit
  Module:    igstkMonocularMarkerTracker.h
  Language:  C++
  Date:      $Date$
  Version:   $Revision$
  Author:    Mazen Alharah, Bastian Kuth (Siemens Heathineers)

  Copyright (c) ISC  Insight Software Consortium.  All rights reserved.
  See IGSTKCopyright.txt or http://www.igstk.org/copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/


#ifndef __igstkMonocularMarkerTracker_h
#define __igstkMonocularMarkerTracker_h

#ifdef _MSC_VER
#pragma warning ( disable : 4018 )
//Warning about: identifier was truncated to '255' characters in the debug
//information (MVC6.0 Debug)
#pragma warning( disable : 4284 )
#endif


#include "igstkTracker.h"
#include "igstkMonocularMarkerTrackerTool.h"

#include <visp3/core/vpConfig.h>
#include <visp3/gui/vpDisplayOpenCV.h>
#include <visp3/blob/vpDot2.h>
#include <visp3/core/vpPixelMeterConversion.h>
#include <visp3/core/vpMeterPixelConversion.h>
#include <visp3/vision/vpPose.h>
#include <visp/vpImageIo.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>


namespace igstk {
        

  
class MonocularMarkerTracker : public Tracker
{

public:
  /** Macro with standard traits declarations. */
  igstkStandardClassTraitsMacro(MonocularMarkerTracker, Tracker)

public:

	igstkSetMacro( PixelSpacing , float        );
	igstkGetMacro( PixelSpacing, float );
	igstkSetMacro( SDD          , float );             // Source-Dectector distance 
	igstkGetMacro( SDD, float );
  

protected:

  /** Constructor */
 MonocularMarkerTracker (void);

  /** Destructor */
  virtual ~MonocularMarkerTracker(void);

  /** The "InternalOpen" method opens communication with a tracking device.
      This method is to be implemented by a descendant class 
      and responsible for device-specific processing */
  virtual ResultType InternalOpen( void );

  /** The "InternalClose" method closes communication with a tracking device.
      This method is to be implemented by a descendant class 
      and responsible for device-specific processing */
  virtual ResultType InternalClose( void );

  /** The "InternalReset" method resets tracker to a known configuration. 
      This method is to be implemented by a descendant class 
      and responsible for device-specific processing */
  virtual ResultType InternalReset( void );

  /** The "InternalStartTracking" method starts tracking.
      This method is to be implemented by a descendant class 
      and responsible for device-specific processing */
  virtual ResultType InternalStartTracking( void );

  /** The "InternalStopTracking" method stops tracking.
      This method is to be implemented by a descendant class 
      and responsible for device-specific processing */
  virtual ResultType InternalStopTracking( void );


  /** The "InternalUpdateStatus" method updates tracker status.
      This method is to be implemented by a descendant class 
      and responsible for device-specific processing */
  virtual ResultType InternalUpdateStatus( void );

  /** The "InternalThreadedUpdateStatus" method updates tracker status.
      This method is called in a separate thread.
      This method is to be implemented by a descendant class
      and responsible for device-specific processing */
  virtual ResultType InternalThreadedUpdateStatus( void );

  /** Print the object information in a stream. */
  //virtual void PrintSelf( std::ostream& os, itk::Indent indent ) const; 

 
  virtual ResultType VerifyTrackerToolInformation ( const TrackerToolType * ); 

  /** The "ValidateSpecifiedFrequency" method checks if the specified  
      frequency is valid for the tracking device that is being used. This 
      method is to be overridden in the derived tracking-device specific  
      classes to take into account the maximum frequency possible in the 
      tracking device
   */
  virtual ResultType ValidateSpecifiedFrequency( double frequencyInHz );         //


  /** This method will remove entries of the traceker tool from internal
      data containers */
  virtual ResultType RemoveTrackerToolFromInternalDataContainers(
            const TrackerToolType * trackerTool ); 

  /** Add tracker tool entry to internal containers */
  virtual ResultType AddTrackerToolToInternalDataContainers(
            const TrackerToolType * trackerTool );

 /*
*/

private:

	//unsigned int sourceToDetector;
	//float ImagerPixelSpacing;
	int device ;
	vpImage<unsigned char> frame ;
	cv::Mat I;
	vpDisplayOpenCV d;
	cv::VideoCapture cap;
    vpCameraParameters cam ;
   std::vector<vpPoint> model3d;
	int computeCount;
	int BigDotCount;
	

	float  m_SDD ;
	float  m_PixelSpacing   ;

  double potenz (double basis);

  double distance(vpImagePoint first, vpImagePoint second);

  void go (int offset, int k, std::vector<std::vector<int>> &combinations, std::vector<int> &com, std::vector<int> &indicis);
  
  std::vector<std::vector<int>> getCombinationVector(int n, int k);

  void processOrder(std::vector<vpPoint> model3d, std::vector<vpDot2> testDots, vpDot2 bigDot, 
	                double &bestResidual, vpHomogeneousMatrix &cMo, vpCameraParameters cam, std::vector<vpDot2> &usedDots);

  double avgSize (std::list<vpDot2> list_dot);

  void sortDots (std::list<vpDot2> list_DotSearch,std::vector<vpDot2> &DotBigs,std::vector<vpDot2> &DotSmalls);

  void searchDotsVISP (std::list<vpDot2> &list_DotSearch, vpImage<unsigned char> frame);
 
void processDots (std::vector<vpDot2> DotBigs,std::vector<vpDot2> DotSmalls,vpHomogeneousMatrix &cMo,
	              std::vector<vpDot2> &usedDots, double &bestResidual, std::vector<vpPoint> model3d,vpCameraParameters cam);




}; // end of class MonocularMarkerTracker

} // end of namespace igstk

#endif //__igstk_MonocularMarkerTracker_h_
